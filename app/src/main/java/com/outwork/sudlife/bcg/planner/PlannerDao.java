package com.outwork.sudlife.bcg.planner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.planner.models.Customerobject;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.ui.models.UserTeams;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlannerDao {
    public static final String TAG = PlannerDao.class.getSimpleName();

    private static PlannerDao instance;
    private Context applicationContext;

    public static PlannerDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new PlannerDao(applicationContext);
        }
        return instance;
    }

    private PlannerDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public long insertPlanner(PlannerModel plannerModel, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.groupid, groupid);
        if (plannerModel.getId() != null)
            values.put(IvokoProvider.Planner.id, plannerModel.getId());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid()))
            values.put(IvokoProvider.Planner.activityid, plannerModel.getActivityid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomerid()))
            values.put(IvokoProvider.Planner.customerid, plannerModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomertype()))
            values.put(IvokoProvider.Planner.customertype, plannerModel.getCustomertype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getBranchcode()))
            values.put(IvokoProvider.Planner.branchcode, plannerModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            values.put(IvokoProvider.Planner.customername, plannerModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFirstname()))
            values.put(IvokoProvider.Planner.firstname, plannerModel.getFirstname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getLastname()))
            values.put(IvokoProvider.Planner.lastname, plannerModel.getLastname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOpportunityid()))
            values.put(IvokoProvider.Planner.opportunityid, plannerModel.getOpportunityid());
        if (plannerModel.getLocalopportunityid() != null)
            values.put(IvokoProvider.Planner.localopportunityid, plannerModel.getLocalopportunityid());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getPurpose()))
            values.put(IvokoProvider.Planner.purpose, plannerModel.getPurpose());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtheractions()))
            values.put(IvokoProvider.Planner.otheractions, plannerModel.getOtheractions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtherfollowupactions()))
            values.put(IvokoProvider.Planner.otherfollowupactions, plannerModel.getOtherfollowupactions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes()))
            values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFollowupaction()))
            values.put(IvokoProvider.Planner.followupaction, plannerModel.getFollowupaction());

        if (plannerModel.getCheckintime() != null)
            values.put(IvokoProvider.Planner.checkintime, plannerModel.getCheckintime());
        if (plannerModel.getCheckouttime() != null)
            values.put(IvokoProvider.Planner.checkouttime, plannerModel.getCheckouttime());
        if (plannerModel.getCheckoutlat() != null)
            values.put(IvokoProvider.Planner.checkoutlat, plannerModel.getCheckoutlat());
        if (plannerModel.getCheckoutlong() != null)
            values.put(IvokoProvider.Planner.checkoutlon, plannerModel.getCheckoutlong());
        if (plannerModel.getCheckinlat() != null)
            values.put(IvokoProvider.Planner.checkinlat, plannerModel.getCheckinlat());
        if (plannerModel.getCheckinlong() != null)
            values.put(IvokoProvider.Planner.checkinlon, plannerModel.getCheckinlong());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckinlocation()))
            values.put(IvokoProvider.Planner.checkinlocation, plannerModel.getCheckinlocation());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckoutlocation()))
            values.put(IvokoProvider.Planner.checkoutlocation, plannerModel.getCheckoutlocation());
        if (plannerModel.getCreateddate() != null)
            values.put(IvokoProvider.Planner.createddate, plannerModel.getCreateddate());
        if (plannerModel.getModifieddate() != null)
            values.put(IvokoProvider.Planner.modifieddate, plannerModel.getModifieddate());
        //if (plannerModel.getModifieddate() != null)
            //values.put(IvokoProvider.Planner.modifieddate, plannerModel.getModifieddate());
        if (plannerModel.getScheduleday() != null)
            values.put(IvokoProvider.Planner.scheduleday, plannerModel.getScheduleday());
        if (plannerModel.getSchedulemonth() != null)
            values.put(IvokoProvider.Planner.schedulemonth, plannerModel.getSchedulemonth());
        if (plannerModel.getScheduleyear() != null)
            values.put(IvokoProvider.Planner.scheduleyear, plannerModel.getScheduleyear());
        if (plannerModel.getScheduletime() != null)
        values.put(IvokoProvider.Planner.scheduletime, plannerModel.getScheduletime());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getLeadid() != null)
            values.put(IvokoProvider.Planner.leadid, plannerModel.getLeadid());
        if (plannerModel.getLocalleadid() != null)
            values.put(IvokoProvider.Planner.localleadid, plannerModel.getLocalleadid());
        if (plannerModel.getCustomerobject() != null)
            values.put(IvokoProvider.Planner.customerobject, new Gson().toJson(plannerModel.getCustomerobject()));
        long oppID = db.insert(IvokoProvider.Planner.TABLE, null, values);
        return oppID;
    }

    public void insertPlannerList(List<PlannerModel> plannerModelList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < plannerModelList.size(); j++) {
            PlannerModel plannerModel = plannerModelList.get(j);
            if (!plannerModel.getIsdeleted()) {
                if (!isRecordExist(db, String.valueOf(plannerModel.getActivityid()))) {
                    insertPlanner(plannerModel, userId, groupid);
                } else {
                    deleteformRecord(String.valueOf(plannerModel.getActivityid()));
                    insertPlanner(plannerModel, userId, groupid);
                }
            }
        }
    }

    public void updatePlannerStatus(String lid, String userId, String cstatus) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.networkstatus, "");
        values.put(IvokoProvider.Planner.completestatus, cstatus);

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlan(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype()))
            values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomerid()))
            values.put(IvokoProvider.Planner.customerid, plannerModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            values.put(IvokoProvider.Planner.customername, plannerModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomertype()))
            values.put(IvokoProvider.Planner.customertype, plannerModel.getCustomertype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getBranchcode()))
            values.put(IvokoProvider.Planner.branchcode, plannerModel.getBranchcode());
        if (plannerModel.getScheduleday() != null)
            values.put(IvokoProvider.Planner.scheduleday, plannerModel.getScheduleday());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getSchedulemonth() != null)
            values.put(IvokoProvider.Planner.schedulemonth, plannerModel.getSchedulemonth());
        if (plannerModel.getScheduleyear() != null)
            values.put(IvokoProvider.Planner.scheduleyear, plannerModel.getScheduleyear());
        if (plannerModel.getScheduletime() != null)
            values.put(IvokoProvider.Planner.scheduletime, plannerModel.getScheduletime());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateleadPlan(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype()))
            values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes()))
            values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateCheckoutPlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getCheckouttime() != null)
            values.put(IvokoProvider.Planner.checkouttime, plannerModel.getCheckouttime());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckoutlocation()))
            values.put(IvokoProvider.Planner.checkoutlocation, plannerModel.getCheckoutlocation());
        if (plannerModel.getCheckoutlat() != null)
            values.put(IvokoProvider.Planner.checkoutlat, plannerModel.getCheckoutlat());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getCheckoutlong() != null)
            values.put(IvokoProvider.Planner.checkoutlon, plannerModel.getCheckoutlong());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getPurpose()))
            values.put(IvokoProvider.Planner.purpose, plannerModel.getPurpose());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtheractions()))
            values.put(IvokoProvider.Planner.otheractions, plannerModel.getOtheractions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtherfollowupactions()))
            values.put(IvokoProvider.Planner.otherfollowupactions, plannerModel.getOtherfollowupactions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFollowupaction()))
            values.put(IvokoProvider.Planner.followupaction, plannerModel.getFollowupaction());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateCheckInPlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());

        values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getCheckintime() != null)
            values.put(IvokoProvider.Planner.checkintime, plannerModel.getCheckintime());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckinlocation()))
            values.put(IvokoProvider.Planner.checkinlocation, plannerModel.getCheckinlocation());
        if (plannerModel.getCheckinlat() != null)
            values.put(IvokoProvider.Planner.checkinlat, plannerModel.getCheckinlat());
        if (plannerModel.getCheckinlong() != null)
            values.put(IvokoProvider.Planner.checkinlon, plannerModel.getCheckinlong());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }

        }

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateReschedulePlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit())
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
        }
        if (plannerModel.getCompletestatus() != null)
            values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateLeadid(int lid, String userId, String id) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.leadid, id);
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner.localleadid + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlannerID(String lid, String userId, String id) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.activityid, id);
        values.put(IvokoProvider.Planner.networkstatus, "");
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlannerdummyOnline(String lid, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.networkstatus, "pending");
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public List<PlannerModel> getOfflineplannerList(String userId, String networkstatus) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        try {
            if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
                Cursor cursor = null;

                // security
                /*String sql = "";
                sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
                sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
                sql += " AND " + IvokoProvider.Planner.networkstatus + " = '" + networkstatus + "'";*/
                try {
                    //cursor = db.rawQuery(sql, null);


                    cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " + IvokoProvider.Planner.userid + " = ?" + " AND "+IvokoProvider.Planner.networkstatus + " = ?"  , new String[]{userId,networkstatus});


                    if (cursor.moveToFirst()) {
                        do {
                            PlannerModel plannerModel = new PlannerModel();
                            plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                            plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                            plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                            plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                            plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                            plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                            plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                            plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                            plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                            plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                            plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                            plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                            plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                            plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                            plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                            plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                            plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                            plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                            plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                            plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                            plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                            plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));

                            plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                            plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                            plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                            plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                            plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                            plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                            plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                            plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                            plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                            plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                            plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                            plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                            plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                            plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                            plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                            plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                            plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                            plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                            plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                    plannerModel.setJointvisit(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                    plannerModel.setJointvisit(true);
                                } else {
                                    plannerModel.setJointvisit(false);
                                }
                            plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                            plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                            plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                            plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                            plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                            plannerModelList.add(plannerModel);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        } catch (IllegalStateException exception) {
            exception.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByDate(String userId, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;

            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.scheduleday + " = ?" + " AND "+
                        IvokoProvider.Planner.schedulemonth + " = ?" + " AND "+
                        IvokoProvider.Planner.scheduleyear + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,String.valueOf(day),String.valueOf(month),String.valueOf(year)," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype))))
                            plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByBranches(String userId, String branchname, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            //security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.customername + " = ?" + " AND "+
                        IvokoProvider.Planner.schedulemonth + " = ?" + " AND "+
                        IvokoProvider.Planner.scheduleyear + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,branchname,String.valueOf(month),String.valueOf(year)," DESC "});



                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerCreationCondition(String userId, String branchname, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;

            // pending security checked

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " AND (" + IvokoProvider.Planner.completestatus + " = '" + 0 + "'";
            sql += " OR " + IvokoProvider.Planner.completestatus + " = '" + 1 + "')";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);



               /* cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.customername + " = ?" + " AND "+
                        IvokoProvider.Planner.scheduleday + " = ?" + " AND "+
                        IvokoProvider.Planner.schedulemonth + " = ?" + " AND "+
                        IvokoProvider.Planner.scheduleyear + " = ?" + " AND  ("+
                        IvokoProvider.Planner.completestatus + " = ?" + " OR "+
                        IvokoProvider.Planner.completestatus + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,branchname,String.valueOf(day),String.valueOf(month),String.valueOf(year),"0","1"," DESC "});
                */

                /*cursor=db.rawQuery("select * from "+IvokoProvider.Planner.TABLE+" where "+ "userid=? and customername=? and scheduleday=? and schedulemonth=? and scheduleyear=? and  (completestatus=?  or completestatus=? ) order by scheduletime=? ",
                        new String [] {userId,branchname,String.valueOf(day),String.valueOf(month),String.valueOf(year),"0","1"," DESC "});*/

                cursor=db.rawQuery("select * from "+IvokoProvider.Planner.TABLE+" where "+ "userid=? and scheduleday=? and schedulemonth=? and scheduleyear=? and  (completestatus=? ) order by scheduletime=? ",
                        new String [] {userId,String.valueOf(day),String.valueOf(month),String.valueOf(year),"1"," DESC "});

                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<String> getPlannerDatesListByBranches(String userId, String branchname, String activitytype, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security


         /*   String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " AND " + IvokoProvider.Planner.activitytype + " = '" + activitytype + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.customername + " = ?" + " AND "+
                        IvokoProvider.Planner.schedulemonth + " = ?" + " AND "+
                        IvokoProvider.Planner.scheduleyear + " = ?" + " AND "+
                        IvokoProvider.Planner.activitytype + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,branchname,String.valueOf(month),String.valueOf(year),activitytype," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        String date;
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)) != 0)
                            if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)) < Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                                date = TimeUtils.getFormattedDatefromUnix(String.valueOf(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime))), "dd/MM/yyyy");
                                if (!plannerModelList.contains(date))
                                    plannerModelList.add(date);
                            }
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByOpportunityID(String userId, String oppid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security

        /*    String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.opportunityid + " = '" + oppid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                // cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.opportunityid + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,oppid," DESC "});



                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListBylocalOpportunityID(String userId, int oppid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security

          /*  String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.localopportunityid + " = '" + oppid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.localopportunityid + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,String.valueOf(oppid)," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByLeadID(String userId, String leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.leadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                // cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.leadid + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,leadid," DESC "});

                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListBylocalLeadID(String userId, int leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.localleadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " AND " +
                        IvokoProvider.Planner.localleadid + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId,String.valueOf(leadid)," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +
                        IvokoProvider.Planner.userid + " = ?" + " ORDER BY " +
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{userId," DESC "});

                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    //siva


    public List<PlannerModel> getPlannerListByLead(String leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;


            // security

          /*  String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.leadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " +

                        IvokoProvider.Planner.leadid + " = ?" + " ORDER BY "+
                        IvokoProvider.Planner.scheduletime  + " = ? ", new String[]{leadid," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public long insertPlannerNotificationList(List<PlannerModel> plannerModelArrayList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values;
        long oppID = 0;
        for (int i = 0; i < plannerModelArrayList.size(); i++) {
            values = new ContentValues();
            values.put(IvokoProvider.PlannerNotification.userid, userId);
            values.put(IvokoProvider.PlannerNotification.groupid, groupid);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModelArrayList.get(i).getId())))
                values.put(IvokoProvider.PlannerNotification.id, plannerModelArrayList.get(i).getId());
            if (Utils.isNotNullAndNotEmpty(plannerModelArrayList.get(i).getActivityid()))
                values.put(IvokoProvider.PlannerNotification.activityid, plannerModelArrayList.get(i).getActivityid());
            if (plannerModelArrayList.get(i).getCustomerid() != null)
                values.put(IvokoProvider.PlannerNotification.customerid, plannerModelArrayList.get(i).getCustomerid());
            if (plannerModelArrayList.get(i).getCustomername() != null)
                values.put(IvokoProvider.PlannerNotification.customername, plannerModelArrayList.get(i).getCustomername());
            if (plannerModelArrayList.get(i).getOpportunityid() != null)
                values.put(IvokoProvider.PlannerNotification.opportunityid, plannerModelArrayList.get(i).getOpportunityid());
            if (plannerModelArrayList.get(i).getLocalopportunityid() != null)
                values.put(IvokoProvider.PlannerNotification.localopportunityid, plannerModelArrayList.get(i).getLocalopportunityid());
            values.put(IvokoProvider.PlannerNotification.activitytype, plannerModelArrayList.get(i).getActivitytype());
            values.put(IvokoProvider.PlannerNotification.type, plannerModelArrayList.get(i).getType());
            values.put(IvokoProvider.PlannerNotification.subtype, plannerModelArrayList.get(i).getSubtype());
            values.put(IvokoProvider.PlannerNotification.purpose, plannerModelArrayList.get(i).getPurpose());

            values.put(IvokoProvider.PlannerNotification.notes, plannerModelArrayList.get(i).getNotes());
            values.put(IvokoProvider.PlannerNotification.planstatus, plannerModelArrayList.get(i).getPlanstatus());
            values.put(IvokoProvider.PlannerNotification.plancreatedby, plannerModelArrayList.get(i).getPlancreatedby());
            values.put(IvokoProvider.PlannerNotification.completestatus, plannerModelArrayList.get(i).getCompletestatus());
            values.put(IvokoProvider.PlannerNotification.notificationstatus, plannerModelArrayList.get(i).getNotificationstatus());
            if (plannerModelArrayList.get(i).getFollowupdate() != null)
                values.put(IvokoProvider.PlannerNotification.followupdate, plannerModelArrayList.get(i).getFollowupdate());
            if (plannerModelArrayList.get(i).getFollowupaction() != null)
                values.put(IvokoProvider.PlannerNotification.followupaction, plannerModelArrayList.get(i).getFollowupaction());
            values.put(IvokoProvider.PlannerNotification.scheduletime, plannerModelArrayList.get(i).getScheduletime());
            if (plannerModelArrayList.get(i).getCheckintime() != null)
                values.put(IvokoProvider.PlannerNotification.checkintime, plannerModelArrayList.get(i).getCheckintime());
            if (plannerModelArrayList.get(i).getCheckouttime() != null)
                values.put(IvokoProvider.PlannerNotification.checkouttime, plannerModelArrayList.get(i).getCheckouttime());
            if (plannerModelArrayList.get(i).getCheckinlocation() != null)
                values.put(IvokoProvider.PlannerNotification.checkinlocation, plannerModelArrayList.get(i).getCheckinlocation());
            if (plannerModelArrayList.get(i).getCheckoutlocation() != null)
                values.put(IvokoProvider.PlannerNotification.checkoutlocation, plannerModelArrayList.get(i).getCheckoutlocation());
            if (plannerModelArrayList.get(i).getCreateddate() != null)
                values.put(IvokoProvider.PlannerNotification.createddate, plannerModelArrayList.get(i).getCreateddate());
            if (plannerModelArrayList.get(i).getModifieddate() != null)
                values.put(IvokoProvider.PlannerNotification.modifieddate, plannerModelArrayList.get(i).getModifieddate());
            if (plannerModelArrayList.get(i).getScheduleday() != null)
                values.put(IvokoProvider.PlannerNotification.scheduleday, plannerModelArrayList.get(i).getScheduleday());
            if (plannerModelArrayList.get(i).getSchedulemonth() != null)
                values.put(IvokoProvider.PlannerNotification.schedulemonth, plannerModelArrayList.get(i).getSchedulemonth());
            if (plannerModelArrayList.get(i).getScheduleyear() != null)
                values.put(IvokoProvider.PlannerNotification.scheduleyear, plannerModelArrayList.get(i).getScheduleyear());
            if (plannerModelArrayList.get(i).getTitle() != null)
                values.put(IvokoProvider.PlannerNotification.title, plannerModelArrayList.get(i).getTitle());
            if (plannerModelArrayList.get(i).getJointvisit() != null) {
                if (plannerModelArrayList.get(i).getJointvisit())
                    values.put(IvokoProvider.PlannerNotification.jointvisit, plannerModelArrayList.get(i).getJointvisit());
            }
            if (plannerModelArrayList.get(i).getReschduledate() != null)
                values.put(IvokoProvider.PlannerNotification.reschduledate, plannerModelArrayList.get(i).getReschduledate());
            if (plannerModelArrayList.get(i).getReshdreason() != null)
                values.put(IvokoProvider.PlannerNotification.resReason, plannerModelArrayList.get(i).getReshdreason());
            if (Utils.isNotNullAndNotEmpty(plannerModelArrayList.get(i).getNetwork_status()))
                values.put(IvokoProvider.PlannerNotification.networkstatus, plannerModelArrayList.get(i).getNetwork_status());
            oppID = db.insert(IvokoProvider.PlannerNotification.TABLE, null, values);
        }
        return oppID;
    }


    public List<PlannerModel> getPlannerNotificationList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            Cursor cursor = null;
            // security


            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE;
            sql += " WHERE " + IvokoProvider.PlannerNotification.userid + " = '" + userId + "'";*/
            ;
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE + " WHERE " + IvokoProvider.PlannerNotification.userid + " = ?" , new String[]{userId});

                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification._ID)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customername)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.purpose)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.title)));
                        plannerModel.setNotificationstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notificationstatus)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.reschduledate)));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }


    public List<PlannerModel> getPlannerNotificationListByDate(String userId, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            Cursor cursor = null;
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE;
            sql += " WHERE " + IvokoProvider.PlannerNotification.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.PlannerNotification.scheduletime + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE + " WHERE " +
                        IvokoProvider.PlannerNotification.userid + " = ?" + " AND " +
                        IvokoProvider.PlannerNotification.scheduleday + " = ?" + " AND " +
                        IvokoProvider.PlannerNotification.schedulemonth + " = ?" + " AND " +
                        IvokoProvider.PlannerNotification.scheduleyear + " = ?" + " ORDER BY "+
                        IvokoProvider.PlannerNotification.scheduletime  + " = ? ", new String[]{userId,String.valueOf(day),String.valueOf(month),String.valueOf(year)," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activityid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.purpose)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.title)));
                        plannerModel.setNotificationstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notificationstatus)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.reschduledate)));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }


    public HashMap<String, Integer> getPlannerListByScheduleDates(int month, int currentDate) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        HashMap<String, Integer> stringIntegerHashMap = null;
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            //


            String query = "  SELECT SUM(CASE WHEN activitytype='activity' AND planstatus= 0 THEN 1 ELSE 0 END)" +
                    " AS activitycount, SUM(CASE WHEN activitytype='visit' " +
                    " AND planstatus= 1 THEN 1 ELSE 0 END) " +
                    "AS visitcount, SUM(CASE WHEN activitytype='visit'" +
                    "  AND completestatus= 1 THEN 1 ELSE 0 END) " +
                    "AS visitscompleted, SUM(CASE WHEN activitytype='activity' " +
                    " AND completestatus= 1 THEN 1 ELSE 0 END)" +
                    " AS activitiescompleted FROM" +
                    " planner WHERE schedulemonth=" + "'" + month + "'" +
                    " AND scheduleday>=1 AND scheduleday<=" + "'" + currentDate + "'";

            try {
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        stringIntegerHashMap = new HashMap<>();
                        stringIntegerHashMap.put("activitycount", cursor.getInt(cursor.getColumnIndex("activitycount")));
                        stringIntegerHashMap.put("visitcount", cursor.getInt(cursor.getColumnIndex("visitcount")));
                        stringIntegerHashMap.put("visitscompleted", cursor.getInt(cursor.getColumnIndex("visitscompleted")));
                        stringIntegerHashMap.put("activitiescompleted", cursor.getInt(cursor.getColumnIndex("activitiescompleted")));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return stringIntegerHashMap;
    }


    public void deleteRecordsBasedOnDay(String day) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.PlannerNotification.TABLE + " WHERE " + IvokoProvider.PlannerNotification.scheduleday + " = '" + day + "'");
        }
    }

    public void updatePlanNotificationStatus(String id, String notificationStatus, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.PlannerNotification.notificationstatus, notificationStatus);
        //   db.update(IvokoProvider.Planner.TABLE, values, "id=" + id, null);
        db.update(IvokoProvider.PlannerNotification.TABLE, values, IvokoProvider.PlannerNotification._ID + " = " + id + " AND " + IvokoProvider.PlannerNotification.userid + " = ?", new String[]{userId});

    }
    //siva end


    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private boolean isRecordExist(SQLiteDatabase db, String id) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " + IvokoProvider.Planner.activityid + " = ?", new String[]{id});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void deleteformRecord(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.Planner.TABLE + " WHERE " + IvokoProvider.Planner.activityid + " = '" + id + "'");
        }
    }



    public void insertUserTeamsList(List<UserTeams> userTeamsList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < userTeamsList.size(); j++) {
            UserTeams userTeamModel = userTeamsList.get(j);
            if (!isUserTeamRecordExist(db, String.valueOf(userTeamModel.getTeamid()))) {
                insertUserTeams(userTeamModel, userId, groupid);
            } else {
                deleteUserTeamRecord(String.valueOf(userTeamModel.getTeamid()));
                insertUserTeams(userTeamModel, userId, groupid);
            }
        }
    }

    public void deleteUserTeamRecord(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.UserTeam.TABLE + " WHERE " + IvokoProvider.UserTeam.teamid + " = '" + id + "'");
        }
    }


    public long insertUserTeams(UserTeams userTeamModel, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.UserTeam.userid, userId);
        values.put(IvokoProvider.UserTeam.groupid, groupid);

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getTeamid()))
            values.put(IvokoProvider.UserTeam.teamid, userTeamModel.getTeamid());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getType()))
            values.put(IvokoProvider.UserTeam.type, userTeamModel.getType());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getName()))
            values.put(IvokoProvider.UserTeam.name, userTeamModel.getName());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getInternalrole()))
            values.put(IvokoProvider.UserTeam.internalrole, userTeamModel.getInternalrole());



        long oppID = db.insert(IvokoProvider.UserTeam.TABLE, null, values);
        return oppID;
    }




    private boolean isUserTeamRecordExist(SQLiteDatabase db, String id) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.UserTeam.TABLE + " WHERE " + IvokoProvider.UserTeam.teamid + " = ?", new String[]{id});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }






    public List<UserTeams> getUserTeamList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<UserTeams> userTeamModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.UserTeam.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.UserTeam.TABLE;
            sql += " WHERE " + IvokoProvider.UserTeam.userid + " = '" + userId + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.UserTeam.TABLE + " WHERE " +
                        IvokoProvider.UserTeam.userid + " = ?", new String[]{userId});


                if (cursor.moveToFirst()) {
                    do {
                        UserTeams userTeamsModel = new UserTeams();
                        userTeamsModel.setTeamid(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.teamid)));
                        userTeamsModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.type)));
                        userTeamsModel.setName(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.name)));
                        userTeamsModel.setInternalrole(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.internalrole)));







                        userTeamModelList.add(userTeamsModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return userTeamModelList;
    }



}







/*
package com.outwork.sudlife.bcg.planner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.planner.models.Customerobject;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.ui.models.UserTeams;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlannerDao {
    public static final String TAG = PlannerDao.class.getSimpleName();

    private static PlannerDao instance;
    private Context applicationContext;

    public static PlannerDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new PlannerDao(applicationContext);
        }
        return instance;
    }

    private PlannerDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public long insertPlanner(PlannerModel plannerModel, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.groupid, groupid);
        if (plannerModel.getId() != null)
            values.put(IvokoProvider.Planner.id, plannerModel.getId());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid()))
            values.put(IvokoProvider.Planner.activityid, plannerModel.getActivityid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomerid()))
            values.put(IvokoProvider.Planner.customerid, plannerModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomertype()))
            values.put(IvokoProvider.Planner.customertype, plannerModel.getCustomertype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getBranchcode()))
            values.put(IvokoProvider.Planner.branchcode, plannerModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            values.put(IvokoProvider.Planner.customername, plannerModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFirstname()))
            values.put(IvokoProvider.Planner.firstname, plannerModel.getFirstname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getLastname()))
            values.put(IvokoProvider.Planner.lastname, plannerModel.getLastname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOpportunityid()))
            values.put(IvokoProvider.Planner.opportunityid, plannerModel.getOpportunityid());
        if (plannerModel.getLocalopportunityid() != null)
            values.put(IvokoProvider.Planner.localopportunityid, plannerModel.getLocalopportunityid());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getPurpose()))
            values.put(IvokoProvider.Planner.purpose, plannerModel.getPurpose());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtheractions()))
            values.put(IvokoProvider.Planner.otheractions, plannerModel.getOtheractions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtherfollowupactions()))
            values.put(IvokoProvider.Planner.otherfollowupactions, plannerModel.getOtherfollowupactions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes()))
            values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFollowupaction()))
            values.put(IvokoProvider.Planner.followupaction, plannerModel.getFollowupaction());
        values.put(IvokoProvider.Planner.scheduletime, plannerModel.getScheduletime());
        if (plannerModel.getCheckintime() != null)
            values.put(IvokoProvider.Planner.checkintime, plannerModel.getCheckintime());
        if (plannerModel.getCheckouttime() != null)
            values.put(IvokoProvider.Planner.checkouttime, plannerModel.getCheckouttime());
        if (plannerModel.getCheckoutlat() != null)
            values.put(IvokoProvider.Planner.checkoutlat, plannerModel.getCheckoutlat());
        if (plannerModel.getCheckoutlong() != null)
            values.put(IvokoProvider.Planner.checkoutlon, plannerModel.getCheckoutlong());
        if (plannerModel.getCheckinlat() != null)
            values.put(IvokoProvider.Planner.checkinlat, plannerModel.getCheckinlat());
        if (plannerModel.getCheckinlong() != null)
            values.put(IvokoProvider.Planner.checkinlon, plannerModel.getCheckinlong());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckinlocation()))
            values.put(IvokoProvider.Planner.checkinlocation, plannerModel.getCheckinlocation());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckoutlocation()))
            values.put(IvokoProvider.Planner.checkoutlocation, plannerModel.getCheckoutlocation());
        if (plannerModel.getCreateddate() != null)
            values.put(IvokoProvider.Planner.createddate, plannerModel.getCreateddate());
        if (plannerModel.getModifieddate() != null)
            values.put(IvokoProvider.Planner.modifieddate, plannerModel.getModifieddate());
        if (plannerModel.getScheduleday() != null)
            values.put(IvokoProvider.Planner.scheduleday, plannerModel.getScheduleday());
        if (plannerModel.getSchedulemonth() != null)
            values.put(IvokoProvider.Planner.schedulemonth, plannerModel.getSchedulemonth());
        if (plannerModel.getScheduleyear() != null)
            values.put(IvokoProvider.Planner.scheduleyear, plannerModel.getScheduleyear());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getLeadid() != null)
            values.put(IvokoProvider.Planner.leadid, plannerModel.getLeadid());
        if (plannerModel.getLocalleadid() != null)
            values.put(IvokoProvider.Planner.localleadid, plannerModel.getLocalleadid());
        if (plannerModel.getCustomerobject() != null)
            values.put(IvokoProvider.Planner.customerobject, new Gson().toJson(plannerModel.getCustomerobject()));
        long oppID = db.insert(IvokoProvider.Planner.TABLE, null, values);
        return oppID;
    }

    public void insertPlannerList(List<PlannerModel> plannerModelList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < plannerModelList.size(); j++) {
            PlannerModel plannerModel = plannerModelList.get(j);
            if (!plannerModel.getIsdeleted()) {
                if (!isRecordExist(db, String.valueOf(plannerModel.getActivityid()))) {
                    insertPlanner(plannerModel, userId, groupid);
                } else {
                    deleteformRecord(String.valueOf(plannerModel.getActivityid()));
                    insertPlanner(plannerModel, userId, groupid);
                }
            }
        }
    }

    public void updatePlannerStatus(String lid, String userId, String cstatus) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.networkstatus, "");
        values.put(IvokoProvider.Planner.completestatus, cstatus);

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlan(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype()))
            values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomerid()))
            values.put(IvokoProvider.Planner.customerid, plannerModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            values.put(IvokoProvider.Planner.customername, plannerModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomertype()))
            values.put(IvokoProvider.Planner.customertype, plannerModel.getCustomertype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getBranchcode()))
            values.put(IvokoProvider.Planner.branchcode, plannerModel.getBranchcode());
        if (plannerModel.getScheduleday() != null)
            values.put(IvokoProvider.Planner.scheduleday, plannerModel.getScheduleday());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getSchedulemonth() != null)
            values.put(IvokoProvider.Planner.schedulemonth, plannerModel.getSchedulemonth());
        if (plannerModel.getScheduleyear() != null)
            values.put(IvokoProvider.Planner.scheduleyear, plannerModel.getScheduleyear());
        values.put(IvokoProvider.Planner.scheduletime, plannerModel.getScheduletime());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateleadPlan(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(plannerModel.getType()))
            values.put(IvokoProvider.Planner.type, plannerModel.getType());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
            values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype()))
            values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        values.put(IvokoProvider.Planner.planstatus, plannerModel.getPlanstatus());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes()))
            values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateCheckoutPlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getCheckouttime() != null)
            values.put(IvokoProvider.Planner.checkouttime, plannerModel.getCheckouttime());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckoutlocation()))
            values.put(IvokoProvider.Planner.checkoutlocation, plannerModel.getCheckoutlocation());
        if (plannerModel.getCheckoutlat() != null)
            values.put(IvokoProvider.Planner.checkoutlat, plannerModel.getCheckoutlat());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (plannerModel.getCheckoutlong() != null)
            values.put(IvokoProvider.Planner.checkoutlon, plannerModel.getCheckoutlong());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getPurpose()))
            values.put(IvokoProvider.Planner.purpose, plannerModel.getPurpose());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtheractions()))
            values.put(IvokoProvider.Planner.otheractions, plannerModel.getOtheractions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOtherfollowupactions()))
            values.put(IvokoProvider.Planner.otherfollowupactions, plannerModel.getOtherfollowupactions());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFollowupaction()))
            values.put(IvokoProvider.Planner.followupaction, plannerModel.getFollowupaction());
        if (plannerModel.getFollowupdate() != null)
            values.put(IvokoProvider.Planner.followupdate, plannerModel.getFollowupdate());
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateCheckInPlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());

        values.put(IvokoProvider.Planner.notes, plannerModel.getNotes());
        values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());
        if (plannerModel.getCheckintime() != null)
            values.put(IvokoProvider.Planner.checkintime, plannerModel.getCheckintime());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCheckinlocation()))
            values.put(IvokoProvider.Planner.checkinlocation, plannerModel.getCheckinlocation());
        if (plannerModel.getCheckinlat() != null)
            values.put(IvokoProvider.Planner.checkinlat, plannerModel.getCheckinlat());
        if (plannerModel.getCheckinlong() != null)
            values.put(IvokoProvider.Planner.checkinlon, plannerModel.getCheckinlong());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
            } else {
                values.put(IvokoProvider.Planner.jointvisit, false);
            }

        }

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateReschedulePlanner(PlannerModel plannerModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.type, plannerModel.getType());
        values.put(IvokoProvider.Planner.subtype, plannerModel.getSubtype());
        values.put(IvokoProvider.Planner.activitytype, plannerModel.getActivitytype());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status()))
            values.put(IvokoProvider.Planner.networkstatus, plannerModel.getNetwork_status());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getTitle()))
            values.put(IvokoProvider.Planner.title, plannerModel.getTitle());
        if (plannerModel.getReschduledate() != null)
            values.put(IvokoProvider.Planner.reschduledate, plannerModel.getReschduledate());
        if (plannerModel.getDevicetimestamp() != null)
            values.put(IvokoProvider.Planner.devicetimestamp, plannerModel.getDevicetimestamp());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason()))
            values.put(IvokoProvider.Planner.resReason, plannerModel.getReshdreason());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorid()))
            values.put(IvokoProvider.Planner.supervisorid, plannerModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
            values.put(IvokoProvider.Planner.supervisorname, plannerModel.getSupervisorname());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit())
                values.put(IvokoProvider.Planner.jointvisit, plannerModel.getJointvisit());
        }
        if (plannerModel.getCompletestatus() != null)
            values.put(IvokoProvider.Planner.completestatus, plannerModel.getCompletestatus());

        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + plannerModel.getLid() + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{plannerModel.getUserid()});
    }

    public void updateLeadid(int lid, String userId, String id) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.leadid, id);
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner.localleadid + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlannerID(String lid, String userId, String id) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.userid, userId);
        values.put(IvokoProvider.Planner.activityid, id);
        values.put(IvokoProvider.Planner.networkstatus, "");
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public void updatePlannerdummyOnline(String lid, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Planner.networkstatus, "pending");
        db.update(IvokoProvider.Planner.TABLE, values, IvokoProvider.Planner._ID + " = " + lid + " AND " +
                IvokoProvider.Planner.userid + " = ?", new String[]{userId});
    }

    public List<PlannerModel> getOfflineplannerList(String userId, String networkstatus) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        try {
            if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
                Cursor cursor = null;
                String sql = "";
                sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
                sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
                sql += " AND " + IvokoProvider.Planner.networkstatus + " = '" + networkstatus + "'";
                try {
                    cursor = db.rawQuery(sql, null);
                    if (cursor.moveToFirst()) {
                        do {
                            PlannerModel plannerModel = new PlannerModel();
                            plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                            plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                            plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                            plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                            plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                            plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                            plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                            plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                            plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                            plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                            plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                            plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                            plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                            plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                            plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                            plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                            plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                            plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                            plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                            plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                            plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                            plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                            plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                            plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                            plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                            plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                            plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                            plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                            plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                            plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                            plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                            plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                            plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                            plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                            plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                            plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                            plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                            plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                            plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                            plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                            plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                    plannerModel.setJointvisit(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                    plannerModel.setJointvisit(true);
                                } else {
                                    plannerModel.setJointvisit(false);
                                }
                            plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                            plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                            plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                            plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                            plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                            plannerModelList.add(plannerModel);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        } catch (IllegalStateException exception) {
            exception.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByDate(String userId, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype))))
                            plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByBranches(String userId, String branchname, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerCreationCondition(String userId, String branchname, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            //sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";  // changed for 2.8.7 release
            sql += " AND " + IvokoProvider.Planner.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " AND (" + IvokoProvider.Planner.completestatus + " = '" + 0 + "'";
            sql += " OR " + IvokoProvider.Planner.completestatus + " = '" + 1 + "')";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<String> getPlannerDatesListByBranches(String userId, String branchname, String activitytype, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.customername + " = '" + branchname + "'";
            sql += " AND " + IvokoProvider.Planner.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.Planner.scheduleyear + " = '" + year + "'";
            sql += " AND " + IvokoProvider.Planner.activitytype + " = '" + activitytype + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        String date;
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)) != 0)
                            if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)) < Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                                date = TimeUtils.getFormattedDatefromUnix(String.valueOf(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime))), "dd/MM/yyyy");
                                if (!plannerModelList.contains(date))
                                    plannerModelList.add(date);
                            }
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByOpportunityID(String userId, String oppid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.opportunityid + " = '" + oppid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListBylocalOpportunityID(String userId, int oppid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.localopportunityid + " = '" + oppid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListByLeadID(String userId, String leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.leadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerListBylocalLeadID(String userId, int leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Planner.localleadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.opportunityid)));
                        plannerModel.setLocalopportunityid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localopportunityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public List<PlannerModel> getPlannerList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    //siva


    public List<PlannerModel> getPlannerListByLead(String leadid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Planner.TABLE;
            sql += " WHERE " + IvokoProvider.Planner.leadid + " = '" + leadid + "'";
            sql += " ORDER BY " + IvokoProvider.Planner.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activityid)));
                        plannerModel.setLocalleadid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.localleadid)));
                        plannerModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.leadid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customername)));
                        plannerModel.setCustomertype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customertype)));
                        plannerModel.setBranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.branchcode)));
                        plannerModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.firstname)));
                        plannerModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.lastname)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.purpose)));
                        plannerModel.setOtheractions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otheractions)));
                        plannerModel.setOtherfollowupactions(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.otherfollowupactions)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlocation)));
                        plannerModel.setCheckinlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlat)));
                        plannerModel.setCheckoutlat(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlat)));
                        plannerModel.setCheckinlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkinlon)));
                        plannerModel.setCheckoutlong(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.checkoutlon)));
                        plannerModel.setDevicetimestamp(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.devicetimestamp)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.title)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorid)));
                        plannerModel.setSupervisorname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.supervisorname)));
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Planner.reschduledate)));
                        plannerModel.setCustomerobject(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Planner.customerobject)), Customerobject.class));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }

    public long insertPlannerNotificationList(List<PlannerModel> plannerModelArrayList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values;
        long oppID = 0;
        for (int i = 0; i < plannerModelArrayList.size(); i++) {
            values = new ContentValues();
            values.put(IvokoProvider.PlannerNotification.userid, userId);
            values.put(IvokoProvider.PlannerNotification.groupid, groupid);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModelArrayList.get(i).getId())))
                values.put(IvokoProvider.PlannerNotification.id, plannerModelArrayList.get(i).getId());
            if (Utils.isNotNullAndNotEmpty(plannerModelArrayList.get(i).getActivityid()))
                values.put(IvokoProvider.PlannerNotification.activityid, plannerModelArrayList.get(i).getActivityid());
            if (plannerModelArrayList.get(i).getCustomerid() != null)
                values.put(IvokoProvider.PlannerNotification.customerid, plannerModelArrayList.get(i).getCustomerid());
            if (plannerModelArrayList.get(i).getCustomername() != null)
                values.put(IvokoProvider.PlannerNotification.customername, plannerModelArrayList.get(i).getCustomername());
            if (plannerModelArrayList.get(i).getOpportunityid() != null)
                values.put(IvokoProvider.PlannerNotification.opportunityid, plannerModelArrayList.get(i).getOpportunityid());
            if (plannerModelArrayList.get(i).getLocalopportunityid() != null)
                values.put(IvokoProvider.PlannerNotification.localopportunityid, plannerModelArrayList.get(i).getLocalopportunityid());
            values.put(IvokoProvider.PlannerNotification.activitytype, plannerModelArrayList.get(i).getActivitytype());
            values.put(IvokoProvider.PlannerNotification.type, plannerModelArrayList.get(i).getType());
            values.put(IvokoProvider.PlannerNotification.subtype, plannerModelArrayList.get(i).getSubtype());
            values.put(IvokoProvider.PlannerNotification.purpose, plannerModelArrayList.get(i).getPurpose());

            values.put(IvokoProvider.PlannerNotification.notes, plannerModelArrayList.get(i).getNotes());
            values.put(IvokoProvider.PlannerNotification.planstatus, plannerModelArrayList.get(i).getPlanstatus());
            values.put(IvokoProvider.PlannerNotification.plancreatedby, plannerModelArrayList.get(i).getPlancreatedby());
            values.put(IvokoProvider.PlannerNotification.completestatus, plannerModelArrayList.get(i).getCompletestatus());
            values.put(IvokoProvider.PlannerNotification.notificationstatus, plannerModelArrayList.get(i).getNotificationstatus());
            if (plannerModelArrayList.get(i).getFollowupdate() != null)
                values.put(IvokoProvider.PlannerNotification.followupdate, plannerModelArrayList.get(i).getFollowupdate());
            if (plannerModelArrayList.get(i).getFollowupaction() != null)
                values.put(IvokoProvider.PlannerNotification.followupaction, plannerModelArrayList.get(i).getFollowupaction());
            values.put(IvokoProvider.PlannerNotification.scheduletime, plannerModelArrayList.get(i).getScheduletime());
            if (plannerModelArrayList.get(i).getCheckintime() != null)
                values.put(IvokoProvider.PlannerNotification.checkintime, plannerModelArrayList.get(i).getCheckintime());
            if (plannerModelArrayList.get(i).getCheckouttime() != null)
                values.put(IvokoProvider.PlannerNotification.checkouttime, plannerModelArrayList.get(i).getCheckouttime());
            if (plannerModelArrayList.get(i).getCheckinlocation() != null)
                values.put(IvokoProvider.PlannerNotification.checkinlocation, plannerModelArrayList.get(i).getCheckinlocation());
            if (plannerModelArrayList.get(i).getCheckoutlocation() != null)
                values.put(IvokoProvider.PlannerNotification.checkoutlocation, plannerModelArrayList.get(i).getCheckoutlocation());
            if (plannerModelArrayList.get(i).getCreateddate() != null)
                values.put(IvokoProvider.PlannerNotification.createddate, plannerModelArrayList.get(i).getCreateddate());
            if (plannerModelArrayList.get(i).getModifieddate() != null)
                values.put(IvokoProvider.PlannerNotification.modifieddate, plannerModelArrayList.get(i).getModifieddate());
            if (plannerModelArrayList.get(i).getScheduleday() != null)
                values.put(IvokoProvider.PlannerNotification.scheduleday, plannerModelArrayList.get(i).getScheduleday());
            if (plannerModelArrayList.get(i).getSchedulemonth() != null)
                values.put(IvokoProvider.PlannerNotification.schedulemonth, plannerModelArrayList.get(i).getSchedulemonth());
            if (plannerModelArrayList.get(i).getScheduleyear() != null)
                values.put(IvokoProvider.PlannerNotification.scheduleyear, plannerModelArrayList.get(i).getScheduleyear());
            if (plannerModelArrayList.get(i).getTitle() != null)
                values.put(IvokoProvider.PlannerNotification.title, plannerModelArrayList.get(i).getTitle());
            if (plannerModelArrayList.get(i).getJointvisit() != null) {
                if (plannerModelArrayList.get(i).getJointvisit())
                    values.put(IvokoProvider.PlannerNotification.jointvisit, plannerModelArrayList.get(i).getJointvisit());
            }
            if (plannerModelArrayList.get(i).getReschduledate() != null)
                values.put(IvokoProvider.PlannerNotification.reschduledate, plannerModelArrayList.get(i).getReschduledate());
            if (plannerModelArrayList.get(i).getReshdreason() != null)
                values.put(IvokoProvider.PlannerNotification.resReason, plannerModelArrayList.get(i).getReshdreason());
            if (Utils.isNotNullAndNotEmpty(plannerModelArrayList.get(i).getNetwork_status()))
                values.put(IvokoProvider.PlannerNotification.networkstatus, plannerModelArrayList.get(i).getNetwork_status());
            oppID = db.insert(IvokoProvider.PlannerNotification.TABLE, null, values);
        }
        return oppID;
    }


    public List<PlannerModel> getPlannerNotificationList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE;
            sql += " WHERE " + IvokoProvider.PlannerNotification.userid + " = '" + userId + "'";
            ;
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification._ID)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customername)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activityid)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.purpose)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.title)));
                        plannerModel.setNotificationstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notificationstatus)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.reschduledate)));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }


    public List<PlannerModel> getPlannerNotificationListByDate(String userId, int day, int month, int year) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.PlannerNotification.TABLE;
            sql += " WHERE " + IvokoProvider.PlannerNotification.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.scheduleday + " = '" + day + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.schedulemonth + " = '" + month + "'";
            sql += " AND " + IvokoProvider.PlannerNotification.scheduleyear + " = '" + year + "'";
            sql += " ORDER BY " + IvokoProvider.PlannerNotification.scheduletime + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        PlannerModel plannerModel = new PlannerModel();
                        plannerModel.setLid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification._ID)));
                        plannerModel.setActivityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activityid)));
                        plannerModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.groupid)));
                        plannerModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.userid)));
                        plannerModel.setCustomerid(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customerid)));
                        plannerModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.customername)));
                        plannerModel.setActivitytype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.activitytype)));
                        plannerModel.setSubtype(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.subtype)));
                        plannerModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.type)));
                        plannerModel.setPurpose(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.purpose)));
                        plannerModel.setPlanstatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.planstatus)));
                        plannerModel.setCompletestatus(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.completestatus)));
                        plannerModel.setScheduletime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduletime)));
                        plannerModel.setCheckintime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkintime)));
                        plannerModel.setCheckouttime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkouttime)));
                        plannerModel.setCheckinlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkinlocation)));
                        plannerModel.setCheckoutlocation(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.checkoutlocation)));
                        plannerModel.setFollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupdate)));
                        plannerModel.setFollowupaction(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.followupaction)));
                        plannerModel.setNotes(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notes)));
                        plannerModel.setScheduleday(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleday)));
                        plannerModel.setSchedulemonth(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.schedulemonth)));
                        plannerModel.setScheduleyear(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.scheduleyear)));
                        plannerModel.setPlancreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.plancreatedby)));
                        plannerModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.networkstatus)));
                        plannerModel.setTitle(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.title)));
                        plannerModel.setNotificationstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.notificationstatus)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equals("1")) {
                                plannerModel.setJointvisit(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.jointvisit)).equalsIgnoreCase("true")) {
                                plannerModel.setJointvisit(true);
                            } else {
                                plannerModel.setJointvisit(false);
                            }
                        plannerModel.setReshdreason(cursor.getString(cursor.getColumnIndex(IvokoProvider.PlannerNotification.resReason)));
                        plannerModel.setReschduledate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.PlannerNotification.reschduledate)));
                        plannerModelList.add(plannerModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return plannerModelList;
    }


    public HashMap<String, Integer> getPlannerListByScheduleDates(int month, int currentDate) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<PlannerModel> plannerModelList = new ArrayList<>();
        HashMap<String, Integer> stringIntegerHashMap = null;
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            Cursor cursor = null;
            String query = "  SELECT SUM(CASE WHEN activitytype='activity' AND planstatus= 0 THEN 1 ELSE 0 END)" +
                    " AS activitycount, SUM(CASE WHEN activitytype='visit' " +
                    " AND planstatus= 1 THEN 1 ELSE 0 END) " +
                    "AS visitcount, SUM(CASE WHEN activitytype='visit'" +
                    "  AND completestatus= 1 THEN 1 ELSE 0 END) " +
                    "AS visitscompleted, SUM(CASE WHEN activitytype='activity' " +
                    " AND completestatus= 1 THEN 1 ELSE 0 END)" +
                    " AS activitiescompleted FROM" +
                    " planner WHERE schedulemonth=" + "'" + month + "'" +
                    " AND scheduleday>=1 AND scheduleday<=" + "'" + currentDate + "'";

            try {
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        stringIntegerHashMap = new HashMap<>();
                        stringIntegerHashMap.put("activitycount", cursor.getInt(cursor.getColumnIndex("activitycount")));
                        stringIntegerHashMap.put("visitcount", cursor.getInt(cursor.getColumnIndex("visitcount")));
                        stringIntegerHashMap.put("visitscompleted", cursor.getInt(cursor.getColumnIndex("visitscompleted")));
                        stringIntegerHashMap.put("activitiescompleted", cursor.getInt(cursor.getColumnIndex("activitiescompleted")));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return stringIntegerHashMap;
    }


    public void deleteRecordsBasedOnDay(String day) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.PlannerNotification.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.PlannerNotification.TABLE + " WHERE " + IvokoProvider.PlannerNotification.scheduleday + " = '" + day + "'");
        }
    }

    public void updatePlanNotificationStatus(String id, String notificationStatus, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.PlannerNotification.notificationstatus, notificationStatus);
        //   db.update(IvokoProvider.Planner.TABLE, values, "id=" + id, null);
        db.update(IvokoProvider.PlannerNotification.TABLE, values, IvokoProvider.PlannerNotification._ID + " = " + id + " AND " + IvokoProvider.PlannerNotification.userid + " = ?", new String[]{userId});

    }
    //siva end


    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private boolean isRecordExist(SQLiteDatabase db, String id) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Planner.TABLE + " WHERE " + IvokoProvider.Planner.activityid + " = ?", new String[]{id});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void deleteformRecord(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.Planner.TABLE + " WHERE " + IvokoProvider.Planner.activityid + " = '" + id + "'");
        }
    }



    public void insertUserTeamsList(List<UserTeams> userTeamsList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < userTeamsList.size(); j++) {
            UserTeams userTeamModel = userTeamsList.get(j);
                if (!isUserTeamRecordExist(db, String.valueOf(userTeamModel.getTeamid()))) {
                    insertUserTeams(userTeamModel, userId, groupid);
                } else {
                    deleteUserTeamRecord(String.valueOf(userTeamModel.getTeamid()));
                    insertUserTeams(userTeamModel, userId, groupid);
                }
        }
    }

    public void deleteUserTeamRecord(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Planner.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.UserTeam.TABLE + " WHERE " + IvokoProvider.UserTeam.teamid + " = '" + id + "'");
        }
    }


    public long insertUserTeams(UserTeams userTeamModel, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.UserTeam.userid, userId);
        values.put(IvokoProvider.UserTeam.groupid, groupid);

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getTeamid()))
            values.put(IvokoProvider.UserTeam.teamid, userTeamModel.getTeamid());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getType()))
            values.put(IvokoProvider.UserTeam.type, userTeamModel.getType());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getName()))
            values.put(IvokoProvider.UserTeam.name, userTeamModel.getName());

        if (Utils.isNotNullAndNotEmpty(userTeamModel.getInternalrole()))
            values.put(IvokoProvider.UserTeam.internalrole, userTeamModel.getInternalrole());



        long oppID = db.insert(IvokoProvider.UserTeam.TABLE, null, values);
        return oppID;
    }




    private boolean isUserTeamRecordExist(SQLiteDatabase db, String id) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.UserTeam.TABLE + " WHERE " + IvokoProvider.UserTeam.teamid + " = ?", new String[]{id});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }






    public List<UserTeams> getUserTeamList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<UserTeams> userTeamModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.UserTeam.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.UserTeam.TABLE;
            sql += " WHERE " + IvokoProvider.UserTeam.userid + " = '" + userId + "'";

            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        UserTeams userTeamsModel = new UserTeams();
                        userTeamsModel.setTeamid(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.teamid)));
                        userTeamsModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.type)));
                        userTeamsModel.setName(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.name)));
                        userTeamsModel.setInternalrole(cursor.getString(cursor.getColumnIndex(IvokoProvider.UserTeam.internalrole)));







                        userTeamModelList.add(userTeamsModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return userTeamModelList;
    }



}*/
