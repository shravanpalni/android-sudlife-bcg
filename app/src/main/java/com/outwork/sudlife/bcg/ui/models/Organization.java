package com.outwork.sudlife.bcg.ui.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Habi on 04-04-2018.
 */
public class Organization {

    @SerializedName("organizationid")
    @Expose
    private String organizationid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("iconurl")
    @Expose
    private String iconurl;
    @SerializedName("bannerimageurl")
    @Expose
    private List<String> bannerimageurl = null;
    @SerializedName("ispremiumuser")
    @Expose
    private Boolean ispremiumuser;

    public String getOrganizationid() {
        return organizationid;
    }

    public void setOrganizationid(String organizationid) {
        this.organizationid = organizationid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getIconurl() {
        return iconurl;
    }

    public void setIconurl(String iconurl) {
        this.iconurl = iconurl;
    }

    public List<String> getBannerimageurl() {
        return bannerimageurl;
    }

    public void setBannerimageurl(List<String> bannerimageurl) {
        this.bannerimageurl = bannerimageurl;
    }

    public Boolean getIspremiumuser() {
        return ispremiumuser;
    }

    public void setIspremiumuser(Boolean ispremiumuser) {
        this.ispremiumuser = ispremiumuser;
    }
}