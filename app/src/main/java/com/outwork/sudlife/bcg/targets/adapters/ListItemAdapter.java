package com.outwork.sudlife.bcg.targets.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.targets.models.TargetSummaryModel;

import java.util.ArrayList;
import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.TargetViewHolder2> {
    private Context context;
    int layoutSize;
    private final LayoutInflater mInflater;
    private List<TargetSummaryModel> targetSummaryModelsList = new ArrayList<>();

    public static class TargetViewHolder2 extends RecyclerView.ViewHolder {
        TextView tvBranchName;
        TextView tvTotal;
        TextView tvAchieved;

        public TargetViewHolder2(View v) {
            super(v);
            tvBranchName = (TextView) itemView.findViewById(R.id.tv_branchname);
            tvTotal = (TextView) itemView.findViewById(R.id.tv_total);
            tvAchieved = (TextView) itemView.findViewById(R.id.tv_achieved);
        }
    }

    public ListItemAdapter(List<TargetSummaryModel> targetSummaryModelsList, Context context) {
        this.context = context;
        this.targetSummaryModelsList = targetSummaryModelsList;
        this.layoutSize = layoutSize;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ListItemAdapter.TargetViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_in_listview, parent, false);
        return new TargetViewHolder2(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemAdapter.TargetViewHolder2 holder, final int position) {
        holder.tvBranchName.setText(targetSummaryModelsList.get(position).getBranchName());
        holder.tvTotal.setText(targetSummaryModelsList.get(position).getTotalValue());
        if (targetSummaryModelsList.get(position).getAchievedValue() != null) {
            holder.tvAchieved.setText(targetSummaryModelsList.get(position).getAchievedValue());
        } else {
            holder.tvAchieved.setText("0");
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return targetSummaryModelsList.size();
    }
}