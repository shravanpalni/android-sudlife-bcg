package com.outwork.sudlife.bcg.more.service;

import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 11/25/2016.
 */

public interface ResourceLibraryService {
    @GET("groups/{groupid}/resourcelibrary/content")
    Call<RestResponse> getResourceList(@Header("utoken") String userToken,
                                       @Path("groupid") String groupid);

    @GET("groups/{groupid}/resourcelibrary/content")
    Call<RestResponse> getResourceList(@Header("utoken") String userToken,
                                       @Path("groupid") String groupid,
                                       @Query("libraryid") String libraryid);

    @GET("AlbumService/albums/{albumid}")
    Call<RestResponse> getPhotos(@Header("utoken") String userToken, @Path("albumid") String albumid);
}