package com.outwork.sudlife.bcg.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.dto.UserDetailsDto;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.restinterfaces.UserRestInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserVerifyActivity extends BaseActivity {
    private IvokoApplication application;
    private boolean isFirstTimeRegister = true;
    private boolean fromSignIn = false;
    private TextView doneText, emailSentText, emailText;
    private ImageView cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_verify);


        application = (IvokoApplication) getApplication();
        cancel = (ImageView) findViewById(R.id.cvcancel);
        emailSentText = (TextView) findViewById(R.id.emailSent);

        if (getIntent().getExtras().containsKey("fromsignup")) {
            if (getIntent().getExtras().getBoolean("fromsignup", false)) {
                String message = getIntent().getExtras().
                        getString("message", "You are already registered with this email id. Please verify your email address to continue");
                emailSentText.setText(message);
            }
        }

        if (getIntent().getExtras().containsKey("fromforgot")) {
            if (getIntent().getExtras().getBoolean("fromforgot", false)) {
                String message = getIntent().getExtras().
                        getString("message", "You have not verified your email id. Please verify your email address to continue");
                emailSentText.setText(message);
            }
        }

        findViewById(R.id.letInBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToNextScreen();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.resendbtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_user_verify, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void navigateToNextScreen() {
        UserDetailsDto userDto = application.getUserDetails();

        if (!userDto.getUserName().equals("") && !userDto.getPassword().equals("")) {
            signInUser(UserVerifyActivity.this, userDto);
        } else {
            Intent Signin = new Intent(UserVerifyActivity.this, SignInActvity.class);
            startActivity(Signin);
            finish();
        }
    }

    private void resendActivation() {
        UserRestInterface client = RestService.createService(UserRestInterface.class);
        Call<RestResponse> user = client.resendActivation(application.getUserDetails().getUserName());
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse restResponse = response.body();
                    String code = restResponse.getCode();
                    String status = restResponse.getStatus();
                    String message = restResponse.getMessage();

                    if (Constants.STATUS_FAILED.equalsIgnoreCase(status)) {
                        Toast.makeText(UserVerifyActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        emailSentText.setText("We have resent verification mail. Please verify your email address by clicking the link in the confirmation mail we sent you");
                        emailSentText.setTextColor(getResources().getColor(R.color.blue_500));
                        Utils.displayToast(UserVerifyActivity.this, "We have resent your verification email");
                    }

                } else {
                    Intent in = new Intent(UserVerifyActivity.this, SignInActvity.class);
                    startActivity(in);
                }


            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Intent in = new Intent(UserVerifyActivity.this, SignInActvity.class);
                startActivity(in);

            }
        });
    }


    public void signInUser(final Context context, final UserDetailsDto userDetailsDto) {


    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }


}
