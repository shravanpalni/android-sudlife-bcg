package com.outwork.sudlife.bcg.opportunity.services;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.outwork.sudlife.bcg.lead.LeadMgr;
import com.outwork.sudlife.bcg.opportunity.OpportunityMgr;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityItems;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.opportunity.models.StagesModel;
import com.outwork.sudlife.bcg.planner.service.PlannerIntentService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Response;

public class OpportunityIntentService extends JobIntentService {
    public static final String TAG = OpportunityIntentService.class.getSimpleName();

    public static final String ACTION_GET_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.GET_OPPORTUNITY";
    public static final String ACTION_GET_OPPORTUNITY_STAGES = "com.outwork.sudlife.bcg.opportunity.services.action.GET_OPPORTUNITY_STAGES";
    public static final String ACTION_POST_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.POST_OPPORTUNITY";
    public static final String ACTION_FORMS_OFFLINE_SYNC = "com.outwork.sudlife.bcg.opportunity.services.action.FORMS_OFFLINE_SYNC";
    public static final String ACTION_UPDATE_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.UPDATE_OPPORTUNITY";
    public static final String ACTION_DELETE_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.DELETE_OPPORTUNITY";
    public static final String ACTION_DELETE_ORDER_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.DELETE_ORDER_OPPORTUNITY";
    public static final String ACTION_ADD_ORDER_OPPORTUNITY = "com.outwork.sudlife.bcg.opportunity.services.action.DELETE_ORDER_OPPORTUNITY";
    public static final String ACTION_FORMS_OFFLINE_LEADS_SYNC = "com.outwork.sudlife.bcg.opportunity.services.action.FORMS_OFFLINE_LEADS_SYNC";
    public static final String ACTION_FORMS_INSERT_LEADS = "com.outwork.sudlife.bcg.opportunity.services.action.FORMS_INSERT_LEADS";

    public static final String ORDER_ID = "com.outwork.sudlife.bcg.opportunity.services.extra.ORDER_ID";
    public static final String OPPORTUNITY_ID = "com.outwork.sudlife.bcg.opportunity.services.extra.OPPORTUNITY_ID";
    public static final String OPPORTUNITY_MODEL = "com.outwork.sudlife.bcg.opportunity.services.extra.OPPORTUNITY_MODEL";
    public static final String STRT_DATE = "com.outwork.sudlife.bcg.opportunity.services.extra.STRT_DATE";
    public static final String END_DATE = "com.outwork.sudlife.bcg.opportunity.services.extra.END_DATE";
    private static final Integer JOBID = 1006;

    private LocalBroadcastManager mgr;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_OPPORTUNITY.equals(action)) {
                final String startdate = intent.getStringExtra(STRT_DATE);
                final String enddate = intent.getStringExtra(END_DATE);
                getServerOpportunity(startdate, enddate);
            }
            if (ACTION_FORMS_OFFLINE_LEADS_SYNC.equals(action)) {
                syncOfflineLeads();
            }
            //leads
            if (ACTION_FORMS_INSERT_LEADS.equals(action)) {
                getServerLeads();
            }
            if (ACTION_GET_OPPORTUNITY_STAGES.equals(action)) {
                getServerOpportunitystages();
            }
            if (ACTION_POST_OPPORTUNITY.equals(action)) {
                final String opprtmodel = intent.getStringExtra(OPPORTUNITY_MODEL);
                postOpportunity(new Gson().fromJson(opprtmodel, OpportunityModel.class));
            }
            if (ACTION_DELETE_OPPORTUNITY.equals(action)) {
                final String opportunityID = intent.getStringExtra(OPPORTUNITY_ID);
                deleteOpportunity(opportunityID);
            }
            if (ACTION_UPDATE_OPPORTUNITY.equals(action)) {
                final String opprtmodel = intent.getStringExtra(OPPORTUNITY_MODEL);
                updateOpportunity(new Gson().fromJson(opprtmodel, OpportunityModel.class));
            }
            if (ACTION_DELETE_ORDER_OPPORTUNITY.equals(action)) {
                final String opportunityID = intent.getStringExtra(OPPORTUNITY_ID);
                final String orderID = intent.getStringExtra(ORDER_ID);
                deleteOrderOpportunity(opportunityID, orderID);
            }
            if (ACTION_FORMS_OFFLINE_SYNC.equals(action)) {
                syncOfflineOpportunities();
            }
        }
    }

    public static void insertOpportunity(Context context, String startdate, String enddate) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_GET_OPPORTUNITY);
        intent.putExtra(STRT_DATE, startdate);
        intent.putExtra(END_DATE, enddate);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void insertOpportunityStages(Context context) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_GET_OPPORTUNITY_STAGES);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void deleteOpportunity(Context context, String opportunityID) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_DELETE_OPPORTUNITY);
        intent.putExtra(OPPORTUNITY_ID, opportunityID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void deleteOrderOpportunity(Context context, String opportunityID, String orderID) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_DELETE_ORDER_OPPORTUNITY);
        intent.putExtra(OPPORTUNITY_ID, opportunityID);
        intent.putExtra(ORDER_ID, orderID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void syncOfflineRecords(Context context) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_FORMS_OFFLINE_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void postOpportunity(Context context, OpportunityModel opportunityModel) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_POST_OPPORTUNITY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void updateOpportunity(Context context, OpportunityModel opportunityModel) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_UPDATE_OPPORTUNITY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void syncOfflineOpportunities() {
        final List<OpportunityModel> opportunityModelList = OpportunityMgr.getInstance(OpportunityIntentService.this).getOpportunityOfflineList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (opportunityModelList.size() > 0) {
            for (final OpportunityModel opportunityModel : opportunityModelList) {
                if (Utils.isNotNullAndNotEmpty(opportunityModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (TextUtils.isEmpty(opportunityModel.getOpportunityid())) {
                            postOpportunity(opportunityModel);
                        } else {
                            updateOpportunity(opportunityModel);
                        }
                    }
                }
            }
            PlannerIntentService.syncPlanstoServer(OpportunityIntentService.this);
        } else {
            PlannerIntentService.syncPlanstoServer(OpportunityIntentService.this);
        }
    }

    private void postOpportunity(final OpportunityModel opportunityModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postOpportunity = client.postOpportunityv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), opportunityModel);
        try {
            Response<RestResponse> restResponse = postOpportunity.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                    OpportunityMgr.getInstance(OpportunityIntentService.this).updateOpportunityOnline(opportunityModel, userid, restResponse.body().getData());
                    Intent intent = new Intent("opp_broadcast");
                    mgr = LocalBroadcastManager.getInstance(OpportunityIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateOpportunity(final OpportunityModel opportunityModel) {
        FormsService client = RestService.createServicev1(FormsService.class);

        Call<RestResponse> updateOpportunity = client.updateOpportunityv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), opportunityModel.getOpportunityid(), opportunityModel);
        try {
            Response<RestResponse> restResponse = updateOpportunity.execute();
            if (Utils.isNotNullAndNotEmpty(restResponse.body().toString())) {
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                        if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                            OpportunityMgr.getInstance(OpportunityIntentService.this).updateOpportunityOnline(opportunityModel, userid, opportunityModel.getOpportunityid());
                            Intent intent = new Intent("opp_broadcast");
                            mgr = LocalBroadcastManager.getInstance(OpportunityIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerOpportunity(final String startdate, String enddate) {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.OPPORTUNITY_LAST_FETCH_TIME, "");
        List<OpportunityModel> opportunityList = OpportunityMgr.getInstance(OpportunityIntentService.this).getOpportunityList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (opportunityList.size() == 0) {
            lastfetchtime = "";
        }
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getOpportunities = client.getOpportFormsv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), startdate, enddate, "", lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        try {
            Response<RestResponse> restResponse = getOpportunities.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (!TextUtils.isEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<OpportunityModel>>() {
                        }.getType();
                        List<OpportunityModel> opportunityModelList = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (opportunityModelList.size() > 0) {
                            OpportunityMgr.getInstance(OpportunityIntentService.this).insertOpportunityList(opportunityModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.OPPORTUNITY_LAST_FETCH_TIME, newFetchTime);

                        PlannerIntentService.insertPlanList(OpportunityIntentService.this, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")), Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerOpportunitystages() {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getOpportunitiesstages = client.getOpportstagesv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        try {
            Response<RestResponse> restResponse = getOpportunitiesstages.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<StagesModel>>() {
                        }.getType();
                        List<StagesModel> stagesModelList = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (stagesModelList.size() > 0) {
                            OpportunityMgr.getInstance(OpportunityIntentService.this).insertOpportunitystagesList(stagesModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.STAGES_LOADED, "loaded");
                        Intent intent = new Intent("oppstage_broadcast");
                        mgr = LocalBroadcastManager.getInstance(OpportunityIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    SharedPreferenceManager.getInstance().putString(Constants.STAGES_LOADED, "notloaded");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            SharedPreferenceManager.getInstance().putString(Constants.STAGES_LOADED, "notloaded");
        }
    }

    private void deleteOpportunity(String opportunityID) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> deleteOpportunity = client.deleteOpportunityv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), opportunityID);
        try {
            Response<RestResponse> restResponse = deleteOpportunity.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                OpportunityMgr.getInstance(OpportunityIntentService.this).deleteOpportunityRecordID(opportunityID);
                Intent intent = new Intent("opp_broadcast");
                mgr = LocalBroadcastManager.getInstance(OpportunityIntentService.this);
                mgr.sendBroadcast(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addOrderOpportunity(final OpportunityModel opportunityModel, final List<OpportunityItems> opportunityItemsList) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> deleteOpportunity = client.postOpportunityOrderv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), opportunityModel.getOpportunityid(), opportunityItemsList);

        try {
            Response<RestResponse> restResponse = deleteOpportunity.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                RestResponse jsonResponse = restResponse.body();
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    OpportunityItems opportunityItems = new OpportunityItems();
                    try {
                        JSONObject jsonObject = new JSONObject(restResponse.body().getData());
                        JSONArray array = jsonObject.getJSONArray("OpportunityItems");
                        JSONObject jsonObject1 = array.getJSONObject(0);
                        opportunityItems = opportunityItemsList.get(0);
                        opportunityItems.setOrderid(Integer.parseInt(jsonObject1.optString("OrderId")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteOrderOpportunity(String opportunityID, String orderID) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> deleteOpportunity = client.deleteOpportunityOrderv1(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), opportunityID, orderID);
        try {
            Response<RestResponse> restResponse = deleteOpportunity.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                RestResponse jsonResponse = restResponse.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void insertLeads(Context context) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_FORMS_INSERT_LEADS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void syncLeadstoServer(Context context) {
        Intent intent = new Intent(context, OpportunityIntentService.class);
        intent.setAction(ACTION_FORMS_OFFLINE_LEADS_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, OpportunityIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    public String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        int j = days;
        cal.add(Calendar.DAY_OF_YEAR, j);
        return s.format(new Date(cal.getTimeInMillis()));
    }


    //leads
    private void syncOfflineLeads() {
        final List<LeadModel> leadModelList = LeadMgr.getInstance(OpportunityIntentService.this).getOfflineLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (leadModelList.size() > 0) {
            for (final LeadModel leadModel : leadModelList) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (TextUtils.isEmpty(leadModel.getLeadid())) {
                            postLead(leadModel);
                        } else {
                            //    updateOpportunity(leadModel);
                        }
                    }
                }
            }
        }
    }


    private void postLead(final LeadModel leadModel) {

        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postLead = client.postLead(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel);
        try {
            Response<RestResponse> restResponse = postLead.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                    LeadMgr.getInstance(OpportunityIntentService.this).updateLeadOnline(leadModel, userid, restResponse.body().getData());
                    Intent intent = new Intent("opp_broadcast");
                    mgr = LocalBroadcastManager.getInstance(OpportunityIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void getServerLeads() {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.OPPORTUNITY_LAST_FETCH_TIME, "");
        List<LeadModel> leadsList = LeadMgr.getInstance(OpportunityIntentService.this).getLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (leadsList.size() == 0) {
            lastfetchtime = "";
        }
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getOpportunities = client.getServerLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        try {
            Response<RestResponse> restResponse = getOpportunities.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (!TextUtils.isEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<LeadModel>>() {
                        }.getType();
                        //  Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                        List<LeadModel> leadModelList = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(OpportunityIntentService.this).insertLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.OPPORTUNITY_LAST_FETCH_TIME, newFetchTime);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}