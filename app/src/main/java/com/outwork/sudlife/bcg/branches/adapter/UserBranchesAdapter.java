package com.outwork.sudlife.bcg.branches.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.List;

/**
 * Created by shravanch on 17-07-2018.
 */

public class UserBranchesAdapter extends RecyclerView.Adapter<UserBranchesAdapter.TaskViewHolder> {

    private final LayoutInflater mInflater;
    private Context context;
    private List<BranchesModel> userbranchesArrayList;


    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public TextView customer_name;
        public TextView customer_type;

        public TaskViewHolder(View v) {
            super(v);
            customer_name = (TextView) v.findViewById(R.id.customer_name);
            customer_type = (TextView) v.findViewById(R.id.customer_type);

            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, customer_name);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, customer_type);
        }
    }

    public UserBranchesAdapter(Context context, List<BranchesModel> userbranchesArrayList) {
        this.context = context;
        this.userbranchesArrayList = userbranchesArrayList;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_branches_list_item, parent, false);
        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, final int position) {
        final BranchesModel branchesModel = (BranchesModel) this.userbranchesArrayList.get(position);

        if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername()))
            holder.customer_name.setText(branchesModel.getCustomername());

        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype())) {
            stringBuilder.append(branchesModel.getCustomertype());
            if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode())) {
                stringBuilder.append(" - " + branchesModel.getBranchcode());
            }
        }
        holder.customer_type.setText(stringBuilder);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userbranchesArrayList.size();
    }
}
