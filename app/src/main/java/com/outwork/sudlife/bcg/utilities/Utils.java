package com.outwork.sudlife.bcg.utilities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.dto.DeviceInfo;

@SuppressLint("SimpleDateFormat")
public class Utils extends AppCompatActivity {

    private String code = null;
    private static final int REQUEST = 112;
    protected static String[] PERMISSIONS_READ_STATE = {
            Manifest.permission.READ_PHONE_STATE
    };
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static boolean isValidEmail(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }

    public static String getPreviousMonthDate(Date date) {
        final SimpleDateFormat format = new SimpleDateFormat("MM-yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DATE, -1);

        Date preMonthDate = cal.getTime();
        return format.format(preMonthDate);
    }

    public static String getNextMonthDate(Date date) {
        final SimpleDateFormat format = new SimpleDateFormat("MM-yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1);

        Date nxtMonthDate = cal.getTime();
        return format.format(nxtMonthDate);
    }

    public static List<String> getpreviousMonths() {
        SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy MM");
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.MONTH, +3);
        System.out.println(format.format(cal1.getTime()));

        List<String> allDates = new ArrayList<>();
        String maxDate = format.format(cal1.getTime());
        SimpleDateFormat monthDate = new SimpleDateFormat("MMMM yyyy MM");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(monthDate.parse(maxDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= 6; i++) {
            String month_name1 = monthDate.format(cal.getTime());
            allDates.add(month_name1);
            cal.add(Calendar.MONTH, -1);
        }
        return allDates;
    }

    public static DeviceInfo getInfosAboutDevice(Activity a, int whatever) {
        String s = "";
        DeviceInfo deviceInfo = new DeviceInfo();
        try {
            PackageInfo pInfo = a.getPackageManager().getPackageInfo(
                    a.getPackageName(), PackageManager.GET_META_DATA);
            s += "\n APP Package Name: " + a.getPackageName();
            deviceInfo.setApprelease(pInfo.versionName);
            deviceInfo.setAppversioncode(Integer.toString(pInfo.versionCode));

        } catch (PackageManager.NameNotFoundException e) {
        }

        deviceInfo.setOs("Android");

        deviceInfo.setLibversion(System.getProperty("os.version") + " ("
                + android.os.Build.VERSION.INCREMENTAL + ")");
        deviceInfo.setOsversion(Integer.toString(Build.VERSION.SDK_INT));
        deviceInfo.setManufacturer(Build.MANUFACTURER);
        deviceInfo.setModel(android.os.Build.MODEL + " ("
                + android.os.Build.PRODUCT + ")");
        // TODO add application version


        // more from
        // http://developer.android.com/reference/android/os/Build.html :
        s += "\n Manufacturer: " + android.os.Build.MANUFACTURER;


        return deviceInfo;
    }
    public static boolean isNotNullAndNotEmpty(String string) {
        return string != null && !string.isEmpty();
    }

    public static boolean isNotNullAndNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
    public static void after15Days() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 15); // add 15 days
        date = cal.getTime();
    }
    public static void setTypefaces(Typeface typeface, TextView... textViews) {
        for (TextView tv : textViews) {
            tv.setTypeface(typeface);
        }
    }
    public static String getNextDate(String curDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        Date date = null;
        try {
            date = format.parse(curDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return format.format(calendar.getTime());
    }

    public static void before15Days() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -15); // add 15 days
        date = cal.getTime();

    }

    public static boolean isValidPhone(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13 ) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }


    public static boolean isValidPhoneNew(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() ==10) {

                check = true;
            } else {
                check = false;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static boolean isValidPhoneNri(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if ((phone.length() !=0)&&(phone.length()>=8)&&(phone.length()<=15)) {

                check = true;
            } else {
                check = false;
            }
        } else {
            check = false;
        }
        return check;
    }

    public final static boolean isValidPhoneNumber(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.PHONE.matcher(target).matches();
    }

    public final static boolean isvalidmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static int getMonthNumber(String monthName) {
        int monthNumber = 0;
        try {
            Date date = new SimpleDateFormat("MMM").parse(monthName);//put your month name here
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            monthNumber = cal.get(Calendar.MONTH);
            System.out.println(monthNumber);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return monthNumber;
    }

    public static List<String> printDatesInMonth(int year, int month) {
        List<String> dates = new ArrayList<>();
        SimpleDateFormat fmt = new SimpleDateFormat("dd EEE MM yyyy");
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month - 1, 1);
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysInMonth; i++) {
            dates.add(fmt.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return dates;
    }

    public static Date stringToDate(String dtStart) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dtStart);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static long getdifferenceBetweenTwoDates(String timestamp, String timestamp2) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

        String inputString1 = TimeUtils.getFormattedDatefromUnix(timestamp, "yyyy-MM-dd hh:mm");
        String inputString2 = TimeUtils.getFormattedDatefromUnix(timestamp2, "yyyy-MM-dd hh:mm");
        long diff = 0;
        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            diff = date2.getTime() - date1.getTime();
            System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String getDeviceOS() {
        return System.getProperty("os.version");
    }

    public static String getdeviceType() {
        return android.os.Build.DEVICE;
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getString(JSONObject jsonObject, String propertyName) throws JSONException {
        if (jsonObject.isNull(propertyName)) {
            return null;
        } else {
            if (TextUtils.isEmpty(jsonObject.getString(propertyName))) {
                return null;
            } else {
                return jsonObject.getString(propertyName);
            }
        }
    }

    public static Boolean getBoolean(JSONObject jsonObject, String propertyName)
            throws JSONException {
        if (jsonObject.isNull(propertyName)) {
            return null;
        } else {
            String str = jsonObject.getString(propertyName);

            if (TextUtils.isEmpty(str)) {
                return null;
            } else {
                if (TextUtils.isDigitsOnly(str)) {
                    return !(Integer.parseInt(str) == 0);
                } else {
                    // parses both "true"/true and "false"/false as boolean
                    return jsonObject.getBoolean(propertyName);
                }
            }
        }
    }


    public static String getFormattedDate(String inputDate, boolean year, boolean dummy) {

        String formatteddate = inputDate;
        Date date1;

        try {
            DateFormat inputDF = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
            date1 = inputDF.parse(inputDate);
        } catch (ParseException e6) {
            try {
                DateFormat inputDF = new SimpleDateFormat("MM/dd/yy hh:mm:ss a");
                date1 = inputDF.parse(inputDate);
            } catch (ParseException e) {
                try {
                    DateFormat inputDF = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
                    date1 = inputDF.parse(inputDate);
                } catch (ParseException e1) {
                    try {
                        DateFormat inputDF = new SimpleDateFormat("dd-MM-yy hh:mm:ss");
                        date1 = inputDF.parse(inputDate);
                    } catch (ParseException e2) {
                        try {
                            DateFormat inputDF = new SimpleDateFormat("yy/MM/dd hh:mm:ss");
                            date1 = inputDF.parse(inputDate);
                        } catch (ParseException e3) {
                            try {
                                DateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                                date1 = inputDF.parse(inputDate);
                            } catch (ParseException e7) {
                                try {
                                    DateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                                    date1 = inputDF.parse(inputDate);
                                } catch (ParseException e4) {
                                    try {
                                        DateFormat inputDF = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
                                        date1 = inputDF.parse(inputDate);
                                    } catch (ParseException e5) {
                                        return inputDate;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (DateUtils.isToday(date1.getTime())) {
            DateFormat outputDate = new SimpleDateFormat("HH:mm");
            return "Today " + outputDate.format(date1);
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);

            Calendar current = Calendar.getInstance();
            current.setTime(new Date());

            if (cal.get(Calendar.YEAR) != current.get(Calendar.YEAR)) {
                DateFormat outputDate = new SimpleDateFormat("MMM dd yyyy, HH:mm");
                //  DateFormat outputDate = new SimpleDateFormat("MMM dd @ hh:mm");
                return outputDate.format(date1);
            } else {

                if (year) {
                    DateFormat outputDate = new SimpleDateFormat("MMM dd HH:mm");
                    return outputDate.format(date1);
                } else {
                    DateFormat outputDate = new SimpleDateFormat("MMM dd");
                    return outputDate.format(date1);
                }


            }
        }
    }

    public static String getFormattedDate(String inputDate, boolean year) {

        String formatteddate = inputDate;
        Date date1;
        try {
            DateFormat inputDF = new SimpleDateFormat("MM/dd/yy HH:mm:ss a");
            date1 = inputDF.parse(inputDate);
        } catch (ParseException e) {
            try {
                DateFormat inputDF = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
                date1 = inputDF.parse(inputDate);
            } catch (ParseException e1) {
                try {
                    DateFormat inputDF = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
                    date1 = inputDF.parse(inputDate);
                } catch (ParseException e2) {
                    try {
                        DateFormat inputDF = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
                        date1 = inputDF.parse(inputDate);
                    } catch (ParseException e3) {
                        try {
                            DateFormat inputDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
                            date1 = inputDF.parse(inputDate);
                        } catch (ParseException e4) {
                            try {
                                DateFormat inputDF = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
                                date1 = inputDF.parse(inputDate);
                            } catch (ParseException e5) {
                                return inputDate;
                            }
                        }
                    }
                }
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);

        Calendar current = Calendar.getInstance();
        current.setTime(new Date());

        DateFormat outputDate = new SimpleDateFormat("E MMM dd yyyy, HH:mm");
        //  DateFormat outputDate = new SimpleDateFormat("MMM dd @ hh:mm");
        return outputDate.format(date1);

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    public static String getMonthShortName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
                simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static String formatDateFromString(String inputFormat, String outputFormat, String inputDate) {
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat);
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat);
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            Log.d("CALENDER", "ParseException - dateFormat");
        }
        return outputDate;
    }

    public static String getOrdinal(final int pDay) {
        if (pDay >= 11 && pDay <= 13) {
            return "th";
        }
        switch (pDay % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getFileExtn(String FileName) {
        String extn = "";

        int dotposition = FileName.lastIndexOf(".");
        //filename_Without_Ext = file.substring(0,dotposition);
        extn = FileName.substring(dotposition + 1, FileName.length());
        if (extn.equalsIgnoreCase("gif") || extn.equalsIgnoreCase("jpeg") || extn.equalsIgnoreCase("jpg") ||
                extn.equalsIgnoreCase("png") || extn.equalsIgnoreCase("webp")) {
            extn = "image";
        }


        return extn;

    }

    public static String getFormattedDate(String timestamp, boolean year, int whatever) {
        String returnvalue = "";

        //Handle null...should be taken care in api
        if (TextUtils.isEmpty(timestamp) || timestamp.equalsIgnoreCase("null")) {
            return returnvalue;
        }

        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date date = new Date(timestamp1);
        //sf.format(date)

        if (year) {
            returnvalue = getFormattedDate(sf.format(date), true);
        } else {
            returnvalue = getFormattedDate(sf.format(date), true, true);
        }


        return returnvalue;
    }

    public static String getFormattedDate(String timestamp) {
        String returnvalue = "";


        if (TextUtils.isEmpty(timestamp)) {
            return returnvalue;
        }

        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat("HH:mm");
        Date date = new Date(timestamp1);
        //sf.format(date)


        return getFormattedDate(sf.format(date), true);


    }


    public static String getFeedListFormatDate(String timestamp, boolean year, int whatever) {
        String returnvalue = "";

        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date date = new Date(timestamp1);
        //sf.format(date)

        if (year) {
            returnvalue = getFormattedDate(sf.format(date), true);
        } else {
            returnvalue = getFormattedDate(sf.format(date), true, false);
        }


        return returnvalue;
    }

    public String getTime(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

    public static void displayToast(Context context, String errorMsg) {
        Toast toast = Toast.makeText(context, errorMsg, Toast.LENGTH_LONG);
        View view = toast.getView();
        // view.setBackgroundColor(getColor(Color.RED));
        view.setBackgroundResource(R.drawable.toast_background_color);
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (56 * scale + 0.5f);
        view.setMinimumHeight(pixels);
        TextView text = (TextView) view.findViewById(android.R.id.message);
/*Here you can do anything with above textview like text.setTextColor(Color.parseColor("#000000"));*/
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static void noNetworkDialog(Activity context) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Oops....No network");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();


    }
}