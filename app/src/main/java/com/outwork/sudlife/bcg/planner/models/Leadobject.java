package com.outwork.sudlife.bcg.planner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Leadobject {

    @SerializedName("branchcode")
    @Expose
    private String branchcode;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customertype")
    @Expose
    private String customertype;
    @SerializedName("leadid")
    @Expose
    private String leadid;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("age")
    @Expose
    private Integer age;

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomertype() {
        return customertype;
    }

    public void setCustomertype(String customertype) {
        this.customertype = customertype;
    }

    public String getLeadid() {
        return leadid;
    }

    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}