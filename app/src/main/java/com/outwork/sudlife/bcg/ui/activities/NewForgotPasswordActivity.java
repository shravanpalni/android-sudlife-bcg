package com.outwork.sudlife.bcg.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.restinterfaces.UserRestInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 15-05-2018.
 */

public class NewForgotPasswordActivity extends BaseActivity {

    private EditText loginField;
    private TextView submitpwd;
    private TextView toolbar_title;
    private ProgressDialog progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_forgot_pwd);

        loginField = (EditText) findViewById(R.id.userpwdLoginField);
        submitpwd = (TextView) findViewById(R.id.pwd_submit);

        initialiseToolBar();

        submitpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // errorText.setText(null);
                if (validate()) {
                    if (isNetworkAvailable()) {
                        progressdialog = new ProgressDialog(NewForgotPasswordActivity.this);
                        progressdialog.setMessage("please wait. . .");
                        progressdialog.show();
                        forgotpassword(loginField.getText().toString());
                        hideKeyboard();
                    } else {
                        showToast("No Internet Connection......");
                    }
                }
            }
        });

        Utils.setTypefaces(IvokoApplication.robotoTypeface, loginField);
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, submitpwd);
    }

    private void forgotpassword(String email) {
        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.newforgotPassword(email);
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                try {
                    if (progressdialog.isShowing()) {
                        progressdialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        RestResponse restResponse = response.body();
                        String status = restResponse.getStatus();
                        if (status.equalsIgnoreCase("Success")) {
                            //Utils.alertDialaog(NewForgotPasswordActivity.this,"Forgot pasword","A link has been sent to your mail.");
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(NewForgotPasswordActivity.this);
                            builder1.setTitle("Forgot pasword");
                            builder1.setMessage("A link has been sent to your mail.");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            finish();
                                        }
                                    });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            // finish();
                        } else {
                            Utils.displayToast(NewForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
                        }
                    } else {
                        Utils.displayToast(NewForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Utils.displayToast(NewForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
            }
        });
    }

    private boolean validate() {
        boolean isValid = true;
        if (loginField.getText().toString().length() == 0) {
            isValid = false;
            loginField.setError("Please enter valid Login id or email");
            return isValid;
        }
        return isValid;
    }

    private void initialiseToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.newforgotpassword);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Forgot password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }
}
