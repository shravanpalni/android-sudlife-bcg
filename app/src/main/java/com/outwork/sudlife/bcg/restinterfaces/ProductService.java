package com.outwork.sudlife.bcg.restinterfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Panch on 3/21/2016.
 */
public interface ProductService {
    @GET("mobile/v1/products")
    Call<RestResponse> getMasterProducts(@Header("utoken") String userToken);
    @GET("mobile/v1/masterlist")
    Call<RestResponse> getMasterList(@Header("utoken") String userToken,
                                     @Query("groupid") String groupid);
}
