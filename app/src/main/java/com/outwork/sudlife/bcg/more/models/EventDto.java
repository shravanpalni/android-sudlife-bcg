package com.outwork.sudlife.bcg.more.models;

import android.text.TextUtils;

import com.outwork.sudlife.bcg.dto.LocationDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 12/6/2015.
 */
public class EventDto extends CommonDto {

        private String eventId;
        private String title;
        private String description;
        private String textDescription;
        private LocationDto eventLocation;
        private String postedDate;
        private String eventStartDate;
        private String eventEndDate;
        private String eventImage;
        private String eventUrl;
        private String shareText="Click here to find more";
        private String shareable="";
        private String shareUrl="";

    public rsvpresponse getResponse() {
        return response;
    }

    public void setResponse(rsvpresponse response) {
        this.response = response;
    }

    private rsvpresponse response;



    private List<Organizer> organizerList;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public LocationDto getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(LocationDto eventLocationDto) {
        this.eventLocation = eventLocationDto;
    }


    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public List<Organizer> getOrganizerList() {
        return organizerList;
    }

    public void setOrganizerList(List<Organizer> organizerList) {
        this.organizerList = organizerList;
    }

    public static class Organizer{
        String name;

        String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public String getShareable() {
        return shareable;
    }

    public void setShareable(String shareable) {
        this.shareable = shareable;
    }


    public static class rsvpresponse {
        public int getResponse() {
            return response;
        }

        public void setResponse(int response) {
            this.response = response;
        }

        public String getGuestcount() {
            return guestcount;
        }

        public void setGuestcount(String guestcount) {
            this.guestcount = guestcount;
        }

        int response;
        String guestcount;


    }

    private static final String JSON_EVENTID = "eventId";
    private static final String JSON_TITLE = "title";
    private static final String JSON_DESC = "description";
    private static final String JSON_TEXTDESC = "textdescription";
    private static final String JSON_EVENT_LOCATION = "location";
    private static final String JSON_POSTEDBY = "postedby";
    private static final String JSON_POST_DATE = "posteddate";
    private static final String JSON_CONTACT = "contact";
    private static final String JSON_FILE = "file";
    private static final String JSON_STARTDATE="eventStartDate";
    private static final String JSON_ENDDATE="eventEndDate";
    private static final String JSON_STARTHOUR="";
    private static final String JSON_ENDHOUR="";
    private static final String JSON_EVENT_IMAGE="imageurl";
    private static final String JSON_ORGANISERS = "organisers";
    private static final String JSON_SHARETEXT="sharetext";
    private static final String JSON_SHAREURL="shareurl";
    private static final String JSON_SHAREABLE="shareable";



    public static EventDto parse(JSONObject eventObject)
            throws JSONException {

        EventDto eventDto = new EventDto();

        if (!TextUtils.isEmpty(eventObject.getString(JSON_EVENTID))){
            eventDto.setEventId(eventObject.getString(JSON_EVENTID));
        }

        if (!TextUtils.isEmpty(eventObject.getString(JSON_TITLE))){
            eventDto.setTitle(eventObject.getString(JSON_TITLE));
        }

        if (!TextUtils.isEmpty(eventObject.getString(JSON_DESC))){
            eventDto.setDescription(eventObject.getString(JSON_DESC));
        }

        if (!TextUtils.isEmpty(eventObject.getString(JSON_TEXTDESC))){
            eventDto.setTextDescription(eventObject.getString(JSON_TEXTDESC));
        }

        eventDto.setEventLocation(parseLocation(eventObject
                .getJSONObject(JSON_EVENT_LOCATION)));

        if (!TextUtils.isEmpty(eventObject.getString(JSON_STARTDATE))){
            eventDto.setEventStartDate(eventObject.getString(JSON_STARTDATE));
        }

        if (!TextUtils.isEmpty(eventObject.getString(JSON_ENDDATE))){
            eventDto.setEventEndDate(eventObject.getString(JSON_ENDDATE));
        }



        if (!TextUtils.isEmpty(eventObject.getString(JSON_EVENT_IMAGE))){
            eventDto.setEventImage(eventObject.getString(JSON_EVENT_IMAGE));
        }

        if(eventObject.has(JSON_ORGANISERS) && !eventObject.isNull(JSON_ORGANISERS)){
            eventDto.setOrganizerList(parseOrganizers(eventObject.optJSONArray(JSON_ORGANISERS)));
        }

        if(eventObject.has(JSON_SHARETEXT) && !eventObject.isNull(JSON_SHARETEXT)){
            eventDto.setShareText(eventObject.getString(JSON_SHARETEXT));
        }

        if(eventObject.has(JSON_SHAREURL) && !eventObject.isNull(JSON_SHAREURL)){
            eventDto.setShareUrl(eventObject.getString(JSON_SHAREURL));
        }

        if(eventObject.has(JSON_SHAREABLE) && !eventObject.isNull(JSON_SHAREABLE)){
            eventDto.setShareable(eventObject.getString(JSON_SHAREABLE));
        }

        if(eventObject.has("rsvpresponse") && !eventObject.isNull("rsvpresponse")){

            JSONObject rsvpobject = eventObject.getJSONObject("rsvpresponse");
            rsvpresponse rsvp = new rsvpresponse();
            if (rsvpobject.getString("response").equalsIgnoreCase("yes")){
                rsvp.setResponse(1);
            }else if (rsvpobject.getString("response").equalsIgnoreCase("no")){
                rsvp.setResponse(0);
            } else {
                rsvp.setResponse(2);
            }
            rsvp.setGuestcount(rsvpobject.getString(JSON_RSVP_GUEST));
            eventDto.setResponse(rsvp);

        }


        return eventDto;
    }
    public static final String JSON_LOC_VENUE = "venuename";
    public static final String JSON_LOC_LINE1 = "line1";
    public static final String JSON_LOC_LINE2 ="line2";
    public static final String JSON_LOC_NAME = "postlocationname";
    public static final String JSON_LOC_CITY = "postlocationcity";
    public static final String JSON_LOC_ZIP = "postlocationzipcode";
    public static final String JSON_LOC_STATE = "state";
    public static final String JSON_LOC_COUNTRY = "postlocationcountrycode";
    public static final String JSON_LOC_LAT = "postlocationlatitude";
    public static final String JSON_LOC_LONG = "postlocationlongitude";







    public static LocationDto parseLocation(JSONObject locObject)
            throws JSONException {

        LocationDto locDto = new LocationDto();
        locDto.setVenuename(locObject.getString(JSON_LOC_VENUE));
        locDto.setAddressline1(locObject.getString(JSON_LOC_LINE1));
        locDto.setAddressline2(locObject.getString(JSON_LOC_LINE2));
        locDto.setLocName(locObject.getString(JSON_LOC_NAME));
        locDto.setCity(locObject.getString(JSON_LOC_CITY));
        locDto.setZipCode(locObject.getString(JSON_LOC_ZIP));
        locDto.setState(locObject.getString(JSON_LOC_STATE));
        locDto.setCountry(locObject.getString(JSON_LOC_COUNTRY));
        locDto.setLatitude(locObject.getString(JSON_LOC_LAT));
        locDto.setLongitude(locObject.getString(JSON_LOC_LONG));

        return locDto;

    }

    public static final String JSON_ORGANIZERNAME = "organizername";

    public static List<Organizer> parseOrganizers(JSONArray orgArray)throws JSONException {

        List<Organizer> organizerList = new ArrayList<Organizer>();


        for (int i = 0; i < orgArray.length(); i++) {
            Organizer organizer = new Organizer();

            JSONObject organizerObject = orgArray.getJSONObject(i);
            if (!TextUtils.isEmpty(organizerObject.getString(JSON_ORGANIZERNAME))){
                organizer.setName(organizerObject.getString(JSON_ORGANIZERNAME));
                organizerList.add(organizer);
            }
        }
        return organizerList;
    }

    public static final String JSON_RSVP_CONTACTPHONE = "contactphone";
    public static final String JSON_RSVP_CONTACTNAME = "contactname";
    public static final String JSON_RSVP_CONTACTMAIL ="contactemail";
    public static final String JSON_RSVP_GUEST = "noofpeople";



    public static JSONObject createRSVPObject(String guestcount) {
        JSONObject serverObject = new JSONObject();
        try{

            serverObject.put(JSON_RSVP_CONTACTPHONE, "");
            serverObject.put(JSON_RSVP_CONTACTNAME, "");
            serverObject.put(JSON_RSVP_CONTACTMAIL, "");
            serverObject.put(JSON_RSVP_GUEST, guestcount);

        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return serverObject;
    }

}
