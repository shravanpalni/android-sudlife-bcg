package com.outwork.sudlife.bcg.opportunity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.outwork.sudlife.bcg.lead.model.CategoryModel;

/**
 * Created by bvlbh on 4/15/2018.
 */

public class ProductModel {


    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    @SerializedName("ProductName")
    @Expose
    private String ProductName;

    @SerializedName("ProductId")
    @Expose
    private String ProductId;

    @SerializedName("IsDeleted")
    @Expose
    private boolean IsDeleted;


    @SerializedName("ModifiedOn")
    @Expose
    private String ModifiedOn;


    public CategoryModel getCategory() {
        return Category;
    }

    public void setCategory(CategoryModel category) {
        Category = category;
    }
    @SerializedName("Category")
    @Expose
    private CategoryModel Category;


}
