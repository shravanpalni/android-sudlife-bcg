package com.outwork.sudlife.bcg.planner.activities;

import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.core.LocationProvider;
import com.outwork.sudlife.bcg.dto.Geocode;
import com.outwork.sudlife.bcg.dto.geocode.STATUS;
import com.outwork.sudlife.bcg.localbase.StaticDataMgr;
import com.outwork.sudlife.bcg.opportunity.OpportunityMgr;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.SupervisorMgr;
import com.outwork.sudlife.bcg.planner.adapter.MultipleItemsSelectionAdapter;
import com.outwork.sudlife.bcg.planner.adapter.MultipleItemsSelectionAdapterForPurpose;
import com.outwork.sudlife.bcg.planner.adapter.SupervisorListAdapter;
import com.outwork.sudlife.bcg.planner.models.DataModel;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.planner.service.PlannerIntentService;
import com.outwork.sudlife.bcg.restinterfaces.ReverseGeoInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OpportunityViewActivity extends BaseActivity implements LocationProvider.LocationCallback {

    private AppCompatSpinner activitytype, supervisor;
    private EditText follouupdate, followupaction, title, reschduledate, reason, checkintime, purpose, notes, plandate;
    private CheckBox checkBox;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private Button reschdule, checkin, confirm, checkout;
    private PlannerModel plannerModel;
    private ArrayAdapter<String> activityTypeAdapter;
    private SupervisorListAdapter supervisorListAdapter;
    private RadioGroup type;
    private RadioButton visit, activity;
    private MultipleItemsSelectionAdapter adapter;
    private List<DataModel> selectedDataList = new LinkedList<>();
    private LocationProvider mLocationProvider;
    private double latitude, longitude;
    private String addressline1, addressline2, locality, city, state, zip;
    private String addressline = "";
    private List<DataModel> selectedDataListPurpose = new LinkedList<>();
    private MultipleItemsSelectionAdapterForPurpose adapterPurpose;
    private TextView name, agegender, income;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                reschduledate.setVisibility(View.VISIBLE);
                reason.setVisibility(View.VISIBLE);
                reschduledate.setEnabled(true);
                reschduledate.setText(TimeUtils.getFormattedDate(date));

                findViewById(R.id.bottomView).setVisibility(View.GONE);
                checkout.setVisibility(View.GONE);
                findViewById(R.id.rescview).setVisibility(View.VISIBLE);
                confirm.setVisibility(View.VISIBLE);

            }
            if (customSelector == 1) {
                follouupdate.setText(TimeUtils.getFormattedDate(date));
                String followupdt = TimeUtils.getFormattedDate(date);
                follouupdate.setText(followupdt);
                follouupdate.setEnabled(true);
                plannerModel.setFollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(followupdt)));
            }
        }

        @Override
        public void onDateTimeCancel() {
            reschduledate.setEnabled(true);
            reschduledate.setVisibility(View.GONE);
            reason.setVisibility(View.GONE);
            follouupdate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunityviewplan);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("plannerModel")) {
            plannerModel = new Gson().fromJson(getIntent().getStringExtra("plannerModel"), PlannerModel.class);
        }
        mLocationProvider = new LocationProvider(OpportunityViewActivity.this, this);
        mLocationProvider.connect();

        initToolBar();
        initializeViews();
        populateData();
        getActivitytypes();
        setListener();
        followupaction.setFocusable(false);
        followupaction.setFocusableInTouchMode(false);
        purpose.setFocusable(false);
        purpose.setFocusableInTouchMode(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationProvider != null)
            mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mLocationProvider != null)
            mLocationProvider.disconnect();
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                getLocation(latitude, longitude);
        }
    }

    private void getSupervisors() {
        final List<SupervisorModel> supervisorModelList = SupervisorMgr.getInstance(OpportunityViewActivity.this).getsupervisorList(userid);
        if (supervisorModelList.size() > 0) {
            supervisorListAdapter = new SupervisorListAdapter(this, supervisorModelList);
            supervisor.setAdapter(supervisorListAdapter);
            supervisor.setSelection(0);
            supervisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    supervisor.setSelection(position);
                    plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                    plannerModel.setSupervisorname(supervisorModelList.get(position).getUsername());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void setListener() {
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.visit) {
                    plannerModel.setType("Visit");
                    plannerModel.setActivitytype("Visit");
                    findViewById(R.id.atype).setVisibility(View.GONE);
                    activitytype.setVisibility(View.GONE);
                } else {
                    plannerModel.setActivitytype("Activity");
                    plannerModel.setType("Activity");
                    findViewById(R.id.atype).setVisibility(View.VISIBLE);
                    activitytype.setVisibility(View.VISIBLE);
                }
            }
        });
        follouupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                follouupdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        reschduledate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reschduledate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        reschdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reschduledate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postRescheduleData();
            }
        });
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCheckinData();
            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCheckOutData();
            }
        });
        followupaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                List<String> strinsArrayList = StaticDataMgr.getInstance(OpportunityViewActivity.this).getListNames(groupId, "followupaction");
                List<DataModel> actionDataModelArraylist = new LinkedList<>();
                for (int i = 0; i < strinsArrayList.size(); i++) {
                    DataModel dataModel = new DataModel();
                    dataModel.setName(strinsArrayList.get(i));
                    dataModel.setEnabledValue(false);
                    actionDataModelArraylist.add(dataModel);
                }

                if (selectedDataList.size() != 0) {
                    List<DataModel> selectedItemList = new ArrayList<>();
                    selectedItemList = adapter.getSelectedItems();
                    multiSelectSpinnerDialogBox(selectedItemList, followupaction);
                } else {
                    multiSelectSpinnerDialogBox(actionDataModelArraylist, followupaction);
                }
            }
        });
        purpose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                List<String> strinsArrayList = StaticDataMgr.getInstance(OpportunityViewActivity.this).getListNames(groupId, "purpose");
                List<DataModel> purposeDataModelArraylist = new LinkedList<>();
                for (int i = 0; i < strinsArrayList.size(); i++) {
                    DataModel dataModel = new DataModel();
                    dataModel.setName(strinsArrayList.get(i));
                    dataModel.setEnabledValue(false);
                    purposeDataModelArraylist.add(dataModel);
                }
                if (selectedDataListPurpose.size() != 0) {
                    List<DataModel> selectedItemList = new ArrayList<>();
                    selectedItemList = adapterPurpose.getSelectedItems();
                    multiSelectSpinnerDialogBoxforPusrPose(selectedItemList, purpose);
                } else {
                    multiSelectSpinnerDialogBoxforPusrPose(purposeDataModelArraylist, purpose);
                }

            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.VISIBLE);
                    getSupervisors();
                } else {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.GONE);
                    supervisor.setVisibility(View.GONE);
                }
            }
        });
        reason.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.vreason) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        notes.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void postCheckinData() {
        if (checkBox.isChecked()) {
            plannerModel.setJointvisit(checkBox.isChecked());
        }
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(1);
        plannerModel.setCheckintime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (Utils.isNotNullAndNotEmpty(addressline))
            plannerModel.setCheckinlocation(addressline);
        plannerModel.setScheduletime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        PlannerMgr.getInstance(OpportunityViewActivity.this).updateCheckInPlanner(plannerModel);
        PlannerIntentService.syncPlanstoServer(OpportunityViewActivity.this);
        showAlert("", "Your Plan is updated successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);

    }

    private void postCheckOutData() {
        if (checkBox.isChecked()) {
            plannerModel.setJointvisit(checkBox.isChecked());
        }
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(2);
        plannerModel.setCheckouttime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (Utils.isNotNullAndNotEmpty(addressline))
            plannerModel.setCheckoutlocation(addressline);
        plannerModel.setPurpose(purpose.getText().toString());
        if (Utils.isNotNullAndNotEmpty(follouupdate.getText().toString()))
            plannerModel.setFollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(follouupdate.getText().toString())));
        plannerModel.setFollowupaction(followupaction.getText().toString());
        plannerModel.setNotes(notes.getText().toString());
        PlannerMgr.getInstance(OpportunityViewActivity.this).updateCheckoutPlanner(plannerModel);
        PlannerIntentService.syncPlanstoServer(OpportunityViewActivity.this);
        showAlert("", "Your Plan is updated successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }

    private void postRescheduleData() {
        if (checkBox.isChecked()) {
            plannerModel.setJointvisit(checkBox.isChecked());
        }
        if (Utils.isNotNullAndNotEmpty(reschduledate.getText().toString())) {
            plannerModel.setReschduledate(Integer.parseInt(TimeUtils.convertDatetoUnix(reschduledate.getText().toString())));
        }
        if (Utils.isNotNullAndNotEmpty(reason.getText().toString())) {
            plannerModel.setReshdreason(reason.getText().toString());
        }
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(3);
        PlannerMgr.getInstance(OpportunityViewActivity.this).updateReschedulePlanner(plannerModel);
        //creating new row
        plannerModel.setActivityid("");
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", reschduledate.getText().toString())));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", reschduledate.getText().toString())));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", reschduledate.getText().toString())));
        plannerModel.setScheduletime(Integer.parseInt(TimeUtils.convertDatetoUnix(reschduledate.getText().toString())));
        plannerModel.setCompletestatus(0);
        PlannerMgr.getInstance(OpportunityViewActivity.this).insertPlanner(plannerModel, userid, groupId);
        PlannerIntentService.syncPlanstoServer(OpportunityViewActivity.this);
        showAlert("", "Your Plan has been Rescheduled",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void multiSelectSpinnerDialogBox(final List<DataModel> dataModelList, final EditText editText) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.multi_spinner_for_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_items);
        adapter = new MultipleItemsSelectionAdapter(this, dataModelList);
        lvItems.setAdapter(adapter);
        deleteDialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                selectedDataList = adapter.getSelectedItems();
                StringBuilder str = new StringBuilder();
                if (selectedDataList.size() != 0) {
                    for (int i = 0; i < selectedDataList.size(); i++) {
                        if (selectedDataList.get(i).isEnabledValue()) {
                            if (Utils.isNotNullAndNotEmpty(str.toString())) {
                                str.append(", " + selectedDataList.get(i).getName());
                            } else {
                                str.append(selectedDataList.get(i).getName());
                            }
                        }
                    }
                    editText.setError(null);
                    deleteDialog.dismiss();
                    editText.setText(str);
                }
            }
        });
        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }


    public void multiSelectSpinnerDialogBoxforPusrPose(final List<DataModel> dataModelList, final EditText editText) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.multi_spinner_for_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_items);
        adapterPurpose = new MultipleItemsSelectionAdapterForPurpose(this, dataModelList);
        lvItems.setAdapter(adapterPurpose);
        deleteDialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                selectedDataListPurpose = adapterPurpose.getSelectedItems();
                StringBuilder str = new StringBuilder();
                if (selectedDataListPurpose.size() != 0) {
                    for (int i = 0; i < selectedDataListPurpose.size(); i++) {
                        if (selectedDataListPurpose.get(i).isEnabledValue()) {
                            if (Utils.isNotNullAndNotEmpty(str.toString())) {
                                str.append(", " + selectedDataListPurpose.get(i).getName());
                            } else {
                                str.append(selectedDataListPurpose.get(i).getName());
                            }
                        }
                    }
                    deleteDialog.dismiss();
                    editText.setText(str);
                }
            }
        });


        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void initializeViews() {
        title = (EditText) findViewById(R.id.title);
        purpose = (EditText) findViewById(R.id.purpose);
        notes = (EditText) findViewById(R.id.notes);
        follouupdate = (EditText) findViewById(R.id.ffollowupdate);
        followupaction = (EditText) findViewById(R.id.ffollowupaction);
        reschduledate = (EditText) findViewById(R.id.vreschddate);
        reason = (EditText) findViewById(R.id.vreason);
        supervisor = (AppCompatSpinner) findViewById(R.id.supervisor);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkintime = (EditText) findViewById(R.id.checkedindate);
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
        reschdule = (Button) findViewById(R.id.reschdule);
        checkin = (Button) findViewById(R.id.checkin);
        checkout = (Button) findViewById(R.id.checkout);
        confirm = (Button) findViewById(R.id.confirm);

        name = (TextView) findViewById(R.id.uName);
        agegender = (TextView) findViewById(R.id.agengender);
        income = (TextView) findViewById(R.id.incometext);

        plandate = (EditText) findViewById(R.id.plandate);
        activitytype = (AppCompatSpinner) findViewById(R.id.activitytype);
    }

    private void populateData() {
        if (Utils.isNotNullAndNotEmpty(plannerModel.getOpportunityid())) {
            OpportunityModel opportunityModel = OpportunityMgr.getInstance(OpportunityViewActivity.this).getOpportunity(plannerModel.getOpportunityid(), userid);
            showuserinfo(opportunityModel);
        } else {
            OpportunityModel opportunityModel = OpportunityMgr.getInstance(OpportunityViewActivity.this).getOfflineOpportunityByID(plannerModel.getId(), userid);
            showuserinfo(opportunityModel);
        }

        if (plannerModel != null)
            if (plannerModel.getCompletestatus() != null)
                if (plannerModel.getCompletestatus() == 0) {
                    findViewById(R.id.rescview).setVisibility(View.GONE);
                    findViewById(R.id.compltview).setVisibility(View.GONE);
                    findViewById(R.id.bottomView).setVisibility(View.VISIBLE);
                    checkout.setVisibility(View.GONE);
                } else if (plannerModel.getCompletestatus() == 1) {  //after checkin
                    findViewById(R.id.rescview).setVisibility(View.GONE);
                    findViewById(R.id.compltview).setVisibility(View.VISIBLE);
                    findViewById(R.id.bottomView).setVisibility(View.GONE);
                    checkout.setVisibility(View.VISIBLE);
                    if (plannerModel.getFollowupdate() != null)
                        if (plannerModel.getFollowupdate() == 0) {
                        } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getFollowupdate()))) {
                            follouupdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getFollowupdate()), "dd/MM/yyyy hh:mm a"));
                        }
                    if (plannerModel.getCheckintime() != null)
                        if (plannerModel.getCheckintime() == 0) {
                        } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getCheckintime()))) {
                            checkintime.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getCheckintime()), "dd/MM/yyyy hh:mm a"));
                            checkintime.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
                        }
                }
        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFirstname())) {
            stringBuilder.append(plannerModel.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getLastname())) {
            stringBuilder.append(plannerModel.getLastname());
        }
        title.setText(stringBuilder.toString());
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                checkBox.setChecked(plannerModel.getJointvisit());
                findViewById(R.id.suprvisorlbl).setVisibility(View.VISIBLE);
                supervisor.setVisibility(View.VISIBLE);
                getSupervisors();
            }
        } else {
            findViewById(R.id.suprvisorlbl).setVisibility(View.GONE);
            supervisor.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (plannerModel.getActivitytype().equalsIgnoreCase("Visit")) {
                visit.setChecked(true);
            } else {
                activity.setChecked(true);
                findViewById(R.id.atype).setVisibility(View.VISIBLE);
                activitytype.setVisibility(View.VISIBLE);
            }
        }
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() == 0) {
                plandate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getScheduletime()))) {
                plandate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy hh:mm a"));
            }
        } else {
            plandate.setText(getResources().getString(R.string.notAvailable));
        }
    }

    private void showuserinfo(OpportunityModel opportunityModel) {
        StringBuilder stringBuilder1 = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getFirstname())) {
            stringBuilder1.append(opportunityModel.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getLastname())) {
            stringBuilder1.append(" ");
            stringBuilder1.append(opportunityModel.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(stringBuilder1.toString())) {
            name.setText(stringBuilder1.toString());
        } else {
            name.setVisibility(View.GONE);
        }

        StringBuilder age = new StringBuilder();
        if (opportunityModel.getAge() != null) {
            age.append(opportunityModel.getAge());
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getGender())) {
            age.append("/");
            age.append(opportunityModel.getGender());
        }
        if (Utils.isNotNullAndNotEmpty(age.toString())) {
            agegender.setText(age.toString());
        } else {
            agegender.setVisibility(View.GONE);
        }
        if (opportunityModel.getIncome() != null) {
            income.setText(String.valueOf(opportunityModel.getIncome()));
        } else {
            income.setVisibility(View.GONE);
        }
    }

    private void getActivitytypes() {
        List<String> activitytypeList = StaticDataMgr.getInstance(OpportunityViewActivity.this).getListNames(groupId, "subtype");
        if (activitytypeList.size() > 0) {
            activityTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, activitytypeList);
            activityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activitytype.setAdapter(activityTypeAdapter);
            for (int i = 0; i < activitytypeList.size(); i++)
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
                    if (plannerModel.getSubtype().equalsIgnoreCase(activitytypeList.get(i).toString())) {
                        activitytype.setSelection(i);
                    }
            activitytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activitytype.setSelection(position);
                    plannerModel.setSubtype(activitytype.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            findViewById(R.id.atype).setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            textView.setText(plannerModel.getCustomername());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        }
    }

    private void getLocation(double lat, double lng) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ReverseGeoInterface.MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String latlong = lat + "," + lng;
        ReverseGeoInterface service = retrofit.create(ReverseGeoInterface.class);
        Call<Geocode> locationdetails = service.getLocation("GEOMETRIC_CENTER", latlong, "AIzaSyB6QdommPXh8xlvGJL9IkauaajCpKfNWpc");

        locationdetails.enqueue(new Callback<Geocode>() {
            @Override
            public void onResponse(Call<Geocode> call, Response<Geocode> response) {
                Geocode geocode = response.body();
                if (geocode != null)
                    if (geocode.getResults() != null)
                        if (geocode.getStatus() == STATUS.OK) {
                            if (geocode.getResults().size() > 0) {
                                if (geocode.getResults().get(0).getAddressComponents() != null) {
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(0).getLongName()))
                                        addressline1 = geocode.getResults().get(0).getAddressComponents().get(0).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(1).getLongName()))
                                        addressline2 = geocode.getResults().get(0).getAddressComponents().get(1).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(2).getLongName()))
                                        locality = geocode.getResults().get(0).getAddressComponents().get(2).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(3).getLongName()))
                                        city = geocode.getResults().get(0).getAddressComponents().get(3).getLongName();
                                    addressline = addressline1 + " " + addressline2 + " " + locality + " " + city;
                                }
                            }
                        }
            }

            @Override
            public void onFailure(Call<Geocode> call, Throwable t) {
            }
        });
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        dismissProgressDialog();
//        switch (requestCode) {
//            case ACCESS_FINE_LOCATION_INTENT_ID: {
//                if (grantResults != null) {
//                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                        dispatchlocationIntent();
//                    } else {
//                        showToast(getResources().getString(R.string.locationtoast));
//                    }
//                } else {
//                    showToast(getResources().getString(R.string.locationtoast));
//                }
//                return;
//            }
//        }
//    }
}
