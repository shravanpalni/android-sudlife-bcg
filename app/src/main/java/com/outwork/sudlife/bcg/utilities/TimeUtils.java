package com.outwork.sudlife.bcg.utilities;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Panch on 11/29/2016.
 */

public class TimeUtils {

    public static String getHoursMinutes12(Date date) {
        String formattedDate = "";
        formattedDate = new SimpleDateFormat("hh:mm a").format(date);
        return formattedDate;
    }

    public static String getDayMonthYear(Date date) {
        String formattedDate = "";
        formattedDate = new SimpleDateFormat("EEEE, MMMM d, yyyy").format(date);
        return formattedDate;
    }

    public static String getFormattedDatefromUnix(String timestamp, boolean year, int whatever) {
        String returnvalue = "";
        try {
            Long timestamp1 = Long.parseLong(timestamp) * 1000;
        } catch (Exception e) {
            return returnvalue;
        }
        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd HH:mm");
//        sf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date(timestamp1);
        //sf.format(date)


        returnvalue = Utils.getFormattedDate(sf.format(date), true, false);

        return returnvalue;
    }

    public static String convertDatetoUnix(String inputDt) {
        String unixTimeStamp = inputDt;
        try {
            DateFormat formatter = new SimpleDateFormat("EEEE, MMMM d, yyyy hh:mm a");
//            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = (Date) formatter.parse(inputDt);
            long output = date.getTime() / 1000L;
            return Long.toString(output);
        } catch (Exception e) {
            try {

                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
//                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date date = (Date) formatter.parse(inputDt);
                long output = date.getTime() / 1000L;
                return Long.toString(output);

            } catch (Exception e1) {
                try {

                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date date = (Date) formatter.parse(inputDt);
                    long output = date.getTime() / 1000L;
                    return Long.toString(output);

                } catch (Exception e2) {
                    return unixTimeStamp;
                }
            }

        }
    }

    public static String getCurrentDate() {

        String currentDate = "";
        Date df = new Date();
        currentDate = new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(df);
        return currentDate;
    }

    public static String getDateFormatted(Date date, String dateformat) {
        String formattedDate = "";
        Date df = new Date();
        SimpleDateFormat sf = new SimpleDateFormat(dateformat);
//        sf.setTimeZone(TimeZone.getTimeZone("GMT"));
        formattedDate = sf.format(date);
        return formattedDate;
    }

    public static String getCurrentDate(String format) {
        String currentDate = "";
        Date df = new Date();
        SimpleDateFormat sf = new SimpleDateFormat(format);
//        sf.setTimeZone(TimeZone.getTimeZone("GMT"));
        currentDate = sf.format(df);
        return currentDate;
    }

    public static String getFormattedDate(Date df) {
        String formattedDate = "";
        formattedDate = new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(df);
        return formattedDate;
    }

    public static String getDateFormat(Date df) {
        String formattedDate = "";
        formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(df);
        return formattedDate;
    }

    public static String getCurrentUnixTimeStamp() {
        String currentDate = "";
        Date df = new Date();
        // currentDate = new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(df);
        return Long.toString(df.getTime() / 1000L);
    }

    public static long getTimeMillis(String dateString, String dateFormat) throws ParseException {
        /*Use date format as according to your need! Ex. - yyyy/MM/dd HH:mm:ss */
        String myDate = dateString;//"2017/12/20 18:10:45";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat/*"yyyy/MM/dd HH:mm:ss"*/);
        Date date = sdf.parse(myDate);
        long millis = date.getTime();

        return millis;
    }

    public static String getFormattedDatefromUnix(String timestamp, String format) {
        String returnvalue = "";
        try {
            Long timestamp1 = Long.parseLong(timestamp) * 1000;
        } catch (Exception e) {
            return returnvalue;
        }
        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat(format);
//        sf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date(timestamp1);
        //sf.format(date)
        returnvalue = sf.format(date);
        return returnvalue;
    }

    public static String getFormattedDatefromUnix(String timestamp, String format, boolean isToday) {
        String returnvalue = "";
        try {
            Long timestamp1 = Long.parseLong(timestamp) * 1000;
        } catch (Exception e) {
            return returnvalue;
        }
        Long timestamp1 = Long.parseLong(timestamp) * 1000;
        SimpleDateFormat sf = new SimpleDateFormat(format);
//        sf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date(timestamp1);
        //sf.format(date)
        if (DateUtils.isToday(timestamp1)) {
            returnvalue = "Today";
        } else {
            returnvalue = sf.format(date);
        }
        return returnvalue;
    }
}
