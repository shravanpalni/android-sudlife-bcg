package com.outwork.sudlife.bcg.planner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class SupervisorDao {
    public static final String TAG = SupervisorDao.class.getSimpleName();

    private static SupervisorDao instance;
    private Context applicationContext;

    public static SupervisorDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new SupervisorDao(applicationContext);
        }
        return instance;
    }

    private SupervisorDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void insertSupevisor(SQLiteDatabase db, SupervisorModel supervisorModel, String userId, String groupid) {
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Superior.userid, userId);
        values.put(IvokoProvider.Superior.groupid, groupid);
        if (Utils.isNotNullAndNotEmpty(supervisorModel.getUsername()))
            values.put(IvokoProvider.Superior.supervisorname, supervisorModel.getUsername().trim());
        if (Utils.isNotNullAndNotEmpty(supervisorModel.getSupervisorid()))
            values.put(IvokoProvider.Superior.supervisorid, supervisorModel.getSupervisorid());
        if (Utils.isNotNullAndNotEmpty(supervisorModel.getEmployeecode()))
            values.put(IvokoProvider.Superior.employeecode, supervisorModel.getEmployeecode());
        try {
            db.insertOrThrow(IvokoProvider.Superior.TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertSupevisorList(List<SupervisorModel> supervisorModelList, String userId, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Superior.TABLE))
            deleteSupervisorList(db, userId);
        for (int j = 0; j < supervisorModelList.size(); j++) {
            SupervisorModel supervisorModel = supervisorModelList.get(j);
            insertSupevisor(db, supervisorModel, userId, groupid);
        }
    }

    public List<SupervisorModel> getsupervisorList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<SupervisorModel> supervisorModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Superior.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Superior.TABLE;
            sql += " WHERE " + IvokoProvider.Superior.userid + " = '" + userId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Superior.TABLE + " WHERE " + IvokoProvider.Superior.userid + " = ?" , new String[]{userId});

                if (cursor.moveToFirst()) {
                    do {
                        SupervisorModel supervisorModel = new SupervisorModel();
                        supervisorModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Superior._ID)));
                        supervisorModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Superior.userid)));
                        supervisorModel.setSupervisorid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Superior.supervisorid)));
                        supervisorModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Superior.groupid)));
                        supervisorModel.setUsername(cursor.getString(cursor.getColumnIndex(IvokoProvider.Superior.supervisorname)));
                        supervisorModel.setEmployeecode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Superior.employeecode)));
                        supervisorModelList.add(supervisorModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return supervisorModelList;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private void deleteSupervisorList(SQLiteDatabase db, String userid) {
        db.delete(IvokoProvider.Superior.TABLE, IvokoProvider.Superior.userid + "=?", new String[]{userid});
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Superior.TABLE + " WHERE " + IvokoProvider.Superior.userid + " = ?", new String[]{userid});
        if (cursor.getCount() <= 0) {
            cursor.close();
        }
        cursor.close();
    }
}