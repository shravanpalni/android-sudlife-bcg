package com.outwork.sudlife.bcg.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Comment implements Serializable {

    @SerializedName("id")
    @Expose
    private String subjectid;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("date")
    @Expose(serialize = false, deserialize = true)
    private String commentdate;

    @SerializedName("name")
    @Expose(serialize = false, deserialize = true)
    private String commentedby;

    @SerializedName("createdby")
    @Expose(serialize = false, deserialize = true)
    private String createdby;

    public Comment() {
    }
    public Comment(String description, String commentdate, String createdby) {
        this.description = description;
        this.commentdate = commentdate;
        this.createdby = createdby;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(String subjectid) {
        this.subjectid = subjectid;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCommentdate() {
        return commentdate;
    }

    public void setCommentdate(String commentdate) {
        this.commentdate = commentdate;
    }

    public String getCommentedby() {
        return commentedby;
    }

    public void setCommentedby(String commentedby) {
        this.commentedby = commentedby;
    }

}
