package com.outwork.sudlife.bcg.planner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.activities.ShowHierarchyActivity;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.adapter.UserTeamNamesAdapter;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.models.UserTeams;
import com.outwork.sudlife.bcg.utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shravanch on 25-04-2019.
 */

public class UserTeamsActivity extends BaseActivity {

    private int planststatus;
    private String plandate;
    private ListView custListView;
    List<UserTeams> userTeamsList = new ArrayList<>();
    private UserTeamNamesAdapter userTeamNamesAdapter;
    private TextView noinfo, toolbar_title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);

        userTeamsList = PlannerMgr.getInstance(UserTeamsActivity.this).getUserTeamList(userid);
        Log.i("shravan","userTeamsList calendar size = = = "+userTeamsList.size());

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
            planststatus = getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
            plandate = getIntent().getStringExtra(Constants.PLAN_DATE);
        }

        initToolbar();
        custListView = (ListView) findViewById(R.id.memberList);


        if (userTeamsList.size() > 0) {

            userTeamNamesAdapter = new UserTeamNamesAdapter(this, userTeamsList, groupId, userid);
            custListView.setAdapter(userTeamNamesAdapter);

        }

        custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                UserTeams dto = (UserTeams) parent.getItemAtPosition(position);
                //Intent intent = new Intent(UserTeamsActivity.this, TeamHierarchyActivity.class);
                Intent intent = new Intent(UserTeamsActivity.this, ShowHierarchyActivity.class);
                intent.putExtra(Constants.PLAN_STATUS, 0);
                intent.putExtra(Constants.PLAN_DATE, plandate);
                startActivity(intent);
                finish();



            }
        });




    }



    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mListToolBar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Select Team");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }



}
