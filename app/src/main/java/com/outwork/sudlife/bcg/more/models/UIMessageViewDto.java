package com.outwork.sudlife.bcg.more.models;

import android.text.TextUtils;

import com.outwork.sudlife.bcg.IvokoApplication;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Panch on 3/7/2016.
 */
public class UIMessageViewDto extends CommonDto {

    private String postedDate;
    private String userid;
    private String userName;
    private String userEmail;
    private String title;
    private String description;
    private String imageUrl;
    private boolean isCommentEnabled;
    private int commentCount;
    private boolean isShareEnabled;

    private String MessageType;//maintained for backward compatibility. This is same as relatedObjecTType;
    private String MessageSubType;//Used to denote HTML/email or Text



//*** Display Data
    public static final String JSON_POST_DATE = "createddate";
    public static final String JSON_USERID = "userid";
    public static final String JSON_POSTED_USERNAME ="username";
    public static final String JSON_POSTED_USEREMAIL ="email";
    public static final String JSON_TITLE = "subject";
    public static final String JSON_DESC = "message";
    public static final String JSON_IMAGEURL ="imageurl";
    public static final String JSON_COMMENTENBALED = "commentflag";
    public static final String JSON_COMMENTCOUNT = "commentcount";
    public static final String JSON_SHAREENABLED ="shareable";

    //** Process Data
    public static final String JSON_POSTEDBY = "postedby";
    public static final String JSON_SHARE="share";
    public static final String JSON_OBJECTID = "relatedObjectID";
    public static final String JSON_OBJECTTYPE = "relatedObjectType";

    public static final String JSON_MSGSUBTYPE = "subtype";
    public static final String JSON_PROPERTIES ="prop";
    public static final String JSON_PROPERTIESTYPE="type";


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getUserName(){
        return userName;
    }

    public void setUserName(String userName){
        this.userName =userName;
    }

    public String getUserEmail(){
        return userEmail;
    }

    public void setUserEmail(String userEmail){
        this.userEmail =userEmail;
    }

    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String messageType) {
        MessageType = messageType;
    }

    public String getMessageSubType() {
        return MessageSubType;
    }

    public void setMessageSubType(String messageSubType) {
        MessageSubType = messageSubType;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isCommentEnabled() {
        return isCommentEnabled;
    }

    public void setCommentEnabled(boolean commentEnabled) {
        isCommentEnabled = commentEnabled;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public boolean isShareEnabled() {
        return isShareEnabled;
    }

    public void setShareEnabled(boolean shareEnabled) {
        isShareEnabled = shareEnabled;
    }



    public static UIMessageViewDto parse(JSONObject mailObject)throws JSONException {

        UIMessageViewDto uiMessageViewDto = new UIMessageViewDto();
        //Identify what type of object it is for display purpose

        uiMessageViewDto.setIvokoType(IvokoApplication.POST_TYPE.EMAIL);

        //Follows the sequence of definition
        uiMessageViewDto.setPostedDate(mailObject.getString(JSON_POST_DATE));
        if (mailObject.has(JSON_POSTEDBY) && !mailObject.isNull(JSON_POSTEDBY)) {
            JSONObject postedBy = mailObject.getJSONObject(JSON_POSTEDBY);

            if (postedBy.has(JSON_USERID) && !postedBy.isNull(JSON_USERID)) {
                uiMessageViewDto.setUserid(postedBy.getString(JSON_USERID));
            }

            if (postedBy.has(JSON_POSTED_USEREMAIL) && !postedBy.isNull(JSON_POSTED_USEREMAIL)) {
                uiMessageViewDto.setUserEmail(postedBy.getString(JSON_POSTED_USEREMAIL));
            }

            if (postedBy.has(JSON_POSTED_USERNAME) && !postedBy.isNull(JSON_POSTED_USERNAME)) {
                uiMessageViewDto.setUserName(postedBy.getString(JSON_POSTED_USERNAME));
            } else {
                if (!TextUtils.isEmpty(uiMessageViewDto.getUserEmail())) {
                    uiMessageViewDto.setUserName(uiMessageViewDto.getUserEmail());
                }
            }
        }
        uiMessageViewDto.setTitle(mailObject.getString(JSON_TITLE));
        if (!TextUtils.isEmpty(mailObject.getString(JSON_DESC))) {
            uiMessageViewDto.setDescription(mailObject.getString(JSON_DESC));
        }
        if (!TextUtils.isEmpty(mailObject.getString(JSON_IMAGEURL))) {
            uiMessageViewDto.setImageUrl(mailObject.getString(JSON_IMAGEURL));
        }

        if (!TextUtils.isEmpty(mailObject.getString(JSON_COMMENTENBALED)) &&
                mailObject.has(JSON_COMMENTENBALED)){
            if (mailObject.getString(JSON_COMMENTENBALED).equalsIgnoreCase("1")){
                uiMessageViewDto.setCommentEnabled(true);
            } else {
                uiMessageViewDto.setCommentEnabled(false);
            }

        } else {
            uiMessageViewDto.setCommentEnabled(false);
        }

        if (mailObject.has(JSON_COMMENTCOUNT) &&!TextUtils.isEmpty(mailObject.getString(JSON_COMMENTCOUNT))
                &&!(mailObject.getString(JSON_COMMENTCOUNT)).equalsIgnoreCase("null")) {

            uiMessageViewDto.setCommentCount(Integer.parseInt(mailObject.getString(JSON_COMMENTCOUNT)));
        } else {
                uiMessageViewDto.setCommentCount(0);
            }


        if ( mailObject.has(JSON_SHARE) && !mailObject.isNull(JSON_SHARE)){
            JSONObject share = mailObject.getJSONObject(JSON_SHARE);

            if (!TextUtils.isEmpty(share.getString(JSON_SHAREENABLED)) &&
                    share.has(JSON_SHAREENABLED)){
                if (share.getString(JSON_SHAREENABLED).equalsIgnoreCase("1")){
                    uiMessageViewDto.setShareEnabled(true);
                } else {
                    uiMessageViewDto.setShareEnabled(false);
                }

            } else {
                uiMessageViewDto.setShareEnabled(false);
            }
        }


        if (!TextUtils.isEmpty(mailObject.getString(JSON_OBJECTID))) {
            uiMessageViewDto.setObjectId(mailObject.getString(JSON_OBJECTID));
        }
        if (!TextUtils.isEmpty(mailObject.getString(JSON_OBJECTTYPE))) {
            uiMessageViewDto.setObjectType(mailObject.getString(JSON_OBJECTTYPE));
            uiMessageViewDto.setMessageType(mailObject.getString(JSON_OBJECTTYPE));

            if (uiMessageViewDto.getMessageType().equalsIgnoreCase("poll")){
                uiMessageViewDto.setIvokoType(IvokoApplication.POST_TYPE.POLL);
            }

            if (uiMessageViewDto.getMessageType().equalsIgnoreCase("event")){
                uiMessageViewDto.setIvokoType(IvokoApplication.POST_TYPE.EVENT);
            }

        }

        if (mailObject.has(JSON_PROPERTIES) && !mailObject.isNull(JSON_PROPERTIES)) {
            JSONObject properties = mailObject.getJSONObject(JSON_PROPERTIES);
                if (properties.has(JSON_PROPERTIESTYPE) && !properties.isNull(JSON_PROPERTIESTYPE)) {
                    uiMessageViewDto.setMessageSubType(properties.getString(JSON_PROPERTIESTYPE));
                } else {
                        uiMessageViewDto.setMessageSubType("");
                }
        }



        return uiMessageViewDto;
    }





}