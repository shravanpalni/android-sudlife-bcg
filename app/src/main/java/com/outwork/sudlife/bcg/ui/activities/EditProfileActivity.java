package com.outwork.sudlife.bcg.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.restinterfaces.ProfileService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.models.ProfileModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    public static final String TAG = EditProfileActivity.class.getSimpleName();

    private EditText ufname, ulname, userEmail, userLocation, userOrg, userDesignation, userPhoneNo, fburl, twitterurl, lnurl, website;
    private TextView cancel, submit;
    private ProfileModel profileModel = new ProfileModel();
    //private ImageView profileImage;
   // private Cloudinary cloudinary;
    //private JSONObject jsonObject1;
    //private String thumburl, baseurl, url1, filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        // Cloudinary
       /* HashMap config = new HashMap();
        config.put("cloud_name", "dd6qjvp5s");
        config.put("api_key", "179961222514518");
        config.put("api_secret", "_TAAjhiGIzCLO92hM1JkpdNVWc8");
        cloudinary = new Cloudinary(config);*/

        initializeViews();
        initializeValues();
        setListeners();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, cancel, submit);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, ufname, ulname, userEmail, userLocation, userPhoneNo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews() {
        ufname = (EditText) findViewById(R.id.uName);
        ulname = (EditText) findViewById(R.id.ulastname);
        userEmail = (EditText) findViewById(R.id.uEmail);
        userPhoneNo = (EditText) findViewById(R.id.uPhone);
        userLocation = (EditText) findViewById(R.id.uLocation);
        cancel = (TextView) findViewById(R.id.cancel);
        submit = (TextView) findViewById(R.id.submit);
        //profileImage = (ImageView) findViewById(R.id.adduserimage);
        userEmail.setEnabled(false);
    }

    private void initializeValues() {
        if (!TextUtils.isEmpty(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, ""))) {
            ufname.setText(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, ""));
        }
        if (!TextUtils.isEmpty(SharedPreferenceManager.getInstance().getString(Constants.EMAIL, ""))) {
            userEmail.setText(SharedPreferenceManager.getInstance().getString(Constants.EMAIL, ""));
        }
        if (!TextUtils.isEmpty(SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, ""))) {
            ulname.setText(SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, ""));
        }
        if (!TextUtils.isEmpty(SharedPreferenceManager.getInstance().getString(Constants.PHONENO, ""))) {
            userPhoneNo.setText(SharedPreferenceManager.getInstance().getString(Constants.PHONENO, ""));
        }
        if (!TextUtils.isEmpty(SharedPreferenceManager.getInstance().getString(Constants.CITY, ""))) {
            userLocation.setText(SharedPreferenceManager.getInstance().getString(Constants.CITY, ""));
        }
       /* if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))) {
            Glide.with(EditProfileActivity.this).load(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))
                    .transform(new CircleTransform(EditProfileActivity.this)).placeholder(R.drawable.addusericonn)
                    .crossFade()
                    .into(profileImage);
        }*/
    }

    private void setListeners() {
       /* profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNotNullAndNotEmpty(ufname.getText().toString())) {
                    ufname.setError("FirstName Required");
                    ufname.requestFocus();
                } else {
                    //SharedPreferenceManager.getInstance().putString(Constants.PROFILE_URL, thumburl);
                    profileModel.setFirstname(ufname.getText().toString());
                    profileModel.setLastname(ulname.getText().toString());
                    profileModel.setEmail(userEmail.getText().toString());
                    profileModel.setCity(userLocation.getText().toString());
                    profileModel.setPhonenumber(userPhoneNo.getText().toString());
                    profileModel.setEmailvisibility("1");
                    profileModel.setNumbervisibility("1");
                    profileModel.setLocationvisibility("1");
                   // profileModel.setUrl1(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""));
                    if (isNetworkAvailable()) {
                        submitProfile(profileModel);
                    } else {
                        showToast("Please check your internet connection");
                    }
                }
            }
        });
    }

    private void submitProfile(final ProfileModel profileModel) {
        ProfileService client = RestService.createServicev1(ProfileService.class);
        Call<RestResponse> updateprofile = client.editUserProfile(userToken, profileModel);
        updateprofile.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                    hideKeyboard();
                    SharedPreferenceManager.getInstance().storeProfileData(profileModel);
                    showAlert("", "Your profile is saved", "ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }, "", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }, false);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Toast.makeText(EditProfileActivity.this, "Profile not updated. Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    /*public void pickImage() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Permission not granted, granting now...");
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_PICK_PHOTO);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, 200);
        }
    }*/

   /* @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dismissProgressDialog();
        switch (requestCode) {
            case REQUEST_PERMISSION_PICK_PHOTO: {
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        pickImage();
                    } else {
                        showToast(getResources().getString(R.string.imagestoast));
                    }
                } else {
                    showToast(getResources().getString(R.string.imagestoast));
                }
                return;
            }
        }
    }*/

  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            createImageFile();
            profileImage.setImageURI(data.getData());
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(data.getData(), filePathColumn, null, null, null);
            if (cursor != null)
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    filePath = cursor.getString(dataColumnIndex);
                    startUpload(filePath);
                }
            if (cursor != null) {
                cursor.close();
            }
        }
    }*/

   /* private void startUpload(final String file) {
        AsyncTask<String, String, String> task = new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                showProgressDialog("Uploading Image......");
            }

            protected String doInBackground(String... profImg) {
                Log.d(TAG, "Running upload task");
                try {
                    jsonObject1 = new JSONObject(cloudinary.uploader().upload(file, ObjectUtils.emptyMap()));
                    baseurl = jsonObject1.optString("url");
                    url1 = cloudinary.url().transformation(
                            new Transformation().width(350).height(200).crop("fill")).generate(jsonObject1.optString("public_id"));
                    thumburl = cloudinary.url().transformation(
                            new Transformation().width(90).height(98).crop("thumb")).generate(jsonObject1.optString("public_id"));
                    Log.d(TAG, jsonObject1.toString());
                } catch (RuntimeException e) {
                    Log.d(TAG, "Error uploading file");
                    return "Error uploading file: " + e.toString();
                } catch (IOException e) {
                    dismissProgressDialog();
                    Log.d(TAG, "Error uploading file");
                    return "Error uploading file: " + e.toString();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                dismissProgressDialog();
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))) {
                    Glide.with(EditProfileActivity.this).load(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))
                            .transform(new CircleTransform(EditProfileActivity.this)).placeholder(R.drawable.ic_user)
                            .crossFade()
                            .into(profileImage);
                }

//                profileImage.setImageURI(Uri.parse(thumburl));
                super.onPostExecute(s);
            }
        };
        task.execute();
    }*/

}
