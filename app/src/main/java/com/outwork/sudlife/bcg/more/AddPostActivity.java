package com.outwork.sudlife.bcg.more;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.more.fragments.ChannelDialog;
import com.outwork.sudlife.bcg.more.models.ChannelDto;
import com.outwork.sudlife.bcg.more.models.Msg;
import com.outwork.sudlife.bcg.more.models.PostMessageDto;
import com.outwork.sudlife.bcg.more.service.PostService;
import com.outwork.sudlife.bcg.restinterfaces.GeneralService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPostActivity extends BaseActivity implements ChannelDialog.OnFragmentInteractionListener {

    public static final int Request_Camera = 100;

    private EditText title;
    private EditText description;
    private LinearLayout imagesLayout, bottombar;

    private TextView addImage, postType, addPoll;

//    private ImageHelper mImageHelper;
    private String mCurrentPhotoPath, memberType;
    private PostMessageDto postMessageDto;
    private Msg inMessage;
    private String userToken, groupId;
    private JSONObject inputJSON;

    private List<ChannelDto> channelDtoList = new ArrayList<ChannelDto>();

    private Call<RestResponse> postMsg;

    private String channelId, channelName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        if (savedInstanceState != null) {
            mCurrentPhotoPath = savedInstanceState.getString("path");
        }
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        memberType = SharedPreferenceManager.getInstance().getString(Constants.LOGINTYPE, "");

        GetChannelTypes();
        initImageHelper();
//        initializeViews();
        setListeners();
        initToolBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.post) {
            if (validate()) {
                showProgressDialog("Posting...");
                postMessageDto = new PostMessageDto();
                // postMessageDto.setMessageType(postTypeCode);
                postMessageDto.setTitle(title.getText().toString());
                postMessageDto.setDescription(description.getText().toString());
                postMessageDto.setPostedDate(Utils.getCurrentDate());
//                postMessageDto.getAttachments().addAll(mImageHelper.getImagePaths());
                try {
                    inputJSON = postMessageDto.getServerRequest();
                } catch (JSONException e) {
                }
                inMessage = new Msg();
                inMessage.setTitle(postMessageDto.getTitle());
                inMessage.setDescription(postMessageDto.getDescription());
                inMessage.setMessageType("publish");
                if (isNetworkAvailable()) {
                    PostMessage();
                } else {
                    dismissProgressDialog();
                    showToast("Oops....!No Internet");
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initImageHelper() {

//        mImageHelper = new ImageHelper(this, false);
//        mImageHelper.setFilePath(Common.IvokoImagePath);
//        mImageHelper.setmImageRemove(new ImageHelper.IImageRemove() {
//
//            @Override
//            public void onImageClickListener(View v) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void deleteImage(String path) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void onDisplayImage(int position) {
//                // TODO Auto-generated method stub
//                Intent in = new Intent(AddPostActivity.this, ImageViewerActivity.class);
//                in.putExtra("imageslist", (String[]) mImageHelper.getImagePaths().toArray(new String[mImageHelper.getImagePaths().size()]));
//                in.putExtra("issubmitted", false);
//                startActivity(in);
//            }
//        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.posttoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("New Post");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void PostMessage() {
        createPostMsgClient(channelId);
        postMsg.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    RestResponse jsonResponse = response.body();
                    if (!TextUtils.isEmpty(jsonResponse.getData())) {
                        Intent in = new Intent();
                        in.putExtra("title", postMessageDto.getTitle());
                        in.putExtra("description", postMessageDto.getDescription());
                        in.putExtra("id", jsonResponse.getData());
                        in.putExtra("msgtype", postMessageDto.getMessageType());
                        setResult(RESULT_OK, in);

//                        if (jsonResponse != null) {
//                            uploadImages(postMessageDto.getAttachments(), jsonResponse.getData(), "message");
//                        }
                        finish();
                    } else {
                        finish();
                    }
                } else {
                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                finish();
            }

        });
    }

    private void initializeViews() {
        postType = (TextView) findViewById(R.id.cPostType);
        title = (EditText) findViewById(R.id.cTitle);
        description = (EditText) findViewById(R.id.cDescription);
        addImage = (TextView) findViewById(R.id.addimage);
        imagesLayout = (LinearLayout) findViewById(R.id.cCaptureImage);
//        mImageHelper.invalidate(imagesLayout, 150, 150);
        bottombar = (LinearLayout) findViewById(R.id.clbottombar);
    }

    private void setListeners() {
        addImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        postType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (channelDtoList.size() > 0) {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction ft = manager.beginTransaction();
                    ChannelDialog dialog = ChannelDialog.newInstance();
                    Bundle bundle = new Bundle();
                    // bundle.putParcelableArrayList("PostTypeList",postTypeList);
                    bundle.putSerializable("ChannelList", (Serializable) channelDtoList);
                    dialog.setArguments(bundle);
                    //    dialog.setStyle(QBDialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    dialog.show(getSupportFragmentManager(), "dialog");
                }
            }
        });
    }


    private File createImageFile(String fileName) throws IOException {
        // Create an image file name
        File storageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/IvokoImages");
        File image = File.createTempFile(
                fileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == Request_Camera) {
//            switch (resultCode) {
//                case Activity.RESULT_OK:
//
//                    mImageHelper.setImagePath(mCurrentPhotoPath);
//                    mImageHelper.invalidate(imagesLayout, 150, 150);
//                    break;
//            }
//        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
//            String[] all_path = data.getStringArrayExtra("all_path");
//
//            for (String string : all_path) {
//                mImageHelper.setImagePath(string);
//            }
//        }
//        mImageHelper.invalidate(imagesLayout, 150, 150);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("path", mCurrentPhotoPath);
        super.onSaveInstanceState(outState);
    }

    private boolean validate() {
        if (TextUtils.isEmpty(postType.getText().toString())) {
            postType.setError("Please select category");
            return false;
        }
        if (TextUtils.isEmpty(title.getText().toString())) {
            title.setError("Please enter title");
            postType.setError(null);
            return false;
        }
        if (TextUtils.isEmpty(description.getText().toString())) {
            title.setError(null);
            postType.setError(null);
            description.setError("Please enter Description");
            return false;
        }
        return true;
    }

    public void showDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(AddPostActivity.this);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddPostActivity.this, android.R.layout.select_dialog_item);
        arrayAdapter.add("Take a picture");
        arrayAdapter.add("Select from gallery");
        builderSingle.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // String strName = arrayAdapter.getItem(which);

                        if (which == 0) {

                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            // Ensure that there's a camera activity to handle the intent
                            if (takePictureIntent.resolveActivity(AddPostActivity.this.getPackageManager()) != null) {
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFile("Classified_" + System.currentTimeMillis());
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                    startActivityForResult(takePictureIntent, Request_Camera);
                                }
                            }
                        } else if (which == 1) {
//                            Intent i = new Intent(AddPostActivity.this, CustomGalleryActivity.class);
//                            i.putExtra(CustomGalleryActivity.IMAGE_ACTION, Action.ACTION_MULTIPLE_PICK);
//                            startActivityForResult(i, 200);
                        }
                    }
                });
        builderSingle.show();
    }


    private void GetChannelTypes() {
        PostService client = RestService.createService(PostService.class);
        Call<RestResponse> channelTypes = client.getChannels(userToken, groupId);
        channelTypes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {

                    RestResponse jsonResponse = response.body();
                    try {

                        if (!TextUtils.isEmpty(jsonResponse.getData())) {
                            JSONArray jsonArray = new JSONArray(jsonResponse.getData());
                            channelDtoList = ChannelDto.parseChannelArray(jsonArray);


                        }

                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });

    }


    private void uploadImages(final List<String> attachments, final String referenceid,
                              final String referenceType) {
        GeneralService fileclient = RestService.createService(GeneralService.class);
        Call<RestResponse> s3Cred = fileclient.getS3Credentials(userToken);
        s3Cred.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsoobject = new JSONObject(response.body().getData());
//                        new ImageUploadMgr(AddPostActivity.this).imageUpload(attachments, referenceid, referenceType, jsoobject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });

    }


    private void createPostMsgClient(String channelid) {

      /*  if (postTypeCode.equalsIgnoreCase("announcements")){
            AdminService client = RestService.createService(AdminService.class);
            postMsg = client.postAdminMessage(userToken, groupId, "publish", inMessage);

        } else {
            MemberPostService client = RestService.createService(MemberPostService.class);
            postMsg = client.postMemmberMessage(userToken, groupId, inMessage);
        }*/

        PostService client = RestService.createService(PostService.class);
        postMsg = client.postMessage(userToken, groupId, channelid, inMessage);
    }

    @Override
    public void onFragmentInteraction(ChannelDto channelDto) {
        if (channelDto != null) {
            if (!TextUtils.isEmpty(channelDto.getChannelId())) {
                postType.setText(channelDto.getChannelName());
                channelId = channelDto.getChannelId();
            }
        }
    }
}
