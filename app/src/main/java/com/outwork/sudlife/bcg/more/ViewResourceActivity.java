package com.outwork.sudlife.bcg.more;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.more.customviews.TouchyWebView;
import com.outwork.sudlife.bcg.more.models.S3Credentials;
import com.outwork.sudlife.bcg.restinterfaces.GeneralService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewResourceActivity extends BaseActivity {

    private ImageView cancel;
    private WebView resourceView;
    private IvokoApplication application;
    private String userToken, bucketName, fileUrl, urlString, extension;
    private S3Credentials credentials;
    private ProgressDialog progressDialog;


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_resource);

        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FILEURL")) {
            fileUrl = getIntent().getStringExtra("FILEURL");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("EXTENSION")) {
            extension = getIntent().getStringExtra("EXTENSION");
        }
        initializeToolBar();
        initUI();
        setWebViewContent();
    }


    private void viewFile(final String fileId, final String extn, final String fileName) {
        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IvokoImages/" + fileName;
        File f = new File(filePath);
        if (f.exists()) {
            openFile(fileName);
        } else {
            try {
                //boolean downlaoded = downloadS3Async(fileId, extn, fileName);
            } catch (Exception e) {
            }
        }
    }

   /* public boolean downloadS3Async(String fileId, String extn, String fileName) throws IOException {
        credentials = getAmazonS3Credentials();
        AmazonAsyncTask amazonAsyncTask = new AmazonAsyncTask(this, credentials, fileId, fileName, extn, credentials.getBucketName());
        final String filename = fileName;
        amazonAsyncTask.setDownloadCompleted(new IDownloadCompleted() {
            @Override
            public boolean setSuccessResponse() {
                openFile(filename);
                return true;
            }

            @Override
            public boolean setError() {
                return false;
            }
        });
        amazonAsyncTask.execute();
        return false;
    }*/


    protected void openFile(String fileName) {
        try {
            Intent install = new Intent(Intent.ACTION_VIEW);
            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IvokoImages/" + fileName;
            Uri fileUri = Uri.fromFile(new File(filePath));

            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

            install.setDataAndType(fileUri, mimeType);
            startActivity(install);
        } catch (ActivityNotFoundException e) {

            new AlertDialog.Builder(this)
                    .setTitle("Failed to display " + fileName)
                    .setMessage("Please install the application which can open " + fileName)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }

    private S3Credentials getAmazonS3Credentials() {

        credentials = application.getS3Credentials();
        if (!TextUtils.isEmpty(credentials.getExpiryTime())) {
            String expirytime = credentials.getExpiryTime();

            DateTime dt = new DateTime(expirytime);
            LocalDateTime dt1 = new LocalDateTime();
            DateTimeZone tz = DateTimeZone.UTC;
            DateTime dt2 = dt1.toDateTime(tz);
            if ((dt2.isBefore(dt)) && !TextUtils.isEmpty(credentials.getAccessKey()) &&
                    !TextUtils.isEmpty(credentials.getSecretKey()) && !TextUtils.isEmpty(credentials.getSessionToken())
                    && !TextUtils.isEmpty(credentials.getBucketName()))
                return credentials;
        }
        GeneralService client = RestService.createService(GeneralService.class);
        Call<RestResponse> s3Cred = client.getS3Credentials(userToken);
        s3Cred.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse.getData());
                        JSONObject s3credentials = new JSONObject(jsonObject.getString("credentials"));
                        bucketName = jsonObject.getString("bucketName");
                        credentials = S3Credentials.parseS3Credentials(s3credentials);
                        credentials.setBucketName(bucketName);
                        application.writeS3Credentials(credentials);
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
        return credentials;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //   getMenuInflater().inflate(R.menu.menu_mail_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    private void setWebViewContent() {
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        resourceView.getSettings().setUseWideViewPort(true);
        resourceView.getSettings().setLoadWithOverviewMode(true);
        resourceView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        resourceView.clearCache(true);
        resourceView.getSettings().setJavaScriptEnabled(true);

        resourceView.getSettings().setBlockNetworkImage(false);
        resourceView.getSettings().setDomStorageEnabled(true);
        resourceView.getSettings().setBuiltInZoomControls(true);
        resourceView.getSettings().setSupportZoom(true);
        resourceView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        resourceView.getSettings().setDisplayZoomControls(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            resourceView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            resourceView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }

        progressDialog = new ProgressDialog(ViewResourceActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        resourceView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(ViewResourceActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });

        String content = "<iframe src='http://docs.google.com/viewer?url=";
        String viewer = "https://docs.google.com/viewer?url=";
        String addition = " width='100%' height='100%' style='border: none;'></iframe>";

        String loadFile = content + fileUrl + addition;
        try {
            urlString = URLEncoder.encode(fileUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        switch (extension.toLowerCase()) {
            case "pdf":
            case "doc":
            case "docx":
            case "xls":
            case "xlsx":
            case "ppt":
            case "pptx":
            case "rtf":
            case "vnd.openxmlformats-officedocument.presentationml.presentation":
            case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            case ".odt":
            case ".ods":
            case ".odp":
            case ".txt":
            case "vnd.ms-excel":
                resourceView.loadUrl(viewer + urlString);
                break;
            default:
                resourceView.loadUrl(urlString);
                break;
        }
    }

    private void initializeToolBar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.vResourceToolBar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(getIntent().getStringExtra("Name"))) {
            textView.setText(getIntent().getStringExtra("Name"));
        } else {
            textView.setText("");
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        }
    }

    private String getExtensionType(String extension) {
        String outextension;
        switch (extension.toLowerCase()) {
            case ".png":
            case "png":
            case ".jpeg":
            case ".jpg":
            case "jpeg":
            case "jpg":
            case ".gif":
            case "gif":
                outextension = "image";
                break;
            default:
                outextension = "noimage";
        }
        return outextension;
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initUI() {
        resourceView = (TouchyWebView) findViewById(R.id.vResource);
    }
}
