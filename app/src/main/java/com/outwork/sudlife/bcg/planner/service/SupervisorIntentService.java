package com.outwork.sudlife.bcg.planner.service;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.bcg.planner.SupervisorMgr;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupervisorIntentService extends JobIntentService {
    public static final String TAG = SupervisorIntentService.class.getSimpleName();

    public static final String ACTION_GET_SUPERVISORS = "com.outwork.sudlife.bcg.planner.service.action.GET_SUPERVISORS";
    private LocalBroadcastManager mgr;
    private static final Integer JOBID = 1002;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_SUPERVISORS.equals(action)) {
                getSupervisors();
            }
        }
    }

    public static void insertSupervisorsList(Context context) {
        Intent intent = new Intent(context, SupervisorIntentService.class);
        intent.setAction(ACTION_GET_SUPERVISORS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, SupervisorIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }
    private void getSupervisors() {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getSupervisors = client.getSupervisors(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getSupervisors.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    if (!TextUtils.isEmpty(jsonResponse.getData())) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<SupervisorModel>>() {}.getType();
                        List<SupervisorModel> supervisorModelList = gson.fromJson(response.body().getData(), listType);
                        if (supervisorModelList.size() > 0) {
                            SupervisorMgr.getInstance(SupervisorIntentService.this).insertSupevisorList(supervisorModelList,
                                    SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),
                                    SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                        }
                        ProductListDownloadService.getMasterListinDB(SupervisorIntentService.this);
                        SharedPreferenceManager.getInstance().putString(Constants.SUPERVISORS_LOADED, "loaded");
                        Intent intent = new Intent("supervisor_broadcast");
                        mgr = LocalBroadcastManager.getInstance(SupervisorIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } else {
                    SharedPreferenceManager.getInstance().putString(Constants.SUPERVISORS_LOADED, "notloaded");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Log.d("", "fail");
                SharedPreferenceManager.getInstance().putString(Constants.SUPERVISORS_LOADED, "notloaded");
            }
        });
    }
}