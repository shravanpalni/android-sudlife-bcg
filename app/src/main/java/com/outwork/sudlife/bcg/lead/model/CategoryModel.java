package com.outwork.sudlife.bcg.lead.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by bvlbh on 4/15/2018.
 */
public class CategoryModel {
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("categoryid")
    @Expose
    private String categoryid;

    public String getCategoryName() {
        return categoryname;
    }

    public void setCategoryName(String categoryName) {
        categoryname = categoryName;
    }

    public String getCategoryId() {
        return categoryid;
    }

    public void setCategoryId(String categoryId) {
        categoryid = categoryId;
    }

}
