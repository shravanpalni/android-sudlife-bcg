package com.outwork.sudlife.bcg.planner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class SupervisorListAdapter extends BaseAdapter {
    private Context context;
    private final LayoutInflater mInflater;
    private List<SupervisorModel> customerList = new ArrayList<>();

    static class ViewHolder {
        public TextView name;
    }

    public SupervisorListAdapter(Context context, List<SupervisorModel> custList) {
        this.context = context;
        this.customerList = custList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.customerList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.customerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_branchname, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.custname);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, viewHolder.name);
            view.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        SupervisorModel dto = (SupervisorModel) this.customerList.get(position);
        if (Utils.isNotNullAndNotEmpty(dto.getEmployeecode())) {
            if (Utils.isNotNullAndNotEmpty(dto.getUsername())) {
                holder.name.setText(dto.getUsername() + " - " + dto.getEmployeecode());
            }
        } else {
            holder.name.setText(dto.getUsername());
        }
        return view;
    }
}