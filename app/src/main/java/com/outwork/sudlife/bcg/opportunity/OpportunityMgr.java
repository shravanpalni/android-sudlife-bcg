package com.outwork.sudlife.bcg.opportunity;

import android.content.Context;

import java.util.List;

import com.outwork.sudlife.bcg.opportunity.db.OpportunityDao;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.opportunity.models.StagesModel;

public class OpportunityMgr {

    private static Context context;
    private static OpportunityDao opportunityDao;

    public OpportunityMgr(Context context) {
        this.context = context;
        opportunityDao = opportunityDao.getInstance(context);
    }

    private static OpportunityMgr instance;

    public static OpportunityMgr getInstance(Context context) {
        if (instance == null)
            instance = new OpportunityMgr(context);
        return instance;
    }

    public long insertLocalOpportunity(OpportunityModel opportunityModel, String status) {
        return opportunityDao.insertLocalOpportunity(opportunityModel, status);
    }

    public void updateOpportunityOnline(OpportunityModel opportunityModel, String userId, String opprtid) {
        opportunityDao.updateOpportunityOnline(opportunityModel, userId, opprtid);
    }

    public void updateLocalOpportunity(OpportunityModel opportunityModel, String userId) {
        opportunityDao.updateLocalOpportunity(opportunityModel, userId);
    }

    public List<OpportunityModel> getOpportunityListforSearch(String userId, String userinput) {
        return opportunityDao.getOpportunityListforSearch(userId, userinput);
    }

    public List<OpportunityModel> getOpportunityListonStage(String userId, String userinput) {
        return opportunityDao.getOpportunityListonStage(userId, userinput);
    }

    public List<OpportunityModel> getOpportunityList(String userId) {
        return opportunityDao.getOpportunityList(userId);
    }


    public List<OpportunityModel> getOpportunityOfflineList(String userId, String newtwork_status) {
        return opportunityDao.getOpportunityOfflineList(userId, newtwork_status);
    }

    public OpportunityModel getOpportunity(String opportunityId, String userId) {
        return opportunityDao.getOpportunity(opportunityId, userId);
    }

    public OpportunityModel getOfflineOpportunityByID(int id, String userId) {
        return opportunityDao.getOfflineOpportunityByID(id, userId);
    }

    public void insertOpportunityList(List<OpportunityModel> list, String userId) {
        opportunityDao.insertOpportunityList(list, userId);
    }

    public void deleteOpportunityRecordID(String id) {
        opportunityDao.deleteOpportunityRecordID(id);
    }

    public void insertOpportunitystagesList(List<StagesModel> list, String userId) {
        opportunityDao.insertOpportunitystagesList(list, userId);
    }

    public List<StagesModel> getOpportunitystagesList(String userId) {
        return opportunityDao.getOpportunitystagesList(userId);
    }

    public String getOpportunitystagescolor(String userId, String stagename) {
        return opportunityDao.getOpportunitystagescolor(userId, stagename);
    }

    public List<String> getOpportunitystagesnamesList(String userId) {
        return opportunityDao.getOpportunitystagesnamesList(userId);
    }
}



