package com.outwork.sudlife.bcg.dto;

public class UserDetailsDto {

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String loginId;
	public String password;
	public int status;
	public LocationDto location;


	public String groupCode;
	
	public String getUserName() {
		return loginId;
	}
	public void setUserName(String userName) {
		this.loginId = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

}
