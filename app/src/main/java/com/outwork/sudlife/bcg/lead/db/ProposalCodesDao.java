package com.outwork.sudlife.bcg.lead.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.lead.model.ProposalCodesModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProposalCodesDao {
    public static final String TAG = ProposalCodesDao.class.getSimpleName();

    private static ProposalCodesDao instance;
    private Context applicationContext;

    public static ProposalCodesDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new ProposalCodesDao(applicationContext);
        }
        return instance;
    }

    private ProposalCodesDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    public long insertProposalCodes(SQLiteDatabase db, ProposalCodesModel dto) {
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getOrganizationid())) {
            values.put(IvokoProvider.ProposalCodes.groupid, dto.getOrganizationid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBandid())) {
            values.put(IvokoProvider.ProposalCodes.bandid, dto.getBandid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBandname())) {
            values.put(IvokoProvider.ProposalCodes.bandname, dto.getBandname());
        }
        if (dto.getMinvalue() != null) {
            values.put(IvokoProvider.ProposalCodes.minvalue, dto.getMinvalue());
        }
        if (dto.getMaxvalue() != null) {
            values.put(IvokoProvider.ProposalCodes.maxvalue, dto.getMaxvalue());
        }
        long proposalid = db.insert(IvokoProvider.ProposalCodes.TABLE, null, values);
        return proposalid;
    }

    public void insertProposalCodesList(List<ProposalCodesModel> proposalCodesModelList, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.ProposalCodes.TABLE))
            deleteProposalCodesList(db, groupid);
        for (int j = 0; j < proposalCodesModelList.size(); j++) {
            ProposalCodesModel proposalCodesModel = proposalCodesModelList.get(j);
            insertProposalCodes(db, proposalCodesModel);
        }
    }

    public List<ProposalCodesModel> getProposalCodesList(String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<ProposalCodesModel> proposalCodesModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.ProposalCodes.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.ProposalCodes.TABLE;
            sql += " WHERE " + IvokoProvider.ProposalCodes.groupid + " = '" + groupid + "'";*/
            try {
                // cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.ProposalCodes.TABLE + " WHERE " + IvokoProvider.ProposalCodes.groupid + " = ?" , new String[]{groupid});


                if (cursor.moveToFirst()) {
                    do {
                        ProposalCodesModel proposalCodesModel = new ProposalCodesModel();
                        proposalCodesModel.setOrganizationid(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.groupid)));
                        proposalCodesModel.setBandid(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.bandid)));
                        proposalCodesModel.setBandname(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.bandname)));
                        proposalCodesModel.setMinvalue(cursor.getInt(cursor.getColumnIndex(IvokoProvider.ProposalCodes.minvalue)));
                        proposalCodesModel.setMaxvalue(cursor.getInt(cursor.getColumnIndex(IvokoProvider.ProposalCodes.maxvalue)));
                        proposalCodesModelList.add(proposalCodesModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return proposalCodesModelList;
    }

   /* public List<ProposalCodesModel> getProposalCodesList(String proposalno, String groupid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<ProposalCodesModel> proposalCodesModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.ProposalCodes.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.ProposalCodes.TABLE;
            sql += " WHERE " + IvokoProvider.ProposalCodes.groupid + " = '" + groupid + "'";
            sql += " AND " + proposalno + " BETWEEN " + "'" + IvokoProvider.ProposalCodes.minvalue + "'  AND  '" + IvokoProvider.ProposalCodes.maxvalue + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        ProposalCodesModel proposalCodesModel = new ProposalCodesModel();
                        proposalCodesModel.setOrganizationid(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.groupid)));
                        proposalCodesModel.setBandid(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.bandid)));
                        proposalCodesModel.setBandname(cursor.getString(cursor.getColumnIndex(IvokoProvider.ProposalCodes.bandname)));
                        proposalCodesModel.setMinvalue(cursor.getInt(cursor.getColumnIndex(IvokoProvider.ProposalCodes.minvalue)));
                        proposalCodesModel.setMaxvalue(cursor.getInt(cursor.getColumnIndex(IvokoProvider.ProposalCodes.maxvalue)));
                        proposalCodesModelList.add(proposalCodesModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return proposalCodesModelList;
    }*/

    private void deleteProposalCodesList(SQLiteDatabase db, String groupid) {
        db.delete(IvokoProvider.Superior.TABLE, IvokoProvider.ProposalCodes.groupid + "=?", new String[]{groupid});
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.ProposalCodes.TABLE + " WHERE " + IvokoProvider.ProposalCodes.groupid + " = ?", new String[]{groupid});
        if (cursor.getCount() <= 0) {
            cursor.close();
        }
        cursor.close();
    }
}