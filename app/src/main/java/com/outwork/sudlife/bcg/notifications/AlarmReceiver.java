package com.outwork.sudlife.bcg.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.activities.CalenderActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

/**
 * Created by Habi on 22-09-2017.
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    private String scheduletime, after15, before15;
    //    private String contentText = "You have a task due";
    private String contentText = "";
    private Date date, date1, date2;
    private boolean isRepeating = false;
    private Uri notifySound;

    public void onReceive(Context context, Intent pintent) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

        if (pintent.getExtras() != null && pintent.getExtras().containsKey("time")) {
            scheduletime = pintent.getStringExtra("time");
        }
        if (pintent.getExtras() != null && pintent.getExtras().containsKey("isrepeating")) {
            isRepeating = pintent.getBooleanExtra("isrepeating", false);
        }

        if (!TextUtils.isEmpty(scheduletime) && !isRepeating) {
            contentText = "You have a scheduled task due at " + scheduletime;
        }

        if (Utils.isNotNullAndNotEmpty(scheduletime) && isRepeating) {
            if (scheduletime.equalsIgnoreCase("9")) {
                contentText = "Good morning. Please check schedule tasks for the day";
                SharedPreferenceManager.getInstance().putBoolean(Constants.NOFITY9, true);
//                TaskIntentService.insertTasksinDB(context, getCalculatedDate("yyyy-MM-dd", -7), getCalculatedDate("yyyy-MM-dd", 7));
            }
            else if (scheduletime.equalsIgnoreCase("17")) {
                contentText = "Reminder: Have you submitted reports today?";
                SharedPreferenceManager.getInstance().putBoolean(Constants.NOTIFY17, true);
            }
        }
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, new Intent(context, CalenderActivity.class), 0);
        try {
            notifySound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notifySound);
        } catch (Exception e) {
            e.printStackTrace();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("OutWork");
        builder.setContentText(contentText);
        builder.setSmallIcon(R.mipmap.ic_app_logo);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(contentText));
        builder.setSound(notifySound);
        if (Build.VERSION.SDK_INT >= 17) {
            builder.setShowWhen(true);
        }
        builder.setContentIntent(pIntent);

        Notification noti = builder.getNotification();

        noti.flags = Notification.FLAG_AUTO_CANCEL;

        final int _id = new Random().nextInt(443254);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Utils.isNotNullAndNotEmpty(contentText)) {
            nm.notify(_id, noti);
        }
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        int j = days;
        cal.add(Calendar.DAY_OF_YEAR, j);
        return s.format(new Date(cal.getTimeInMillis()));
    }
}
