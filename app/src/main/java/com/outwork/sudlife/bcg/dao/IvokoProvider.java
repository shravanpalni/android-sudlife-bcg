package com.outwork.sudlife.bcg.dao;

import android.provider.BaseColumns;

public class IvokoProvider {
    public static final String TAG = IvokoProvider.class.getSimpleName();

    public static class Settings implements BaseColumns {
        public static final String TABLE = "settings";

        public static final String KEY = "key";
        public static final String VALUE = "value";

        public static final String KEY_DEVICE_ID = "device_id";
        public static final String KEY_LAST_LAT = "lastLat";
        public static final String KEY_LAST_LONG = "lastLong";
        public static final String KEY_LOCATION_NAME = "locName";
        public static final String KEY_USER_EMAIL_ID = "emailid";
        public static final String KEY_FIRST_NAME = "userFirstName";
        public static final String KEY_LAST_NAME = "userLastName";
        public static final String KEY_DEVICE_TYPE = "deviceType";
        public static final String KEY_USER_COUNTRY = "userCountry";
        public static final String KEY_USER_CITY = "userCity";
    }

    public static class Category implements BaseColumns {
        public static final String TABLE = "category";

        public static final String CATEGORY_CNTRYCODE = "category_country_code";
        public static final String CATEGORY_PARENT_CODE = "category_parent_code";
        public static final String CATEGORY_STATUS = "status";
        public static final String CATEGORY_IMAGE = "image_url";
        public static final String CATEGORY_TAG = "tag";
        public static final String CATEGORY_CODE = "category_code";
        public static final String CATEGORY_NAME = "category_name";
        public static final String CATEGORY_TYPE = "type";
        public static final String CATEGORY_DESC = "description";
        public static final String CATEGORY_IMAGE_NAME = "imagename";
        public static final String TYPE = "cat_type";
    }

    public static class SubCategory implements BaseColumns {
        public static final String TABLE = "subcategory";

        public static final String SUB_CATEGORY_CODE = "sub_category_code";
        public static final String SUB_CATEGORY_NAME = "sub_category_name";
        public static final String CATEGORY_CODE = "category_code";

    }

    public static class ImagesProvider implements BaseColumns {
        public static final String TABLE = "images_provider";

        public static final String IMAGEPATH = "imagepath";
        public static final String REFERENCEID = "referenceid";
        public static final String STATUS = "status";
        public static final String FILEID = "fileId";
        public static final String BUCKET_NAME = "bucketname";
        public static final String CREDENTIALS = "credentials";
        public static final String REFERENCETYPE = "referencetype";
    }

    public static class SyncTable implements BaseColumns {
        public static final String TABLE = "SyncTable";

        public static final String SYNC_USERID = "userid";
        public static final String SYNC_GROUPID = "groupid";
        public static final String SYNC_USERTOKEN = "usertoken";
        public static final String SYNC_OBJECTID = "objectid";
        public static final String SYNC_OBJECTTYPE = "objecttype";
        public static final String SYNC_OBJECTDATA = "objectdata";
        public static final String SYNC_STATUS = "status";
        public static final String SYNC_CREATEDDATE = "createddate";
        public static final String SYNC_SYNCDATE = "syncdate";
        public static final String SYNC_FORMNAME = "syncformname";
    }

    public static class Contact implements BaseColumns {
        public static final String TABLE = "contact";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String contactid = "contactid";
        public static final String salutation = "salutation";//new
        public static final String status = "status";//new
        public static final String practicing = "practicing";//new
        public static final String gender = "gender";//new
        public static final String customerid = "customerid";
        public static final String customername = "customername";
        public static final String firstname = "firstname";
        public static final String lastname = "lastname";
        public static final String displayname = "displayname";
        public static final String designation = "designation";
        public static final String speciality = "speciality";
        public static final String classification = "classification";
        public static final String phoneno = "phoneno";
        public static final String cemail = "email";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
        public static final String internaltype = "internaltype";
        public static final String contacttype = "contacttype";
        public static final String address = "address";//new
    }

    public static class MonthlySummeryForBranch implements BaseColumns {
        public static final String TABLE_NAME = "monthlysummeryforbranches";
        public static final String FTM = "ftm";
        public static final String USER_ID = "userid";
        public static final String ACTIVITIES_PLANNED = "activitiesplanned";
        public static final String VISITS_PLANNED = "visitsplanned";
        public static final String EXPECTED_CONNECTIONS = "expectedconncetions";
        public static final String EXPECTED_LEADS = "expectedleads";
        public static final String EXPECTED_BUSINESS = "expectedbusiness";
        public static final String ACTIVITES_COMPLETED = "activitiescompleted";
        public static final String VISITS_COMPLETED = "vistiscompleted";
        public static final String ACHIEVED_CONNECTION = "achievedconnection";
        public static final String ACHIEVED_LEADS = "achievedleads";
        public static final String ACHIEVED_BUSINESS = "achievedbusiness";
        public static final String STATUS_OF_SUCCESS_OR_FAIL = "statusofsuccessorfail";
        public static final String NETWORK_STATUS = "networkstatus";
        public static final String OFFLINE_OR_ONLINE_STATUS = "offlineoronlinestatus";
        public static final String CUSTOMER_NAME = "customername";
        public static final String TARGET_ID = "targetid";
        public static final String OBJECT_ID = "objectid";
        public static final String MONTH = "month";
        public static final String DATE = "date";
        public static final String YEAR = "year";
    }

    public static class Customer implements BaseColumns {
        public static final String TABLE = "customer";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String customerid = "customerid";
        public static final String customername = "customername";
        public static final String customercode = "customercode";
        public static final String phoneno = "phoneno";
        public static final String organizationtype = "orgtype";
        public static final String internaltype = "inttype";
        public static final String status = "status";
        public static final String sudlat = "sudlat";
        public static final String sudlong = "sudlong";
        public static final String banklat = "banklat";
        public static final String banklong = "banklong";
        public static final String soname = "soname";
        public static final String region = "region";
        public static final String zone = "zone";
        public static final String email = "email";
        public static final String branchfoundationday = "branchfoundationday";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
        public static final String address = "address";
        public static final String addressline1 = "addressline1";
        public static final String locality = "locality";
        public static final String isdelete = "isdelete";
    }

    public static class TxnMonth implements BaseColumns {
        public static final String TABLE = "txnmonth";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String monthno = "monthno";
        public static final String year = "year";
        public static final String monthname = "monthname";
        public static final String amount = "amount";
        public static final String currency = "currency";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
    }


    public static class TxnDay implements BaseColumns {
        public static final String TABLE = "txnday";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String monthno = "monthno";
        public static final String year = "year";
        public static final String monthname = "monthname";
        public static final String dayno = "dayno";
        public static final String dayname = "dayname";
        public static final String amount = "amount";
        public static final String currency = "currency";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
    }

    public static class Transaxion implements BaseColumns {
        public static final String TABLE = "transaxion";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String txndayid = "txndayid";
        public static final String txnrefno = "txnrefno";  //server;
        public static final String monthno = "monthno";
        public static final String year = "year";
        public static final String dayno = "dayno";
        public static final String category = "category";
        public static final String subcategory = "subcategory";
        public static final String description = "description";
        public static final String fromlocation = "fromlocation";
        public static final String tolocation = "tolocation";
        public static final String distance = "distance";
        public static final String amount = "amount";
        public static final String currency = "currency";
        public static final String files = "files";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
    }

    public static class Localforms implements BaseColumns {
        public static final String TABLE = "localforms";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String formid = "formid";
        public static final String formtype = "formtype";
        public static final String formname = "formname";  //server;
        public static final String description = "description";
        public static final String status = "status";
        public static final String createddate = "createddate";
        public static final String createdby = "createdby";
        public static final String modifieddate = "modifieddate";

    }

    public static class Forms implements BaseColumns {
        public static final String TABLE = "forms";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String fid = "fid"; //serverid
        public static final String formid = "formid";
        public static final String formtype = "formtype";
        public static final String formname = "formname";
        public static final String description = "description";
        public static final String status = "status";
        public static final String createddate = "createddate";
        public static final String visiteddate = "visiteddate";
        public static final String modifieddate = "modifieddate";
        public static final String name = "name";
        public static final String email = "email";
        public static final String phone = "phone";
        public static final String visitedby = "visitedby";
        public static final String date = "date";
        public static final String products = "products";
        public static final String title = "title";
        public static final String contactname = "contactname";
        public static final String notes = "notes";
        public static final String followupdate = "followupdate";
        public static final String followupaction = "followupaction";
        public static final String organizationtype = "organizationtype";
        public static final String localcontactid = "localcontactid"; //new
        public static final String contactid = "contactid";
        public static final String customerid = "customerid";
        public static final String designation = "designation";
        public static final String ctclassification = "ctclassification";
        public static final String speciality = "speciality";
        public static final String firstname = "firstname";
        public static final String outcome = "outcome";
        public static final String source = "source";
        public static final String remarks = "remarks";
        public static final String address = "address";
        public static final String customername = "customername";
        public static final String orderobject = "orderobject";
        public static final String orderitems = "orderitems";
        public static final String orderdate = "orderdate";
        public static final String value = "value";
        public static final String totalunits = "totalunits";
        public static final String localattachment = "localattachment";
        public static final String attachment = "attachment";
    }

    public static class Planner implements BaseColumns {
        public static final String TABLE = "planner";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String id = "id"; //serverid
        public static final String activityid = "activityid";
        public static final String customertype = "customertype";
        public static final String branchcode = "branchcode";
        public static final String customername = "customername";
        public static final String customerid = "customerid";
        public static final String firstname = "firstname";
        public static final String lastname = "lastname";
        public static final String localcustomerid = "localcustomerid";
        public static final String contactid = "contactid";
        public static final String localcontactid = "localcontactid";
        public static final String opportunityid = "opportunityid";
        public static final String localopportunityid = "localopportunityid";
        public static final String activitytype = "activitytype";//visit & activity
        public static final String subtype = "subtype"; //visit & activity
        public static final String type = "type"; //visit & activity
        public static final String purpose = "purpose";
        public static final String otheractions = "otheractions";
        public static final String otherfollowupactions = "otherfollowupactions";
        public static final String planstatus = "planstatus";//#0-Unplanned 1- Planned 2- Approved 3-Rejected
        public static final String completestatus = "completestatus";// -Incomplete 1- Started 2- Completed
        public static final String scheduletime = "scheduletime";
        public static final String checkintime = "checkintime";
        public static final String checkouttime = "checkouttime";
        public static final String checkinlocation = "checkinlocation";
        public static final String checkoutlocation = "checkoutlocation";
        public static final String checkinlat = "checkinlat";
        public static final String checkinlon = "checkinlon";
        public static final String checkoutlat = "checkoutlat";
        public static final String checkoutlon = "checkoutlon";
        public static final String devicetimestamp = "devicetimestamp";
        public static final String outcome = "outcome";
        public static final String followupdate = "followupdate";
        public static final String followupaction = "followupaction";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
        public static final String createdby = "createdby";
        public static final String modifiedby = "modifiedby";
        public static final String notes = "notes";
        public static final String productspromoted = "productspromoted";
        public static final String scheduleday = "scheduleday";
        public static final String schedulemonth = "schedulemonth";
        public static final String scheduleyear = "scheduleyear";
        public static final String networkstatus = "networkstatus";
        public static final String status = "status";
        public static final String leadobject = "leadobject";
        public static final String plancreatedby = "plancreatedby";
        public static final String title = "title";
        public static final String jointvisit = "jointvisit";
        public static final String leadid = "leadid";
        public static final String localleadid = "localleadid";
        public static final String customerobject = "customerobject";
        public static final String supervisorid = "supervisorid";
        public static final String supervisorname = "supervisorname";
        public static final String reschduledate = "reschduledate";
        public static final String resReason = "resReason";
        public static final String isdeleted = "isdeleted";
    }

    public static class Superior implements BaseColumns {
        public static final String TABLE = "superior";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String supervisorid = "supervisorid";
        public static final String supervisorname = "supervisorname";
        public static final String employeecode = "employeecode";
    }
    public static class ProposalCodes implements BaseColumns {
        public static final String TABLE = "proposalcodes";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String bandid = "bandid";
        public static final String bandname = "bandname";
        public static final String minvalue = "minvalue";
        public static final String maxvalue = "maxvalue";
    }

    //siva
    public static class PlannerNotification implements BaseColumns {
        public static final String TABLE = "plannernotification";
        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String id = "id"; //serverid
        public static final String activityid = "activityid";
        public static final String customername = "customername";
        public static final String customerid = "customerid";
        public static final String localcustomerid = "localcustomerid";
        public static final String contactid = "contactid";
        public static final String localcontactid = "localcontactid";
        public static final String opportunityid = "opportunityid";
        public static final String localopportunityid = "localopportunityid";
        public static final String activitytype = "activitytype";//visit & activity
        public static final String subtype = "subtype"; //visit & activity
        public static final String type = "type"; //visit & activity
        public static final String purpose = "purpose";
        public static final String planstatus = "planstatus";//#0-Unplanned 1- Planned 2- Approved 3-Rejected
        public static final String completestatus = "completestatus";//#0-Incomplete 1- Started 2- Completed
        public static final String scheduletime = "scheduletime";
        public static final String checkintime = "checkintime";
        public static final String checkouttime = "checkouttime";
        public static final String checkinlocation = "checkinlocation";
        public static final String checkoutlocation = "checkoutlocation";
        public static final String outcome = "outcome";
        public static final String followupdate = "followupdate";
        public static final String followupaction = "followupaction";
        public static final String createddate = "createddate";
        public static final String modifieddate = "modifieddate";
        public static final String createdby = "createdby";
        public static final String modifiedby = "modifiedby";
        public static final String notes = "notes";
        public static final String productspromoted = "productspromoted";
        public static final String scheduleday = "scheduleday";
        public static final String schedulemonth = "schedulemonth";
        public static final String scheduleyear = "scheduleyear";
        public static final String networkstatus = "networkstatus";
        public static final String status = "tatus";
        public static final String plancreatedby = "plancreatedby";
        public static final String title = "title";
        public static final String jointvisit = "jointvisit";
        public static final String supervisor = "supervisor";
        public static final String notificationstatus = "notificationstatus";
        public static final String reschduledate = "reschduledate";
        public static final String resReason = "resReason";
        public static final String isdeleted = "isdeleted";

    }
//siva end

    public static class LocalProducts implements BaseColumns {
        public static final String TABLE = "localproducts";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String productid = "productid";
        public static final String productname = "productname";  //server;
        public static final String status = "status";
        public static final String categoryname = "categoryname";
        public static final String categoryid = "categoryid";
        public static final String createddate = "createddate";
        public static final String createdby = "createdby";
        public static final String modifieddate = "modifieddate";
    }


    public static class StaticList implements BaseColumns {
        public static final String TABLE = "staticlist";

        public static final String listid = "listid";
        public static final String groupid = "groupid";
        public static final String listtype = "listtype";
        public static final String listcode = "listcode";  //server;
        public static final String name = "name";
        public static final String createddate = "createddate";
        public static final String createdby = "createdby";
        public static final String modifieddate = "modifieddate";
    }

    public static class Opportunity implements BaseColumns {
        public static final String TABLE = "opportunity";

        public static final String opportunityid = "opportunityid";
        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String firstname = "firstname";
        public static final String lastname = "lastname";
        public static final String stage = "stage";
        public static final String gender = "gender";
        public static final String age = "age";
        public static final String occupation = "occupation";
        public static final String income = "income";
        public static final String familymembers = "familymembers";
        public static final String value = "value";
        public static final String closingdate = "closingdate";
        public static final String notes = "notes";
        public static final String items = "items";
        public static final String network_status = "network_status";
    }

    public static class AlarmTable implements BaseColumns {
        public static final String TABLE = "alarmtable";

        public static final String objectid = "objectid";
        public static final String objecttype = "objecttype";
        public static final String alarmtime = "alarmtime";
        public static final String alarmdaytime = "alarmdaytime";
    }

    public static class OpportunityStages implements BaseColumns {
        public static final String TABLE = "opportunitystages";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String stageid = "stageid";//server;
        public static final String name = "name";
        public static final String description = "description";
        public static final String defaultorder = "defaultorder";
        public static final String isdefault = "isdefault";
        public static final String modifiedon = "modifiedon";
        public static final String colorcode = "colorcode";
    }

    public static class Lead implements BaseColumns {
        public static final String TABLE = "lead";
        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String leadid = "leadid";
        public static final String leadcode = "leadcode";
        public static final String firstname = "firstname";
        public static final String lastname = "lastname";
        public static final String isnri = "isnri";
        public static final String countryname = "countryname"; // country code
        public static final String countrycode = "countrycode"; // country code
        public static final String contactno = "contactno";
        public static final String emailid = "emailid";
        public static final String alternative_contactno = "alternative_contactno";
        public static final String existing_sublife_customer = "existing_sublife_customer";
        public static final String premium_paid = "premium_paid";
        public static final String ispremium_paid = "ispremium_paid";
        public static final String product_name = "product_name";
        public static final String product_one = "product_one";
        public static final String product_two = "product_two";
        public static final String address = "address";
        public static final String city = "city";
        public static final String pincode = "pincode";
        public static final String occupation = "occupation";
        public static final String source_of_lead = "source_of_lead";
        public static final String other = "other";
        public static final String bank = "bank";
        public static final String branch_code = "branch_code";
        public static final String no_of_family_members = "no_of_family_members";
        public static final String branch_name = "branch_name";
        public static final String zcc_support_requried = "zcc_support_requried";
        public static final String gender = "gender";
        public static final String campaigndate = "campaigndate";
        public static final String preferred_language = "preferred_language";
        public static final String preferred_date = "preferred_date";
        public static final String preferred_time = "preferred_time";
        public static final String status = "status";
        public static final String ismarried = "ismarried";
        public static final String lead_stage = "lead_stage";
        public static final String sub_status = "sub_status";
        public static final String premium_expected = "premium_expected";
        public static final String next_followup_date = "next_followup_date";
        public static final String first_appointment_date = "first_appointment_date";
        public static final String conversion_propensity = "conversion_propensity";
        public static final String visiting_time = "visiting_time";
        public static final String visiting_date = "visiting_date";
        public static final String proposal_number = "proposal_number";
        public static final String lead_created_date = "lead_created_date";
        public static final String region = "region";
        public static final String married = "married";
        public static final String education = "education";
        public static final String customer_call_connected = "customer_call_connected";
        public static final String income_band = "income_band";
        public static final String lead_type = "lead_type";
        public static final String remarks = "remarks";
        public static final String relation = "relation";
        public static final String working = "working";
        public static final String age = "age";
        public static final String appointement_date = "appointement_date";
        public static final String appointement_time = "appointement_time";
        public static final String financial_planning_done_for = "financial_planning_done_for";
        public static final String tentative_investment_years = "tentative_investment_years";
        public static final String financial_planning_in_future_for = "financial_planning_in_future_for";
        public static final String tentative_amount = "tentative_amount";
        public static final String expected_actual_premium = "expected_actual_premium";
        public static final String actual_premium = "actual_premium";
        public static final String notes = "notes";
        public static final String createdby = "createdby";
        public static final String createdon = "createdon";
        public static final String modifiedby = "modifiedby";
        public static final String modifiedon = "modifiedon";
        public static final String items = "items";
        public static final String network_status = "network_status";
    }




    public static class UserTeam implements BaseColumns {
        public static final String TABLE = "userteam";

        public static final String groupid = "groupid";
        public static final String userid = "userid";
        public static final String teamid = "teamid";
        public static final String type = "type";
        public static final String name = "name";
        public static final String internalrole = "internalrole";

    }



}
