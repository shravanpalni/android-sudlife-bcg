package com.outwork.sudlife.bcg.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Share {

    @SerializedName("shareable")
    @Expose
    private Integer shareable;
    @SerializedName("shareurl")
    @Expose
    private String shareurl;
    @SerializedName("sharetext")
    @Expose
    private String sharetext;
    @SerializedName("shareflag")
    @Expose
    private String shareflag;

    public Integer getShareable() {
        return shareable;
    }

    public void setShareable(Integer shareable) {
        this.shareable = shareable;
    }

    public String getShareurl() {
        return shareurl;
    }

    public void setShareurl(String shareurl) {
        this.shareurl = shareurl;
    }

    public String getSharetext() {
        return sharetext;
    }

    public void setSharetext(String sharetext) {
        this.sharetext = sharetext;
    }

    public String getShareflag() {
        return shareflag;
    }

    public void setShareflag(String shareflag) {
        this.shareflag = shareflag;
    }

}
