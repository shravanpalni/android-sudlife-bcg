package com.outwork.sudlife.bcg.planner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class CalendarDayAdapter extends RecyclerView.Adapter<CalendarDayAdapter.MyViewHolder> {

    private Context context;
    private List<String> dates;
    private LayoutInflater mLayoutInflater;
    private int selectedPosition;
    private int sposition;

    public CalendarDayAdapter(Context context, List<String> dates, int selectedPosition) {
        this.context = context;
        this.mLayoutInflater = LayoutInflater.from(this.context);
        this.dates = dates;
        this.selectedPosition = selectedPosition;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.calender_cell, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(new SimpleDateFormat("EEE").format(simpleDateFormat.parse(dates.get(position))));
            stringBuilder.append("\n");
            stringBuilder.append(new SimpleDateFormat("dd").format(simpleDateFormat.parse(dates.get(position))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.cell_text.setText(stringBuilder);
        if (selectedPosition == position) {
            holder.cell_text.setBackgroundResource(R.drawable.circle_frame);
            holder.cell_text.setTextColor(Color.parseColor("#ffffff"));
            sposition = position;
        } else {
            holder.cell_text.setTextColor(Color.parseColor("#de000000"));
            holder.cell_text.setBackground(null);
        }
    }

    @Override
    public int getItemCount() {
        return dates.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(dates.get(position));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cell_text;

        public MyViewHolder(View item) {
            super(item);
            cell_text = (TextView) item.findViewById(R.id.calendar_txtDay);
            Utils.setTypefaces(IvokoApplication.robotoLightTypeface, cell_text);
        }
    }

    public void setSelectedItem(int position) {
        selectedPosition = position;
    }

    public void dataSetChanged(int sPosition) {
        this.sposition = sPosition;
        notifyDataSetChanged();
        setSelectedItem(sPosition);
    }
}
