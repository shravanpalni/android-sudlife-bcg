package com.outwork.sudlife.bcg.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.restinterfaces.UserRestInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.fragments.ResetPasswordFragment;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {

    private EditText emailField;
    private TextView sendpwd, errorText, signin;
    private ImageView cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        emailField = (EditText) findViewById(R.id.userNameField);
        sendpwd = (TextView) findViewById(R.id.resend);
        errorText = (TextView) findViewById(R.id.errorText);
        initialiseToolBar();
        sendpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorText.setText(null);
                if (validate()) {
                    forgotpassword(emailField.getText().toString());
                    hideKeyboard();
                }
            }
        });
    }

    private boolean validate() {
        boolean isValid = true;
        if (emailField.getText().toString().length() == 0) {
            isValid = false;
            emailField.setError("Please enter valid Username");
            return isValid;
        }
        return isValid;
    }

    private void initialiseToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.forgotpassword);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Forgot password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void forgotpassword(String email) {

        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.newforgotPassword(email);
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse restResponse = response.body();
                    String code = restResponse.getCode();
                    if (code.equalsIgnoreCase("USR-200")) {
                        FragmentManager manager = getSupportFragmentManager();
                        ResetPasswordFragment fragment = new ResetPasswordFragment();
                        FragmentTransaction ft = manager.beginTransaction();
                        ft.replace(android.R.id.content, fragment);
                        ft.addToBackStack(null);
                        ft.commitAllowingStateLoss();
                    } else {
                        Utils.displayToast(ForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
                    }
                } else {
                    Utils.displayToast(ForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Utils.displayToast(ForgotPasswordActivity.this, "We are not able to verify this email. Please enter your registered email");
            }
        });
    }
}
