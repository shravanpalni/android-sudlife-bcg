package com.outwork.sudlife.bcg.branches.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.branches.adapter.CustomerOnlineAdapter;
import com.outwork.sudlife.bcg.branches.adapter.SOSpinnerAdapter;
import com.outwork.sudlife.bcg.branches.adapter.TeamHierarchtFirstLevelAdapter;
import com.outwork.sudlife.bcg.branches.adapter.TeamHierarchtFourthLevelAdapter;
import com.outwork.sudlife.bcg.branches.adapter.TeamHierarchtSecondLevelAdapter;
import com.outwork.sudlife.bcg.branches.adapter.TeamHierarchtThirdLevelAdapter;
import com.outwork.sudlife.bcg.branches.adapter.UserBranchesAdapter;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.branches.models.Childlayer;
import com.outwork.sudlife.bcg.branches.models.RecyclerTouchListener;
import com.outwork.sudlife.bcg.branches.models.TeamHierarchyModel;
import com.outwork.sudlife.bcg.branches.services.CustomerService;
import com.outwork.sudlife.bcg.planner.activities.CreatePlanActivity;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.models.Userprofile;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 16-07-2019.
 */

public class ShowHierarchyActivity extends BaseActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    public static ShowHierarchyActivity showHierarchyActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_hierarchy);
        showHierarchyActivity = this;


        initToolBar();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }


    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Select Branch");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }


    public static class HierarchyFragment extends Fragment {

        private int planststatus;
        private String plandate, userteamid, userToken, userType;
        private int userLevel;
        private LinearLayout main_layout;
        private fr.ganfra.materialspinner.MaterialSpinner firstspinnerList, secondspinnerList, thirdspinnerList, fourthspinnerList, SOSpinner;
        private RecyclerView listview;
        private List<BranchesModel> branchesModelList;
        private List<BranchesModel> newBranchesList;
        private TextView user_branches, no_data;
        private RelativeLayout view_layout;
        private List<TeamHierarchyModel> teamHierarchyModelList = new ArrayList<>();
        private List<Childlayer> listChildlayer1 = new ArrayList<>();
        private List<Childlayer> listChildlayer2 = new ArrayList<>();
        private List<Childlayer> listChildlayer3 = new ArrayList<>();
        private TeamHierarchtFirstLevelAdapter teamHierarchtFirstLevelAdapter;
        private TeamHierarchtSecondLevelAdapter teamHierarchtSecondLevelAdapter;
        private TeamHierarchtThirdLevelAdapter teamHierarchtThirdLevelAdapter;
        private TeamHierarchtFourthLevelAdapter teamHierarchtFourthLevelAdapter;
        private ProgressDialog dialog = null;
        private ArrayList<Userprofile> teamMembersHeirarchyArrayList = new ArrayList();
        private ArrayList<Userprofile> newSOlist;
        private SOSpinnerAdapter soSpinnerAdapter;
        private ScrollView scrollView;
        private String regionalUserId, regionalTeamId, coUserId, coTeamId, areaUserId, areaTeamId, locationUserid, locationTeamId;
        private String zhUserId = "";
        private String zhTeamId = "";


        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);


            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
                planststatus = getActivity().getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
            }
            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
                plandate = getActivity().getIntent().getStringExtra(Constants.PLAN_DATE);
            }

            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("USERTEAMID")) {
                userteamid = getActivity().getIntent().getStringExtra("USERTEAMID");
            }

            userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
            userLevel = SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0);
            userType = SharedPreferenceManager.getInstance().getString(Constants.SUPERVISOREMPTYPE, "");

            Log.i("Habi", "userLevel = = = = =" + userLevel);
            Log.i("Habi", "userType = = = = =" + userType);


        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_team_hierarchy, container, false);

            main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);
            scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);

            firstspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) rootView.findViewById(R.id.spinner);
            secondspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) rootView.findViewById(R.id.spinner_child1);
            thirdspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) rootView.findViewById(R.id.spinner_child2);
            fourthspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) rootView.findViewById(R.id.spinner_child4);

            SOSpinner = (fr.ganfra.materialspinner.MaterialSpinner) rootView.findViewById(R.id.spinner_so);
            listview = (RecyclerView) rootView.findViewById(R.id.listview);
            listview.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), listview, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {


                    Intent intent = new Intent(getActivity(), CreatePlanActivity.class);
                    intent.putExtra("customer", new Gson().toJson(newBranchesList.get(position)));
                    intent.putExtra("customerid", newBranchesList.get(position).getCustomerid());
                    intent.putExtra("finish", true);
                    intent.putExtra(Constants.PLAN_STATUS, planststatus);
                    intent.putExtra(Constants.PLAN_DATE, plandate);
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
            view_layout = (RelativeLayout) rootView.findViewById(R.id.view1);
            user_branches = (TextView) rootView.findViewById(R.id.user_branches);
            no_data = (TextView) rootView.findViewById(R.id.no_data);
            if (isNetworkAvailable()) {
                scrollView.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.GONE);
                getTeamHierarchy(userteamid);
            } else {
                showSimpleAlert("", "Oops...No Internet connection.");
                no_data.setVisibility(View.VISIBLE);
                no_data.setText("No Internet.");

            }
            //initToolBar();
            setListeners();


            return rootView;
        }


        public boolean isNetworkAvailable() {
            ConnectivityManager cn = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nf = cn.getActiveNetworkInfo();
            return nf != null && nf.isConnected();
        }

        public void showSimpleAlert(String title, String message) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(title).setMessage(message).setPositiveButton("OK", null).show();
        }


        private void setListeners() {


            firstspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0) {
                        Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getName());
                        Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getHaschildren());
                        Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getChildlayers().size());
                        Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());
                        listChildlayer1 = teamHierarchyModelList.get(0).getChildlayers().get(position).getChildlayers();


                        if (userType.equalsIgnoreCase("Zonal Business Director")) {


                            Log.i("Habi", "rhuserid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid());
                            Log.i("Habi", "regionid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());

                            regionalTeamId = "";
                            regionalUserId = "";

                            regionalUserId = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            regionalTeamId = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();


                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("RH")) {

                            Log.i("Habi", "couserid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid());
                            Log.i("Habi", "controllingofficeid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());


                            coTeamId = "";
                            coUserId = "";

                            coUserId = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            coTeamId = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();

                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("RL")) {

                            Log.i("Habi", "areauserid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid());
                            Log.i("Habi", "areaid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());

                            areaTeamId = "";
                            areaUserId = "";

                            areaUserId = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            areaTeamId = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();

                            getExclusiveBranchesForUserId("");

                        } else if (userType.equalsIgnoreCase("AH")) {

                            Log.i("Habi", "locationuserid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid());
                            Log.i("Habi", "locationid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());

                            locationTeamId = "";
                            locationUserid = "";

                            locationUserid = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            locationTeamId = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();
                            getExclusiveBranchesForUserId("");

                        }/*else if (userType.equalsIgnoreCase("LH")) {

                            Log.i("Habi", "locationuserid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid());
                            Log.i("Habi", "locationid = = = = =" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());

                            locationTeamId = "";
                            locationUserid = "";

                            locationUserid = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            locationTeamId = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();

                        }*/


                        if (listChildlayer1.size() > 0) {

                            setAdapterList(listChildlayer1);
                        } else {
                            // showToast("No  levels available");

                            String teamid = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();
                            String supervisorId = teamHierarchyModelList.get(0).getChildlayers().get(position).getSupervisorid();
                            Log.i("Habi", "first spinner supervisorId = = = =" + supervisorId);
                            if (isNetworkAvailable()) {
                                getTeamHierarchyMembers(teamid, supervisorId);
                            } else {
                                showSimpleAlert("", "Oops...No Internet connection.");
                            }
                            Log.i("Habi", "secondspinnerList selected");
                        }

                    } else {
                        // firstspinnerList.setSelection(0);
                        secondspinnerList.setVisibility(View.GONE);
                        thirdspinnerList.setVisibility(View.GONE);
                        fourthspinnerList.setVisibility(View.GONE);
                        SOSpinner.setVisibility(View.GONE);
                       /* if(newBranchesList.size()>0){
                            listview.setVisibility(View.VISIBLE);
                        }else {
                            listview.setVisibility(View.GONE);
                        }*/
                        // listview.setVisibility(View.GONE); modified
                        view_layout.setVisibility(View.GONE);
                        Log.i("Habi first spinner", "position <0 selected");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            secondspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0) {
                        Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getName());
                        Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getHaschildren());
                        Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getChildlayers().size());
                        listChildlayer2 = listChildlayer1.get(position).getChildlayers();

                        if (userType.equalsIgnoreCase("Zonal Business Director")) {


                            Log.i("Habi", "couserid = = = = =" + listChildlayer1.get(position).getSupervisorid());
                            Log.i("Habi", "controllingofficeid = = = = =" + listChildlayer1.get(position).getTeamid());

                            coTeamId = "";
                            coUserId = "";

                            coUserId = listChildlayer1.get(position).getSupervisorid();
                            coTeamId = listChildlayer1.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("RH")) {
                            Log.i("Habi", "areauserid = = = = =" + listChildlayer1.get(position).getSupervisorid());
                            Log.i("Habi", "areaid = = = = =" + listChildlayer1.get(position).getTeamid());


                            areaUserId = "";
                            areaTeamId = "";

                            areaUserId = listChildlayer1.get(position).getSupervisorid();
                            areaTeamId = listChildlayer1.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("CO")) {

                            Log.i("Habi", "locationuserid = = = = =" + listChildlayer1.get(position).getSupervisorid());
                            Log.i("Habi", "locationid = = = = =" + listChildlayer1.get(position).getTeamid());


                            locationUserid = "";
                            locationTeamId = "";

                            locationUserid = listChildlayer1.get(position).getSupervisorid();
                            locationTeamId = listChildlayer1.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");

                        } else if (userType.equalsIgnoreCase("AH")) {

                            Log.i("Habi", "souserid = = = = =" + listChildlayer1.get(position).getSupervisorid());
                            Log.i("Habi", "soid = = = = =" + listChildlayer1.get(position).getTeamid());

                        }


                        if (listChildlayer2.size() > 0) {
                            setAdapterList2(listChildlayer2);
                        } else {
                            String teamid = listChildlayer1.get(position).getTeamid();
                            String supervisorId = listChildlayer1.get(position).getSupervisorid();
                            Log.i("Habi", "second spinner supervisorId = = = =" + supervisorId);
                            if (isNetworkAvailable()) {
                                getTeamHierarchyMembers(teamid, supervisorId);
                            } else {
                                showSimpleAlert("", "Oops...No Internet connection.");
                            }
                            Log.i("Habi", "secondspinnerList selected");
                        }
                    } else {
                        thirdspinnerList.setVisibility(View.GONE);
                        fourthspinnerList.setVisibility(View.GONE);
                        SOSpinner.setVisibility(View.GONE);
                       /* if(newBranchesList.size()>0){
                            listview.setVisibility(View.VISIBLE);
                        }else {
                            listview.setVisibility(View.GONE);
                        }*/
                        // listview.setVisibility(View.GONE); modified
                        view_layout.setVisibility(View.GONE);
                        Log.i("Habi second spinner", "position <0 selected");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            thirdspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0) {
                        Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                        Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getHaschildren());
                        Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getChildlayers().size());
                        listChildlayer3 = listChildlayer2.get(position).getChildlayers();

                        if (userType.equalsIgnoreCase("Zonal Business Director")) {


                            Log.i("Habi", "ahuserid = = = = =" + listChildlayer2.get(position).getSupervisorid());
                            Log.i("Habi", "areaid = = = = =" + listChildlayer2.get(position).getTeamid());


                            areaTeamId = "";
                            areaUserId = "";

                            areaUserId = listChildlayer2.get(position).getSupervisorid();
                            areaTeamId = listChildlayer2.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("RH")) {

                            Log.i("Habi", "locationuserid = = = = =" + listChildlayer2.get(position).getSupervisorid());
                            Log.i("Habi", "locationid = = = = =" + listChildlayer2.get(position).getTeamid());

                            locationTeamId = "";
                            locationTeamId = "";

                            locationUserid = listChildlayer2.get(position).getSupervisorid();
                            locationTeamId = listChildlayer2.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("CO")) {

                            Log.i("Habi", "souserid = = = = =" + listChildlayer2.get(position).getSupervisorid());
                            Log.i("Habi", "soid = = = = =" + listChildlayer2.get(position).getTeamid());

                        } else if (userType.equalsIgnoreCase("AH")) {

                            Log.i("Habi", " = = = = =" + listChildlayer2.get(position).getSupervisorid());
                            Log.i("Habi", " = = = = =" + listChildlayer2.get(position).getTeamid());

                        }


                        if (listChildlayer3.size() > 0) {
                            setAdapterList3(listChildlayer3);
                        } else {
                            String teamid = listChildlayer2.get(position).getTeamid();
                            String supervisorId = listChildlayer2.get(position).getSupervisorid();
                            Log.i("Habi", "third spinner supervisorId = = = =" + supervisorId);
                            if (isNetworkAvailable()) {
                                getTeamHierarchyMembers(teamid, supervisorId);
                            } else {
                                showSimpleAlert("", "Oops...No Internet connection.");
                            }
                            Log.i("Habi", "secondspinnerList selected");
                        }
                    } else {

                        fourthspinnerList.setVisibility(View.GONE);
                        SOSpinner.setVisibility(View.GONE);
                        /*if(newBranchesList.size()>0){
                            listview.setVisibility(View.VISIBLE);
                        }else {
                            listview.setVisibility(View.GONE);
                        }*/
                        // listview.setVisibility(View.GONE);  //modified
                        view_layout.setVisibility(View.GONE);
                        Log.i("Habi second spinner", "position <0 selected");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            fourthspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                    if (position >= 0) {
                        String teamid = listChildlayer3.get(position).getTeamid();
                        String supervisorId = listChildlayer3.get(position).getSupervisorid();
                        Log.i("Habi", "forth spinner supervisorId = = = =" + supervisorId);
                        if (userType.equalsIgnoreCase("Zonal Business Director")) {


                            Log.i("Habi", "lhuserid = = = = =" + listChildlayer3.get(position).getSupervisorid());
                            Log.i("Habi", "locationid = = = = =" + listChildlayer3.get(position).getTeamid());


                            locationTeamId = "";
                            locationUserid = "";

                            locationUserid = listChildlayer3.get(position).getSupervisorid();
                            locationTeamId = listChildlayer3.get(position).getTeamid();
                            getExclusiveBranchesForUserId("");


                        } else if (userType.equalsIgnoreCase("RH")) {

                            Log.i("Habi", "souserid = = = = =" + listChildlayer3.get(position).getSupervisorid());
                            Log.i("Habi", "soid = = = = =" + listChildlayer3.get(position).getTeamid());


                        } else if (userType.equalsIgnoreCase("CO")) {

                            Log.i("Habi", "111 = = = = =" + listChildlayer3.get(position).getSupervisorid());
                            Log.i("Habi", "111 = = = = =" + listChildlayer3.get(position).getTeamid());

                        } else if (userType.equalsIgnoreCase("AH")) {

                            Log.i("Habi", "222 = = = = =" + listChildlayer3.get(position).getSupervisorid());
                            Log.i("Habi", "222 = = = = =" + listChildlayer3.get(position).getTeamid());

                        } else if (userType.equalsIgnoreCase("LH")) {

                            Log.i("Habi", "LH222 = = = = =" + listChildlayer3.get(position).getSupervisorid());
                            Log.i("Habi", "LH222 = = = = =" + listChildlayer3.get(position).getTeamid());

                        }


                        if (isNetworkAvailable()) {
                            getTeamHierarchyMembers(teamid, supervisorId);
                        } else {
                            showSimpleAlert("", "Oops...No Internet connection.");
                        }
                    } else {
                        //fourthspinnerList.setVisibility(View.GONE);
                        SOSpinner.setVisibility(View.GONE);
                        listview.setVisibility(View.GONE);
                        view_layout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


        }


        private void setAdapterList(final List<Childlayer> childlayers) {
            secondspinnerList.setVisibility(View.VISIBLE);
//        secondspinnerList.setHint(list.get(0).getSupervisordisplayrole());
            teamHierarchtSecondLevelAdapter = new TeamHierarchtSecondLevelAdapter(getActivity(), childlayers);
            secondspinnerList.setAdapter(teamHierarchtSecondLevelAdapter);
            teamHierarchtSecondLevelAdapter.notifyDataSetChanged();
        }

        private void setAdapterList2(final List<Childlayer> list2) {
            thirdspinnerList.setVisibility(View.VISIBLE);
            teamHierarchtThirdLevelAdapter = new TeamHierarchtThirdLevelAdapter(getActivity(), list2);
            thirdspinnerList.setAdapter(teamHierarchtThirdLevelAdapter);
            teamHierarchtThirdLevelAdapter.notifyDataSetChanged();
        }


        private void setAdapterList3(final List<Childlayer> list3) {
            fourthspinnerList.setVisibility(View.VISIBLE);
            teamHierarchtFourthLevelAdapter = new TeamHierarchtFourthLevelAdapter(getActivity(), list3);
            fourthspinnerList.setAdapter(teamHierarchtFourthLevelAdapter);
            teamHierarchtFourthLevelAdapter.notifyDataSetChanged();
        }


        private void getTeamHierarchy(String userteamid) {
            showProgressDialog("Loading, please wait....");
            CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchy(userToken, userteamid);
            responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                    if (response.isSuccessful()) {
                        dismissProgressDialog();
                        if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                            Type listType = new TypeToken<ArrayList<TeamHierarchyModel>>() {
                            }.getType();
                            teamHierarchyModelList = new Gson().fromJson(response.body().getData(), listType);
                            if (Utils.isNotNullAndNotEmpty(teamHierarchyModelList)) {
//                            if (Utils.isNotNullAndNotEmpty(teamHierarchyModelList.get(0).getSupervisordisplayrole()))
//                                firstspinnerList.setHint(teamHierarchyModelList.get(0).getSupervisordisplayrole());
                                firstspinnerList.setVisibility(View.VISIBLE);
                                teamHierarchtFirstLevelAdapter = new TeamHierarchtFirstLevelAdapter(getActivity(), teamHierarchyModelList.get(0).getChildlayers());
                                firstspinnerList.setAdapter(teamHierarchtFirstLevelAdapter);
                            } else {
                                scrollView.setVisibility(View.GONE);
                                main_layout.setVisibility(View.GONE);
                                firstspinnerList.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                                no_data.setText("No Branches");

                            }
                        }
                    } else {
                        dismissProgressDialog();
                        scrollView.setVisibility(View.GONE);
                        main_layout.setVisibility(View.GONE);
                        firstspinnerList.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        no_data.setText("No Branches");

                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dismissProgressDialog();
                    scrollView.setVisibility(View.GONE);
                    main_layout.setVisibility(View.GONE);
                    firstspinnerList.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    no_data.setText("No Branches");


                }
            });
        }


        private void getTeamHierarchyMembers(String teamid, String supervisorId) {
            showProgressDialog("Loading, please wait....");
            CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
            responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                    if (response.isSuccessful()) {
                        dismissProgressDialog();
                        if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                            teamMembersHeirarchyArrayList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                            }.getType());
                            if (Utils.isNotNullAndNotEmpty(teamMembersHeirarchyArrayList)) {

                                newSOlist = new ArrayList<Userprofile>();

                                for (Userprofile teammemberList : teamMembersHeirarchyArrayList) {

                                    if (!teammemberList.getUserid().equalsIgnoreCase(supervisorId)) {

                                        newSOlist.add(teammemberList);

                                    }

                                }


                                Log.i("Habi", "listChildlayer1.size() = = = =" + listChildlayer1.size());
                                Log.i("Habi", "listChildlayer2.size() = = = =" + listChildlayer2.size());
                                Log.i("Habi", "listChildlayer3.size() = = = =" + listChildlayer3.size());


                                SOSpinner.setVisibility(View.VISIBLE);
//                            SOSpinner.setHint("SO members");
                                //soSpinnerAdapter = new SOSpinnerAdapter(getActivity(), teamMembersHeirarchyArrayList);
                                soSpinnerAdapter = new SOSpinnerAdapter(getActivity(), newSOlist);
                                SOSpinner.setAdapter(soSpinnerAdapter);
                                soSpinnerAdapter.notifyDataSetChanged();
                                SOSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (position >= 0) {
                                            String uid = newSOlist.get(position).getUserid();
                                            Log.i("Habi", "userId - - - " + uid);
                                            String orgid = newSOlist.get(position).getOrganizationid();
                                            Log.i("Habi", "orgid - - - " + orgid);
                                            if (isNetworkAvailable()) {
                                                //getBranchesForUserId(uid);
                                                getExclusiveBranchesForUserId(uid);
                                            } else {
                                                showSimpleAlert("", "Oops...No Internet connection.");
                                            }
                                        } else {
                                            Log.i("Habi so spinner", "position <0 ");

                                          /*  if(newBranchesList.size()>0){
                                                listview.setVisibility(View.VISIBLE);
                                            }else {
                                                listview.setVisibility(View.GONE);
                                            }*/
                                            //listview.setVisibility(View.GONE);   //modified
                                            view_layout.setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });
                            } else {
                                showToast("No SO available");
                                SOSpinner.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        dismissProgressDialog();
                        showToast("No SO available");
                        SOSpinner.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dismissProgressDialog();
                    showToast("No SO available");
                    SOSpinner.setVisibility(View.GONE);
                }
            });
        }



/*private void getBranchesForUserId(String uid) {
            showProgressDialog("Loading, please wait....");
            CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> responseCallGetTargets = servcieForTragets.getBranchesForUserId(userToken, uid);
            responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                    if (response.isSuccessful()) {
                        dismissProgressDialog();
                        final List<BranchesModel> branchesList = new ArrayList<>();
                        if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                            branchesModelList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<BranchesModel>>() {
                            }.getType());
//                        for (BranchesModel branchesModel : branchesModelList) {
//                            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
//                                branchesList.add(branchesModel);
//                            }
//                        }
                            if (*//*Utils.isNotNullAndNotEmpty(branchesModelList)*//*branchesModelList.size() > 0) {

                                //newBranchesList = new ArrayList<>();
                                newBranchesList.clear();

                                for (BranchesModel branchesModelList : branchesModelList) {

                                    if (!branchesModelList.getIsdeleted()) {

                                        newBranchesList.add(branchesModelList);

                                    }
                                }

                                // Log.i("ShowHierarchyActivity","newBranchesList = = "+newBranchesList.size());

                                UserBranchesAdapter userBranchesAdapter = new UserBranchesAdapter(getActivity(), newBranchesList);
                                //listview = (RecyclerView) findViewById(R.id.listview);
                                view_layout.setVisibility(View.VISIBLE);
                                listview.setVisibility(View.VISIBLE);
                                listview.setAdapter(userBranchesAdapter);
                                listview.getAdapter().notifyDataSetChanged();
                                listview.setLayoutManager(new LinearLayoutManager(getActivity()));


                            } else {
                                dismissProgressDialog();
                                listview.setVisibility(View.GONE);
                                user_branches.setVisibility(View.VISIBLE);

                            }
                        }
                    } else {
                        dismissProgressDialog();
                        listview.setVisibility(View.GONE);
                        user_branches.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dismissProgressDialog();
                    listview.setVisibility(View.GONE);
                    user_branches.setVisibility(View.VISIBLE);
                }
            });
        }*/

        private void getExclusiveBranchesForUserId(String uid) {
            showProgressDialog("Loading, please wait....");
            CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> responseCallGetTargets = servcieForTragets.getExclusiveBranchesForUser(userToken, uid, locationUserid, areaUserId, coUserId, regionalUserId,
                    zhUserId, locationTeamId, areaTeamId, coTeamId, zhTeamId, regionalTeamId);
            responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                    if (response.isSuccessful()) {

                        try {

                            dismissProgressDialog();
                            branchesModelList = new ArrayList<BranchesModel>();
                            if (Utils.isNotNullAndNotEmpty(response.body().getData().toString())) {
                                branchesModelList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<BranchesModel>>() {
                                }.getType());
//                        for (BranchesModel branchesModel : branchesModelList) {
//                            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
//                                branchesList.add(branchesModel);
//                            }
//                        }
                                if (!branchesModelList.isEmpty() && branchesModelList.size() > 0) {

                                    newBranchesList = new ArrayList<>();

                                    for (BranchesModel branchesModelList : branchesModelList) {

                                        if (!branchesModelList.getIsdeleted()) {

                                            newBranchesList.add(branchesModelList);

                                        }

                                    }

                                    // Log.i("ShowHierarchyActivity","newBranchesList = = "+newBranchesList.size());

                                    UserBranchesAdapter userBranchesAdapter = new UserBranchesAdapter(getActivity(), newBranchesList);
                                    //listview = (RecyclerView) findViewById(R.id.listview);
                                    view_layout.setVisibility(View.VISIBLE);
                                    listview.setVisibility(View.VISIBLE);
                                    listview.setAdapter(userBranchesAdapter);
                                    listview.getAdapter().notifyDataSetChanged();
                                    listview.setLayoutManager(new LinearLayoutManager(getActivity()));


                                } else {
                                    dismissProgressDialog();
                                    listview.setVisibility(View.GONE);
                                    user_branches.setVisibility(View.VISIBLE);

                                }
                            } else {

                                if (listview.getVisibility() == View.VISIBLE) {

                                    listview.setVisibility(View.GONE);

                                }

                                user_branches.setVisibility(View.VISIBLE);

                                //Toast.makeText(getActivity(),"No Branches available",Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {

                            e.printStackTrace();
                        }


                    } else {
                        dismissProgressDialog();
                        listview.setVisibility(View.GONE);
                        user_branches.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dismissProgressDialog();
                    listview.setVisibility(View.GONE);
                    user_branches.setVisibility(View.VISIBLE);
                }
            });
        }


        public void showToast(String message) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }


        public void showProgressDialog(String message) {
            if (dialog == null) dialog = new ProgressDialog(getActivity());
            dialog.setMessage(message);

            dialog.setCancelable(false);
            try {
                if (!dialog.isShowing()) dialog.show();
            } catch (Exception e) {

            }
        }

        public void dismissProgressDialog() {
            try {
                if (dialog.isShowing()) dialog.dismiss();
            } catch (Exception e) {

            }
        }


    }


    public static class BranchesFragment extends Fragment {

        private TextView noinfo;
        private ProgressDialog dialog = null;
        private RelativeLayout searchLayout;
        private EditText searchText;
        List<BranchesModel> branchesModelList = new ArrayList<>();
        private CustomerOnlineAdapter customerOnlineAdapter;
        private ListView custListView;
        private int plan_status;
        private String type, displaydate, fromclass;
        boolean areBranchesLoaded = false;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);


            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("type")) {
                type = getActivity().getIntent().getStringExtra("type");
            }
            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
                displaydate = getActivity().getIntent().getStringExtra(Constants.PLAN_DATE);
            }
            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
                plan_status = getActivity().getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
            }
            if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("fromclass")) {
                fromclass = getActivity().getIntent().getStringExtra("fromclass");
            }

        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_customer_list, container, false);

            Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.mListToolBar);
            toolbar.setVisibility(View.GONE);

            noinfo = (TextView) rootView.findViewById(R.id.noMembers);
            searchLayout = (RelativeLayout) rootView.findViewById(R.id.searchLayout);
            searchText = (EditText) rootView.findViewById(R.id.searchText);
            custListView = (ListView) rootView.findViewById(R.id.memberList);
            //noinfo.setVisibility(View.VISIBLE);


            return rootView;
        }


        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if (isVisibleToUser && !areBranchesLoaded) {
                if (isNetworkAvailable()) {
                    new GetCustomerList().execute();
                } else {
                    showSimpleAlert("", "Oops...No Internet connection.");
                    noinfo.setVisibility(View.VISIBLE);
                    noinfo.setText("No Internet.");

                }
                areBranchesLoaded = true;
            }
        }

        public void showSimpleAlert(String title, String message) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(title).setMessage(message).setPositiveButton("OK", null).show();
        }


        private void setListener() {


            custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                    Intent in = new Intent(getActivity(), CreatePlanActivity.class);
                    in.putExtra("customer", new Gson().toJson(dto));
                    in.putExtra("customerid", dto.getCustomerid());
                    in.putExtra(Constants.PLAN_STATUS, plan_status);
                    in.putExtra(Constants.PLAN_DATE, displaydate);
                    startActivity(in);
                    //getActivity().finish();


                    /*if (Utils.isNotNullAndNotEmpty(type)) {
                        if (type.equalsIgnoreCase("select")) {
                            if (Utils.isNotNullAndNotEmpty(fromclass)) {
                                if (fromclass.equalsIgnoreCase("lead")) {
                                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                    Intent in = new Intent(getActivity(), CreateNewLeadActivity.class);
                                    in.putExtra("customer", new Gson().toJson(dto));
                                    in.putExtra("customerid", dto.getCustomerid());
                                    in.putExtra(Constants.PLAN_STATUS, plan_status);
                                    in.putExtra(Constants.PLAN_DATE, displaydate);
                                    startActivity(in);
                                    getActivity().finish();
                                } else if (fromclass.equalsIgnoreCase("plan")) {
                                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                    Intent in = new Intent(getActivity(), CreatePlanActivity.class);
                                    in.putExtra("customer", new Gson().toJson(dto));
                                    in.putExtra("customerid", dto.getCustomerid());
                                    in.putExtra(Constants.PLAN_STATUS, plan_status);
                                    in.putExtra(Constants.PLAN_DATE, displaydate);
                                    startActivity(in);
                                    getActivity().finish();
                                }
                            }
                        } else {
                            BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                            Intent in = new Intent(getActivity(), ViewCustomerActivity.class);
                            in.putExtra("customer", new Gson().toJson(dto));
                            in.putExtra("customerid", dto.getCustomerid());
                            startActivity(in);
                        }
                    } else {
                        BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                        Intent in = new Intent(getActivity(), ViewCustomerActivity.class);
                        in.putExtra("customer", new Gson().toJson(dto));
                        in.putExtra("customerid", dto.getCustomerid());
                        startActivity(in);
                    }*/
                }
            });


        }


        public boolean isNetworkAvailable() {
            ConnectivityManager cn = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nf = cn.getActiveNetworkInfo();
            return nf != null && nf.isConnected();
        }

        public void showProgressDialog(String message) {
            if (dialog == null) dialog = new ProgressDialog(getActivity());
            dialog.setMessage(message);

            dialog.setCancelable(false);
            try {
                if (!dialog.isShowing()) dialog.show();
            } catch (Exception e) {

            }
        }

        public void dismissProgressDialog() {
            try {
                if (dialog.isShowing()) dialog.dismiss();
            } catch (Exception e) {

            }
        }


        private void setNoCustomers() {
//            if (progressBar.VISIBLE == 0) {
//                progressBar.setVisibility(View.GONE);
//            }
            noinfo.setVisibility(View.VISIBLE);
            noinfo.setText("No Branches");
        }


        private void setUpSearchLayout() {

            searchLayout.setVisibility(View.VISIBLE);

            searchText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (customerOnlineAdapter != null && s != null) {
                        customerOnlineAdapter.getFilter().filter(s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }


        private class GetCustomerList extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {


                String modifiedDate = "";
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.BRANCHES_LAST_FETCH_TIME, ""))) {
                    modifiedDate = new BranchesMgr(getActivity()).getmaxModifiedDate(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                } else {
                    modifiedDate = "";
                    SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "1531739767");
                }
                CustomerService client = RestService.createServicev1(CustomerService.class);
                Call<RestResponse> getcustomers = client.getcustomerassignees(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "", modifiedDate);
                try {
                    Response<RestResponse> restResponse = getcustomers.execute();
                    int statusCode = restResponse.code();
                    if (statusCode == 200) {
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<ArrayList<BranchesModel>>() {
                            }.getType();
                            branchesModelList = gson.fromJson(restResponse.body().getData(), listType);
                           /* if (branchesModelList.size() > 0) {
                                new BranchesMgr(getActivity()).insertNewCustomerList(branchesModelList);
                            }*/

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
                }


                return "Executed";
            }

            @Override
            protected void onPostExecute(String result) {


                //getCustomerList();
                setListener();
                //Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, noinfo);
                //Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText), otherbranch);
                // getCustomerList();


                if (branchesModelList.size() > 0) {
                    noinfo.setVisibility(View.GONE);
                    customerOnlineAdapter = new CustomerOnlineAdapter(getActivity(), branchesModelList, SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                    custListView.setAdapter(customerOnlineAdapter);
                    //progressBar.setVisibility(View.GONE);
                    setUpSearchLayout();
                /*if (customerList.size() > 10) {
                    setUpSearchLayout();
                }*/
                } else {
                    setNoCustomers();
                }

                dismissProgressDialog();

            }

            @Override
            protected void onPreExecute() {

                showProgressDialog("Loading . . .");


            }

            @Override
            protected void onProgressUpdate(Void... values) {
            }
        }


    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new HierarchyFragment();
            } else if (position == 1) {
                fragment = new BranchesFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "By Teams";
                case 1:
                    return "By Branches";
            }
            return null;
        }


    }


}
