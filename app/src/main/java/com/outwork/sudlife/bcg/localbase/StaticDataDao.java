package com.outwork.sudlife.bcg.localbase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.dao.IvokoProvider.StaticList;

/**
 * Created by Panch on 2/16/2017.
 */

public class StaticDataDao {
    public static final String TAG = StaticDataDao.class.getSimpleName();

    private static StaticDataDao instance;
    private Context applicationContext;

    public static StaticDataDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new StaticDataDao(applicationContext);
        }
        return instance;
    }

    private StaticDataDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }


    public void insertStaticData(StaticDataDto staticDto, String groupId, SQLiteDatabase db) {
        if (!isRecordExist(db, staticDto.getType(), staticDto.getValue(), groupId)) {
            ContentValues values = new ContentValues();
            values.put(StaticList.listtype, staticDto.getType());
            values.put(StaticList.name, staticDto.getValue());
            values.put(StaticList.groupid, groupId);
            boolean createSuccessful = db.insert(StaticList.TABLE, null, values) > 0;
            if (createSuccessful) {
                Log.e(TAG, " created.");
            }
        }
    }

    public void insertStaticList(List<StaticDataDto> productList, String groupId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        deleteMasterData(db, groupId);
        for (int i = 0; i < productList.size(); i++) {
            StaticDataDto prodDto = (StaticDataDto) productList.get(i);
            insertStaticData(prodDto, groupId, db);
        }
    }

    public List<StaticDataDto> getListbyType(String groupId, String listtype) {

        List<StaticDataDto> staticList = new ArrayList<StaticDataDto>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, StaticList.TABLE)) {
            Cursor cursor = null;

            //security
           /* String sql = "";
            sql += "SELECT * FROM " + StaticList.TABLE;
            sql += " WHERE " + StaticList.groupid + " = '" + groupId + "'";
            sql += " AND " + StaticList.listtype + " = '" + listtype + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);

             /*   cursor=db.rawQuery("select * from "+StaticList.TABLE+" where "+ "groupid=? and listtype=? ",
                        new String [] {groupId, listtype});*/

                cursor = db.rawQuery("SELECT * FROM " + StaticList.TABLE + " WHERE " + StaticList.groupid + " = ?" + " AND " +StaticList.listtype + " = ?"  , new String[]{groupId,listtype});



                if (cursor.moveToFirst()) {
                    do {
                        StaticDataDto dto = new StaticDataDto();
                        dto.setName(cursor.getString(cursor.getColumnIndex(StaticList.name)));
                        dto.setType(cursor.getString(cursor.getColumnIndex(StaticList.listtype)));
                        staticList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return staticList;
    }


    public List<StaticDataDto> getListbyType(String groupId, String listtype, String userInput) {

        List<StaticDataDto> staticList = new ArrayList<StaticDataDto>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, StaticList.TABLE)) {
            Cursor cursor = null;
            // security
            /*String sql = "";
            sql += "SELECT * FROM " + StaticList.TABLE;
            sql += " WHERE " + StaticList.groupid + " = '" + groupId + "'";
            sql += " AND " + StaticList.listtype + " = '" + listtype + "'";
            sql += " AND " + StaticList.name + " LIKE '%" + userInput + "%'";*/


            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+StaticList.TABLE+" where "+ "groupid=? and listtype=? and name like ? ",
                        new String [] {groupId, listtype, '%' + userInput + '%'});



                if (cursor.moveToFirst()) {
                    do {
                        StaticDataDto dto = new StaticDataDto();
                        dto.setName(cursor.getString(cursor.getColumnIndex(StaticList.name)));
                        dto.setType(cursor.getString(cursor.getColumnIndex(StaticList.listtype)));
                        staticList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }

            }
        }

        return staticList;
    }

    public List<StaticDataDto> getStaticList(String groupId, String listtype) {

        List<StaticDataDto> staticList = new ArrayList<StaticDataDto>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, StaticList.TABLE)) {
            Cursor cursor = null;
            // security
            /*String sql = "";
            sql += "SELECT * FROM " + StaticList.TABLE;
            sql += " WHERE " + StaticList.groupid + " = '" + groupId + "'";
            sql += " AND " + StaticList.listtype + " = '" + listtype + "'";*/


            try {
                //cursor = db.rawQuery(sql, null);

               /* cursor=db.rawQuery("select * from "+StaticList.TABLE+" where "+ "groupid=? and listtype=? ",
                        new String [] {groupId, listtype});*/

                cursor = db.rawQuery("SELECT * FROM " + StaticList.TABLE + " WHERE " + StaticList.groupid + " = ?" + " AND " +StaticList.listtype + " = ?"  , new String[]{groupId,listtype});

                if (cursor.moveToFirst()) {
                    do {
                        StaticDataDto dto = new StaticDataDto();
                        dto.setName(cursor.getString(cursor.getColumnIndex(StaticList.name)));
                        dto.setType(cursor.getString(cursor.getColumnIndex(StaticList.listtype)));
                        staticList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }

            }
        }

        return staticList;
    }

    public List<String> getListNamesbyModifiedDate(String groupId, String listtype) {
        List<String> staticList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, StaticList.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + StaticList.TABLE;
            sql += " WHERE " + StaticList.groupid + " = '" + groupId + "'";
            sql += " AND " + StaticList.listtype + " = '" + listtype + "'";
            sql += " ORDER BY " + StaticList.modifieddate + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);
              /*  cursor=db.rawQuery("select * from "+StaticList.TABLE+" where "+ "groupid=? and listtype=? order by modifieddate=? ",
                        new String [] {groupId,listtype, " ASC "});*/

                cursor = db.rawQuery("SELECT * FROM " + StaticList.TABLE + " WHERE " + StaticList.groupid + " = ?" + " AND " +StaticList.listtype + " = ?" + " ORDER BY " +StaticList.modifieddate + " = ?" , new String[]{groupId,listtype," ASC "});

                if (cursor.moveToFirst()) {
                    do {
                        String name = cursor.getString(cursor.getColumnIndex(StaticList.name));
                        staticList.add(name);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return staticList;
    }

    public List<String> getListNames(String groupId, String listtype) {
        List<String> staticList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, StaticList.TABLE)) {
            Cursor cursor = null;

            //security
         /*   String sql = "";
            sql += "SELECT * FROM " + StaticList.TABLE;
            sql += " WHERE " + StaticList.groupid + " = '" + groupId + "'";
            sql += " AND " + StaticList.listtype + " = '" + listtype + "'";*/
            try {
//                cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT * FROM " + StaticList.TABLE + " WHERE " + StaticList.listtype + " = ?" + " AND " + StaticList.groupid + " = ?", new String[]{listtype, groupId});
                if (cursor.moveToFirst()) {
                    do {
                        String name = cursor.getString(cursor.getColumnIndex(StaticList.name));
                        staticList.add(name);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return staticList;
    }


    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private boolean isRecordExist(SQLiteDatabase db, String type, String name, String groupid) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + StaticList.TABLE + " WHERE " + StaticList.listtype + " = ?" + " AND " + StaticList.name + " = ?" + " AND " + StaticList.groupid + " = ?", new String[]{type, name, groupid});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    private void deleteMasterData(SQLiteDatabase db, String groupId) {
        db.delete(IvokoProvider.StaticList.TABLE, IvokoProvider.StaticList.groupid + "=?", new String[]{groupId});
    }

}