package com.outwork.sudlife.bcg.opportunity.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.opportunity.OpportunityMgr;
import com.outwork.sudlife.bcg.opportunity.adapter.ExpandableListAdapter;
import com.outwork.sudlife.bcg.opportunity.models.StagesModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;

public class FilterFragment extends Fragment {
    // TODO: Rename and change types of parameters
    private String status;
    //    private Toolbar toolbar;
    private ImageView close;
    private ExpandableListAdapter listAdapter;
    private OnFragmentInteractionListener mListener;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private String result = "";
    private HashMap<String, List<String>> listDataChild;

    public FilterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        View view = inflater.inflate(R.layout.layout_exp_filter, container, false);

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        close = (ImageView) view.findViewById(R.id.close);
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataHeader.add("Stage");
        List<String> stage = new ArrayList<>();
        for (StagesModel stagesModel : OpportunityMgr.getInstance(getActivity()).getOpportunitystagesList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""))) {
            stage.add(stagesModel.getName());
        }
        listDataChild.put(listDataHeader.get(0), stage); // Header, Child data
        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                result = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                mListener.onClickItem(result);
                getActivity().onBackPressed();
                return false;
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onClickItem(String result);
    }
}
