package com.outwork.sudlife.bcg.planner.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.adapter.TeamMembersListAdapter;
import com.outwork.sudlife.bcg.planner.models.TeamMembers;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.targets.services.RestServicesForTragets;
import com.outwork.sudlife.bcg.ui.base.AppBaseActivity;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Android on 13-06-2018.
 */

public class TeamMembersListActivity extends AppBaseActivity {

    private TextView toolbar_title,team_task_nomessages;
    private ListView tpRecyclerView;
    private SwipeRefreshLayout tpSwipeRefreshLayout;
    ArrayList<TeamMembers> teamMembersArrayList = new ArrayList<>();


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_members_list);
        initToolBar();
        team_task_nomessages = (TextView) findViewById(R.id.team_members_list_nomessages);
        tpRecyclerView = (ListView) findViewById(R.id.team_membersListrecyclerview);
        tpSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.team_membersrefresh_layout);
        getTeamMembers();
    }

    private void initToolBar() {
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Team MembersList");
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
    }



    public void getTeamMembers() {
        showProgressDialog("loading . . .");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful())
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamMembers>>() {
                        }.getType();
                        ArrayList<TeamMembers> tmArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (tmArrayList != null) {
                            if (tmArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                for (int i = 0; i < tmArrayList.size(); i++) {
                                    if (tmArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(tmArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    tpRecyclerView.setVisibility(View.VISIBLE);
                                    TeamMembersListAdapter teamMembersAdapter = new TeamMembersListAdapter(TeamMembersListActivity.this, teamMembersArrayList);
                                    tpRecyclerView.setAdapter(teamMembersAdapter);
                                    dismissProgressDialog();
                                }
                            } else {
                                tpRecyclerView.setVisibility(View.GONE);
                                team_task_nomessages.setVisibility(View.VISIBLE);
                                dismissProgressDialog();
                            }
                        } else {
                            tpRecyclerView.setVisibility(View.GONE);
                            team_task_nomessages.setVisibility(View.VISIBLE);
                            dismissProgressDialog();
                        }
                    }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }


}
