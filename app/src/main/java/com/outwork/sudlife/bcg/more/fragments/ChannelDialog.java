package com.outwork.sudlife.bcg.more.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.more.adapter.ChannelTypesAdapter;
import com.outwork.sudlife.bcg.more.models.ChannelDto;

import java.util.ArrayList;
import java.util.List;


public class ChannelDialog extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView mylist;

    ChannelDto channelDto;
    List<ChannelDto> channelDtoList = new ArrayList<ChannelDto>();


    public static ChannelDialog newInstance() {
        ChannelDialog fragment = new ChannelDialog();
        return fragment;
    }

    public ChannelDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            channelDtoList = (List<ChannelDto>)bundle.getSerializable("ChannelList");
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_expcategory, null, false);
        mylist = (ListView) view.findViewById(R.id.categorydialog);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setTitle("Select Category");

        ChannelTypesAdapter channelTypesAdapter = new ChannelTypesAdapter(getActivity(),channelDtoList);
        mylist.setAdapter(channelTypesAdapter);

        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                channelDto = (ChannelDto) parent.getItemAtPosition(position);

                mListener.onFragmentInteraction(channelDto);
                dismiss();
            }
        });
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(channelDto);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
           /* throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");*/
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //public void onFragmentInteraction(Uri uri);
        public void onFragmentInteraction(ChannelDto channelDto);
    }
}
