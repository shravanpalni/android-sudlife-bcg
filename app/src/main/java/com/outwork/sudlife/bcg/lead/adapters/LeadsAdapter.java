package com.outwork.sudlife.bcg.lead.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.lead.LeadMgr;
import com.outwork.sudlife.bcg.lead.activities.ViewLeadActivity;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.LeadViewHolder> implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;
    private List<LeadModel> leadModels;
    private LeadFilter leadFilter;
    private String type, stage;
    private List<LeadModel> leadModelArrayList = new ArrayList<>();

    public static class LeadViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvCityName, tvconversionprop, tvLeadStatus, leadcode,status;
        //View status;
        CardView cardview;

        public LeadViewHolder(View v) {
            super(v);
            tvName = (TextView) itemView.findViewById(R.id.tv_branchname);
            tvCityName = (TextView) itemView.findViewById(R.id.tv_cityName);
            tvconversionprop = (TextView) itemView.findViewById(R.id.cnvsprop);
            tvLeadStatus = (TextView) itemView.findViewById(R.id.lead_status);
            leadcode = (TextView) itemView.findViewById(R.id.leadcode);
            status = (TextView) itemView.findViewById(R.id.status);
            cardview = (CardView) itemView.findViewById(R.id.cv_lead);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, tvName);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, tvCityName, tvconversionprop, tvLeadStatus, leadcode);
        }
    }

    public LeadsAdapter(List<LeadModel> leadModels, Context context, String type, String stage) {
        this.context = context;
        this.leadModels = leadModels;
        this.leadModelArrayList = leadModels;
        this.type = type;
        this.stage = stage;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public LeadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lead_list_item, parent, false);
        return new LeadViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeadViewHolder holder, final int position) {
        final LeadModel dto = (LeadModel) this.leadModels.get(position);
        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname()))
            stringBuilder.append(dto.getFirstname());
        if (Utils.isNotNullAndNotEmpty(dto.getLastname()))
            stringBuilder.append(" " + dto.getLastname());
        StringBuilder branchBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname()))
            branchBuilder.append(dto.getBankbranchname());
        if (Utils.isNotNullAndNotEmpty(dto.getBank()))
            branchBuilder.append(" ( " + dto.getBank() + " )");
        holder.tvName.setText(stringBuilder.toString());
        holder.tvCityName.setText(branchBuilder.toString());
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage()))
            holder.tvLeadStatus.setText(dto.getLeadstage());
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            holder.tvconversionprop.setText(dto.getStatus());
        } else {
            holder.tvconversionprop.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode()))
            holder.leadcode.setText(dto.getLeadcode());
        if (Utils.isNotNullAndNotEmpty(leadModels.get(position).getNetwork_status())) {
            if (leadModels.get(position).getNetwork_status().equalsIgnoreCase("offline")) {
                holder.status.setText("Offline");
                holder.status.setVisibility(View.VISIBLE);

            } else {
                holder.status.setVisibility(View.GONE);
            }
        } else {
            holder.status.setVisibility(View.GONE);
        }
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewLeadActivity.class);
                intent.putExtra("leadobj", new Gson().toJson(dto));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (leadModels.size() > 0) {
            return leadModels.size();
        } else return 0;
    }

    @Override
    public Filter getFilter() {
        if (leadFilter == null)
            leadFilter = new LeadFilter();
        return leadFilter;
    }

    private class LeadFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                results.values = leadModelArrayList;
                results.count = leadModelArrayList.size();
            } else if (Utils.isNotNullAndNotEmpty(type) && stage != null) {
                if (type.equalsIgnoreCase("name")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyName(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("branchname")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyBranchname(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("bankname")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyBankname(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("contactno")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyContactno(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("sudcode")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbySUDCode(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                }
            } else {
                List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                        getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                results.values = fileredCustList;
                results.count = fileredCustList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                leadModels = (List<LeadModel>) results.values;
                notifyDataSetChanged();
            } else {
                leadModels = leadModelArrayList;
                notifyDataSetChanged();
            }
        }
    }
}