package com.outwork.sudlife.bcg.more.models;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChannelDto {

    private String channelId;
    private String channelType;
    private String channelName;
    private String channelTitle;
    private String channelCode;


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }




    public static final String JSON_CHANNEL_ID = "channelid";
    public static final String JSON_CHANNELTITLE="Title";
    public static final String JSON_CHANNELNAME = "channelname";
    public static final String JSON_CHANNELTYPE = "type";


    public static ChannelDto parse(JSONObject feedObject) throws JSONException {

        ChannelDto channelDto = new ChannelDto();

        if (!TextUtils.isEmpty(feedObject.getString(JSON_CHANNEL_ID))) {
            channelDto.setChannelId(feedObject.getString(JSON_CHANNEL_ID));
        }


        if (!TextUtils.isEmpty(feedObject.getString(JSON_CHANNELTITLE))) {
            channelDto.setChannelTitle(feedObject.getString(JSON_CHANNELTITLE));
        }

        if (!TextUtils.isEmpty(feedObject.getString(JSON_CHANNELNAME))) {
            channelDto.setChannelName(feedObject.getString(JSON_CHANNELNAME));
        }

        if (!TextUtils.isEmpty(feedObject.getString(JSON_CHANNELTYPE))) {
            channelDto.setChannelType(feedObject.getString(JSON_CHANNELTYPE));
        }

        return channelDto;
    }


    public static List<ChannelDto> parseChannelArray(JSONArray channelArray) throws JSONException {

        List<ChannelDto> channelDtoList = new ArrayList<ChannelDto>();

        for (int i = 0; i < channelArray.length(); i++) {
            ChannelDto channelDto = new ChannelDto();

            JSONObject attendance = channelArray.getJSONObject(i);
            if (!TextUtils.isEmpty(attendance.getString(JSON_CHANNEL_ID)) && !(attendance.getString(JSON_CHANNEL_ID).equalsIgnoreCase("null"))) {
                channelDto.setChannelId(attendance.getString(JSON_CHANNEL_ID));
            }

            if (!TextUtils.isEmpty(attendance.getString(JSON_CHANNELNAME)) && !(attendance.getString(JSON_CHANNELNAME).equalsIgnoreCase("null"))) {
                channelDto.setChannelName(attendance.getString(JSON_CHANNELNAME));
            }


            channelDtoList.add(channelDto);
        }

        return channelDtoList;
    }


    public static final String JSON_FORM_ID = "formid";
    public static final String JSON_FORMNAME="formname";


    public static List<ChannelDto> parseFormArray(JSONArray formArray) throws JSONException {

        List<ChannelDto> channelDtoList = new ArrayList<ChannelDto>();

        for (int i = 0; i < formArray.length(); i++) {
            ChannelDto channelDto = new ChannelDto();

            JSONObject attendance = formArray.getJSONObject(i);
            if (!TextUtils.isEmpty(attendance.getString(JSON_FORM_ID)) && !(attendance.getString(JSON_FORM_ID).equalsIgnoreCase("null"))) {
                channelDto.setChannelId(attendance.getString(JSON_FORM_ID));
            }

            if (!TextUtils.isEmpty(attendance.getString(JSON_FORMNAME)) && !(attendance.getString(JSON_FORMNAME).equalsIgnoreCase("null"))) {
                channelDto.setChannelName(attendance.getString(JSON_FORMNAME));
            }


            channelDtoList.add(channelDto);
        }

        return channelDtoList;
    }



}