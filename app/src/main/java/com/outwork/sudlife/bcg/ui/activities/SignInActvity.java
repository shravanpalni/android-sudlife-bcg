package com.outwork.sudlife.bcg.ui.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.services.ContactsIntentService;
import com.outwork.sudlife.bcg.core.LocationProvider;
import com.outwork.sudlife.bcg.dto.DeviceInfo;
import com.outwork.sudlife.bcg.dto.UserDetailsDto;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.bcg.notifications.firebase.PlayServicesHelper;
import com.outwork.sudlife.bcg.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.service.PlannerIntentService;
import com.outwork.sudlife.bcg.planner.service.SupervisorIntentService;
import com.outwork.sudlife.bcg.restinterfaces.GeneralService;
import com.outwork.sudlife.bcg.restinterfaces.POJO.PermissionSettings;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.restinterfaces.UserRestInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.base.GenericWaitActivity;
import com.outwork.sudlife.bcg.ui.models.LoginModel;
import com.outwork.sudlife.bcg.ui.models.UserTeams;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActvity extends BaseActivity implements LocationProvider.LocationCallback {

    private EditText userNameField;
    private EditText passwordField;
    private TextView forgotpwd, signInBtn, terms_txt;
    private CheckBox passwordVisible, terms;
    private String apprelease = "";
    private String appversion = "";
    private String libversion = "";
    private String os = "";
    private String osversion = "";
    private String model = "";
    private String manufacturer = "";
    private DeviceInfo deviceInfo;
    private boolean isLocationEnabled = false;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private LocationManager locationManager;
    private Location location;
    private double latitude;
    private double longitude;
    private String provider_info;
    private boolean isGPSEnabled, isNetworkEnabled, canGetLocation;
    public static final int REQUEST_PERMISSION_LOCATION = 16;
    public static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        deviceInfo = Utils.getInfosAboutDevice(this, 1);
        apprelease = deviceInfo.getApprelease();
        appversion = deviceInfo.getAppversioncode();
        libversion = deviceInfo.getLibversion();
        os = deviceInfo.getOs();
        osversion = deviceInfo.getOsversion();
        model = deviceInfo.getModel();
        manufacturer = deviceInfo.getManufacturer();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        setContentView(R.layout.activity_signin);
        userNameField = (EditText) findViewById(R.id.userNameField);
        passwordField = (EditText) findViewById(R.id.passwordField);
        forgotpwd = (TextView) findViewById(R.id.forgotpassword);
        signInBtn = (TextView) findViewById(R.id.signInButton);
        passwordVisible = (CheckBox) findViewById(R.id.passwordVisibility);
        terms = (CheckBox) findViewById(R.id.terms);
        terms_txt = (TextView) findViewById(R.id.terms_txt);
        setListeners();
        initGoogleAPIClient();
        checkPermissions();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, terms_txt, (EditText) findViewById(R.id.userNameField), (EditText) findViewById(R.id.passwordField));
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, signInBtn, forgotpwd);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setListeners() {
        passwordVisible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passwordField.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    passwordField.setInputType(129);
                }
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                terms.setChecked(false);
                hideKeyboard();
                showAlert("", "By accepting you authorize \n" + "to collect your Personal Information (PI) like: name,mobile number,location.\n" +
                        "For details on PI handling, refer to our privacy policy available on \n" + "SUD Life website including ‘Third Party Redirection’ section.", "Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        terms.setChecked(true);
                    }
                }, "Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        terms.setChecked(false);
                    }
                }, true);
            }
        });
        forgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(SignInActvity.this, ForgotPasswordActivity.class));
                startActivity(new Intent(SignInActvity.this, NewForgotPasswordActivity.class));
            }
        });
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if (validate()) {
                    if (isNetworkAvailable()) {
                        showProgressDialog("Signing In.....");
                        connectToServer(userNameField.getText().toString(), passwordField.getText().toString());
                    } else {
                        showToast("No Internet Connection......");
                    }
                }
            }
        });
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActvity.this, SignupActivity.class));
                finish();
            }
        });
    }

    private boolean validate() {
        boolean isValid = true;
        if (userNameField.getText().toString().length() == 0) {
            isValid = false;
            userNameField.setError("Please enter valid Username");
            return isValid;
        }
        if (passwordField.getText().toString().length() == 0) {
            isValid = false;
            passwordField.setError("Please enter your password");
            return isValid;
        }
        if (!terms.isChecked()) {
            isValid = false;
            showToast("Please accept Terms and Conditions");
            return isValid;
        }
        return isValid;
    }

    private void connectToServer(final String userName, final String password) {
        UserDetailsDto userDetails = new UserDetailsDto();
        userDetails.setUserName(userName);
        userDetails.setPassword(password);
        SharedPreferenceManager.getInstance().putString(Constants.password, password);
        signInUser(userDetails);
    }

    public void signInUser(final UserDetailsDto userDetailsDto) {
        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.login(userDetailsDto.getUserName(), userDetailsDto.getPassword());
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    String code = jsonResponse.getCode();
                    String status = jsonResponse.getStatus();
                    if (Constants.STATUS_FAILED.equalsIgnoreCase(status) || Constants.STATUS_FAILURE.equalsIgnoreCase(status)) {
                        if (code.equals("OWAPI-500")) {
                            Utils.displayToast(SignInActvity.this, "Incorrect username or password");
                            setSigninProgress();
                        } else {
                            Utils.displayToast(SignInActvity.this, "Incorrect username or password");
                            setSigninProgress();
                        }
                    } else {
                        LoginModel loginModel = new Gson().fromJson(jsonResponse.getData(), LoginModel.class);
                        if (loginModel != null) {
                            storeSharedPreferences(loginModel);
                            new PlayServicesHelper(SignInActvity.this);
                            if (!Utils.isNotNullAndNotEmpty(loginModel.getLogintime())) {
                                Intent in = new Intent(SignInActvity.this, ResetPasswordActivity.class);
                                in.putExtra("screenfrom", "Signin");
                                startActivity(in);
                                finish();
                            } else {
                                startMainActivity();
                            }
                        } else {
                            Utils.displayToast(SignInActvity.this, "Please check your network connection and try again");
                            setSigninProgress();
                        }
                    }
                } else {
                    Utils.displayToast(SignInActvity.this, "Incorrect username or password");
                    setSigninProgress();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                Utils.displayToast(SignInActvity.this, "Please check your network connection and try again");
                setSigninProgress();
            }
        });
    }

    private void setSigninProgress() {
        signInBtn.setText("Login");
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void startMainActivity() {
        if (isLocationEnabled == false) {
            SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
            updatePermissions();
        }
        startOutWork();
    }

    private void startOutWork() {
        SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.CONTACTS_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.PROFILE_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.SUPERVISORS_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.STAGES_LOADED, "notloaded");
        SharedPreferenceManager.getInstance().putString(Constants.PROPOSAL_CODES_LOADED, "notloaded");

        if (isNetworkEnabled) {
            ProductListDownloadService.getProductListinDB(SignInActvity.this);
            ProductListDownloadService.getMasterListinDB(SignInActvity.this);
            ContactsIntentService.insertCustomersinDB(SignInActvity.this);
            //ContactsIntentService.insertHierarchyCustomersinDB(SignInActvity.this);
            OpportunityIntentService.insertOpportunityStages(SignInActvity.this);
            SupervisorIntentService.insertSupervisorsList(SignInActvity.this);

            LeadIntentService.insertLeadstoServer(SignInActvity.this);
            LeadIntentService.insertProposalCodes(SignInActvity.this);
            List<String> stringList = new ArrayList<>();
            stringList.add(Utils.getPreviousMonthDate(new Date()));
            stringList.add(TimeUtils.getCurrentDate("MM-yyyy"));
            stringList.add(Utils.getNextMonthDate(new Date()));
            for (int i = 0; i < stringList.size(); i++) {
                String[] date = stringList.get(i).split("-");
                PlannerIntentService.insertPlanList(SignInActvity.this, date[0], date[1]);
            }
//            startService(new Intent(this, ProfileIntentService.class));
        }
        Intent in = new Intent(SignInActvity.this, GenericWaitActivity.class);
        startActivity(in);
        finish();
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(SignInActvity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else {
            }
        } else
            showSettingDialog();
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        int fineLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED || coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignInActvity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    /* Show Location Access Dialog */
    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(60 * 1000);
        locationRequest.setFastestInterval(60 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        isLocationEnabled = true;
                        SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
//                        updateGPSStatus("GPS is Enabled in your device");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(SignInActvity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        isLocationEnabled = false;
                        SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "Result OK");
                        isLocationEnabled = true;
//                        updateGPSStatus("GPS is Enabled in your device");
                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("Settings", "Result Cancel");
                        isLocationEnabled = false;
                        SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                        updateGPSStatus("GPS is Disabled in your device");
                        finish();
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //If permission granted show location dialog if APIClient is not null
                        if (mGoogleApiClient == null) {
                            initGoogleAPIClient();
                            isLocationEnabled = true;
                            SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                        } else {
                            isLocationEnabled = true;
                            SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                        }
                    } else {
                        isLocationEnabled = false;
                        SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                        updateGPSStatus("Location Permission denied.");
                    }
                } else {
                    isLocationEnabled = false;
                    SharedPreferenceManager.getInstance().putBoolean(Constants.isLocationEnabled, isLocationEnabled);
                    updateGPSStatus("Location Permission denied.");
                }
                return;
            }
        }
    }

    /**
     * Update GPSTracker latitude and longitude
     */
    public void updateGPSCoordinates() {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    private void updatePermissions() {
        PermissionSettings permissionSettings = new PermissionSettings();
        permissionSettings.setCoarselocation("0");
        permissionSettings.setFinelocation("0");
        GeneralService client = RestService.createService(GeneralService.class);
        Call<RestResponse> permission = client.updatepermissions(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""),
                SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), permissionSettings);
        permission.enqueue(new Callback<RestResponse>() {

            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
    }

    private void storeSharedPreferences(LoginModel dto) {
        SharedPreferenceManager.getInstance().putString(Constants.USER_TOKEN, dto.getUsertoken());
        SharedPreferenceManager.getInstance().putString(Constants.LOGINID, dto.getLoginID());
        SharedPreferenceManager.getInstance().putString(Constants.LOGININTIME, dto.getLogintime());
        if (Utils.isNotNullAndNotEmpty(dto.getInternalrole())) {
            SharedPreferenceManager.getInstance().putString(Constants.INTERNALROLE, dto.getInternalrole());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getDisplayrole())) {
            SharedPreferenceManager.getInstance().putString(Constants.DISPLAYROLE, dto.getDisplayrole());
        }
        if (dto.getFeatures() != null) {
            if (dto.getFeatures().getFeatured()) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.isFeatureEnabled, true);
            }
            if (dto.getFeatures().getIsorderproductavailable()) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.isOrderProductsAvailable, true);
            }
            if (dto.getFeatures().getIsproductavailable()) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.isProductEnabled, true);
            }
            if (dto.getFeatures().getIshealthcare()) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.isHealthCare, true);
            }
            if (dto.getFeatures().getIstaskavailable()) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.isTaskAvailable, true);
            }
        }
        if (dto.getOrganization() != null) {
            SharedPreferenceManager.getInstance().putString(Constants.GROUPID, dto.getOrganization().getOrganizationid());
            SharedPreferenceManager.getInstance().putString(Constants.GRP_DISPLAYNAME, dto.getOrganization().getDisplayname());
            SharedPreferenceManager.getInstance().putString(Constants.GROUPTITLE, dto.getOrganization().getName());
            SharedPreferenceManager.getInstance().putString(Constants.GRP_ICON, dto.getOrganization().getIconurl());
            if (Utils.isNotNullAndNotEmpty(dto.getOrganization().getBannerimageurl()))
                SharedPreferenceManager.getInstance().putString(Constants.GRP_IMAGE, dto.getOrganization().getBannerimageurl().get(0));
            SharedPreferenceManager.getInstance().putString(Constants.GRP_CODE, dto.getOrganization().getCode());
        }
        if (dto.getUserprofile() != null) {
            SharedPreferenceManager.getInstance().putString(Constants.GROUPID, dto.getUserprofile().getOrganizationid());
            SharedPreferenceManager.getInstance().putString(Constants.USERID, dto.getUserprofile().getUserid());
            SharedPreferenceManager.getInstance().putString(Constants.USERNAME, dto.getUserprofile().getDisplayname());
            SharedPreferenceManager.getInstance().putString(Constants.FIRSTNAME, dto.getUserprofile().getFirstname());
            SharedPreferenceManager.getInstance().putString(Constants.LASTNAME, dto.getUserprofile().getLastname());
            SharedPreferenceManager.getInstance().putString(Constants.GRP_OBJECTTYPE, dto.getUserprofile().getRole());
            SharedPreferenceManager.getInstance().putString(Constants.USER_EMAIL, dto.getUserprofile().getEmail());
            SharedPreferenceManager.getInstance().putString(Constants.EMAIL, dto.getUserprofile().getEmail());
            SharedPreferenceManager.getInstance().putString(Constants.LOGINTYPE, dto.getUserprofile().getRole());
            SharedPreferenceManager.getInstance().putString(Constants.PROFILE_URL, dto.getUserprofile().getAvatar());
            SharedPreferenceManager.getInstance().putString(Constants.PHONENO, dto.getUserprofile().getPhonenumber());
            SharedPreferenceManager.getInstance().putString(Constants.SALUTATION, dto.getUserprofile().getSalutation());
            SharedPreferenceManager.getInstance().putString(Constants.WEBSITE, dto.getUserprofile().getWebsite());
            SharedPreferenceManager.getInstance().putString(Constants.EMPLOYEETYPE, dto.getUserprofile().getEmployeetype());
            SharedPreferenceManager.getInstance().putString(Constants.SUPERVISOREMPTYPE, dto.getUserprofile().getSupervisoremptype());
            SharedPreferenceManager.getInstance().putInt(Constants.USERLEVEL, dto.getUserprofile().getUserlevel());
        }


        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""))) {
            getAllTeamsForUser();
        }


    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            //getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            // Try to get location if you GPS Service is enabled
            if (isGPSEnabled) {
                this.canGetLocation = true;
                Log.d("abc", "Application use GPS Service");
                /*
                 * This provider determines location using
                 * satellites. Depending on conditions, this provider may take a while to return
                 * a location fix.
                 */
                provider_info = LocationManager.GPS_PROVIDER;
            } else if (isNetworkEnabled) { // Try to get location if you Network Service is enabled
                this.canGetLocation = true;
                Log.d("abc", "Application use Network State to get GPS coordinates");
                /*
                 * This provider determines location based on
                 * availability of cell tower and WiFi access points. Results are retrieved
                 * by means of a network lookup.
                 */
                provider_info = LocationManager.NETWORK_PROVIDER;
            }

            // Check if we have location permission
            int fineLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (fineLocationPermission != PackageManager.PERMISSION_GRANTED || coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        SignInActvity.PERMISSIONS_LOCATION,
                        SignInActvity.REQUEST_PERMISSION_LOCATION);
            } else {
                // Application can use GPS or Network Provider
                if (!provider_info.isEmpty()) {
//                    locationManager.requestLocationUpdates(
//                            provider_info,
//                            MIN_TIME_BW_UPDATES,
//                            MIN_DISTANCE_CHANGE_FOR_UPDATES,
//                            new LocListener()
//                    );

                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(provider_info);
                        updateGPSCoordinates();
                    }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Log.e("abc", "Impossible to connect to LocationManager", e);
        }
        return location;
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
        }
    }






    public void getAllTeamsForUser() {
        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.getallteams(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    if (!TextUtils.isEmpty(response.body().getData())) {

                        Type listType = new TypeToken<ArrayList<UserTeams>>() {
                        }.getType();

                        List<UserTeams> userTeamsList = new Gson().fromJson(response.body().getData(), listType);

                        if (userTeamsList.size() > 0) {
                           Log.i("shravan","UserTeamsList signin size = = = ="+userTeamsList.size());
                            PlannerMgr.getInstance(SignInActvity.this).insertUserTeamsList(userTeamsList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));

                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

                Utils.displayToast(SignInActvity.this, "Please check your network connection and try again");

            }
        });
    }
}
