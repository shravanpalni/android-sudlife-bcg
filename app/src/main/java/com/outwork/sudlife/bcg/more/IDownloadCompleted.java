package com.outwork.sudlife.bcg.more;

/**
 * Created by Panch on 8/11/2015.
 */
public interface IDownloadCompleted {

    public boolean setSuccessResponse();

    public boolean setError();
}
