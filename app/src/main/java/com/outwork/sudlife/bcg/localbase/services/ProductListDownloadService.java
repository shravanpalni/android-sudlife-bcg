package com.outwork.sudlife.bcg.localbase.services;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.localbase.ProductDataMgr;
import com.outwork.sudlife.bcg.localbase.ProductMasterDto;
import com.outwork.sudlife.bcg.localbase.StaticDataDto;
import com.outwork.sudlife.bcg.localbase.StaticDataMgr;
import com.outwork.sudlife.bcg.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.bcg.restinterfaces.ProductService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListDownloadService extends JobIntentService {
    public static final String TAG = ProductListDownloadService.class.getSimpleName();

    public static final String ACTION_GET_PRODUCTLIST = "com.outwork.sudlife.bcg.localbase.services.action.GET_PRODUCTLIST";
    public static final String ACTION_GET_MASTERLIST = "com.outwork.sudlife.bcg.localbase.services.action.GET_MASTERLIST";
    private LocalBroadcastManager mgr;
    public static final Integer JOBID = 1001;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_PRODUCTLIST.equals(action)) {
                syncProductList();
            }
            if (ACTION_GET_MASTERLIST.equals(action)) {
                syncStaticList();
            }
        }
    }

    public static void getProductListinDB(Context context) {
        Intent intent = new Intent(context, ProductListDownloadService.class);
        intent.setAction(ACTION_GET_PRODUCTLIST);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ProductListDownloadService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void getMasterListinDB(Context context) {
        Intent intent = new Intent(context, ProductListDownloadService.class);
        intent.setAction(ACTION_GET_MASTERLIST);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ProductListDownloadService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void syncProductList() {
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        final String groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");

        ProductService client = RestService.createServicev1(ProductService.class);
        Call<RestResponse> getProducts = client.getMasterProducts(userToken);
        getProducts.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<ProductMasterDto>>() {
                        }.getType();
                        List<ProductMasterDto> productList = new Gson().fromJson(response.body().getData(), listType);
                        if (productList.size() > 0) {
                            new ProductDataMgr(ProductListDownloadService.this).insertMasterProductList(productList, groupId);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void syncStaticList() {
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        final String groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        ProductService client = RestService.createServicev1(ProductService.class);
        Call<RestResponse> getMasterList = client.getMasterList(userToken, groupId);
        getMasterList.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (!TextUtils.isEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<StaticDataDto>>() {
                        }.getType();
                        List<StaticDataDto> staticList = new Gson().fromJson(response.body().getData(), listType);
                        if (staticList.size() > 0) {
                            new StaticDataMgr(getApplicationContext()).insertStaticList(staticList, groupId);
                        }
                        OpportunityIntentService.insertOpportunityStages(ProductListDownloadService.this);
                        Intent intent = new Intent("masterlist_broadcast");
                        mgr = LocalBroadcastManager.getInstance(ProductListDownloadService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}