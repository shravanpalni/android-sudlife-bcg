package com.outwork.sudlife.bcg.restinterfaces;

import com.outwork.sudlife.bcg.dto.Geocode;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Panch on 2/17/2017.
 */
public interface ReverseGeoInterface {
    String MAP_URL = "https://maps.googleapis.com";

    @GET("/maps/api/geocode/json")
    Call<Geocode> getLocation(@Query("location_type") String lctype,
                              @Query("latlng") String input,
                              @Query("key") String key);
}

