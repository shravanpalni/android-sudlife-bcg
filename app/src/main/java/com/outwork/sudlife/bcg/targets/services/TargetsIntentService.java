package com.outwork.sudlife.bcg.targets.services;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.targets.db.MSBDao;
import com.outwork.sudlife.bcg.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.bcg.targets.models.MSBModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bvlbh on 3/26/2018.
 */

public class TargetsIntentService extends JobIntentService {
    public static final String TAG = TargetsIntentService.class.getSimpleName();

    public static final String ACTION_GET_LIST_OF_BRANCHES = "com.outwork.sudlife.bcg.targets.services.action.GET_LIST_OF_BRANCHES";
    public static final String ACTION_UPDATE_TARGET_RECORD = "com.outwork.sudlife.bcg.targets.services.action.UPDATE_TARGET_RECORD";
    private LocalBroadcastManager mgr;
    private static final Integer JOBID = 1005;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (ACTION_GET_LIST_OF_BRANCHES.equals(intent.getAction())) {
            getListOfTargets();
        }
        if (ACTION_UPDATE_TARGET_RECORD.equals(intent.getAction())) {
            syncOfflineMTPs();
        }
    }

    public static void insertTargetRecords(Context context) {
        Intent intent = new Intent(context, TargetsIntentService.class);
        intent.setAction(ACTION_GET_LIST_OF_BRANCHES);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, TargetsIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void updateRecord(Context context) {
        Intent intent = new Intent(context, TargetsIntentService.class);
        intent.setAction(ACTION_UPDATE_TARGET_RECORD);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, TargetsIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public void getListOfTargets() {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LAST_FETCH_TIME, "");
        List<MSBModel> msbModelList = MSBDao.getInstance(TargetsIntentService.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (msbModelList.size() == 0) {
            lastfetchtime = "";
        }
        String monthno = Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");

        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTargets(userToken, monthno, year);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.body() != null) {
                    Type listType = new TypeToken<ArrayList<MSBModel>>() {
                    }.getType();
                    ArrayList<MSBModel> targetsArrayList = new Gson().fromJson(response.body().getData(), listType);
                    if (targetsArrayList != null) {
                        if (targetsArrayList.size() > 0) {
                            TargetsMgr.getInstance(TargetsIntentService.this).insertTargetsList(targetsArrayList);
                        }
                    }
                    SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LOADED, "loaded");
                    SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LAST_FETCH_TIME, newFetchTime);
                    Intent intent = new Intent("targets_broadcast");
                    mgr = LocalBroadcastManager.getInstance(TargetsIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LOADED, "notloaded");
            }
        });
    }

    public void getListOfTargetsTillDate() {
        String monthno = Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String date = Utils.formatDateFromString("yyyy-MM-dd", "dd", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");

        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getUserTargetstillDate(userToken, monthno, year, date);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                Type listType = new TypeToken<ArrayList<MSBModel>>() {
                }.getType();
                if (response.body() != null) {
                    ArrayList<MSBModel> targetsArrayList = new Gson().fromJson(response.body().getData(), listType);
                    if (targetsArrayList != null) {
                        if (targetsArrayList.size() > 0) {
                            TargetsMgr.getInstance(TargetsIntentService.this).insertTargetsList(targetsArrayList);
                        }
                    }
                    SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LOADED, "loaded");
                    Intent intent = new Intent("targets_broadcast");
                    mgr = LocalBroadcastManager.getInstance(TargetsIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                SharedPreferenceManager.getInstance().putString(Constants.TARGETS_LOADED, "notloaded");
            }
        });
    }


    public void updateTargetRecord(final MSBModel msbModel) {
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        RestServicesForTragets client = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> updateTarget = client.updateTarget(userToken, msbModel.getTargetid(), msbModel);
        try {
            updateTarget.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                    int statusCode = response.code();
                    if (statusCode == 200) {
                        if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                            TargetsMgr.getInstance(TargetsIntentService.this).updateTargetRecordByStatus(msbModel);
                            Intent intent = new Intent("targets_broadcast");
                            mgr = LocalBroadcastManager.getInstance(TargetsIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    //   Toast.makeText(TargetsIntentService.this, "", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncOfflineMTPs() {
//        final ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(TargetsIntentService.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
//        ArrayList<MSBModel> offlineFarmerList = new ArrayList<>();
//        if (msbModelArrayList != null) {
//            for (int i = 0; i < msbModelArrayList.size(); i++) {
//                if (msbModelArrayList.get(i).getOfflineoronlinestatus() != null) {
//                    if (msbModelArrayList.get(i).getOfflineoronlinestatus().equalsIgnoreCase("offline")) {
//                        offlineFarmerList.add(msbModelArrayList.get(i));
//                    }
//                }
//            }
//        }
        final ArrayList<MSBModel> offlineFarmerList = TargetsMgr.getInstance(TargetsIntentService.this).getMSBOfflineRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");

        if (offlineFarmerList.size() > 0) {
            for (final MSBModel msbModel : offlineFarmerList) {
                if (Utils.isNotNullAndNotEmpty(msbModel.getOfflineoronlinestatus())) {
                    if (isNetworkAvailable()) {
                        if (msbModel.getId().length() > 0) {
                            updateTargetRecord(msbModel);
                        } else {
                        }
                    }
                }
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }
}
