package com.outwork.sudlife.bcg.restinterfaces;

import com.outwork.sudlife.bcg.ui.models.ProfileModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Panch on 3/21/2016.
 */
public interface ProfileService {
    @GET("AppUserService/user/{userid}/profile")
    Call<RestResponse> getUserProfile(@Header("utoken") String userToken, @Path("userid") String userid);

    @POST("AppUserService/user/profile")
    Call<RestResponse> updateUserProfile(@Header("utoken") String userToken, @Body ProfileModel profileModel);


    @PUT("mobile/v1/user/profile")
    Call<RestResponse> editUserProfile(@Header("utoken") String userToken, @Body ProfileModel profileModel);

    @GET("mobile/v1/user/profile")
    Call<RestResponse> getUserProfile(@Header("utoken") String userToken, @Body ProfileModel profileModel);

}