package com.outwork.sudlife.bcg.lead.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class LeadDao {
    public static final String TAG = LeadDao.class.getSimpleName();

    private static LeadDao instance;
    private Context applicationContext;

    public static LeadDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new LeadDao(applicationContext);
        }
        return instance;
    }

    private LeadDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            } else {
                cursor.close();
            }
        }
        return false;
    }

    //leads
    public long insertLocalLead(LeadModel dto, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {         //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {         //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }




        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getCreatedon() != null) {
            values.put(IvokoProvider.Lead.createdon, dto.getCreatedon());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        values.put(IvokoProvider.Lead.network_status, network_status);

        long leadID = db.insert(IvokoProvider.Lead.TABLE, null, values);
        return leadID;
    }

    public long insertLead(SQLiteDatabase db, LeadModel dto) {
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }


        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {         //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {          //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }



        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getCreatedon() != null) {
            values.put(IvokoProvider.Lead.createdon, dto.getCreatedon());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        long leadID = db.insert(IvokoProvider.Lead.TABLE, null, values);
        return leadID;
    }

    public void updateLead(LeadModel dto, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {                 //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {                 //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }



        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        values.put(IvokoProvider.Lead.network_status, network_status);

        boolean success = db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = ' " + dto.getId() + " ' AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{dto.getUserid()}) >0;



        // boolean success =  db.insert(IvokoProvider.Lead.TABLE, null, values)>0;
         Log.i("shravan","lead updated successfully = = = ="+success);

    }

    public void insertLeadsList(List<LeadModel> leadModelList, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < leadModelList.size(); j++) {
            LeadModel leadModel = leadModelList.get(j);
            if (!isLeadRecordExist(db, leadModel.getLeadid())) {
                if (!leadModel.getIsdeleted())
                    insertLead(db, leadModel);
            } else {
                if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
                    deleteLeadRecordID(leadModel.getLeadid());
                    if (!leadModel.getIsdeleted())
                        insertLead(db, leadModel);
                }
            }
        }
    }

    public List<LeadModel> getLeadsList(String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            //security checked
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " DESC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + "  ORDER BY " + IvokoProvider.Lead.lead_created_date + " = ?" , new String[]{userid ," DESC "});


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        try {
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndexOrThrow(IvokoProvider.Lead.first_appointment_date)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadsListonLeadStage(String userid, String leadstage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + leadstage + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.lead_stage + " = ?" , new String[]{userid ,leadstage});

                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;

    }

    public LeadModel getLeadbyId(String leadid, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        LeadModel leadModel = new LeadModel();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.leadid + " = '" + leadid + "'";
            sql += " AND " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.leadid + " = ?" + " AND " + IvokoProvider.Lead.userid + " = ?" + " LIMIT 1" , new String[]{leadid,userid});


                if (cursor.moveToFirst()) {
                    do {
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModel;
    }

    public LeadModel getLeadbylocalId(int leadid, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        LeadModel leadModel = new LeadModel();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead._ID + " = '" + leadid + "'";
            sql += " AND " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead._ID + " = ?" + " AND " + IvokoProvider.Lead.userid + " = ?" + " LIMIT 1" , new String[]{String.valueOf(leadid),userid});

                if (cursor.moveToFirst()) {
                    do {
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModel;
    }

    public List<LeadModel> getOfflineLeadsList(String userId, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        try {
            if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
                Cursor cursor = null;

                // security
                /*String sql = "";
                sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
                sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
                sql += " AND " + IvokoProvider.Lead.network_status + " = '" + network_status + "'";*/
                try {
                    //cursor = db.rawQuery(sql, null);
                    cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.network_status + " = ?"  , new String[]{userId,network_status});

                    if (cursor.moveToFirst()) {
                        do {
                            LeadModel leadModel = new LeadModel();
                            leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                            leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                            leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                            leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                            leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                            leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                            leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                            leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                            leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                            leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                            leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                            leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                            leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                    leadModel.setIsexistingcustomer(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                    leadModel.setIsexistingcustomer(true);
                                } else {
                                    leadModel.setIsexistingcustomer(false);
                                }
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                    leadModel.setIspremiumpaid(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                    leadModel.setIspremiumpaid(true);
                                } else {
                                    leadModel.setIspremiumpaid(false);
                                }
                            leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                            leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                            leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                            leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                            leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                            leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                            leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                            leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                            leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                            leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                            leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                    leadModel.setZccsupportrequired(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                    leadModel.setZccsupportrequired(true);
                                } else {
                                    leadModel.setZccsupportrequired(false);
                                }
                            leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                            if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                                leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                            leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                            leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                            leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                            leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                            leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                            leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                            leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                            leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                            leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                            leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                            leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                            leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                            leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                    leadModel.setIsmarried(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                    leadModel.setIsmarried(true);
                                } else {
                                    leadModel.setIsmarried(false);
                                }
                            leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                            leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                            leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                    leadModel.setIscustomercallconnected(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                    leadModel.setIscustomercallconnected(true);
                                } else {
                                    leadModel.setIscustomercallconnected(false);
                                }
                            leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                            leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                            leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                            leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                            leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                            leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                            leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                            leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                            leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                            leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                            leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                            leadModelList.add(leadModel);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        } catch (IllegalStateException exception) {
            exception.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return leadModelList;
    }

    public boolean isProposalNoExist(String proposalno, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.proposal_number + " = ?", new String[]{userid, proposalno});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean isContactNoExist(String contactNo, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?"
                + " AND " + IvokoProvider.Lead.contactno + " = ?", new String[]{userid, contactNo});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public List<LeadModel> getLeadwithContactNo(String userId, String contactno) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // pending security checked

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " = '" + contactno + "'";
            sql += " AND( " + IvokoProvider.Lead.lead_stage + " = 'Proposition presented' ";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " = 'Open' ";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " = 'Contacted & meeting fixed' )";*/
            try {
                //cursor = db.rawQuery(sql, null);

                //cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.contactno + " = ?" +  , new String[]{String.valueOf(leadid),userid});
                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and contactno=? and (lead_stage=? or lead_stage=? or lead_stage=?) ",
                        new String [] {userId,contactno,"Proposition presented","Open","Contacted & meeting fixed"});

                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        //leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getConvertedLeadwithContactNo(String userId, String contactno) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
  /*          String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " = '" + contactno + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = 'Converted' ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.contactno + " = ?" + " AND " + IvokoProvider.Lead.lead_stage + " = ?", new String[]{userId,contactno,"Converted"});

                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        //leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<String> getLeadsNamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> leadNamesList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            //security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" , new String[]{userId});

                if (cursor.moveToFirst()) {
                    do {
                        StringBuilder stringBuilder = new StringBuilder();
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)))) {
                            stringBuilder.append(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        }
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)))) {
                            if (Utils.isNotNullAndNotEmpty(stringBuilder.toString())) {
                                stringBuilder.append(" " + cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            } else {
                                stringBuilder.append(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            }
                        }
                        leadNamesList.add(stringBuilder.toString());
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadNamesList;
    }

    public List<LeadModel> getLeadListforSearch(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            //security

            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
//            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lastname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.branch_name + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.bank + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.contactno + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.conversion_propensity + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.leadcode + " LIKE '" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and (firstname like ? or lastname like ? or branch_name like ? or bank like ? or contactno like ? or conversion_propensity like ? or leadcode like ?) order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});




                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));
                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyName(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            //security
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lastname + " LIKE '" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and (firstname like ? or lastname like ? ) order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});



                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyBranchname(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.branch_name + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and branch_name like ?  order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyBankname(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.bank + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and bank like ?  order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyContactno(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;


            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and contactno like ?  order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbySUDCode(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.leadcode + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and leadcode like ?  order by lead_created_date=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});

                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListforfilter(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String[] name = userInput.split(" ");
            // pending

            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " = '" + name[0] + "'";
            sql += " AND " + IvokoProvider.Lead.lastname + " = '" + name[1] + "'";
            sql += " OR " + IvokoProvider.Lead.branch_name + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.bank + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.conversion_propensity + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " LIKE '%" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);



                /*cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and (firstname=? and lastname=? or branch_name like ? or bank like ? or conversion_propensity like ? or lead_stage like ?)  order by lead_created_date=? ",
                        new String [] {userId,name[0],name[1],'%' + userInput + '%', " ASC "});*/


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String startdate, String enddate, String leadstage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // pending  security checked

          /*  String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + leadstage + "'";
            sql += " AND " + IvokoProvider.Lead.lead_created_date + " BETWEEN " + "'" + startdate + "'  AND  '" + enddate + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_stage=? and lead_created_date between ? and ?   order by lead_created_date=? ",
                        new String [] {userId,leadstage,startdate,enddate, " ASC "});
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String startdate, String enddate) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;

            // pending security checked

         /*   String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_created_date + " BETWEEN " + "'" + startdate + "'  AND  '" + enddate + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Lead.TABLE+" where "+ "userid=? and lead_created_date between ? and ?   order by lead_created_date=? ",
                        new String [] {userId,startdate,enddate, " ASC "});


                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public void deleteLeadRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.leadid + " = '" + id + "'");
        }
    }

    private boolean isLeadRecordExist(SQLiteDatabase db, String leadID) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.leadid + " = ?", new String[]{leadID});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void updateLeadOnline(LeadModel dto, String userId, String leadId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Lead.leadid, leadId);
        values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        values.put(IvokoProvider.Lead.network_status, "");

        db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{userId});
    }

    public void updateLeaddumyOnline(LeadModel dto, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Lead.network_status, "pending");

        db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{userId});
    }
}








/*
package com.outwork.sudlife.bcg.lead.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class LeadDao {
    public static final String TAG = LeadDao.class.getSimpleName();

    private static LeadDao instance;
    private Context applicationContext;

    public static LeadDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new LeadDao(applicationContext);
        }
        return instance;
    }

    private LeadDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            } else {
                cursor.close();
            }
        }
        return false;
    }

    //leads
    public long insertLocalLead(LeadModel dto, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {         //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {         //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }




        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getCreatedon() != null) {
            values.put(IvokoProvider.Lead.createdon, dto.getCreatedon());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        values.put(IvokoProvider.Lead.network_status, network_status);

        long leadID = db.insert(IvokoProvider.Lead.TABLE, null, values);
        return leadID;
    }

    public long insertLead(SQLiteDatabase db, LeadModel dto) {
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }


        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {         //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {          //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }



        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getCreatedon() != null) {
            values.put(IvokoProvider.Lead.createdon, dto.getCreatedon());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        long leadID = db.insert(IvokoProvider.Lead.TABLE, null, values);
        return leadID;
    }

    public void updateLead(LeadModel dto, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getUserid())) {
            values.put(IvokoProvider.Lead.userid, dto.getUserid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getGroupid())) {
            values.put(IvokoProvider.Lead.groupid, dto.getGroupid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadid())) {
            values.put(IvokoProvider.Lead.leadid, dto.getLeadid());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCampaigndate())) {
            values.put(IvokoProvider.Lead.campaigndate, dto.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            values.put(IvokoProvider.Lead.firstname, dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            values.put(IvokoProvider.Lead.lastname, dto.getLastname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIsnri())) {
            values.put(IvokoProvider.Lead.isnri, dto.getIsnri());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountryname())) {                 //country code
            values.put(IvokoProvider.Lead.countryname, dto.getCountryname());
        }

        if (Utils.isNotNullAndNotEmpty(dto.getCountrycode())) {                 //country code
            values.put(IvokoProvider.Lead.countrycode, dto.getCountrycode());
        }



        if (Utils.isNotNullAndNotEmpty(dto.getContactno())) {
            values.put(IvokoProvider.Lead.contactno, dto.getContactno());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEmail())) {
            values.put(IvokoProvider.Lead.emailid, dto.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAlternatecontactno())) {
            values.put(IvokoProvider.Lead.alternative_contactno, dto.getAlternatecontactno());
        }
        if (dto.getAge() != null) {
            values.put(IvokoProvider.Lead.age, dto.getAge());
        }
        if (dto.getIsexistingcustomer() != null) {
            if (dto.getIsexistingcustomer()) {
                values.put(IvokoProvider.Lead.existing_sublife_customer, true);
            } else {
                values.put(IvokoProvider.Lead.existing_sublife_customer, false);
            }
        }
        if (dto.getIspremiumpaid() != null) {
            if (dto.getIspremiumpaid()) {
                values.put(IvokoProvider.Lead.ispremium_paid, dto.getIspremiumpaid());
            } else {
                values.put(IvokoProvider.Lead.ispremium_paid, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProductname())) {
            values.put(IvokoProvider.Lead.product_name, dto.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getAddress())) {
            values.put(IvokoProvider.Lead.address, dto.getAddress());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCity())) {
            values.put(IvokoProvider.Lead.city, dto.getCity());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPincode())) {
            values.put(IvokoProvider.Lead.pincode, dto.getPincode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
            values.put(IvokoProvider.Lead.occupation, dto.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadsource())) {
            values.put(IvokoProvider.Lead.source_of_lead, dto.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getOtherleadsource())) {
            values.put(IvokoProvider.Lead.other, dto.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBank())) {
            values.put(IvokoProvider.Lead.bank, dto.getBank());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchcode())) {
            values.put(IvokoProvider.Lead.branch_code, dto.getBankbranchcode());
        }
        if (dto.getFamilymemberscount() != null) {
            values.put(IvokoProvider.Lead.no_of_family_members, dto.getFamilymemberscount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname())) {
            values.put(IvokoProvider.Lead.branch_name, dto.getBankbranchname());
        }
        if (dto.getZccsupportrequired() != null) {
            if (dto.getZccsupportrequired()) {
                values.put(IvokoProvider.Lead.zcc_support_requried, dto.getZccsupportrequired());
            } else {
                values.put(IvokoProvider.Lead.zcc_support_requried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getPreferredlanguage())) {
            values.put(IvokoProvider.Lead.preferred_language, dto.getPreferredlanguage());
        }
        if (dto.getPreferreddatetime() != null) {
            values.put(IvokoProvider.Lead.preferred_date, dto.getPreferreddatetime());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            values.put(IvokoProvider.Lead.status, dto.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getSubstatus())) {
            values.put(IvokoProvider.Lead.sub_status, dto.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.expected_actual_premium, dto.getExpectedpremium());
        }
        if (dto.getPremiumpaid() != null) {
            values.put(IvokoProvider.Lead.premium_paid, dto.getPremiumpaid());
        }
        if (dto.getNextfollowupdate() != null) {
            values.put(IvokoProvider.Lead.next_followup_date, dto.getNextfollowupdate());
        }
        if (dto.getFirstappointmentdate() != null) {
            values.put(IvokoProvider.Lead.first_appointment_date, dto.getFirstappointmentdate());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getConversionpropensity())) {
            values.put(IvokoProvider.Lead.conversion_propensity, dto.getConversionpropensity());
        }
        if (dto.getVisitingdatetime() != null) {
            values.put(IvokoProvider.Lead.visiting_time, dto.getVisitingdatetime());
        }
        if (dto.getProposalnumber() != null) {
            values.put(IvokoProvider.Lead.proposal_number, dto.getProposalnumber());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCreatedby())) {
            values.put(IvokoProvider.Lead.createdby, dto.getCreatedby());
        }
        if (dto.getModifiedon() != null) {
            values.put(IvokoProvider.Lead.modifiedon, dto.getModifiedon());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getModifiedby())) {
            values.put(IvokoProvider.Lead.modifiedby, dto.getModifiedby());
        }
        if (dto.getIsmarried() != null) {
            if (dto.getIsmarried()) {
                values.put(IvokoProvider.Lead.ismarried, dto.getIsmarried());
            } else {
                values.put(IvokoProvider.Lead.ismarried, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getIncomeband())) {
            values.put(IvokoProvider.Lead.income_band, dto.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getType())) {
            values.put(IvokoProvider.Lead.lead_type, dto.getType());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks())) {
            values.put(IvokoProvider.Lead.remarks, dto.getRemarks());
        }
        if (dto.getIscustomercallconnected() != null) {
            if (dto.getIscustomercallconnected()) {
                values.put(IvokoProvider.Lead.customer_call_connected, dto.getIscustomercallconnected());
            } else {
                values.put(IvokoProvider.Lead.customer_call_connected, false);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getEducation())) {
            values.put(IvokoProvider.Lead.education, dto.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getExpectedpremium())) {
            values.put(IvokoProvider.Lead.premium_expected, dto.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfor())) {
            values.put(IvokoProvider.Lead.financial_planning_done_for, dto.getFinancialplanningfor());
        }
        if (dto.getTentaiveinvestmentyears() != null) {
            values.put(IvokoProvider.Lead.tentative_investment_years, dto.getTentaiveinvestmentyears());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getFinancialplanningfuturefor())) {
            values.put(IvokoProvider.Lead.financial_planning_in_future_for, dto.getFinancialplanningfuturefor());
        }
        if (dto.getLeadcreatedon() != null) {
            values.put(IvokoProvider.Lead.lead_created_date, dto.getLeadcreatedon());
        }
        if (dto.getTentativeamount() != null) {
            values.put(IvokoProvider.Lead.tentative_amount, dto.getTentativeamount());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode())) {
            values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage())) {
            values.put(IvokoProvider.Lead.lead_stage, dto.getLeadstage());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct1())) {
            values.put(IvokoProvider.Lead.product_one, dto.getProduct1());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getProduct2())) {
            values.put(IvokoProvider.Lead.product_two, dto.getProduct2());
        }
        if (dto.getActualpremiumpaid() != null) {
            values.put(IvokoProvider.Lead.actual_premium, dto.getActualpremiumpaid());
        }
        values.put(IvokoProvider.Lead.network_status, network_status);

        db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = ' " + dto.getId() + " ' AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{dto.getUserid()});



      // boolean success =  db.insert(IvokoProvider.Lead.TABLE, null, values)>0;
       // Log.i("shravan","lead updated successfully = = = ="+success);

    }

    public void insertLeadsList(List<LeadModel> leadModelList, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < leadModelList.size(); j++) {
            LeadModel leadModel = leadModelList.get(j);
            if (!isLeadRecordExist(db, leadModel.getLeadid())) {
                if (!leadModel.getIsdeleted())
                    insertLead(db, leadModel);
            } else {
                if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
                    deleteLeadRecordID(leadModel.getLeadid());
                    if (!leadModel.getIsdeleted())
                        insertLead(db, leadModel);
                }
            }
        }
    }

    public List<LeadModel> getLeadsList(String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " DESC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        try {
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndexOrThrow(IvokoProvider.Lead.first_appointment_date)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadsListonLeadStage(String userid, String leadstage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + leadstage + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;

    }

    public LeadModel getLeadbyId(String leadid, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        LeadModel leadModel = new LeadModel();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.leadid + " = '" + leadid + "'";
            sql += " AND " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModel;
    }

    public LeadModel getLeadbylocalId(int leadid, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        LeadModel leadModel = new LeadModel();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead._ID + " = '" + leadid + "'";
            sql += " AND " + IvokoProvider.Lead.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModel;
    }

    public List<LeadModel> getOfflineLeadsList(String userId, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        try {
            if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
                Cursor cursor = null;
                String sql = "";
                sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
                sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
                sql += " AND " + IvokoProvider.Lead.network_status + " = '" + network_status + "'";
                try {
                    cursor = db.rawQuery(sql, null);
                    if (cursor.moveToFirst()) {
                        do {
                            LeadModel leadModel = new LeadModel();
                            leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                            leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                            leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                            leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                            leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                            leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                            leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                            leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                            leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                            leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                            leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                            leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                            leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                    leadModel.setIsexistingcustomer(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                    leadModel.setIsexistingcustomer(true);
                                } else {
                                    leadModel.setIsexistingcustomer(false);
                                }
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                    leadModel.setIspremiumpaid(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                    leadModel.setIspremiumpaid(true);
                                } else {
                                    leadModel.setIspremiumpaid(false);
                                }
                            leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                            leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                            leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                            leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                            leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                            leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                            leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                            leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                            leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                            leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                            leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                    leadModel.setZccsupportrequired(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                    leadModel.setZccsupportrequired(true);
                                } else {
                                    leadModel.setZccsupportrequired(false);
                                }
                            leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                            if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                                leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                            leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                            leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                            leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                            leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                            leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                            leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                            leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                            leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                            leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                            leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                            leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                            leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                            leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                    leadModel.setIsmarried(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                    leadModel.setIsmarried(true);
                                } else {
                                    leadModel.setIsmarried(false);
                                }
                            leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                            leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                            leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                                if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                    leadModel.setIscustomercallconnected(true);
                                } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                    leadModel.setIscustomercallconnected(true);
                                } else {
                                    leadModel.setIscustomercallconnected(false);
                                }
                            leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                            leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                            leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                            leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                            leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                            leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                            leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                            leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                            leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                            leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                            leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                            leadModelList.add(leadModel);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        } catch (IllegalStateException exception) {
            exception.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return leadModelList;
    }

    public boolean isProposalNoExist(String proposalno, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?" + " AND " + IvokoProvider.Lead.proposal_number + " = ?", new String[]{userid, proposalno});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean isContactNoExist(String contactNo, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.userid + " = ?"
                + " AND " + IvokoProvider.Lead.contactno + " = ?", new String[]{userid, contactNo});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public List<LeadModel> getLeadwithContactNo(String userId, String contactno) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " = '" + contactno + "'";
            sql += " AND( " + IvokoProvider.Lead.lead_stage + " = 'Proposition presented' ";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " = 'Open' ";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " = 'Contacted & meeting fixed' )";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        //leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getConvertedLeadwithContactNo(String userId, String contactno) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " = '" + contactno + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = 'Converted' ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        //leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<String> getLeadsNamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> leadNamesList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        StringBuilder stringBuilder = new StringBuilder();
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)))) {
                            stringBuilder.append(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        }
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)))) {
                            if (Utils.isNotNullAndNotEmpty(stringBuilder.toString())) {
                                stringBuilder.append(" " + cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            } else {
                                stringBuilder.append(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                            }
                        }
                        leadNamesList.add(stringBuilder.toString());
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadNamesList;
    }

    public List<LeadModel> getLeadListforSearch(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
//            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lastname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.branch_name + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.bank + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.contactno + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.conversion_propensity + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.leadcode + " LIKE '" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));
                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyName(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " LIKE '" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lastname + " LIKE '" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyBranchname(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.branch_name + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyBankname(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.bank + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbyContactno(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.contactno + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListSearchbySUDCode(String userId, String userInput, String stage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + stage + "'";
            sql += " AND " + IvokoProvider.Lead.leadcode + " LIKE '" + userInput + "%'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListforfilter(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String[] name = userInput.split(" ");
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND (" + IvokoProvider.Lead.firstname + " = '" + name[0] + "'";
            sql += " AND " + IvokoProvider.Lead.lastname + " = '" + name[1] + "'";
            sql += " OR " + IvokoProvider.Lead.branch_name + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.bank + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.conversion_propensity + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Lead.lead_stage + " LIKE '%" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        if (cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)) != 0)
                            leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String startdate, String enddate, String leadstage) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_stage + " = '" + leadstage + "'";
            sql += " AND " + IvokoProvider.Lead.lead_created_date + " BETWEEN " + "'" + startdate + "'  AND  '" + enddate + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String startdate, String enddate) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<LeadModel> leadModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Lead.TABLE;
            sql += " WHERE " + IvokoProvider.Lead.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Lead.lead_created_date + " BETWEEN " + "'" + startdate + "'  AND  '" + enddate + "'";
            sql += " ORDER BY " + IvokoProvider.Lead.lead_created_date + " ASC ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        LeadModel leadModel = new LeadModel();
                        leadModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead._ID)));
                        leadModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.userid)));
                        leadModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.groupid)));
                        leadModel.setLeadid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadid)));
                        leadModel.setCampaigndate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.campaigndate)));
                        leadModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.firstname)));
                        leadModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lastname)));
                        leadModel.setIsnri(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.isnri)));
                        leadModel.setCountryname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countryname)));      //country code
                        leadModel.setCountrycode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.countrycode)));      //country code
                        leadModel.setContactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.contactno)));
                        leadModel.setAlternatecontactno(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.alternative_contactno)));
                        leadModel.setEmail(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.emailid)));
                        leadModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.age)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equals("1")) {
                                leadModel.setIsexistingcustomer(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.existing_sublife_customer)).equalsIgnoreCase("true")) {
                                leadModel.setIsexistingcustomer(true);
                            } else {
                                leadModel.setIsexistingcustomer(false);
                            }
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equals("1")) {
                                leadModel.setIspremiumpaid(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ispremium_paid)).equalsIgnoreCase("true")) {
                                leadModel.setIspremiumpaid(true);
                            } else {
                                leadModel.setIspremiumpaid(false);
                            }
                        leadModel.setProductname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_name)));
                        leadModel.setAddress(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.address)));
                        leadModel.setPincode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.pincode)));
                        leadModel.setCity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.city)));
                        leadModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.occupation)));
                        leadModel.setLeadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.source_of_lead)));
                        leadModel.setOtherleadsource(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.other)));
                        leadModel.setBank(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.bank)));
                        leadModel.setBankbranchcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_code)));
                        leadModel.setFamilymemberscount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.no_of_family_members)));
                        leadModel.setBankbranchname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.branch_name)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equals("1")) {
                                leadModel.setZccsupportrequired(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.zcc_support_requried)).equalsIgnoreCase("true")) {
                                leadModel.setZccsupportrequired(true);
                            } else {
                                leadModel.setZccsupportrequired(false);
                            }
                        leadModel.setPreferredlanguage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_language)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))))
                            leadModel.setPreferreddatetime(Integer.parseInt(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.preferred_date))));
                        leadModel.setStatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.status)));
                        leadModel.setSubstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.sub_status)));
                        leadModel.setExpectedpremium(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.premium_expected)));
                        leadModel.setPremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.premium_paid)));
                        leadModel.setFirstappointmentdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.first_appointment_date)));
                        leadModel.setNextfollowupdate(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.next_followup_date)));
                        leadModel.setConversionpropensity(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.conversion_propensity)));
                        leadModel.setVisitingdatetime(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.visiting_date)));
                        leadModel.setProposalnumber(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.proposal_number)));
                        leadModel.setLeadcreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.lead_created_date)));
                        leadModel.setCreatedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.createdby)));
                        leadModel.setModifiedby(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.modifiedby)));
                        leadModel.setCreatedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.createdon)));
                        leadModel.setModifiedon(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.modifiedon)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equals("1")) {
                                leadModel.setIsmarried(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.ismarried)).equalsIgnoreCase("true")) {
                                leadModel.setIsmarried(true);
                            } else {
                                leadModel.setIsmarried(false);
                            }
                        leadModel.setIncomeband(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.income_band)));
                        leadModel.setType(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_type)));
                        leadModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.remarks)));
                        if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)) != null)
                            if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equals("1")) {
                                leadModel.setIscustomercallconnected(true);
                            } else if (cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.customer_call_connected)).equalsIgnoreCase("true")) {
                                leadModel.setIscustomercallconnected(true);
                            } else {
                                leadModel.setIscustomercallconnected(false);
                            }
                        leadModel.setEducation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.education)));

                        leadModel.setFinancialplanningfor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_done_for)));
                        leadModel.setTentaiveinvestmentyears(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_investment_years)));
                        leadModel.setFinancialplanningfuturefor(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.financial_planning_in_future_for)));
                        leadModel.setTentativeamount(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.tentative_amount)));
                        leadModel.setLeadcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.leadcode)));
                        leadModel.setLeadstage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.lead_stage)));
                        leadModel.setProduct1(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_one)));
                        leadModel.setProduct2(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.product_two)));
                        leadModel.setActualpremiumpaid(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Lead.actual_premium)));
                        leadModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Lead.network_status)));
                        leadModelList.add(leadModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return leadModelList;
    }

    public void deleteLeadRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Lead.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.leadid + " = '" + id + "'");
        }
    }

    private boolean isLeadRecordExist(SQLiteDatabase db, String leadID) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Lead.TABLE + " WHERE " + IvokoProvider.Lead.leadid + " = ?", new String[]{leadID});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void updateLeadOnline(LeadModel dto, String userId, String leadId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Lead.leadid, leadId);
        values.put(IvokoProvider.Lead.leadcode, dto.getLeadcode());
        values.put(IvokoProvider.Lead.network_status, "");

        db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{userId});
    }

    public void updateLeaddumyOnline(LeadModel dto, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Lead.network_status, "pending");

        db.update(IvokoProvider.Lead.TABLE, values, IvokoProvider.Lead._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Lead.userid + " = ?", new String[]{userId});
    }
}*/
