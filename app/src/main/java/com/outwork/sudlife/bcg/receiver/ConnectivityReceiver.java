package com.outwork.sudlife.bcg.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.outwork.sudlife.bcg.branches.services.ContactsIntentService;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.targets.services.TargetsIntentService;

/**
 * Created by Habi on 27-05-2017.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(final Context context, Intent arg1) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            ContactsIntentService.syncCustomerstoServer(context);
            LeadIntentService.syncLeadstoServer(context);
            TargetsIntentService.updateRecord(context);
        }
    }
}