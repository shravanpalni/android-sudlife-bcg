package com.outwork.sudlife.bcg.listener;

import android.view.View;

/**
 * Created by Habi on 28-12-2017.
 */

public interface ItemClickListener {
    void onClick(View view, int position);
}
