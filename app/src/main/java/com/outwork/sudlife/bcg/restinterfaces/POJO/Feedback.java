package com.outwork.sudlife.bcg.restinterfaces.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panch on 4/12/2016.
 */
public class Feedback {



        @SerializedName("from")
        @Expose
        private String fromemail;

        @SerializedName("feedback")
        @Expose
        private String feedback;


        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }

        public String getFromemail() {
            return fromemail;
        }

        public void setFromemail(String fromemail) {
            this.fromemail = fromemail;
        }


}
