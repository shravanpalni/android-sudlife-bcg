package com.outwork.sudlife.bcg.more.models;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Attachment {
    private String url;
    private String fileName;
    private String extn;


    private String fileId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtn() {
        return extn;
    }

    public void setExtn(String extn) {
        this.extn = extn;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }



    public static final String JSON_FILE_ID = "fileid";
    public static final String JSON_FILE_URL = "fileurl";
    public static final String JSON_EXTN ="extension";
    public static final String JSON_FILENAME = "fileName";

    public static List<Attachment> parseFile(JSONArray filesArray)throws JSONException {

       List<Attachment> attachmentList = new ArrayList<Attachment>();


        for (int i = 0; i < filesArray.length(); i++) {
            Attachment attachment = new Attachment();

            JSONObject file = filesArray.getJSONObject(i);
            if (!TextUtils.isEmpty(file.getString(JSON_FILE_ID)) && !(file.getString(JSON_FILE_ID).equalsIgnoreCase("null"))){
               attachment.setFileId(file.getString(JSON_FILE_ID));
               attachment.setExtn(file.getString(JSON_EXTN));
               attachment.setUrl(file.getString(JSON_FILE_URL));
               attachment.setFileName(file.getString(JSON_FILENAME));
               attachmentList.add(attachment);
            }
        }
        return attachmentList;
    }



}
