package com.outwork.sudlife.bcg.planner.activities;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onesignal.OneSignal;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.bcg.branches.activities.ShowHierarchyActivity;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.listener.DatePickerListener;
import com.outwork.sudlife.bcg.listener.RecyclerTouchListener;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.adapter.CalendarDayAdapter;
import com.outwork.sudlife.bcg.planner.adapter.PlannerAdapter;
import com.outwork.sudlife.bcg.planner.adapter.UserTeamNamesAdapter;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.planner.service.PlannerIntentService;
import com.outwork.sudlife.bcg.receiver.ConnectivityReceiver;
import com.outwork.sudlife.bcg.targets.models.MSBModel;
import com.outwork.sudlife.bcg.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.bcg.ui.base.AppBaseActivity;
import com.outwork.sudlife.bcg.ui.models.UserTeams;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import org.joda.time.DateTime;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

public class CalenderActivity extends AppBaseActivity implements DatePickerListener {

    private List<String> dateList = new ArrayList<>();
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView planRecyclerView, calRV;
    private ProgressBar msgListProgressBar;
    private TextView nomessageview, month, toolbar_title;
    private Boolean isFabOpen = false;
    private boolean doubleBackToExitPressedOnce = true;
    private FloatingActionButton addplan, fab1, fab2;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private long currentmnth;
    private LinearLayout activityView, leadView;
    private LinearLayoutManager linearLayoutManager;
    private CalendarDayAdapter calendarDayAdapter;
    private PlannerAdapter plannerAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private int day, monthno, year;
    private String toolbardate, displaydate;
    private BottomSheetDialog bottomSheetDialog;
    private ConnectivityReceiver mNetworkReceiver;
    private int ftm = 0, activitiesPlanned = 0, achievedConnection = 0, achievedLeads = 0, achievedBusiness = 0, visitsPlanned = 0,
            expectedConnections = 0, leadsExpected = 0, expectedBusiness = 0, gapToTarget = 0;

    List<UserTeams> userTeamsList = new ArrayList<>();
    List<String> userTeamNamelist = new ArrayList<>();
    public static CalenderActivity calenderActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_dashboard);
        calenderActivity = this;

        Log.i("shravan", "userToken = = = " + userToken);

        userTeamsList = PlannerMgr.getInstance(CalenderActivity.this).getUserTeamList(userid);
        Log.i("shravan", "userTeamsList calendar size = = = " + userTeamsList.size());


        for (UserTeams us : userTeamsList) {

            String a = us.getName();
            userTeamNamelist.add(a);

        }

        Log.i("shravan", "userTeamNamelist calendar size = = = " + userTeamNamelist.size());

        mgr = LocalBroadcastManager.getInstance(this);
        mNetworkReceiver = new ConnectivityReceiver();

        OneSignal.sendTag("userid", SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        OneSignal.syncHashedEmail(SharedPreferenceManager.getInstance().getString(Constants.USER_EMAIL, ""));
        OneSignal.sendTag("group", SharedPreferenceManager.getInstance().getString(Constants.GRP_CODE, ""));
        OneSignal.sendTag("grouptitle", SharedPreferenceManager.getInstance().getString(Constants.GRP_DISPLAYNAME, ""));
        OneSignal.sendTag("email", SharedPreferenceManager.getInstance().getString(Constants.USER_EMAIL, ""));
        OneSignal.sendTag("EMPID", SharedPreferenceManager.getInstance().getString(Constants.LOGINID, ""));
        OneSignal.sendTag("appVersion", getResources().getString(R.string.app_version));

        if (SharedPreferenceManager.getInstance().getString(Constants.PLANNER_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                List<String> stringList = new ArrayList<>();
                stringList.add(Utils.getPreviousMonthDate(new Date()));
                stringList.add(TimeUtils.getCurrentDate("MM-yyyy"));
                stringList.add(Utils.getNextMonthDate(new Date()));
                for (int i = 0; i < stringList.size(); i++) {
                    String[] date = stringList.get(i).split("-");
                    PlannerIntentService.insertPlanList(CalenderActivity.this, date[0], date[1]);
                }
            }
        }
        if (isNetworkAvailable()) {
            if (PlannerMgr.getInstance(CalenderActivity.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0)
                LeadIntentService.syncLeadstoServer(CalenderActivity.this);
        }
        planRecyclerView = (RecyclerView) findViewById(R.id.fflogRecycler);
        calRV = (RecyclerView) findViewById(R.id.calenderRecycler);
        nomessageview = (TextView) findViewById(R.id.ffnomessages);
        month = (TextView) findViewById(R.id.month);
        msgListProgressBar = (ProgressBar) findViewById(R.id.ffmessageListProgressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.task_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        addplan = (FloatingActionButton) findViewById(R.id.addplan);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        activityView = (LinearLayout) findViewById(R.id.faview1);
        leadView = (LinearLayout) findViewById(R.id.faview2);
        fab_open = AnimationUtils.loadAnimation(CalenderActivity.this, R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(CalenderActivity.this, R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(CalenderActivity.this, R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(CalenderActivity.this, R.anim.rotate_backward);

        initToolBar(TimeUtils.getCurrentDate("EEE dd MMM yyyy"));
        toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        displaydate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        showlist(day, monthno, year);

        currentmnth = Long.parseLong(TimeUtils.getCurrentDate("MM"));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateList = getDaysBetweenDates(dateFormat.parse(getCalculatedDate("yyyy-MM-dd", -30)), dateFormat.parse(getCalculatedDate("yyyy-MM-dd", 40)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        month.setText(getMonth(currentmnth));
        if (dateList.size() > 0) {
            calendarDayAdapter = new CalendarDayAdapter(CalenderActivity.this, dateList, dateList.size() - 40);
            linearLayoutManager = new LinearLayoutManager(CalenderActivity.this, LinearLayoutManager.HORIZONTAL, false);
            calRV.setLayoutManager(linearLayoutManager);
            calRV.setAdapter(calendarDayAdapter);
            calRV.scrollToPosition(dateList.size() - 40);
            linearLayoutManager.scrollToPositionWithOffset(dateList.size() - 40, 3);
        }
        setListener();
        HashMap<String, Integer> stringIntegerHashMap = PlannerMgr.getInstance(CalenderActivity.this).getPlannerListByScheduleDates(monthno, day);
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(CalenderActivity.this).getMSBRecordsBasedOnMonth(userid, monthno);
        if (msbModelArrayList.size() > 0) {
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
            }
            ((TextView) findViewById(R.id.tv_planned_activities)).setText(String.valueOf(activitiesPlanned));
            ((TextView) findViewById(R.id.tv_planned_visits)).setText(String.valueOf(visitsPlanned));
            ((TextView) findViewById(R.id.tv_achieved_activities)).setText(String.valueOf(stringIntegerHashMap.get("activitycount")));
            ((TextView) findViewById(R.id.tv_achieved_visits)).setText(String.valueOf(stringIntegerHashMap.get("visitcount")));
        }
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, nomessageview, month);
    }

    private void setListener() {
        calRV.addOnItemTouchListener(new RecyclerTouchListener(this, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                calendarDayAdapter.dataSetChanged(position);
                calRV.scrollToPosition(position);
                if (dateList.size() > 0) {
                    initToolBar(TimeUtils.getDateFormatted(Utils.stringToDate(dateList.get(position)), "EEE dd MMM yyyy"));
                    toolbardate = TimeUtils.getDateFormatted(Utils.stringToDate(dateList.get(position)), "yyyy-MM-dd");
                    displaydate = TimeUtils.getDateFormatted(Utils.stringToDate(dateList.get(position)), "dd/MM/yyyy hh:mm a");
                    try {
                        month.setText(getMonth(Long.parseLong(new SimpleDateFormat("MM").format(simpleDateFormat.parse(dateList.get(position))))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
            }
        }));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Utils.isNotNullAndNotEmpty(toolbardate)) {
                    day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                    monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                    year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                    showlist(day, monthno, year);
                }
            }
        };
        addplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialogForvisit();
//                animateFAB();
            }
        });
    }

    public void bottomSheetDialogForvisit() {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_visit_item, null);
        LinearLayout addvisit = (LinearLayout) modalbottomsheet.findViewById(R.id.addvisit);
        LinearLayout addlead = (LinearLayout) modalbottomsheet.findViewById(R.id.addlead);
        bottomSheetDialog = new BottomSheetDialog(CalenderActivity.this);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(true);
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.show();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (TextView) modalbottomsheet.findViewById(R.id.visit),
                (TextView) modalbottomsheet.findViewById(R.id.lead));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) modalbottomsheet.findViewById(R.id.select_tv));
        addvisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*Intent intent = new Intent(CalenderActivity.this, ShowHierarchyActivity.class);
                intent.putExtra(Constants.PLAN_STATUS, 0);
                intent.putExtra(Constants.PLAN_DATE, displaydate);
                startActivity(intent);
                bottomSheetDialog.dismiss();*/

                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                        SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 5) {

                    if (userTeamsList.size() > 1) {

                        showDialog();

                    } else {

                        // added on Feb 4
                        //Intent intent = new Intent(CalenderActivity.this, TeamHierarchyActivity.class);
                        Intent intent = new Intent(CalenderActivity.this, ShowHierarchyActivity.class);
                        intent.putExtra(Constants.PLAN_STATUS, 0);
                        intent.putExtra(Constants.PLAN_DATE, displaydate);
                        startActivity(intent);
                        bottomSheetDialog.dismiss();
                    }
                } else {
                    Intent intent = new Intent(CalenderActivity.this, ListCustomerActivity.class);
                    intent.putExtra(Constants.PLAN_STATUS, 0);
                    intent.putExtra(Constants.PLAN_DATE, displaydate);
                    intent.putExtra("type", "select");
                    intent.putExtra("fromclass", "plan");
                    startActivity(intent);
                    bottomSheetDialog.dismiss();
                }
            }
        });

        addlead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(CalenderActivity.this, ListCustomerActivity.class);
                intent.putExtra("type", "select");
                intent.putExtra("fromclass", "lead");
                intent.putExtra("fromCalendar", true);
                startActivity(intent);*/
                showToast("To Be Enabled");
                //bottomSheetDialog.dismiss();
            }
        });
    }


    private void showDialog() {


        final Dialog dialog = new Dialog(this);

        dialog.setTitle("Select Team");

        View view = getLayoutInflater().inflate(R.layout.userteam_dialog, null);

        ListView lv = (ListView) view.findViewById(R.id.memberList);
        UserTeamNamesAdapter clad = new UserTeamNamesAdapter(CalenderActivity.this, userTeamsList, groupId, userid);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                UserTeams dto = (UserTeams) parent.getItemAtPosition(position);
                //Intent intent = new Intent(CalenderActivity.this, TeamHierarchyActivity.class);
                Intent intent = new Intent(CalenderActivity.this, ShowHierarchyActivity.class);
                intent.putExtra(Constants.PLAN_STATUS, 0);
                intent.putExtra(Constants.PLAN_DATE, displaydate);
                intent.putExtra("USERTEAMID", dto.getTeamid());
                startActivity(intent);
                dialog.cancel();
                bottomSheetDialog.dismiss();


            }
        });

        dialog.setContentView(view);

        dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.module_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("plan_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("lead_broadcast"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (mNetworkReceiver != null)
                unregisterReceiver(mNetworkReceiver);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.isNotNullAndNotEmpty(dateList))
            if (!dateList.get(dateList.size() - 40).toString().equalsIgnoreCase(TimeUtils.getCurrentDate("yyyy-MM-dd"))) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    dateList = getDaysBetweenDates(dateFormat.parse(getCalculatedDate("yyyy-MM-dd", -30)), dateFormat.parse(getCalculatedDate("yyyy-MM-dd", 40)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (dateList.size() > 0) {
                    calendarDayAdapter = new CalendarDayAdapter(CalenderActivity.this, dateList, dateList.size() - 40);
                    linearLayoutManager = new LinearLayoutManager(CalenderActivity.this, LinearLayoutManager.HORIZONTAL, false);
                    calRV.setLayoutManager(linearLayoutManager);
                    calRV.setAdapter(calendarDayAdapter);
                    calRV.scrollToPosition(dateList.size() - 40);
                    linearLayoutManager.scrollToPositionWithOffset(dateList.size() - 40, 3);
                }
                initToolBar(TimeUtils.getCurrentDate("EEE dd MMM yyyy"));
                toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
                displaydate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
            } else if (Utils.isNotNullAndNotEmpty(toolbardate)) {
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
            }
    }

    public static List<String> getDaysBetweenDates(Date startdate, Date enddate) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            Date result = calendar.getTime();
            dates.add(new SimpleDateFormat("yyyy-MM-dd").format(result));
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void initToolBar(String datetime) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText(datetime);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public String getMonth(long month) {
        return new DateFormatSymbols().getMonths()[(int) month - 1];
    }

    public void showlist(int day, int monthno, int year) {
        plannerModelList.clear();
        planRecyclerView.setAdapter(null);
        plannerModelList = PlannerMgr.getInstance(CalenderActivity.this).getPlannerListByDate(userid, day, monthno, year);
        if (plannerModelList.size() > 0) {
            nomessageview.setVisibility(View.INVISIBLE);
            if (msgListProgressBar.VISIBLE == 0) {
                msgListProgressBar.setVisibility(View.INVISIBLE);
            }
            plannerAdapter = new PlannerAdapter(CalenderActivity.this, plannerModelList, "plan");
            planRecyclerView.setAdapter(plannerAdapter);
            planRecyclerView.setLayoutManager(new LinearLayoutManager(CalenderActivity.this));
        } else {
            nomessageview.setText("No Plans");
            nomessageview.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDateSelected(DateTime dateSelected) {
        Log.d(" HorizontalPicker ", " Selected date = " + dateSelected.toString());
        initToolBar(TimeUtils.getDateFormatted(dateSelected.toDate(), "EEE dd MMM yyyy"));
        toolbardate = TimeUtils.getDateFormatted(dateSelected.toDate(), "yyyy-MM-dd");
        displaydate = TimeUtils.getDateFormatted(dateSelected.toDate(), "dd/MM/yyyy hh:mm a");
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        showlist(day, monthno, year);
    }
}

