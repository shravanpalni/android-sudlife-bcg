package com.outwork.sudlife.bcg.lead.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class LeadAppointmentAdapter extends RecyclerView.Adapter<LeadAppointmentAdapter.AppointmentViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PlannerModel> plannerModelList = new ArrayList<>();

    public static class AppointmentViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        private CardView card_view;
        public TextView line2, status;
        View offline;

        public AppointmentViewHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.profileImage);
            name = (TextView) v.findViewById(R.id.contactname);
            line2 = (TextView) v.findViewById(R.id.contactline2);
            status = (TextView) v.findViewById(R.id.status);
            card_view = (CardView) v.findViewById(R.id.card_view);
            offline = v.findViewById(R.id.networkstatus);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, name);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, line2, status);
        }
    }

    public LeadAppointmentAdapter(Context context, List<PlannerModel> plannerModelList) {
        this.context = context;
        this.plannerModelList = plannerModelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plan, parent, false);
        return new AppointmentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder holder, final int position) {
        final PlannerModel plannerModel = (PlannerModel) this.plannerModelList.get(position);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
            holder.offline.setVisibility(View.VISIBLE);
        } else {
            holder.offline.setVisibility(View.GONE);
        }
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        String lead = "Lead";
        // generate color based on a key (same key returns the same color), useful for list/grid views
        int color2 = generator.getColor(lead.substring(0, 1));
        TextDrawable drawable = TextDrawable.builder().buildRound(lead.substring(0, 1), color2);
        if (drawable != null) {
            holder.picture.setImageDrawable(drawable);
        }
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() != 0) {
                holder.name.setText((TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "EEE dd MMM yyyy")));
            } else {
                holder.name.setVisibility(View.GONE);
            }
        } else {
            holder.name.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
            holder.line2.setText(plannerModel.getSubtype());
        } else {
            holder.line2.setVisibility(View.GONE);
        }
        if (plannerModel.getCompletestatus() != null) {
            if (plannerModel.getCompletestatus() == 0) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0)) {
                    if (plannerModel.getScheduletime() != null && plannerModel.getScheduletime() != 0)
                        if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                                .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                            holder.status.setVisibility(View.GONE);
                        } else if (plannerModel.getScheduletime() > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                            holder.status.setVisibility(View.GONE);
                        } else {
                            holder.status.setVisibility(View.VISIBLE);
                            holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_orange_400));
                            holder.status.setText("Missed");
                        }
                }
            }
            if (plannerModel.getCompletestatus() == 2) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0)) {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_brown_400));
                    holder.status.setText("Attended");
                }
            }
            if (plannerModel.getCompletestatus() == 3) {
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_light_yellow_800));
                holder.status.setText("Reschedule");
            }
        } else {
            holder.status.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return plannerModelList.size();
    }
}