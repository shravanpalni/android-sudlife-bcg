package com.outwork.sudlife.bcg.ui.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.bcg.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.bcg.lead.activities.LeadsListActivity;
import com.outwork.sudlife.bcg.planner.activities.CPlannerActivity;
import com.outwork.sudlife.bcg.receiver.ConnectivityReceiver;
import com.outwork.sudlife.bcg.ui.base.AppBaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

public class DashBoardActivity extends AppBaseActivity {

    //private ImageView groupImage;
    private LinearLayout targets, planner, performance, leads, customers, morelayout;
    private boolean isFeatureEnabled = false;
    private boolean doubleBackToExitPressedOnce = true;
    private ConnectivityReceiver mNetworkReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board1);

        isFeatureEnabled = SharedPreferenceManager.getInstance().getBoolean(Constants.isFeatureEnabled, false);
        mNetworkReceiver = new ConnectivityReceiver();

        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        Log.i("shravan","utoken = = = = "+userToken);

        initUI();
        initToolBar();
        setListeners();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (TextView) findViewById(R.id.textChat),
                (TextView) findViewById(R.id.texttask), (TextView) findViewById(R.id.textexpense),
                (TextView) findViewById(R.id.aText), (TextView) findViewById(R.id.textView4), (TextView) findViewById(R.id.textmore));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isNetworkAvailable()) {
            ProductListDownloadService.getProductListinDB(DashBoardActivity.this);
            ProductListDownloadService.getMasterListinDB(DashBoardActivity.this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    private void initUI() {
        customers = (LinearLayout) findViewById(R.id.customers);
        planner = (LinearLayout) findViewById(R.id.planner);
        leads = (LinearLayout) findViewById(R.id.leads);
        targets = (LinearLayout) findViewById(R.id.targets);
        performance = (LinearLayout) findViewById(R.id.performance);
        morelayout = (LinearLayout) findViewById(R.id.morelayout);
        //groupImage = (ImageView) findViewById(R.id.groupImage);

        /*Glide.with(DashBoardActivity.this).load(SharedPreferenceManager.getInstance().getString(Constants.GRP_IMAGE, ""))
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(groupImage);*/
    }

    private void setListeners() {
        customers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFeatureEnabled) {
                    Intent intent = new Intent(DashBoardActivity.this, ListCustomerActivity.class);
                    startActivity(intent);
                } else {
                    showSimpleAlert("", "Please contact us to enable");
                }
            }
        });
        planner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashBoardActivity.this, CPlannerActivity.class));
            }
        });
        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("To Be Enabled");
                //startActivity(new Intent(DashBoardActivity.this, LeadsListActivity.class));
            }
        });
        targets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("To Be Enabled");
              //   startActivity(new Intent(DashBoardActivity.this, ListOfTargetsWithCardsActivity.class));
            }
        });
        performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("To Be Enabled");
               // startActivity(new Intent(DashBoardActivity.this, MyPerformanceViewActivity.class));
            }
        });
        morelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showToast("To Be Enabled");
              //  startActivity(new Intent(DashBoardActivity.this, ListMoreActivity.class));
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (mNetworkReceiver != null)
                unregisterReceiver(mNetworkReceiver);
        }
    }
}

