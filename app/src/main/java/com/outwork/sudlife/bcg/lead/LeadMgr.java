package com.outwork.sudlife.bcg.lead;

import android.content.Context;

import com.outwork.sudlife.bcg.lead.db.LeadDao;
import com.outwork.sudlife.bcg.lead.model.LeadModel;

import java.util.List;

public class LeadMgr {

    private static Context context;
    private static LeadDao leadDao;

    public LeadMgr(Context context) {
        this.context = context;
        leadDao = leadDao.getInstance(context);
    }

    private static LeadMgr instance;

    public static LeadMgr getInstance(Context context) {
        if (instance == null)
            instance = new LeadMgr(context);
        return instance;
    }

    public void insertLeadsList(List<LeadModel> list, String userId) {
        leadDao.insertLeadsList(list, userId);
    }

    public long insertLocalLead(LeadModel leadModel, String status) {
        return leadDao.insertLocalLead(leadModel, status);
    }

    public void updateLeadOnline(LeadModel leadModel, String userId, String leadId) {
        leadDao.updateLeadOnline(leadModel, userId, leadId);
    }

    public void updateLeaddumyOnline(LeadModel leadModel, String userId) {
        leadDao.updateLeaddumyOnline(leadModel, userId);
    }

    public void updateLead(LeadModel leadModel, String network_status) {
        leadDao.updateLead(leadModel, network_status);
    }

    public List<LeadModel> getLeadsList(String userId) {
        return leadDao.getLeadsList(userId);
    }

    public List<LeadModel> getLeadsListonLeadStage(String userId, String leadstage) {
        return leadDao.getLeadsListonLeadStage(userId, leadstage);
    }

    public LeadModel getLeadbyId(String leadId, String userId) {
        return leadDao.getLeadbyId(leadId, userId);
    }

    public LeadModel getLeadbylocalId(int leadId, String userId) {
        return leadDao.getLeadbylocalId(leadId, userId);
    }

    public List<LeadModel> getOfflineLeadsList(String userId, String newtwork_status) {
        return leadDao.getOfflineLeadsList(userId, newtwork_status);
    }

    public List<String> getLeadsNamesList(String userId) {
        return leadDao.getLeadsNamesList(userId);
    }

    public List<LeadModel> getLeadListforSearch(String userId, String userinput) {
        return leadDao.getLeadListforSearch(userId, userinput);
    }

    public List<LeadModel> getLeadListforfilter(String userId, String userinput) {
        return leadDao.getLeadListforfilter(userId, userinput);
    }

    public List<LeadModel> getLeadListSearchbyName(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyName(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbyBranchname(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyBranchname(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbyBankname(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyBankname(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbyContactno(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyContactno(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbySUDCode(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbySUDCode(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String strtdate, String endate, String leadstage) {
        return leadDao.getLeadListbetweendates(userId, strtdate, endate, leadstage);
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String strtdate, String endate) {
        return leadDao.getLeadListbetweendates(userId, strtdate, endate);
    }
}



