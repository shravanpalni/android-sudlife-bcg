package com.outwork.sudlife.bcg.planner.service;

import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 11/29/2016.
 */

public interface PlannerService {
    @POST("mobile/v1/plan")
    Call<RestResponse> postPlan(@Header("utoken") String userToken,
                                @Body PlannerModel plannerModel);

    @GET("mobile/v1/plan/userplans/{month}/{year}")
    Call<RestResponse> getPlans(@Header("utoken") String userToken,
                                @Path("month") String month,
                                @Path("year") String year,
                                @Query("lastfetchtime") String lastfetchtime);

    @GET("mobile/v1/plan/userplans/{userid}/{month}/{year}")
    Call<RestResponse> getTeamMemberPlans(@Header("utoken") String userToken,
                                          @Path("userid") String userid,
                                          @Path("month") String month,
                                          @Path("year") String year);


    @PUT("mobile/v1/plan/approval/{activityid}/{planstatus}")
    Call<RestResponse> updateApprove(@Header("utoken") String userToken,
                                     @Path("activityid") String activityid,
                                     @Path("planstatus") String planstatus);

    @DELETE("mobile/v1/plan/{activityid}")
    Call<RestResponse> deletePlan(@Header("utoken") String userToken,
                                  @Path("activityid") String activityid);

    @PUT("mobile/v1/plan/details")
    Call<RestResponse> updatePlan(@Header("utoken") String userToken,
                                  @Body PlannerModel plannerModel);

    @PUT("mobile/v1/plan/checkin/{activityid}")
    Call<RestResponse> checkin(@Header("utoken") String userToken,
                               @Path("activityid") String activityid,
                               @Body PlannerModel plannerModel);

    @PUT("mobile/v1/plan/checkout/{activityid}")
    Call<RestResponse> checkout(@Header("utoken") String userToken,
                                @Path("activityid") String activityid,
                                @Body PlannerModel plannerModel);


    @PUT("mobile/v1/plan/reschedule/{activityid}")
    Call<RestResponse> reschdule(@Header("utoken") String userToken,
                                 @Path("activityid") String activityid,
                                 @Body PlannerModel plannerModel);

    @GET("mobile/v1/user/supervisorslist")
    Call<RestResponse> getSupervisors(@Header("utoken") String userToken);
}
