package com.outwork.sudlife.bcg.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Habi on 04-04-2018.
 */
public class LoginModel {

    @SerializedName("usertoken")
    @Expose
    private String usertoken;
    @SerializedName("loginID")
    @Expose
    private String loginID;
    @SerializedName("logintime")
    @Expose
    private String logintime;
    @SerializedName("logintype")
    @Expose
    private String logintype;
    @SerializedName("organization")
    @Expose
    private Organization organization;
    @SerializedName("userprofile")
    @Expose
    private Userprofile userprofile;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("lastlogindate")
    @Expose
    private String lastlogindate;
    @SerializedName("features")
    @Expose
    private Features features;
    @SerializedName("haswebaccess")
    @Expose
    private Boolean haswebaccess;
    @SerializedName("subscribedteam")
    @Expose
    private Subscribedteam subscribedteam;
    @SerializedName("internalrole")
    @Expose
    private String internalrole;
    @SerializedName("displayrole")
    @Expose
    private String displayrole;

    public String getUsertoken() {
        return usertoken;
    }

    public void setUsertoken(String usertoken) {
        this.usertoken = usertoken;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getLogintype() {
        return logintype;
    }

    public void setLogintype(String logintype) {
        this.logintype = logintype;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Userprofile getUserprofile() {
        return userprofile;
    }

    public void setUserprofile(Userprofile userprofile) {
        this.userprofile = userprofile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(String lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Boolean getHaswebaccess() {
        return haswebaccess;
    }

    public void setHaswebaccess(Boolean haswebaccess) {
        this.haswebaccess = haswebaccess;
    }

    public Subscribedteam getSubscribedteam() {
        return subscribedteam;
    }

    public void setSubscribedteam(Subscribedteam subscribedteam) {
        this.subscribedteam = subscribedteam;
    }

    public String getInternalrole() {
        return internalrole;
    }

    public void setInternalrole(String internalrole) {
        this.internalrole = internalrole;
    }

    public String getDisplayrole() {
        return displayrole;
    }

    public void setDisplayrole(String displayrole) {
        this.displayrole = displayrole;
    }
}