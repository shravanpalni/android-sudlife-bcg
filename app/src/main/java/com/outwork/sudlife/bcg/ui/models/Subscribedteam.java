package com.outwork.sudlife.bcg.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Habi on 26-04-2018.
 */

class Subscribedteam {

    @SerializedName("InternalRole")
    @Expose
    private String internalRole;
    @SerializedName("DisplayRole")
    @Expose
    private Object displayRole;
    @SerializedName("teamid")
    @Expose
    private String teamid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("hasparent")
    @Expose
    private Boolean hasparent;
    @SerializedName("parentteamid")
    @Expose
    private Object parentteamid;
    @SerializedName("parent")
    @Expose
    private Object parent;
    @SerializedName("haschildren")
    @Expose
    private Boolean haschildren;
    @SerializedName("iconurl")
    @Expose
    private String iconurl;
    @SerializedName("level")
    @Expose
    private Integer level;

    public String getInternalRole() {
        return internalRole;
    }

    public void setInternalRole(String internalRole) {
        this.internalRole = internalRole;
    }

    public Object getDisplayRole() {
        return displayRole;
    }

    public void setDisplayRole(Object displayRole) {
        this.displayRole = displayRole;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Boolean getHasparent() {
        return hasparent;
    }

    public void setHasparent(Boolean hasparent) {
        this.hasparent = hasparent;
    }

    public Object getParentteamid() {
        return parentteamid;
    }

    public void setParentteamid(Object parentteamid) {
        this.parentteamid = parentteamid;
    }

    public Object getParent() {
        return parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public Boolean getHaschildren() {
        return haschildren;
    }

    public void setHaschildren(Boolean haschildren) {
        this.haschildren = haschildren;
    }

    public String getIconurl() {
        return iconurl;
    }

    public void setIconurl(String iconurl) {
        this.iconurl = iconurl;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
