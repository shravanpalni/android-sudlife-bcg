package com.outwork.sudlife.bcg.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResourceModel {
    @SerializedName("libraryid")
    @Expose
    private String libraryid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("contenttype")
    @Expose
    private String contenttype;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("contentid")
    @Expose
    private String contentid;
    @SerializedName("fileobject")
    @Expose
    private FileModel fileobject;

    public String getLibraryid() {
        return libraryid;
    }

    public void setLibraryid(String libraryid) {
        this.libraryid = libraryid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }

    public FileModel getFileobject() {
        return fileobject;
    }

    public void setFileobject(FileModel fileobject) {
        this.fileobject = fileobject;
    }
}
