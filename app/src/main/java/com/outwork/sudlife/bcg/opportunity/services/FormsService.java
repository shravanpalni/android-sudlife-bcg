package com.outwork.sudlife.bcg.opportunity.services;

import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityItems;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 11/29/2016.
 */

public interface FormsService {
    @POST("mobile/v1/opportunity")
    Call<RestResponse> postOpportunityv1(@Header("utoken") String userToken,
                                         @Query("groupid") String groupId,
                                         @Body OpportunityModel opportunityModel);

    @PUT("mobile/v1/opportunity/{opportunityid}")
    Call<RestResponse> updateOpportunityv1(@Header("utoken") String userToken,
                                           @Path("opportunityid") String opportunityid,
                                           @Body OpportunityModel oppotunityModel);

    @GET("mobile/v1/opportunity")
    Call<RestResponse> getOpportFormsv1(@Header("utoken") String userToken,
                                        @Query("StartDate") String startdate,
                                        @Query("EndDate") String enddate,
                                        @Query("Type") String type,
                                        @Query("LastFetchTime") String lastfetchtime);

    @GET("mobile/v1/opportunity/stages")
    Call<RestResponse> getOpportstagesv1(@Header("utoken") String userToken);

    @DELETE("mobile/v1/opportunity/{opportunityid}")
    Call<RestResponse> deleteOpportunityv1(@Header("utoken") String userToken,
                                           @Path("opportunityid") String opportunityid);

    /****************opportunity order v1**************/

    @POST("mobile/v1/opportunity/{opportunityid}/order")
    Call<RestResponse> postOpportunityOrderv1(@Header("utoken") String userToken,
                                              @Path("opportunityid") String opportunityid,
                                              @Body List<OpportunityItems> opportunityItemsList);

    @DELETE("mobile/v1/opportunity/{opportunityid}/order/{orderid}")
    Call<RestResponse> deleteOpportunityOrderv1(@Header("utoken") String userToken,
                                                @Path("opportunityid") String opportunityid,
                                                @Path("orderid") String orderid);

    @GET("mobile/v1/opportunity/{opportunityid}/details")
    Call<RestResponse> getOpportOrderFormsv1(@Header("utoken") String userToken,
                                             @Path("opportunityid") String opportunityid);

    //lead
    @GET("mobile/v1/products")
    Call<RestResponse> getProducts(@Header("utoken") String userToken);

    @POST("mobile/v1/lead")
    Call<RestResponse> postLead(@Header("utoken") String userToken,
                                @Body LeadModel leadModel);

    @PUT("mobile/v1/lead/{leadid}")
    Call<RestResponse> updateLead(@Header("utoken") String userToken,
                                  @Path("leadid") String leadid,
                                  @Body LeadModel leadModel);

    @GET("mobile/v1/lead")
    Call<RestResponse> getServerLeads(@Header("utoken") String userToken,
                                      @Query("lastfetchtime") String lastfetchtime);

    @GET("mobile/v1/lead/{userid}")
    Call<RestResponse> getServerLeadsbyUserID(@Header("utoken") String userToken,
                                              @Path("userid") String userid,
                                              @Query("lastfetchtime") String lastfetchtime);

    @POST("mobile/v1/familymember")
    Call<RestResponse> postFM(@Header("utoken") String userToken,
                              @Body LeadModel leadModel);

    @PUT("mobile/v1/familymember/{familymemberid}")
    Call<RestResponse> updateFamilyMember(@Header("utoken") String userToken,
                                          @Path("familymemberid") String familymemberid,
                                          @Body LeadModel leadModel);

    @GET("mobile/v1/familymember/{leadid}")
    Call<RestResponse> getFamilyMembers(@Header("utoken") String userToken,
                                        @Path("leadid") String leadid);

    @GET("mobile/v1/lead/proposalcodes")
    Call<RestResponse> getProposalCodes(@Header("utoken") String userToken);

    @GET("mobile/v1/lead/usedproposalcodes")
    Call<RestResponse> getusedProposalCodes(@Header("utoken") String userToken,
                                            @Query("lastfetchtime") String lastfetchtime);
}
