package com.outwork.sudlife.bcg.planner;

import android.content.Context;

import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.ui.models.UserTeams;

import java.util.HashMap;
import java.util.List;


public class PlannerMgr {

    private static Context context;
    private static PlannerDao plannerDao;

    public PlannerMgr(Context context) {
        this.context = context;
        plannerDao = plannerDao.getInstance(context);
    }

    private static PlannerMgr instance;

    public static PlannerMgr getInstance(Context context) {
        if (instance == null)
            instance = new PlannerMgr(context);
        return instance;
    }

    public List<PlannerModel> getOfflineplannerList(String userId, String status) {
        return plannerDao.getOfflineplannerList(userId, status);
    }

    public List<PlannerModel> getPlannerList(String userId) {
        return plannerDao.getPlannerList(userId);
    }

    public List<PlannerModel> getPlannerListByLead(String userId) {
        return plannerDao.getPlannerListByLead(userId);
    }

    public List<PlannerModel> getPlannerListByDate(String userId, int day, int month, int year) {
        return plannerDao.getPlannerListByDate(userId, day, month, year);
    }

    public List<PlannerModel> getPlannerListByBranches(String userId, String branchname, int month, int year) {
        return plannerDao.getPlannerListByBranches(userId, branchname, month, year);
    }

    public List<PlannerModel> getPlannerCreationCondition(String userId, String branchname, int day, int month, int year) {
        return plannerDao.getPlannerCreationCondition(userId, branchname, day, month, year);
    }

    public List<String> getPlannerDatesListByBranches(String userId, String branchname, String activitytype, int month, int year) {
        return plannerDao.getPlannerDatesListByBranches(userId, branchname, activitytype, month, year);
    }

    public List<PlannerModel> getPlannerListByOpportunityID(String userId, String oppid) {
        return plannerDao.getPlannerListByOpportunityID(userId, oppid);
    }

    public List<PlannerModel> getPlannerListBylocalOpportunityID(String userId, int oppid) {
        return plannerDao.getPlannerListBylocalOpportunityID(userId, oppid);
    }

    public List<PlannerModel> getPlannerListByLeadID(String userId, String leadid) {
        return plannerDao.getPlannerListByLeadID(userId, leadid);
    }

    public List<PlannerModel> getPlannerListBylocalLeadID(String userId, int leadid) {
        return plannerDao.getPlannerListBylocalLeadID(userId, leadid);
    }

    public void insertPlannerList(List<PlannerModel> list, String userId, String groupid) {
        plannerDao.insertPlannerList(list, userId, groupid);
    }

    public long insertPlanner(PlannerModel PlannerModel, String userId, String groupid) {
        return plannerDao.insertPlanner(PlannerModel, userId, groupid);
    }

    public void updatePlan(PlannerModel plannerModel) {
        plannerDao.updatePlan(plannerModel);
    }

    public void updateCheckoutPlanner(PlannerModel plannerModel) {
        plannerDao.updateCheckoutPlanner(plannerModel);
    }

    public void updateleadPlan(PlannerModel plannerModel) {
        plannerDao.updateleadPlan(plannerModel);
    }

    public void updateCheckInPlanner(PlannerModel plannerModel) {
        plannerDao.updateCheckInPlanner(plannerModel);
    }

    public void updateReschedulePlanner(PlannerModel plannerModel) {
        plannerDao.updateReschedulePlanner(plannerModel);
    }

    public void updatePlannerStatus(String lid, String userId, String cstatus) {
        plannerDao.updatePlannerStatus(lid, userId, cstatus);
    }

    public void updatePlannerID(String lid, String userId, String fid) {
        plannerDao.updatePlannerID(lid, userId, fid);
    }

    public void updatePlannerdummyOnline(String lid, String userId) {
        plannerDao.updatePlannerdummyOnline(lid, userId);
    }

    public void updateLeadid(int leadlocalid, String userId, String leadid) {
        plannerDao.updateLeadid(leadlocalid, userId, leadid);
    }


    public void insertPlannerNotificationList(List<PlannerModel> list, String userId, String groupid) {
        plannerDao.insertPlannerNotificationList(list, userId, groupid);
    }

    public void deleteRecordsBasedOnDay(String day) {
        plannerDao.deleteRecordsBasedOnDay(day);
    }

    public List<PlannerModel> getPlannerNotificationListByDate(String userId, int day, int month, int year) {
        return plannerDao.getPlannerNotificationListByDate(userId, day, month, year);
    }

    public HashMap<String, Integer> getPlannerListByScheduleDates(int month, int currentDate) {
        return plannerDao.getPlannerListByScheduleDates(month, currentDate);
    }


    public List<PlannerModel> getPlannerNotificationList(String userId) {
        return plannerDao.getPlannerNotificationList(userId);
    }

    public void updatePlanNotificationStatus(String activityId, String notificationStatus, String userId) {
        plannerDao.updatePlanNotificationStatus(activityId, notificationStatus, userId);
    }




    public void insertUserTeamsList(List<UserTeams> list, String userId, String groupid) {
        plannerDao.insertUserTeamsList(list, userId,groupid);
    }


    public List<UserTeams> getUserTeamList(String userId) {
        return plannerDao.getUserTeamList(userId);
    }
}