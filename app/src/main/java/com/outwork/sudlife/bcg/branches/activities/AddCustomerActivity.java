package com.outwork.sudlife.bcg.branches.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.branches.services.ContactsIntentService;
import com.outwork.sudlife.bcg.core.LocationProvider;
import com.outwork.sudlife.bcg.dto.Geocode;
import com.outwork.sudlife.bcg.dto.geocode.STATUS;
import com.outwork.sudlife.bcg.restinterfaces.POJO.GeoAddress;
import com.outwork.sudlife.bcg.restinterfaces.ReverseGeoInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.fragments.AddressFragment;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddCustomerActivity extends BaseActivity implements
        LocationProvider.LocationCallback,
        AddressFragment.OnFragmentInteractionListener {
    public static final String TAG = AddCustomerActivity.class.getSimpleName();

    private EditText custlocation, custorgtype;
    private TextView changelocation;
    private AutoCompleteTextView custcontact;
    private Button submit;
    private boolean isAddressChanged = false;
    private LocationProvider mLocationProvider;
    private double latitude, longitude;
    private String maddressline1, maddressline2, mcity, mstate, mzip, mfulladdress, mlocality, mlandmark;
    private String caddressline1, caddressline2, clocality, ccity, cstate, czip, cfulladdress;
    private BranchesModel branchesModel;
    private GeoAddress geoCurrentAddress = new GeoAddress();
    private boolean isSubmitted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        maddressline2 = "";
        maddressline1 = "";
        mlocality = "";
        mlandmark = "";
        mcity = "";
        caddressline2 = "";

        if (savedInstanceState != null) {
            isAddressChanged = savedInstanceState.getBoolean("addresschange");
            maddressline1 = savedInstanceState.getString("line1");
            mlocality = savedInstanceState.getString("locality");
            mcity = savedInstanceState.getString("city");
            mlandmark = savedInstanceState.getString("landmark");
        }

        mLocationProvider = new LocationProvider(this, this);
        mLocationProvider.connect();
        initializeViews();
        setListeners();
        initToolBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationProvider != null)
            mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mLocationProvider != null)
            mLocationProvider.disconnect();
    }

    private void initializeViews() {
        branchesModel = new BranchesModel();
        custcontact = (AutoCompleteTextView) findViewById(R.id.fCustomerName);
        custorgtype = (EditText) findViewById(R.id.fOrgType);
        custlocation = (EditText) findViewById(R.id.flocation);
        changelocation = (TextView) findViewById(R.id.changeloc);
        submit = (Button) findViewById(R.id.fsubmit);
    }

    private void createCustomerinfo() {
        branchesModel.setGroupid(groupId);
        branchesModel.setUserid(userid);
        branchesModel.setClassification(0);
        if (!TextUtils.isEmpty(custcontact.getText().toString())) {
            branchesModel.setCustomername(custcontact.getText().toString());
        }
        if (!TextUtils.isEmpty(custorgtype.getText().toString())) {
            branchesModel.setCustomertype(custorgtype.getText().toString());
        }
        GeoAddress address = createAddressObject(maddressline1, maddressline2, mlocality, mzip,
                mcity, mlandmark, mstate, false);
        branchesModel.setAddress(address);
    }

    private void setListeners() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, BranchesMgr.getInstance(AddCustomerActivity.this).getCustomerNamesList(userid));
        custcontact.setThreshold(1);//will start working from first character
        custcontact.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView

        changelocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new AddressFragment().newInstance(maddressline1, mlocality, mlandmark, mcity);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(android.R.id.content, fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (custcontact.getText().toString().isEmpty()) {
                    custcontact.setError("Customer Name Required");
                    custcontact.requestFocus();
                } else {
                    submit.setEnabled(false);
                    submit.setClickable(false);
                    isSubmitted = true;
                    createCustomerinfo();
                    BranchesMgr.getInstance(AddCustomerActivity.this).insertLocalCustomer(branchesModel, "offline");
                    ContactsIntentService.syncCustomerstoServer(AddCustomerActivity.this);
                    showAlert("", "Customer is saved successfully",
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent();
                                    intent.putExtra("customerObj", new Gson().toJson(branchesModel));
                                    setResult(RESULT_OK, intent);
                                    finish();
//                                    finish();
                                }
                            },
                            "",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }, false);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("addresschange", isAddressChanged);
        outState.putString("line1", maddressline1);
        outState.putString("locality", mlocality);
        outState.putString("city", mcity);
        outState.putString("landmark", mlandmark);
        outState.putString("fulladdress", mfulladdress);
        super.onSaveInstanceState(outState);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Add Customer");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                getLocation(latitude, longitude);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onSetAddress(String iaddressline, String ilocality, String ilandmark, String icity, String ifulladdress, boolean isAddressupdated) {
        if (!TextUtils.isEmpty(ifulladdress)) {
            custlocation.setText(ifulladdress);
            maddressline1 = iaddressline;
            mlocality = ilocality;
            mcity = icity;
            mlandmark = ilandmark;
            isAddressChanged = true;
            maddressline2 = "";
        }
    }

    private void getLocation(double lat, double lng) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ReverseGeoInterface.MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String latlong = lat + "," + lng;
        ReverseGeoInterface service = retrofit.create(ReverseGeoInterface.class);
        final Call<Geocode> locationdetails = service.getLocation("GEOMETRIC_CENTER", latlong, "AIzaSyB6QdommPXh8xlvGJL9IkauaajCpKfNWpc");
        locationdetails.enqueue(new Callback<Geocode>() {
            @Override
            public void onResponse(Call<Geocode> call, Response<Geocode> response) {
                Geocode geocode = response.body();
                if (geocode.getStatus() == STATUS.OK) {
                    if (geocode != null)
                        if (geocode.getResults() != null)
                            if (geocode.getResults().size() > 0) {
                                if (geocode.getResults().get(0).getAddressComponents() != null) {
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(0).getLongName()))
                                        caddressline1 = geocode.getResults().get(0).getAddressComponents().get(0).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(1).getLongName()))
                                        caddressline2 = geocode.getResults().get(0).getAddressComponents().get(1).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(2).getLongName()))
                                        clocality = geocode.getResults().get(0).getAddressComponents().get(2).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(3).getLongName()))
                                        ccity = geocode.getResults().get(0).getAddressComponents().get(3).getLongName();
                                    setCurrentAddressText(caddressline1, caddressline2, clocality, ccity, "", "", "");
                                }
                            }
                }
            }

            @Override
            public void onFailure(Call<Geocode> call, Throwable t) {
            }
        });
    }

    private void setCurrentAddressText(String adline1, String adline2, String adlocality, String adcity,
                                       String adzip, String state, String countrycode) {
        if (countrycode.equalsIgnoreCase("US")) {
            cfulladdress = adline1 + " " + adline2;
            geoCurrentAddress.setAddressline1(adline1);
            geoCurrentAddress.setCity(adcity);
            geoCurrentAddress.setState(state);
            custlocation.setText(cfulladdress);
            if (!isAddressChanged) {
                maddressline1 = adline1;
                mlocality = adlocality;
                mcity = adcity;
                mlandmark = "";
                //  maddressline2 = adline2;
                mzip = adzip;
                mstate = state;
            }
        } else {
            cfulladdress = adline1 + " ";
            if (!TextUtils.isEmpty(adline2)) {
                cfulladdress = cfulladdress + adline2 + " ";
            } else if (!TextUtils.isEmpty(adlocality)) {
                cfulladdress = cfulladdress + adlocality + " ";
            }
            //Do we really need to check for adline2 and locality crosc check again
            if (!TextUtils.isEmpty(adcity)) {
                cfulladdress = cfulladdress + adcity;
            }
            if (!TextUtils.isEmpty(adline2)) {
                geoCurrentAddress.setAddressline1(adline1 + adline2);
            } else {
                geoCurrentAddress.setAddressline1(adline1);
            }
            geoCurrentAddress.setLocality(adlocality);
            geoCurrentAddress.setCity(adcity);
            geoCurrentAddress.setState(state);
            if (!isAddressChanged) {
                maddressline1 = adline1;
                if (!TextUtils.isEmpty(adline2)) {
                    maddressline1 = maddressline1 + " " + adline2;
                }
                custlocation.setText(cfulladdress);
                mlocality = adlocality;
                mcity = adcity;
                mlandmark = "";
                //  maddressline2 = adline2;
                mzip = adzip;
                mstate = state;
            }
        }
    }

    private GeoAddress createAddressObject(String line1, String line2, String locality,
                                           String zip, String city, String landmark, String state, boolean isCurrent) {
        GeoAddress addressObj = new GeoAddress();

        if (!TextUtils.isEmpty(line2)) {
            addressObj.setAddressline1(line1 + " " + line2);
        } else {
            addressObj.setAddressline1(line1);
        }
        addressObj.setLocality(locality);
        addressObj.setCity(city);
        addressObj.setLandmark(landmark);
        addressObj.setState(state);
        return addressObj;
    }
}
