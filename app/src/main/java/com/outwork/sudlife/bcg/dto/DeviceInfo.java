package com.outwork.sudlife.bcg.dto;

/**
 * Created by Panch on 2/15/2017.
 */

public class DeviceInfo {

    private String apprelease;
    private String appversioncode;
    private String libversion;
    private String osversion;
    private String model;
    private  String os;
    private String manufacturer;

    public String getApprelease() {
        return apprelease;
    }

    public void setApprelease(String apprelease) {
        this.apprelease = apprelease;
    }

    public String getAppversioncode() {
        return appversioncode;
    }

    public void setAppversioncode(String appversioncode) {
        this.appversioncode = appversioncode;
    }

    public String getLibversion() {
        return libversion;
    }

    public void setLibversion(String libversion) {
        this.libversion = libversion;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
