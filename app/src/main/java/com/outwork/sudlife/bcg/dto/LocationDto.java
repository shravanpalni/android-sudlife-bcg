package com.outwork.sudlife.bcg.dto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LocationDto implements Serializable {

	private String latitude;
	private String longitude;
	private String locName;
	private String zipCode;
	private String city;
	private String state;
	private String venuename;
	private String addressline1;
	private String addressline2;
	private String country;
	
	public void parseByPlaceId(JSONObject locationJSON) throws JSONException {
		
		if(locationJSON.getString("status").equals("OK")){
			
			JSONObject results = locationJSON.getJSONObject("result");
			
			latitude = results.getJSONObject("geometry").getJSONObject("location").getString("lat");
			longitude = results.getJSONObject("geometry").getJSONObject("location").getString("lng");
			
			JSONArray address_foramt = results.getJSONArray("address_components");
			
			for(int i=0;i<address_foramt.length();i++){
				
				JSONObject userLocation = address_foramt.getJSONObject(i);
				String type = (String) userLocation.getJSONArray("types").get(0);
				if(type.equals("sublocality_level_1")){
					locName = userLocation.getString("long_name");
				}else if(type.equals("locality")){
					city = userLocation.getString("long_name");
				}else if(type.equals("country")){
					country = userLocation.getString("long_name");
				}else if(type.equals("postal_code")){
					zipCode = userLocation.getString("long_name");
				}else if(type.equals("administrative_area_level_1")){
					state = userLocation.getString("long_name");
				}
				
			}
			
		}
		
	}
	
	public void parse(JSONObject locationJSON) throws JSONException {
		
		if(locationJSON.getString("status").equals("OK")){
			
			JSONArray results = locationJSON.getJSONArray("results");
			JSONObject address = results.getJSONObject(0);
			JSONArray address_foramt = address.getJSONArray("address_components");
			
			for(int i=0;i<address_foramt.length();i++){
				
				JSONObject userLocation = address_foramt.getJSONObject(i);
				String type = (String) userLocation.getJSONArray("types").get(0);
				if(type.equals("sublocality_level_1")){
					locName = userLocation.getString("long_name");
				}else if(type.equals("locality")){
					city = userLocation.getString("long_name");
				}else if(type.equals("country")){
					country = userLocation.getString("long_name");
				}else if(type.equals("postal_code")){
					zipCode = userLocation.getString("long_name");
				}else if(type.equals("administrative_area_level_1")){
					state = userLocation.getString("long_name");
				}
				
			}
			
		}
		
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLocName() {
		return locName;
	}
	public void setLocName(String locName) {
		this.locName = locName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	public String getAddressline1() {
		return addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getVenuename() {
		return venuename;
	}

	public void setVenuename(String venuename) {
		this.venuename = venuename;
	}





	
}
