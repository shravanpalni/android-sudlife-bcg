package com.outwork.sudlife.bcg.targets.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.branches.activities.AddCustomerActivity;
import com.outwork.sudlife.bcg.branches.adapter.CustomerOnlineAdapter;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
 import com.outwork.sudlife.bcg.targets.services.TargetsIntentService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.text.DateFormatSymbols;
import java.util.List;

import static android.widget.LinearLayout.VISIBLE;

public class ListOfTargetsActivity extends BaseActivity {
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;
    private ListView custListView;
    private String groupId;
    private ProgressBar progressBar;
    private FloatingActionButton addMember;
    private List<BranchesModel> customerList;
    private CustomerOnlineAdapter customerOnlineAdapter;
    private RelativeLayout searchLayout;
    private TextView noinfo;
    private String userId, type;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    TextView tvMonth;
    String monthsArray[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_targets_list);

       tvMonth = (TextView) findViewById(R.id.toolbar_title);
        long currentmnth = Long.parseLong(TimeUtils.getCurrentDate("MM"));
        tvMonth.setText(getMonth(currentmnth));

        tvMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthsCalendarDialog();
            }
        });
        mgr = LocalBroadcastManager.getInstance(this);
        if (SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                TargetsIntentService.insertTargetRecords(ListOfTargetsActivity.this);
            }
        }
        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
        progressBar = (ProgressBar) findViewById(R.id.mProgressBar);
        noinfo = (TextView) findViewById(R.id.noMembers);
        custListView = (ListView) findViewById(R.id.lv_targets_list);
        addMember = (FloatingActionButton) findViewById(R.id.addcustomer);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("TYPE")) {
            type = getIntent().getStringExtra("TYPE");
        }
        initToolbar();
        getCustomerList();
        setListener();
       /* findViewById(R.id.tv_monthly_summery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListOfTargetsActivity.this, MonthlySummeryViewActivity.class);
                startActivity(intent);
            }
        });*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getCustomerList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("targets_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    private void setListener() {
        addMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNotNullAndNotEmpty(type) && type.equalsIgnoreCase("ADD")) {
                    Intent intent = new Intent(ListOfTargetsActivity.this, AddCustomerActivity.class);
                    startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
                } else {
                    Intent in = new Intent(ListOfTargetsActivity.this, AddCustomerActivity.class);
                    startActivity(in);
                }
            }
        });
        custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                BranchesModel branchesModel = (BranchesModel) parent.getItemAtPosition(position);
                Intent intent = new Intent(ListOfTargetsActivity.this, BranchesViewDetailsActivity.class);
                intent.putExtra("customerObj", new Gson().toJson(branchesModel));
                startActivity(intent);
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getCustomerList();
            }
        };
    }

    private void getCustomerList() {
        customerList = new BranchesMgr(this).getonlineCustomerList(groupId, userId, "");
        if (customerList.size() > 0) {
            customerOnlineAdapter = new CustomerOnlineAdapter(this, customerList, groupId, userId);
            custListView.setAdapter(customerOnlineAdapter);
            progressBar.setVisibility(View.GONE);
            if (customerList.size() > 10) {
                setUpSearchLayout();
            }
        } else {
            setNoCustomers();
        }
    }

    private void setNoCustomers() {
        if (progressBar.VISIBLE == 0) {
            progressBar.setVisibility(View.GONE);
        }
        noinfo.setVisibility(VISIBLE);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mListToolBar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Targets");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setUpSearchLayout() {
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchLayout.setVisibility(VISIBLE);
        EditText searchText = (EditText) findViewById(R.id.searchText);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (customerOnlineAdapter != null && s != null) {
                    customerOnlineAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public String getMonth(long month) {
        return new DateFormatSymbols().getMonths()[(int) month - 1];
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                data.putExtra("customerObj", data.getStringExtra("customerObj"));
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    public void monthsCalendarDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.list_item_for_month, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_month);
        ArrayAdapter arrayAdapter = new ArrayAdapter(ListOfTargetsActivity.this, android.R.layout.simple_list_item_1, monthsArray);
        lvItems.setAdapter(arrayAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvMonth.setText(adapterView.getItemAtPosition(i).toString());
                deleteDialog.dismiss();
            }
        });

        deleteDialog.show();
    }
}