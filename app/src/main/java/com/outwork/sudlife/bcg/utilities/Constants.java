package com.outwork.sudlife.bcg.utilities;

public class Constants {
    public static final String APP = "OutWork";
    public static final String ME = "Sudlife";

    //Status constants.
    public static final String STATUS_FAILED = "Failed";
    public static final String STATUS_FAILURE = "FAILURE";
    public static final String STATUS_SUCCESS = "Success";

    public static final String CUSTOMERS_LOADED = "customers_loaded";
    public static final String CONTACTS_LOADED = "contacts_loaded";
    public static final String PROFILE_LOADED = "profile_loaded";
    public static final String ENABLE_PROGRESS = "false";
    public static final String PLANNER_LOADED = "planner_loaded";
    public static final String PROPOSAL_CODES_LOADED = "proposalcodes_LOADED";
    public static final String PROPOSAL_USED_CODES_LOADED = "proposalusedcodes_LOADED";
    public static final String LEADS_LOADED = "leads_loaded";
    public static final String TARGETS_LOADED = "targets_loaded";
    public static final String SUPERVISORS_LOADED = "supervisors_loaded";
    public static final String STAGES_LOADED = "stages_loaded";

    //Constants about user status.
    public static final String USER_STATUS_KEY = "status_key";

    public static final int USER_REGISTERED = 0;
    public static final int USER_ACTIVE = 1;
    public static final int USER_INACTIVE = 2;

    //Constants for loginType
    public static final int LOGIN_EMAIL_TYPE = 1;

    //Shared Preference key.
    public static final String preference_file_key = "com.outwork.sudlife.bcg.PREFERENCE_FILE_KEY";
    public static final String USER_TOKEN_KEY = "usertoken";

    public static final String USER_STATUS = "user_status";
    public static final String FEED_DATA = "feed_data";
    public static final String ADMIN_FEED_DATA = "admin_feed_data";
    public static final String SOCIAL_FEED_DATA = "social_feed_data";
    public static final String S3CREDENTIALS = "s3credentials";
    public static final String ACCESSKEY = "accesskey";
    public static final String SECRETKEY = "secretkey";
    public static final String SESSIONTOKEN = "sessiontoken";
    public static final String EXPIRYTIME = "expirytime";
    public static final String BUCKETNAME = "bucketname";


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String PROPERTY_REG_ID = "registration_id";

    //************************************timings
    public static final String PUNCHIN_FLAG = "punchinflag";
    public static final String PUNCH_TIMESHEETID = "timesheetid";

    //************************************fcm token
    public static final String FCM_TOKEN = "fcm_token";
    public static final String OLD_FCM_TOKEN = "old_fcm_token";

    //******************************signin
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_TOKEN = "user_token";
    public static final String USERID = "user_id";
    public static final String USERNAME = "user_name";
    public static final String LOGINTYPE = "login_type";
    public static final String LOGINID = "loginID";
    public static final String LOGININTIME = "loginintime";
    public static final String INTERNALROLE = "internalrole";
    public static final String DISPLAYROLE = "displayrole";
    public static final String GROUPTITLE = "group_title";
    public static final String GROUPID = "groupid";
    public static final String GRP_OBJECTTYPE = "grp_objecttype";

    public static final String GRP_DISPLAYNAME = "grp_displayname";
    public static final String GRP_ICON = "groupicon";
    public static final String GRP_IMAGE = "groupimage";
    public static final String GRP_CODE = "groupcode";

    public static final String isFeatureEnabled = "isFeatureEnabled";
    public static final String isProductEnabled = "isProductEnabled";
    public static final String isHealthCare = "isHealthCare";
    public static final String isOrderProductsAvailable = "isOrderProductsAvailable";
    public static final String isTaskAvailable = "isTaskAvailable";


    //*******************profile
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "EMAIL";
    public static final String PROFILE_URL = "PROFILE_URL";
    public static final String PHONENO = "PHONENO";
    public static final String CITY = "CITY";
    public static final String COUNTRY = "COUNTRY";
    public static final String SALUTATION = "SALUTATION";
    public static final String WEBSITE = "WEBSITE";
    public static final String EMPLOYEETYPE = "employeetype";
    public static final String SUPERVISOREMPTYPE = "supervisoremptype";
    public static final String USERLEVEL = "userlevel";

    public static final String emailvisibility = "emailvisibility";
    public static final String linkedinvisibility = "linkedinvisibility";
    public static final String fbvisibility = "fbvisibility";
    public static final String twittervisibility = "twittervisibility";
    public static final String orgvisibility = "orgvisibility";
    public static final String designationvisibility = "designationvisibility";
    public static final String locationvisibility = "locationvisibility";
    public static final String websitevisibility = "websitevisibility";
    public static final String numbervisibility = "numbervisibility";

    //*****************************************task
    public static final String COMPLETED = "Completed";
    public static final String RESCHEDULE = "ReScheduled";
    public static final String NOTSTARTED = "NotStarted";
    public static final String INPROGRESS = "InProgress";
    public static final String PENDING = "Pending";

    public static final String OPPORTUNITY_LAST_FETCH_TIME = "opportunity_lastfetchtime";
    public static final String LEAD_LAST_FETCH_TIME = "lead_lastfetchtime";
    public static final String PROPOSAL_LAST_FETCH_TIME = "proposal_lastfetchtime";
    public static final String BRANCHES_LAST_FETCH_TIME = "branches_lastfetchtime";
    public static final String LEADBYUSER_LAST_FETCH_TIME = "leadbyuser_lastfetchtime";
    public static final String TARGETS_LAST_FETCH_TIME = "targets_lastfetchtime";
    public static final String TIMING_LAST_FETCH_TIME = "timing_lastfetchtime";
    public static final String LAST_FETCH_TIME = "lastfetchtime";
    public static final String isLocationEnabled = "isLocationEnabled";

    public static final String NOFITY7 = "NOTIFY7";
    public static final String NOFITY9 = "NOTIFY9";
    public static final String NOTIFY17 = "NOTIFY17";


    public static String success = "Success";
    public static String failed = "Failed";

    //*****************************************mtp
    public static String month = "month";
    public static String year = "year";
    public static final String MTP_LAST_FETCH_TIME = "mtp_lastfetchtime";

    //*****************************************planner
    public static final String PLANNER_LAST_FETCH_TIME = "plan_lastfetchtime";
    public static final String PLAN_STATUS = "planstatus";
    public static final String PLAN_TYPE = "plantype";
    public static final String PLAN_DATE = "plandate";
    public static final String PLANNED = "planned";
    public static final String UNPLANNED = "unplanned";
    public static final String SUPERVISORS = "supervisor";
    public static final String SUPERVISOR_ID = "supervisorID";
    public static final String password = "pwd";
}
