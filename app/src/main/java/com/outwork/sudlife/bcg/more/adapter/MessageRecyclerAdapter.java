package com.outwork.sudlife.bcg.more.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.more.models.MessageModel;
import com.outwork.sudlife.bcg.more.models.UIMessageViewDto;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Panch on 11/23/2015.
 */
public class MessageRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<MessageModel> adminList = new ArrayList<MessageModel>();
    private String description;
    private static final int VIEW_ORDINARY = 0;
    private static final int VIEW_EXTRA = 1;

    public static class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView username;
        TextView postedDate;
        ImageView postType;
        TextView cDescription;
        TextView title;
        ImageView feedImage;
        RelativeLayout additional;
        ImageView addShare;
        ImageView addComment;
        TextView commentCount;

        public FeedViewHolder(View v) {
            super(v);
            postType = (ImageView) v.findViewById(R.id.cPostType);
            username = (TextView) v.findViewById(R.id.cTitle);
            postedDate = (TextView) v.findViewById(R.id.cPostedDate);
            cDescription = (TextView) v.findViewById(R.id.cDescription);
            title = (TextView) v.findViewById(R.id.cTitle2);
            additional = (RelativeLayout) v.findViewById(R.id.additional);
            addShare = (ImageView) itemView.findViewById(R.id.addshare);
            addComment = (ImageView) itemView.findViewById(R.id.addcomment);
            commentCount = (TextView) itemView.findViewById(R.id.commentcount);

            // v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    class FeedExtraViewHolder extends FeedViewHolder {

        ImageView feedImage;

        public FeedExtraViewHolder(View itemView) {
            super(itemView);
            this.feedImage = (ImageView) itemView.findViewById(R.id.feedImage);
        }
    }

    @Override
    public int getItemViewType(int position) {
//        CommonDto dto = (CommonDto) this.adminList.get(position);
//        UIMessageViewDto feeddto = (UIMessageViewDto) dto;
//
//        if (!TextUtils.isEmpty(feeddto.getImageUrl())) {
//            return VIEW_EXTRA;
//        } else {
            return VIEW_ORDINARY;
//        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MessageRecyclerAdapter(Context mContext, List<MessageModel> listadminitems) {
        this.context = mContext;
        this.adminList = listadminitems;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        switch (viewType) {
            case VIEW_EXTRA:
                View v = inflater.inflate(R.layout.list_feed_item_extra, parent, false);
                viewHolder = new FeedExtraViewHolder(v);
                break;
            case VIEW_ORDINARY:
                View v1 = inflater.inflate(R.layout.list_feed_item_latest, parent, false);
                viewHolder = new FeedViewHolder(v1);
                break;
            default:
                View v3 = inflater.inflate(R.layout.list_feed_item_latest, parent, false);
                viewHolder = new FeedViewHolder(v3);
                break;
        }
        return viewHolder;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

//        CommonDto commonDto = (CommonDto) this.adminList.get(position).;
//        UIMessageViewDto dto = (UIMessageViewDto) commonDto;
//
//        switch (holder.getItemViewType()) {
//            case VIEW_ORDINARY:
//                FeedViewHolder viewHolder = (FeedViewHolder) holder;
//                configureOrdinaryViewHolder(viewHolder, dto);
//                break;
//            case VIEW_EXTRA:
//                FeedExtraViewHolder viewHolderExtra = (FeedExtraViewHolder) holder;
//                configureExtraViewHolder(viewHolderExtra, dto);
//                break;
//            default:
//                FeedViewHolder viewHolder1 = (FeedViewHolder) holder;
//                configureOrdinaryViewHolder(viewHolder1, dto);
//                break;
//
//        }


    }


    @Override
    public int getItemCount() {

        if (adminList != null) {
            return adminList.size();
        }
        return 0;
    }

    public void setItems(List<MessageModel> msgList) {
        if (this.adminList.size() > 0) {
            this.adminList.clear();
        }
        this.adminList = msgList;

    }

    private void configureOrdinaryViewHolder(FeedViewHolder holder, UIMessageViewDto dto) {
        final String userName;
        if (!TextUtils.isEmpty(dto.getUserName())) {
            userName = dto.getUserName();
        } else {
            userName = dto.getUserName();
        }

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate color based on a key (same key returns the same color), useful for list/grid views
        final int color2;
        final String name;
        if (!TextUtils.isEmpty(dto.getUserName())) {
            color2 = generator.getColor(dto.getUserName().substring(0, 1));
            name = dto.getUserName().substring(0, 1).toUpperCase();
        } else {
            color2 = 12343;
            name = "N";
        }

        TextDrawable drawable = TextDrawable.builder().buildRound(name, color2);
        if (drawable != null) {
            holder.postType.setImageDrawable(drawable);
        } else {
            holder.postType.setImageDrawable(context.getResources().getDrawable(R.mipmap.messagee));
        }

        holder.username.setText(userName);


        // holder.postType.setBackgroundResource(R.mipmap.messagee);
        if (!TextUtils.isEmpty(dto.getPostedDate())) {
            String formattedDay = Utils.getFeedListFormatDate(dto.getPostedDate(), true, 2);
            holder.postedDate.setText(formattedDay);
        }


        holder.title.setText(dto.getTitle());

        //   holder.cDescription.setText(dto.getTitle());


        if (!TextUtils.isEmpty(dto.getDescription())) {
            if (dto.getMessageType().equalsIgnoreCase("announce") || dto.getMessageType().equalsIgnoreCase("promotion")) {
                description = Html.fromHtml(dto.getDescription()).toString();
            } else {
                description = dto.getDescription();
            }
            //   String descriptionText = dto.getDescription();
            description = description.replaceAll("\r\n\\s+/\\s+/", " ");
            holder.cDescription.setText(description);
        }

        setAdditionalLayout(holder, dto);


    }

    private void configureExtraViewHolder(FeedExtraViewHolder holder, UIMessageViewDto dto) {
        final String userName;
        if (!TextUtils.isEmpty(dto.getUserName())) {
            userName = dto.getUserName();
        } else {
            userName = dto.getUserName();
        }

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate color based on a key (same key returns the same color), useful for list/grid views
        final int color2;
        final String name;
        if (!TextUtils.isEmpty(dto.getUserName())) {
            color2 = generator.getColor(dto.getUserName().substring(0, 1));
            name = dto.getUserName().substring(0, 1).toUpperCase();
        } else {
            color2 = 12343;
            name = "N";
        }

        TextDrawable drawable = TextDrawable.builder().buildRound(name, color2);
        if (drawable != null) {
            holder.postType.setImageDrawable(drawable);
        } else {
            holder.postType.setImageDrawable(context.getResources().getDrawable(R.mipmap.messagee));
        }

        holder.username.setText(userName);


        // holder.postType.setBackgroundResource(R.mipmap.messagee);
        if (!TextUtils.isEmpty(dto.getPostedDate())) {
            String formattedDay = Utils.getFeedListFormatDate(dto.getPostedDate(), true, 2);
            holder.postedDate.setText(formattedDay);
        }


        holder.title.setText(dto.getTitle());

        //   holder.cDescription.setText(dto.getTitle());


        if (!TextUtils.isEmpty(dto.getDescription())) {
            if (dto.getMessageType().equalsIgnoreCase("announce") || dto.getMessageType().equalsIgnoreCase("promotion")) {
                description = Html.fromHtml(dto.getDescription()).toString();
            } else {
                description = dto.getDescription();
            }
            //   String descriptionText = dto.getDescription();
            description = description.replaceAll("\r\n\\s+/\\s+/", " ");
            holder.cDescription.setText(description);
        }
        if (!TextUtils.isEmpty(dto.getImageUrl())) {
            setImageView(dto.getImageUrl(), holder);
        }
        setAdditionalLayout(holder, dto);

    }

    private void setImageView(String imageUrl, FeedExtraViewHolder holder) {
        holder.feedImage.setVisibility(View.VISIBLE);
        //holder.feedImage.setAspectRatio(1.78);
       /* Glide.with(context)
                .load(imageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(new ColorDrawable(ContextCompat.getColor(context, R.color.text_color_light_grey)))
                .into(holder.feedImage);*/


    }

    private void setAdditionalLayout(FeedViewHolder holder, UIMessageViewDto dto) {

        if (dto.isCommentEnabled() || dto.isShareEnabled()) {
            holder.additional.setVisibility(View.VISIBLE);
            if (dto.isCommentEnabled()) {
                holder.addComment.setVisibility(View.VISIBLE);
            }
            if (dto.getCommentCount() > 0) {
                holder.commentCount.setVisibility(View.VISIBLE);
                holder.commentCount.setText(String.valueOf(dto.getCommentCount()));
            } else {
                holder.commentCount.setVisibility(GONE);
            }
           /* if (dto.isShareEnabled()){
                holder.addShare.setVisibility(View.VISIBLE);
            } else {
                holder.addShare.setVisibility(GONE);
            }*/
        } else {
            holder.additional.setVisibility(GONE);
        }
    }

    ;

    private void setAdditionalLayout(FeedExtraViewHolder holder, UIMessageViewDto dto) {

        if (dto.isCommentEnabled() || dto.isShareEnabled()) {
            holder.additional.setVisibility(View.VISIBLE);
            if (dto.isCommentEnabled()) {
                holder.addComment.setVisibility(View.VISIBLE);
            }

            if (dto.getCommentCount() > 0) {
                holder.commentCount.setVisibility(View.VISIBLE);
                holder.commentCount.setText(String.valueOf(dto.getCommentCount()));
            }
            if (dto.isShareEnabled()) {
                holder.addShare.setVisibility(View.VISIBLE);
            }
        }

    }

    ;
}