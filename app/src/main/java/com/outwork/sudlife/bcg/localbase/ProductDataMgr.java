package com.outwork.sudlife.bcg.localbase;

import android.content.Context;

import java.util.List;

import com.outwork.sudlife.bcg.branches.models.ContactModel;

/**
 * Created by Panch on 2/23/2017.
 */

public class ProductDataMgr {
    private static Context context;
    private static ProductMasterDao productMasterDao;
    private static ContactModel contactModel;

    public ProductDataMgr(Context context) {
        this.context = context;
        productMasterDao = productMasterDao.getInstance(context);
    }

    private static ProductDataMgr instance;

    public static ProductDataMgr getInstance(Context context) {
        if (instance == null)
            instance = new ProductDataMgr(context);

        return instance;
    }

    public void insertMasterProductList(List<ProductMasterDto> prodList,String groupId) {
        productMasterDao.insertMasterProuctList(prodList,groupId);
    }

    public List<ProductMasterDto> getProductList( String groupId){
           return productMasterDao.getProductList(groupId);

    }

    public List<ProductMasterDto> getProductList( String groupId,String userInput){
        return productMasterDao.getProductList(groupId,userInput);

    }

    public String getProductId(String groupId,String prodName){
        return productMasterDao.getProductId(groupId,prodName);
    }

}



