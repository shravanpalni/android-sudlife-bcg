package com.outwork.sudlife.bcg;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.onesignal.OneSignal;

import com.outwork.sudlife.bcg.dto.UserDetailsDto;
import com.outwork.sudlife.bcg.dto.UserObjectDto;
import com.outwork.sudlife.bcg.more.models.S3Credentials;
import com.outwork.sudlife.bcg.notifications.OutworkNotificationOpenedHandler;
import com.outwork.sudlife.bcg.notifications.OutworkNotificationReceivedHandler;

import com.outwork.sudlife.bcg.utilities.AnalyticsApplication;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import io.fabric.sdk.android.Fabric;


public class IvokoApplication extends AnalyticsApplication {
    public enum POST_TYPE {
        MESSAGE, MAIL, EMAIL, SOCIAL, CONVERSATIONS, MEMBERPOSTS, PROMOTIONS,
        ANNOUNCEMENTS, POLL, EVENT,
        CLASSIFIED, ASK_SERVICE_PROVIDER, RECOMM_SERVICE_PROVIDER, CLASSIFIED_BUY,
    };

    public static Typeface robotoBlackTypeface, robotoBoldTypeface,
            robotoMediumTypeface, robotoCondensedBoldTypeface, robotoCondensedLightTypeface, robotoCondensedRegularTypeface,
            robotoTypeface, robotoLightTypeface, robotoThinTypeface;
    public static String robotoMediumFont, robotoThinFont;
    private UserDetailsDto userDetails = null;
    private static IvokoApplication instance = null;

    private int AppVersion = 1;

    public int getAppVersion() {
        return AppVersion;
    }

    private UserObjectDto userObject;

    public UserObjectDto getUserObject() {
        return userObject;
    }

    public void setUserObject(UserObjectDto userObject) {
        this.userObject = userObject;
    }

    public UserDetailsDto getUserDetails() {
        return userDetails;
    }

    private String groupId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String currentGroupId) {
        this.groupId = currentGroupId;
    }

    public static IvokoApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        FirebaseApp.initializeApp(IvokoApplication.this);
        if (userDetails == null) {
            generateUserDetails();
        }
        SharedPreferenceManager.getInstance().init(this);
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OutworkNotificationOpenedHandler(IvokoApplication.this))
                .setNotificationReceivedHandler(new OutworkNotificationReceivedHandler())
                .init();
        OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);

        robotoMediumFont = "Roboto-Medium.ttf";
        robotoThinFont = "Roboto-Thin.ttf";
        robotoMediumTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        robotoBlackTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
        robotoBoldTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        robotoCondensedLightTypeface = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Light.ttf");
        robotoCondensedBoldTypeface = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        robotoCondensedRegularTypeface = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        robotoTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        robotoLightTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        robotoThinTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void generateUserDetails() {
        userDetails = new UserDetailsDto();
        SharedPreferences preference = getSharedPreferences(Constants.preference_file_key, Context.MODE_PRIVATE);
        userDetails.setUserName(preference.getString(Constants.USER_EMAIL, ""));
        userDetails.setPassword(preference.getString(Constants.USER_PASSWORD, ""));
        userDetails.setStatus(preference.getInt(Constants.USER_STATUS, -1));
    }

    public void writePreference(String userName, String password) {
        SharedPreferences preference = getSharedPreferences(Constants.preference_file_key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preference.edit();
        edit.remove(Constants.USER_EMAIL);
        edit.remove(Constants.USER_PASSWORD);
        edit.commit();
        edit.putString(Constants.USER_EMAIL, userName);
        edit.putString(Constants.USER_PASSWORD, password);
        edit.putInt(Constants.USER_STATUS, 1);
        edit.commit();
    }

    public void saveFCMToken(Context context, String FCMtoken) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor;
        editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_firebase_instance_id_key), FCMtoken);
        editor.commit();
    }

    public static String getFCMToken(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String key = context.getString(R.string.pref_firebase_instance_id_key);
        String default_value = context.getString(R.string.pref_firebase_instance_id_default_key);
        return sharedPreferences.getString(key, default_value);
    }

    public void saveOldFCMToken(Context context, String FCMtoken) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor;
        editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_firebase_instance_old_key), FCMtoken);
        editor.remove(context.getString(R.string.pref_firebase_instance_id_key));
        editor.commit();
    }

    public void writeS3Credentials(S3Credentials credentials) {
        SharedPreferences preference = getSharedPreferences(Constants.preference_file_key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preference.edit();
        edit.putString(Constants.ACCESSKEY, credentials.getAccessKey());
        edit.putString(Constants.SECRETKEY, credentials.getSecretKey());
        edit.putString(Constants.SESSIONTOKEN, credentials.getSessionToken());
        edit.putString(Constants.EXPIRYTIME, credentials.getExpiryTime());
        edit.putString(Constants.BUCKETNAME, credentials.getBucketName());
        edit.commit();
    }

    public S3Credentials getS3Credentials() {
        S3Credentials credentials = new S3Credentials();
        SharedPreferences preference = getSharedPreferences(Constants.preference_file_key, Context.MODE_PRIVATE);
        credentials.setAccessKey(preference.getString(Constants.ACCESSKEY, ""));
        credentials.setSessionToken(preference.getString(Constants.SESSIONTOKEN, ""));
        credentials.setSecretKey(preference.getString(Constants.SECRETKEY, ""));
        credentials.setExpiryTime(preference.getString(Constants.EXPIRYTIME, ""));
        credentials.setBucketName(preference.getString(Constants.BUCKETNAME, ""));
        return credentials;
    }

}
