package com.outwork.sudlife.bcg.more.models;

import android.text.TextUtils;

import com.outwork.sudlife.bcg.IvokoApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailDto extends CommonDto {

    private String MessageId;
    private String MessageType;
    private String Title;
    private String Description;
    private String postedDate;
    private String userid;
    private String userName;
    private String userEmail;
    private String textDescription;


    private List<Attachment> attachmentList;
    private List<Email> toemailList;
    private List<Email> ccemailList;

    private String shareText="Click here to find more";
    private String shareable="";
    private String shareUrl="";


    private boolean isCommentEnabled;
    private int commentCount;
    private boolean isShareEnabled;


    public static final String JSON_MESSAGGEID = "messageid";
    public static final String JSON_TITLE = "title";
    public static final String JSON_MSGTYPE = "type";
    public static final String JSON_DESC = "description";
    public static final String JSON_EXPIRY = "expiry";

    public static final String JSON_POSTEDBY = "postedby";
    public static final String JSON_POSTED_USEREMAIL ="useremail";
    public static final String JSON_POSTED_USERNAME ="username";
    public static final String JSON_POST_DATE = "posteddate";
    public static final String JSON_POST_GRP = "postedgrp";
    public static final String JSON_FILE = "file";
    public static final String JSON_USERID = "userid";


    public static final String JSON_GROUP_IDS = "groupids";
    public static final String JSON_GROUP_NAMES = "groupnames";
    public static final String JSON_USEREMAIL ="email";
    public static final String JSON_USERNAME ="name";
    public static final String JSON_TO_EMAIL = "to";
    public static final String JSON_CC_EMAIL = "cc";
    private static final String JSON_SHARETEXT="sharetext";
    private static final String JSON_SHAREURL="shareurl";


    public static final String JSON_COMMENTENBALED = "commentflag";
    public static final String JSON_COMMENTCOUNT = "commentcount";
    public static final String JSON_SHARE="share";
    public static final String JSON_SHAREENABLED ="shareable";




    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String messageType) {
        MessageType = messageType;
    }

    public String getMessageId() {
        return MessageId;
    }

    public void setMessageId(String messageId) {
        this.MessageId = messageId;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }



    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public String getUserName(){
        return userName;
    }

    public void setUserName(String userName){
        this.userName =userName;
    }

    public String getUserEmail(){
        return userEmail;
    }

    public void setUserEmail(String userEmail){
        this.userEmail =userEmail;
    }

    public List<Email> getccemailList() {
        return ccemailList;
    }

    public void setccemailList(List<Email> ccemailList) {
        this.ccemailList = ccemailList;
    }

    public List<Email> getToemailList() {
        return toemailList;
    }

    public void setToemailList(List<Email> toemailList) {
        this.toemailList = toemailList;
    }


    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareable() {
        return shareable;
    }

    public void setShareable(String shareable) {
        this.shareable = shareable;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public boolean isCommentEnabled() {
        return isCommentEnabled;
    }

    public void setCommentEnabled(boolean commentEnabled) {
        isCommentEnabled = commentEnabled;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public boolean isShareEnabled() {
        return isShareEnabled;
    }

    public void setShareEnabled(boolean shareEnabled) {
        isShareEnabled = shareEnabled;
    }


    public static MailDto parse(JSONObject mailObject)throws JSONException {

        MailDto mailDto = new MailDto();
        mailDto.setIvokoType(IvokoApplication.POST_TYPE.MAIL);
        if (!TextUtils.isEmpty(mailObject.getString(JSON_MESSAGGEID))) {
            mailDto.setMessageId(mailObject.getString(JSON_MESSAGGEID));
            mailDto.setObjectId(mailObject.getString(JSON_MESSAGGEID));
        }

        mailDto.setTitle(mailObject.getString(JSON_TITLE));

        if (!TextUtils.isEmpty(mailObject.getString(JSON_DESC))) {
            mailDto.setDescription(mailObject.getString(JSON_DESC));
        }
        mailDto.setPostedDate(mailObject.getString(JSON_POST_DATE));
        mailDto.setMessageType(mailObject.getString(JSON_MSGTYPE));
        mailDto.setUserid(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_USERID));



        if(mailObject.has("from") && !mailObject.isNull("from")){
            if (!TextUtils.isEmpty(mailObject.getJSONObject("from").getString(JSON_USEREMAIL))){
                mailDto.setUserEmail(mailObject.getJSONObject("from").getString(JSON_USEREMAIL));
            }
            if (!TextUtils.isEmpty(mailObject.getJSONObject("from").getString(JSON_USERNAME))){
                mailDto.setUserName(mailObject.getJSONObject("from").getString(JSON_USERNAME));
            }
        }

        mailDto.setUserEmail(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_POSTED_USEREMAIL));
        mailDto.setUserName(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_POSTED_USERNAME));

        if(mailObject.has(JSON_FILE) && !mailObject.isNull(JSON_FILE)){
             mailDto.setAttachmentList(Attachment.parseFile(mailObject.optJSONArray(JSON_FILE)));
        } else {
            List<Attachment> attachmentList = new ArrayList<Attachment>();
            mailDto.setAttachmentList(attachmentList);//Set empty;
        }

        if(mailObject.has(JSON_TO_EMAIL) && !mailObject.isNull(JSON_TO_EMAIL)){
            mailDto.setToemailList(parseToEmails(mailObject.optJSONArray(JSON_TO_EMAIL)));
        } else {
            List<Email> toEmailList = new ArrayList<Email>();
            mailDto.setToemailList(toEmailList);
        }

        if(mailObject.has(JSON_CC_EMAIL) && !mailObject.isNull(JSON_CC_EMAIL)){
            mailDto.setToemailList(parseccemails(mailObject.optJSONArray(JSON_CC_EMAIL)));
        } else {
            List<Email> ccEmailList = new ArrayList<Email>();
            mailDto.setccemailList(ccEmailList);
        }

        if ( mailObject.has(JSON_COMMENTENBALED) &&!TextUtils.isEmpty(mailObject.getString(JSON_COMMENTENBALED))){
            if (mailObject.getString(JSON_COMMENTENBALED).equalsIgnoreCase("1")){
                mailDto.setCommentEnabled(true);
            } else {
                mailDto.setCommentEnabled(false);
            }

        } else {
            mailDto.setCommentEnabled(false);
        }

        if ( mailObject.has(JSON_SHAREENABLED) &&!TextUtils.isEmpty(mailObject.getString(JSON_SHAREENABLED))){
            if (mailObject.getString(JSON_SHAREENABLED).equalsIgnoreCase("1")){
                mailDto.setShareEnabled(true);
            } else {
                mailDto.setShareEnabled(false);
            }

        } else {
            mailDto.setShareEnabled(false);
        }


        if ( mailObject.has(JSON_COMMENTCOUNT) && !TextUtils.isEmpty(mailObject.getString(JSON_COMMENTCOUNT))
                && !mailObject.getString(JSON_COMMENTCOUNT).equalsIgnoreCase("null")) {

            mailDto.setCommentCount(Integer.parseInt(mailObject.getString(JSON_COMMENTCOUNT)));
        } else {
            mailDto.setCommentCount(0);
        }

        if(mailObject.has(JSON_SHARETEXT) && !mailObject.isNull(JSON_SHARETEXT)){
            mailDto.setShareText(mailObject.getString(JSON_SHARETEXT));
        }

        if(mailObject.has(JSON_SHAREURL) && !mailObject.isNull(JSON_SHAREURL)){
            mailDto.setShareUrl(mailObject.getString(JSON_SHAREURL));
        }

        return mailDto;
    }

    public static MailDto parseEmail(JSONObject mailObject)throws JSONException {

        MailDto mailDto = new MailDto();
        if (mailObject == null){
            return mailDto;
        }
        mailDto.setIvokoType(IvokoApplication.POST_TYPE.EMAIL);
        if (!TextUtils.isEmpty(mailObject.getString(JSON_MESSAGGEID)) && !mailObject.isNull(JSON_MESSAGGEID)) {
            mailDto.setMessageId(mailObject.getString(JSON_MESSAGGEID));
            mailDto.setObjectId(mailObject.getString(JSON_MESSAGGEID));
        }

        mailDto.setTitle(mailObject.getString(JSON_TITLE));

        if (!TextUtils.isEmpty(mailObject.getString(JSON_DESC))) {
            mailDto.setDescription(mailObject.getString(JSON_DESC));
        }
        mailDto.setPostedDate(mailObject.getString(JSON_POST_DATE));
        mailDto.setMessageType(mailObject.getString(JSON_MSGTYPE));
        mailDto.setUserid(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_USERID));

        mailDto.setUserEmail(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_POSTED_USEREMAIL));
        mailDto.setUserName(mailObject.getJSONObject(JSON_POSTEDBY).getString(JSON_POSTED_USERNAME));



        if(mailObject.has(JSON_FILE) && !mailObject.isNull(JSON_FILE)){
            mailDto.setAttachmentList(Attachment.parseFile(mailObject.optJSONArray(JSON_FILE)));
        }

        if(mailObject.has(JSON_TO_EMAIL) && !mailObject.isNull(JSON_TO_EMAIL)){
            mailDto.setToemailList(parseToEmails(mailObject.optJSONArray(JSON_TO_EMAIL)));
        }
        // mailDto.setFileObjects(parseFile(mailObject.optJSONArray(JSON_FILE)));
        if(mailObject.has(JSON_CC_EMAIL) && !mailObject.isNull(JSON_CC_EMAIL)){
            mailDto.setccemailList(parseccemails(mailObject.optJSONArray(JSON_CC_EMAIL)));
        }

        if(mailObject.has(JSON_SHARETEXT) && !mailObject.isNull(JSON_SHARETEXT)){
            mailDto.setShareText(mailObject.getString(JSON_SHARETEXT));
        }

        if(mailObject.has(JSON_SHAREURL) && !mailObject.isNull(JSON_SHAREURL)){
            mailDto.setShareUrl(mailObject.getString(JSON_SHAREURL));
        }

        if ( mailObject.has(JSON_SHAREENABLED) &&!TextUtils.isEmpty(mailObject.getString(JSON_SHAREENABLED))){
            if (mailObject.getString(JSON_SHAREENABLED).equalsIgnoreCase("1")){
                mailDto.setShareEnabled(true);
            } else {
                mailDto.setShareEnabled(false);
            }

        } else {
            mailDto.setShareEnabled(false);
        }

        return mailDto;
    }

    public static final String JSON_FILE_ID = "fileid";
    public static final String JSON_FILE_URL = "fileurl";

    public Map<String, String> fileObjects = new HashMap<String, String>();

    public Map<String, String> getFileObjects() {
        return fileObjects;
    }

    public void setFileObjects(Map<String, String> fileObjects) {
        this.fileObjects = fileObjects;
    }

    public static Map<String, String> parseFile(JSONArray filesArray)throws JSONException {

        Map<String, String> files = new HashMap<String, String>();

        if(filesArray==null){
            return files;
        }

        for (int i = 0; i < filesArray.length(); i++) {
            JSONObject file = filesArray.getJSONObject(i);
            if (!TextUtils.isEmpty(file.getString(JSON_FILE_ID))){
                files.put(file.getString(JSON_FILE_ID),file.getString(JSON_FILE_URL));
            }
        }
        return files;
    }

    public static List<Email> parseToEmails(JSONArray toemailArray)throws JSONException {

        Map<String, String> toEmails = new HashMap<String, String>();

        List<Email> toEmailList = new ArrayList<Email>();

        if(toemailArray==null){
            return toEmailList;
        }

        for (int i = 0; i < toemailArray.length(); i++) {
            JSONObject toemail = toemailArray.getJSONObject(i);
            Email email = new Email();

            if (!TextUtils.isEmpty(toemail.getString(JSON_USEREMAIL))){
                email.setEmail(toemail.getString(JSON_USEREMAIL));
            }
            if (!TextUtils.isEmpty(toemail.getString(JSON_USERNAME))){
                email.setUsername(toemail.getString(JSON_USERNAME));
            }

            toEmailList.add(email);

        }
        return toEmailList;
    }

    public static List<Email> parseccemails(JSONArray emailArray)throws JSONException {



        List<Email> ccEmailList = new ArrayList<Email>();

        if(emailArray==null){
            return ccEmailList;
        }

        for (int i = 0; i < emailArray.length(); i++) {
            JSONObject ccemail = emailArray.getJSONObject(i);
            Email email = new Email();

            if (!TextUtils.isEmpty(ccemail.getString(JSON_USEREMAIL))){
                email.setEmail(ccemail.getString(JSON_USEREMAIL));
            }
            if (!TextUtils.isEmpty(ccemail.getString(JSON_USERNAME))){
                email.setUsername(ccemail.getString(JSON_USERNAME));
            }

            ccEmailList.add(email);

        }
        return ccEmailList;
    }

}