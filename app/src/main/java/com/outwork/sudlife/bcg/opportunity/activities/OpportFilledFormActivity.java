package com.outwork.sudlife.bcg.opportunity.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.opportunity.OpportunityMgr;
import com.outwork.sudlife.bcg.opportunity.adapter.OpportunityAdapter;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.opportunity.fragments.FilterFragment;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;

public class OpportFilledFormActivity extends BaseActivity implements FilterFragment.OnFragmentInteractionListener {

    private RecyclerView mOpportunityRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private Boolean enableProgress = false;
    private ProgressBar progressBar, opportunityProgressBar, topprogress;
    private OpportunityAdapter opportunityAdapter;
    private TextView nomessageview, toolbartitle;
    private ProgressBar rprogressBar;
    private List<OpportunityModel> opportunityList = new ArrayList<>();
    private FloatingActionButton addOpportunity;
    private LinearLayout searchLayout;
    private RelativeLayout relativeLayout;
    private String formname, formid;
    private EditText searchText;
    private boolean isfiltered = false;
    private Toolbar toolbar;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filled_forms);
//        if (SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LOADED, "").equals("notloaded")) {
//            if (isNetworkAvailable()) {
//                OpportunityIntentService.insertOpportunity(OpportFilledFormActivity.this);
//            }
//        }
        mgr = LocalBroadcastManager.getInstance(this);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FORMID")) {
            formid = getIntent().getStringExtra("FORMID");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FORMNAME")) {
            formname = getIntent().getStringExtra("FORMNAME");
        }
        initUI();
        initialiseToolBar();
        initListeners();
        initializeList();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initializeList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("opp_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialiseToolBar() {
        toolbartitle.setText("Leads");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setUpSearchLayout() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText = (EditText) findViewById(R.id.searchText);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (opportunityAdapter != null && s != null) {
                    opportunityAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void initUI() {
        toolbar = (Toolbar) findViewById(R.id.ffToolBar);
        toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        nomessageview = (TextView) findViewById(R.id.ffnomessages);
        addOpportunity = (FloatingActionButton) findViewById(R.id.addform);
        progressBar = (ProgressBar) findViewById(R.id.ffcprogress_bar);
        opportunityProgressBar = (ProgressBar) findViewById(R.id.ffmessageListProgressBar);
        topprogress = (ProgressBar) findViewById(R.id.fftopprogressbar);
        mOpportunityRecyclerView = (RecyclerView) findViewById(R.id.ffRecyclerView);
        rprogressBar = (ProgressBar) findViewById(R.id.progressBar);
        linearLayoutManager = new LinearLayoutManager(OpportFilledFormActivity.this);
        relativeLayout = (RelativeLayout) findViewById(R.id.r1);
        searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
    }

    private void initListeners() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                initializeList();
            }
        };
        addOpportunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpportFilledFormActivity.this, AddOpportunityActivity.class);
                intent.putExtra("FORMNAME", formname);
                intent.putExtra("FORMID", formid);
                intent.putExtra("type", "opportunity");
                startActivity(intent);
            }
        });
    }

    private void initializeList() {
//        isfiltered = false;
        OpportunityIntentService.syncOfflineRecords(OpportFilledFormActivity.this);
        nomessageview.setVisibility(View.INVISIBLE);
        opportunityList.clear();
        opportunityList = OpportunityMgr.getInstance(OpportFilledFormActivity.this).getOpportunityList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

        if (opportunityList.size() > 0) {
            if (opportunityProgressBar.VISIBLE == 0) {
                opportunityProgressBar.setVisibility(View.INVISIBLE);
            }
            nomessageview.setVisibility(View.GONE);
            opportunityAdapter = new OpportunityAdapter(OpportFilledFormActivity.this, opportunityList);
            mOpportunityRecyclerView.setLayoutManager(linearLayoutManager);
            mOpportunityRecyclerView.setAdapter(opportunityAdapter);
            if (opportunityList.size() > 10) {
                setUpSearchLayout();
            }
        } else {
            setNoMessages();
        }
    }

    private void setNoMessages() {
        if (opportunityList.size() == 0) {
            mOpportunityRecyclerView.setAdapter(null);
            nomessageview.setVisibility(View.VISIBLE);
            nomessageview.setText(" No Filled Leads ");
        }
        if (opportunityProgressBar.VISIBLE == 0) {
            opportunityProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onClickItem(String result) {
        opportunityList.clear();
        opportunityList = OpportunityMgr.getInstance(OpportFilledFormActivity.this).getOpportunityListonStage(userid,
                result);
//        isfiltered = true;
        if (opportunityList.size() > 0) {
            if (opportunityProgressBar.VISIBLE == 0) {
                opportunityProgressBar.setVisibility(View.INVISIBLE);
            }
            nomessageview.setVisibility(View.GONE);
            opportunityAdapter = new OpportunityAdapter(OpportFilledFormActivity.this, opportunityList);
            mOpportunityRecyclerView.setLayoutManager(linearLayoutManager);
            mOpportunityRecyclerView.setAdapter(opportunityAdapter);
        } else {
            setNoMessages();
        }
    }
}
