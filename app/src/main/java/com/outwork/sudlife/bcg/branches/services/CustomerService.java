package com.outwork.sudlife.bcg.branches.services;

import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.branches.models.ContactModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 2/19/2017.
 */

public interface CustomerService {
    @GET("mobile/v1/customer")
    Call<RestResponse> getcustomers(@Header("utoken") String userToken,
                                    @Query("groupid") String groupid,
                                    @Query("id") String id,
                                    @Query("date") String date);


    @GET(" mobile/v1/customer/branchassignees")
    Call<RestResponse> getcustomerassignees(@Header("utoken") String userToken,
                                    @Query("groupid") String groupid,
                                    @Query("id") String id,
                                    @Query("date") String date);





    @GET("mobile/v1/contact")
    Call<RestResponse> getContacts(@Header("utoken") String userToken,
                                   @Query("groupid") String groupid,
                                   @Query("id") String id,
                                   @Query("date") String date);

    @POST("Customer/customers")
    Call<RestResponse> addCustomer(@Header("utoken") String userToken,
                                   @Query("groupid") String groupid,
                                   @Body BranchesModel branchesModel);

    @POST("contact/contacts")
    Call<RestResponse> addContact(@Header("utoken") String userToken,
                                  @Query("groupid") String groupid,
                                  @Body ContactModel contactModel);

    @PUT("mobile/v1/contact/{contactid}")
    Call<RestResponse> updateContact(@Header("utoken") String userToken,
                                     @Path("contactid") String contactid,
                                     @Body ContactModel contactModel);

    @PUT("mobile/v1/customer/{customerid}")
    Call<RestResponse> updateCustomer(@Header("utoken") String userToken,
                                      @Path("customerid") String customerid,
                                      @Body BranchesModel branchesModel);

    @GET("admin/v1/teams/team/hierarchy")
    Call<RestResponse> getTeamHierarchy(@Header("utoken") String userToken,
                                        @Query("teamid") String teamid);



    @GET("admin/v1/teams/{teamId}/hierarchy/members")
    Call<RestResponse> getTeamHierarchyMembers(@Header("utoken") String userToken,
                                               @Path("teamId") String teamId);


    @GET("mobile/v1/customer/{userid}")
    Call<RestResponse> getBranchesForUserId(@Header("utoken") String userToken,
                                            @Path("userid") String userid);



    @GET("mobile/v1/customer/assignedbranches")
    Call<RestResponse> getExclusiveBranchesForUser(@Header("utoken") String userToken,
                                                   @Query("souserid") String souserid,
                                                    @Query("lhuserid") String lhuserid,
                                                    @Query("ahuserid") String ahuserid,
                                                    @Query("couserid") String couserid,
                                                    @Query("rhuserid") String rhuserid,
                                                    @Query("zhuserid") String zhuserid,
                                                    @Query("locationid") String locationid,
                                                    @Query("areaid") String areaid,
                                                    @Query("controllingofficeid") String controllingofficeid,
                                                    @Query("zoneid") String zoneid,
                                                    @Query("regionid") String regionid);

}
