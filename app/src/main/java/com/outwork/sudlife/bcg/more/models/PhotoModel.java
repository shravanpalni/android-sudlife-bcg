package com.outwork.sudlife.bcg.more.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Panch on 12/23/2015.
 */

public class PhotoModel implements Parcelable {
    String name;
    String url;
    String thumburl;
    String mainurl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
    public PhotoModel() {
    }
    // Getters & Setters here

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainurl() {
        return mainurl;
    }

    public void setMainurl(String mainurl) {
        this.mainurl = mainurl;
    }

    public String getThumburl() {
        return thumburl;
    }

    public void setThumburl(String thumburl) {
        this.thumburl = thumburl;
    }


    public static final String JSON_ID = "Id";
    public static final String JSON_TITLE = "Title";
    public static final String JSON_THUMBURL = "storageurl";
    public static final String JSON_MAINURL = "altstorageurl";
    public static final String JSON_FILEID = "fileID";
    public static final String JSON_PHOTOTITLE= "title";
    public static final String JSON_ALBUM_IMAGE="ImageUrl";

    public static PhotoModel parsePhotoJSON(JSONObject jsonObject) throws JSONException {

        PhotoModel dto = new PhotoModel();

        if(jsonObject.has(JSON_TITLE) && !jsonObject.isNull(JSON_TITLE)){
            dto.setName(jsonObject.getString(JSON_TITLE));
        }

        if(jsonObject.has(JSON_PHOTOTITLE) && !jsonObject.isNull(JSON_PHOTOTITLE)){
            dto.setName(jsonObject.getString(JSON_PHOTOTITLE));
        }


        if(jsonObject.has(JSON_THUMBURL) && !jsonObject.isNull(JSON_THUMBURL)){
            dto.setThumburl(jsonObject.getString(JSON_THUMBURL));
        }

        if(jsonObject.has(JSON_MAINURL) && !jsonObject.isNull(JSON_MAINURL)){
            dto.setMainurl(jsonObject.getString(JSON_MAINURL));
        }
        dto.setUrl(dto.getThumburl());

        if(jsonObject.has(JSON_ID) && !jsonObject.isNull(JSON_ID)){
            dto.setId(jsonObject.getString(JSON_ID));
        }

        if(jsonObject.has(JSON_FILEID) && !jsonObject.isNull(JSON_FILEID)){
            dto.setId(jsonObject.getString(JSON_FILEID));
        }
        return dto;

    }

    public static PhotoModel parseAlbumJSON(JSONObject jsonObject) throws JSONException {

        PhotoModel dto = new PhotoModel();

        if(jsonObject.has(JSON_TITLE) && !jsonObject.isNull(JSON_TITLE)){
            dto.setName(jsonObject.getString(JSON_TITLE));
        }

        if(jsonObject.has(JSON_PHOTOTITLE) && !jsonObject.isNull(JSON_PHOTOTITLE)){
            dto.setName(jsonObject.getString(JSON_PHOTOTITLE));
        }


        if(jsonObject.has(JSON_ALBUM_IMAGE) && !jsonObject.isNull(JSON_ALBUM_IMAGE)){
            dto.setUrl(jsonObject.getString(JSON_ALBUM_IMAGE));
        }

        if(jsonObject.has(JSON_ID) && !jsonObject.isNull(JSON_ID)){
            dto.setId(jsonObject.getString(JSON_ID));
        }

        if(jsonObject.has(JSON_FILEID) && !jsonObject.isNull(JSON_FILEID)){
            dto.setId(jsonObject.getString(JSON_FILEID));
        }
        dto.setThumburl(dto.getUrl());
        dto.setMainurl("");
        return dto;

    }


    public static ArrayList<PhotoModel> getPhotolist(JSONArray jsonArray) throws JSONException {

        ArrayList<PhotoModel> photoDtoList = new ArrayList<PhotoModel>();

        if (jsonArray.length()==0){
            return photoDtoList;
        }

        for(int i = 0;i<jsonArray.length();i++){
            JSONObject obj = jsonArray.getJSONObject(i);
            photoDtoList.add(PhotoModel.parsePhotoJSON(obj));
        }

        return photoDtoList;

    }

    public static ArrayList<PhotoModel> getAlbumList(JSONArray jsonArray) throws JSONException {

        ArrayList<PhotoModel> photoDtoList = new ArrayList<PhotoModel>();

        if (jsonArray.length()==0){
            return photoDtoList;
        }

        for(int i = 0;i<jsonArray.length();i++){
            JSONObject obj = jsonArray.getJSONObject(i);
            photoDtoList.add(PhotoModel.parseAlbumJSON(obj));
        }

        return photoDtoList;

    }

    protected PhotoModel(Parcel in) {
        name = in.readString();
        url = in.readString();
        thumburl=in.readString();
        mainurl=in.readString();
    }


    public static final Creator<PhotoModel> CREATOR = new Creator<PhotoModel>() {
        @Override
        public PhotoModel createFromParcel(Parcel in) {
            return new PhotoModel(in);
        }


        @Override
        public PhotoModel[] newArray(int size) {
            return new PhotoModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
        dest.writeString(thumburl);
        dest.writeString(mainurl);
    }
}