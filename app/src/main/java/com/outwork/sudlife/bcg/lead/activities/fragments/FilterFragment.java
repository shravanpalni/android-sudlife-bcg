package com.outwork.sudlife.bcg.lead.activities.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.lead.LeadMgr;
import com.outwork.sudlife.bcg.lead.adapters.ExpandableListAdapter;
import com.outwork.sudlife.bcg.listener.ItemClickListener;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class FilterFragment extends Fragment implements ItemClickListener {
    // TODO: Rename and change types of parameters
    private String status, sdate, edate;
    private ImageView close;
    private EditText startdate, enddate;
    private Button submit;
    private RelativeLayout datesview;
    //    private AppCompatSpinner dates;
    private ExpandableListAdapter listAdapter;
    private OnFragmentInteractionListener mListener;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private List<String> datesList = new ArrayList<>();
    private String result = "";
    private HashMap<String, List<String>> listDataChild;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                startdate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                sdate = TimeUtils.convertDatetoUnix(visitdt);
                startdate.setText(visitdt);
                startdate.setEnabled(true);
                startdate.setError(null);
            }
            if (customSelector == 1) {
                enddate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                edate = TimeUtils.convertDatetoUnix(visitdt);
                enddate.setText(visitdt);
                enddate.setEnabled(true);
                enddate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            startdate.setEnabled(true);
            enddate.setEnabled(true);
        }
    };

    public FilterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        View view = inflater.inflate(R.layout.layout_exp_filter, container, false);

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        startdate = (EditText) view.findViewById(R.id.strtdate);
        enddate = (EditText) view.findViewById(R.id.enddate);
        submit = (Button) view.findViewById(R.id.submit);
        datesview = (RelativeLayout) view.findViewById(R.id.datesview);
        close = (ImageView) view.findViewById(R.id.close);
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataHeader.add("Name");
        listDataHeader.add("Branch Name");
        listDataHeader.add("Bank Name");
        listDataHeader.add("Date");
        List<String> namesList = new ArrayList<>();
        for (String branchname : LeadMgr.getInstance(getActivity()).getLeadsNamesList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""))) {
            namesList.add(branchname);
        }
        List<String> branchesList = new ArrayList<>();
        for (String branchname : BranchesMgr.getInstance(getActivity()).getCustomerNamesList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""))) {
            branchesList.add(branchname);
        }
        List<String> banksList = new ArrayList<>();
        for (String branchname : BranchesMgr.getInstance(getActivity()).getCustomerTypesList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""))) {
            banksList.add(branchname);
        }
        listDataChild.put(listDataHeader.get(0), namesList); // Header, Child data
        listDataChild.put(listDataHeader.get(1), branchesList); // Header, Child data
        listDataChild.put(listDataHeader.get(2), banksList); // Header, Child data
        listDataChild.put(listDataHeader.get(3), datesList); // Header, Child data
        setdataToList();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) view.findViewById(R.id.toolbar_title));
        return view;
    }

    private void setdataToList() {
        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        listAdapter.setClickListener(this);
        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getChildFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enddate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getChildFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(startdate.getText().toString())) {
                    startdate.setError("Date required");
                    startdate.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(enddate.getText().toString())) {
                    enddate.setError("Date required");
                    enddate.requestFocus();
                } else {
                    if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
                        ((BaseActivity) getActivity()).showToast("Startdate should not be greater than Endnddate");
                    } else {
                        result = sdate + " " + edate;
                        mListener.onClickItem(result);
                        getActivity().onBackPressed();
                    }
                }
            }
        });
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition == 3) {
                    datesview.setVisibility(View.VISIBLE);
                } else {
                    datesview.setVisibility(View.GONE);
                }
            }
        });
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                if (groupPosition == 3) {
                    datesview.setVisibility(View.GONE);
                }
            }
        });
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                result = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                mListener.onClickItem(result);
                getActivity().onBackPressed();
                return false;
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view, int position) {
        if (view != null) {
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onClickItem(String result);
    }
}
