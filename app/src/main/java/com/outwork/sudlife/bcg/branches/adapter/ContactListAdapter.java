package com.outwork.sudlife.bcg.branches.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.ContactMgr;
import com.outwork.sudlife.bcg.branches.models.ContactModel;
import com.outwork.sudlife.bcg.utilities.Utils;

/**
 * Created by Panch on 2/23/2017.
 */

public class ContactListAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;
    private List<ContactModel> contactList = new ArrayList<>();
    private String groupid, userid;
    private ContactFilter mContactFilter;

    static class ViewHolder {
        public ImageView picture;
        public TextView name;
        public TextView line2, offline;
    }

    public ContactListAdapter(Context context, List<ContactModel> contactList, String groupId, String userId) {
        this.context = context;
        this.contactList = contactList;
        this.groupid = groupId;
        this.userid = userId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (contactList != null) {
            return this.contactList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.contactList.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_contact, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.picture = (ImageView) view.findViewById(R.id.profileImage);
            viewHolder.name = (TextView) view.findViewById(R.id.contactname);
            viewHolder.line2 = (TextView) view.findViewById(R.id.contactline2);
            viewHolder.offline = (TextView) view.findViewById(R.id.offline);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, viewHolder.name);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, viewHolder.line2);
            view.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        ContactModel dto = (ContactModel) this.contactList.get(position);
        if (Utils.isNotNullAndNotEmpty(dto.getCdisplayname()))
            holder.name.setText(dto.getCdisplayname());
        String customerName = dto.getCustomername();
        if (Utils.isNotNullAndNotEmpty(customerName) && Utils.isNotNullAndNotEmpty(dto.getCdisplayname())) {
            if (customerName.equalsIgnoreCase(dto.getCdisplayname())) {
                holder.line2.setVisibility(View.GONE);
            } else {
                holder.line2.setVisibility(View.VISIBLE);
                holder.line2.setText(customerName);
            }
        } else {
            if (Utils.isNotNullAndNotEmpty(customerName)) {
                holder.line2.setVisibility(View.VISIBLE);
                holder.line2.setText(customerName);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getNetwork_status())) {
            holder.offline.setVisibility(View.VISIBLE);
        } else {
            holder.offline.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(dto.getCdisplayname()))
            if (dto.getCdisplayname().length() > 0) {
                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
                // generate color based on a key (same key returns the same color), useful for list/grid views
                int color2 = generator.getColor(dto.getCdisplayname().substring(0, 1));
                TextDrawable drawable = TextDrawable.builder().buildRound(dto.getCdisplayname().substring(0, 1), color2);
                if (drawable != null) {
                    holder.picture.setImageDrawable(drawable);
                }
            }
        return view;
    }

    @Override
    public Filter getFilter() {
        if (mContactFilter == null)
            mContactFilter = new ContactFilter();
        return mContactFilter;
    }

    private class ContactFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
            } else {
                List<ContactModel> filterContactList = new ContactMgr(context)
                        .getContactSearchList(groupid, userid, constraint.toString());
                // Finally set the filtered values and size/count
                results.values = filterContactList;
                results.count = filterContactList.size();
            }
            // Return our FilterResults object
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                contactList = (ArrayList<ContactModel>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
                //notifyDataSetChanged();
            }
        }
    }
}