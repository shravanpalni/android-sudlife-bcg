package com.outwork.sudlife.bcg.planner.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.localbase.StaticDataMgr;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.SupervisorMgr;
import com.outwork.sudlife.bcg.planner.adapter.SupervisorListAdapter;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditPlanActivity extends BaseActivity {

    private EditText vplandate, others;
    private TextView typelabel, suprvisorlabel, toolbar_title, branch_display;
    private AppCompatSpinner supervisor, activitytype;
    private CheckBox checkBox;
    private Button submit;
    private Boolean ismandatory = false;
    private RadioGroup type;
    private RadioButton visit, activity;
    private TextInputLayout otherslabel;
    private SupervisorListAdapter supervisorListAdapter;
    private ArrayAdapter<String> activityTypeAdapter;
    private List<SupervisorModel> supervisorModelList = new ArrayList<>();
    private List<String> activitytypeList = new ArrayList<>();
    private PlannerModel plannerModel = new PlannerModel();

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                vplandate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                vplandate.setText(visitdt);
                vplandate.setEnabled(true);
            }
        }

        @Override
        public void onDateTimeCancel() {
            vplandate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("plannerModel")) {
            plannerModel = new Gson().fromJson(getIntent().getStringExtra("plannerModel"), PlannerModel.class);
        }

        initToolBar();
        initViews();
        getActivitytypes();
        setListeners();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, visit, activity, vplandate, (TextView) findViewById(R.id.others));
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface,
                (TextView) findViewById(R.id.name), (TextView) findViewById(R.id.textView1),
                (TextView) findViewById(R.id.suprvisorlbl), (TextView) findViewById(R.id.atype));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, submit);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Edit Plan");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void initViews() {
        branch_display = (TextView) findViewById(R.id.branch_display);
        typelabel = (TextView) findViewById(R.id.atype);
        suprvisorlabel = (TextView) findViewById(R.id.suprvisorlbl);
        vplandate = (EditText) findViewById(R.id.vplandate);
        submit = (Button) findViewById(R.id.submit);
        submit.setVisibility(View.VISIBLE);
        submit.setText("Submit");
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
        supervisor = (AppCompatSpinner) findViewById(R.id.supervisor);
        activitytype = (AppCompatSpinner) findViewById(R.id.activitytype);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        otherslabel = (TextInputLayout) findViewById(R.id.otherslabel);
        others = (EditText) findViewById(R.id.others);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (plannerModel.getActivitytype().equalsIgnoreCase("Visit")) {
                visit.setChecked(true);
            } else {
                activity.setChecked(true);
                typelabel.setVisibility(View.VISIBLE);
                activitytype.setVisibility(View.VISIBLE);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            branch_display.setText(plannerModel.getCustomername());
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() == 0) {
                vplandate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getScheduletime()))) {
                vplandate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy hh:mm a"));
            }
        }
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                checkBox.setChecked(plannerModel.getJointvisit());
                suprvisorlabel.setVisibility(View.VISIBLE);
                supervisor.setVisibility(View.VISIBLE);
                getSupervisors();
            }
        }
    }

    private void getActivitytypes() {
        activitytypeList = StaticDataMgr.getInstance(EditPlanActivity.this).getListNames(groupId, "subtype");
        if (activitytypeList.size() > 0) {
            activityTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, activitytypeList);
            activityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activitytype.setAdapter(activityTypeAdapter);
            for (int i = 0; i < activitytypeList.size(); i++)
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
                    if (plannerModel.getSubtype().equalsIgnoreCase(activitytypeList.get(i).toString())) {
                        activitytype.setSelection(i);
                    }
            activitytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activitytype.setSelection(position);
                    plannerModel.setSubtype(activitytype.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            typelabel.setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void getSupervisors() {
        supervisorModelList = SupervisorMgr.getInstance(EditPlanActivity.this).getsupervisorList(userid);
        if (supervisorModelList.size() > 0) {
            supervisorListAdapter = new SupervisorListAdapter(this, supervisorModelList);
            supervisor.setAdapter(supervisorListAdapter);
            supervisor.setSelection(0);
            for (int i = 0; i < supervisorModelList.size(); i++)
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
                    if (plannerModel.getSupervisorname().trim().replaceAll(" ", "").equalsIgnoreCase(supervisorModelList.get(i).getUsername().trim().replaceAll(" ", ""))) {
                        supervisor.setSelection(i);
                        otherslabel.setVisibility(View.GONE);
                        return;
                    } else {
                        supervisor.setSelection(i);
                        otherslabel.setVisibility(View.VISIBLE);
                        others.setText(plannerModel.getSupervisorname());
                    }
            supervisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (supervisorModelList.get(position).getUsername().equalsIgnoreCase("Others")) {
                        otherslabel.setVisibility(View.VISIBLE);
                        ismandatory = true;
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                    } else {
                        otherslabel.setVisibility(View.GONE);
                        supervisor.setSelection(position);
                        ismandatory = false;
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                        plannerModel.setSupervisorname(supervisorModelList.get(position).getUsername());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void setListeners() {
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.visit) {
                    plannerModel.setType("Visit");
                    plannerModel.setActivitytype("Visit");
                    typelabel.setVisibility(View.GONE);
                    activitytype.setVisibility(View.GONE);
                } else {
                    plannerModel.setType("Activity");
                    plannerModel.setActivitytype("Activity");
                    typelabel.setVisibility(View.VISIBLE);
                    activitytype.setVisibility(View.VISIBLE);
                }
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suprvisorlabel.setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.VISIBLE);
                    getSupervisors();
                } else {
                    suprvisorlabel.setVisibility(View.GONE);
                    supervisor.setVisibility(View.GONE);
                    otherslabel.setVisibility(View.GONE);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                    vplandate.setError("Date required");
                    vplandate.requestFocus();
                } else if (ismandatory && !Utils.isNotNullAndNotEmpty(others.getText().toString())) {
                    others.setError("Mention Other Supervisor Name");
                    others.requestFocus();
                } else {
                    updatePlan(0, "Your Plan has been updated successfully");
                }
            }
        });
        vplandate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vplandate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, 5);
                Date minDate = calendar.getTime();

                calendar.add(Calendar.DATE, 30);
                Date maxDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(maxDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
    }

    private void updatePlan(int compltestatus, String msg) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (visit.isChecked()) {
            plannerModel.setType("Visit");
            plannerModel.setActivitytype("Visit");
        } else {
            plannerModel.setType("Activity");
            plannerModel.setActivitytype("Activity");
            plannerModel.setSubtype(activitytype.getSelectedItem().toString());
        }
        plannerModel.setJointvisit(checkBox.isChecked());
        if (Utils.isNotNullAndNotEmpty(others.getText().toString())) {
            plannerModel.setSupervisorname(others.getText().toString());
        }
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
        plannerModel.setScheduletime(Integer.parseInt(TimeUtils.convertDatetoUnix(vplandate.getText().toString())));
        plannerModel.setCompletestatus(compltestatus);
        plannerModel.setNetwork_status("offline");
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        PlannerMgr.getInstance(EditPlanActivity.this).updatePlan(plannerModel);
        LeadIntentService.syncLeadstoServer(EditPlanActivity.this);
        showAlert("", msg,
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }
}
