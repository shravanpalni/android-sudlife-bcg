package com.outwork.sudlife.bcg.utilities;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.outwork.sudlife.bcg.R;


/**
 * Created by Panch on 3/29/2016.
 */
public class AnalyticsApplication extends Application {

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
           mTracker = analytics.newTracker(R.xml.globe);
        }
        return mTracker;
    }
}
