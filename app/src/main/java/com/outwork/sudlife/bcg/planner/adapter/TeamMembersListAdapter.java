package com.outwork.sudlife.bcg.planner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.models.TeamMembers;

import java.util.ArrayList;

/**
 * Created by Android on 13-06-2018.
 */

public class TeamMembersListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<TeamMembers> tmemArrayList;

    public TeamMembersListAdapter(Context context, ArrayList<TeamMembers> tmemArrayList) {
        this.context = context;
        this.tmemArrayList = tmemArrayList;
    }

    @Override
    public int getCount() {
        return tmemArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.member_list_item, viewGroup, false);
        TextView tvUser = (TextView) itemView.findViewById(R.id.spinner_item);
        tvUser.setText(tmemArrayList.get(i).getFirstname() + " " + tmemArrayList.get(i).getLastname());
        return itemView;
    }
}
