package com.outwork.sudlife.bcg.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {
    private PendingIntent alarmIntent;
    private Timer mTimer1;
    private TimerTask mTt1;
    private Handler mTimerHandler = new Handler();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mTimer1 = new Timer();
        mTt1 = new TimerTask() {
            public void run() {
                mTimerHandler.post(new Runnable() {
                    public void run() {
                        //TODO
                        addScheduledNofications();
                    }
                });
            }
        };
        mTimer1.schedule(mTt1, 1, 60000);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class MyTimerTask extends TimerTask {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        public void run() {
            //  addScheduledNofications();
        }
    }

    private void addScheduledNofications() {
        List<PlannerModel> plannerModelList = PlannerMgr.getInstance(MyService.this).getPlannerNotificationListByDate(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), Integer.parseInt(TimeUtils.getCurrentDate("dd")), Integer.parseInt(TimeUtils.getCurrentDate("MM")), Integer.parseInt(TimeUtils.getCurrentDate("yyyy")));
        //   List<PlannerModel> plannerNotificationlListFirst = PlannerMgr.getInstance(MyService.this).getPlannerNotificationList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        List<PlannerModel> plannerModelArrayList = null;
        if (plannerModelList.size() != 0) {
            plannerModelArrayList = new ArrayList<>();
            for (int i = 0; i < plannerModelList.size(); i++) {
                if (plannerModelList.get(i).getNotificationstatus() == null) {
                    plannerModelArrayList.add(plannerModelList.get(i));
                }
            }
            Calendar cal = Calendar.getInstance();
            //subtracting a day
            cal.add(Calendar.DATE, -1);
            SimpleDateFormat s = new SimpleDateFormat("dd");
            String previousDay = s.format(new Date(cal.getTimeInMillis()));
            PlannerMgr.getInstance(MyService.this).deleteRecordsBasedOnDay(previousDay);
            if (plannerModelArrayList.size() > 0) {
                String notificationSentHour = null;
                String notificationTimeMintues = null;
                StringBuilder stringBuilder = new StringBuilder();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                for (PlannerModel plannerModel : plannerModelArrayList) {
                    stringBuilder.append(plannerModel.getCustomername() + ",");
                }
                if (plannerModelArrayList.size() == 1) {
                    if (plannerModelArrayList.get(0).getNotificationstatus() == null) {
                        scheduleRepeatingRTCNotificationForSingle(getApplicationContext(), 10, 10, "You have a scheduled task in" + plannerModelList.get(0).getCustomername() + "Branch", String.valueOf(plannerModelList.get(0).getLid()), "1");
                    }
                } else {
                    List<PlannerModel> plannerModelArrayListModelList = new ArrayList<>();
                    List<PlannerModel> plannerModelArrayList2 = new ArrayList<>();
                    for (int i = 0; i < plannerModelArrayList.size(); i++) {
                        if (plannerModelArrayList.get(i).getNotificationstatus() == null) {
                            plannerModelArrayListModelList.add(plannerModelArrayList.get(i));
                        } else if (plannerModelArrayList.get(i).getNotificationstatus().equalsIgnoreCase("1")) {
                            plannerModelArrayList2.add(plannerModelArrayList.get(i));
                        }
                    }
                    if (plannerModelArrayListModelList.size() != 0) {
                        scheduleRepeatingRTCNotification(MyService.this, 10, 10, "You have a scheduled tasks in " + stringBuilder + " Branches", plannerModelArrayListModelList, "1");
                    }
                }
            } else {
                if (plannerModelList.size() != 0) {
                    String notificationSentHour = null;
                    String notificationTimeMintues = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    ArrayList<PlannerModel> plannerModelArrayList2 = new ArrayList<>();
                    for (int i = 0; i < plannerModelList.size(); i++) {
                        if (plannerModelList.get(i).getNotificationstatus().equalsIgnoreCase("1")) {
                            plannerModelArrayList2.add(plannerModelList.get(i));
                        }
                    }
                    if (plannerModelArrayList2.size() != 0) {
                        int hours = 0, minutes = 0, seconds = 0;
                        for (int i = 0; i < plannerModelArrayList2.size(); i++) {
                            Date date = null;
                            try {
                                date = formatter.parse(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModelArrayList2.get(i).getScheduletime()), "yyyy-MM-dd HH:mm:ss"));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String notificationTime = new SimpleDateFormat("HH:mm").format(date);
                            java.text.DateFormat df = new SimpleDateFormat("HH:mm");
                            try {
                                Date dateNotification = df.parse(notificationTime);
                                Date date2 = df.parse("00:15");
                                long diff = dateNotification.getTime() - date2.getTime();
                                int timeInSeconds = (int) (diff / 1000);
                                hours = timeInSeconds / 3600;
                                timeInSeconds = timeInSeconds - (hours * 3600);
                                minutes = timeInSeconds / 60;
                                timeInSeconds = timeInSeconds - (minutes * 60);
                                Date notificationSentdate = formatter.parse(TimeUtils.getFormattedDatefromUnix(String.valueOf(diff), "yyyy-MM-dd HH:mm:ss"));
                                notificationSentHour = new SimpleDateFormat("HH").format(notificationSentdate);
                                notificationTimeMintues = new SimpleDateFormat("mm").format(notificationSentdate);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            scheduleRepeatingRTCNotificationForSingle(getApplicationContext(), hours, minutes
                                    , "You have a scheduled task in" + plannerModelArrayList2.get(i).getCustomername() + "Branch", String.valueOf(plannerModelArrayList2.get(i).getLid()), "2");
                        }
                    }
                }
            }
        }
    }


    public void scheduleRepeatingRTCNotification(Context context, int hour, int min, String contentOfNotification, List<PlannerModel> plannerModelList, String notificationStatus) {
        Calendar calendar = Calendar.getInstance();
        int hr = calendar.get(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        Intent myIntent = new Intent(context, AlarmReceiverForPlans.class);
        myIntent.putExtra("time", String.valueOf(hour));
        myIntent.putExtra("isrepeating", true);
        myIntent.putExtra("contentText", contentOfNotification);
        myIntent.putExtra("typeOfNotification", "double");
        myIntent.putExtra("notificationStatus", notificationStatus);
        GlobalData.getInstance().setPlannerModelArrayList(plannerModelList);
        alarmIntent = PendingIntent.getBroadcast(context, hour, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    public void scheduleRepeatingRTCNotificationForSingle(Context context, int hour, int min, String contentOfNotification, String activityId, String notificationStatus) {
        Calendar calendar = Calendar.getInstance();
        int hr = calendar.get(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        Intent myIntent = new Intent(context, AlarmReceiverForPlans.class);
        myIntent.putExtra("time", String.valueOf(hour));
        myIntent.putExtra("isrepeating", true);
        myIntent.putExtra("contentText", contentOfNotification);
        myIntent.putExtra("id", activityId);
        myIntent.putExtra("typeOfNotification", "single");
        myIntent.putExtra("notificationStatus", notificationStatus);
        alarmIntent = PendingIntent.getBroadcast(context, hour, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    private void stopTimer() {
        if (mTimer1 != null) {
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    private void startTimer() {

    }
}
//un used code
 /*   Date date = null; // You will need try/catch around this
                    Date date2 = null;
                    try {
                        date = formatter.parse(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModelList.get(i).getScheduletime()), "yyyy-MM-dd HH:mm:ss"));
                        date2 = formatter.parse(TimeUtils.getFormattedDatefromUnix(TimeUtils.getCurrentUnixTimeStamp(), "yyyy-MM-dd HH:mm:ss"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String formattedDate = new SimpleDateFormat("HH/mm/ss.SSS").format(date);
                    millis = date.getTime();
                    millis2 = 900000;
                    long diffmillis = (millis - millis2);
                    int m = (int) ((millis / 60) % 60);
                    int h = (int) ((millis / (60 * 60)) % 24);*/