package com.outwork.sudlife.bcg.branches.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.outwork.sudlife.bcg.restinterfaces.POJO.GeoAddress;
/**
 * Created by Panch on 2/22/2017.
 */
public class BranchesModel {

    private int id;
    private String network_status;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("customertype")
    @Expose
    private String customertype;
    @SerializedName("soname")
    @Expose
    private String soname;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customercode")
    @Expose
    private String branchcode;
    @SerializedName("classification")
    @Expose
    private Integer classification;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("modifieddate")
    @Expose
    private String modifieddate;
    @SerializedName("branchcode")
    @Expose
    private String customercode;
    @SerializedName("sudlat")
    @Expose
    private String sudlat;
    @SerializedName("sudlong")
    @Expose
    private String sudlong;
    @SerializedName("banklat")
    @Expose
    private String banklat;
    @SerializedName("banklong")
    @Expose
    private String banklong;
    @SerializedName("phoneno")
    @Expose
    private String phoneno;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("addressline1")
    @Expose
    private String addressline1;
    @SerializedName("address")
    @Expose
    private GeoAddress address;
    @SerializedName("branchfoundationday")
    @Expose
    private String branchfoundationday;
    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }


    public String getCustomertype() {
        return customertype;
    }

    public void setCustomertype(String customertype) {
        this.customertype = customertype;
    }

    public String getSoname() {
        return soname;
    }

    public void setSoname(String soname) {
        this.soname = soname;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public Integer getClassification() {
        return classification;
    }

    public void setClassification(Integer classification) {
        this.classification = classification;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getCustomercode() {
        return customercode;
    }

    public void setCustomercode(String customercode) {
        this.customercode = customercode;
    }

    public String getSudlat() {
        return sudlat;
    }

    public void setSudlat(String sudlat) {
        this.sudlat = sudlat;
    }

    public String getSudlong() {
        return sudlong;
    }

    public void setSudlong(String sudlong) {
        this.sudlong = sudlong;
    }

    public String getBanklat() {
        return banklat;
    }

    public void setBanklat(String banklat) {
        this.banklat = banklat;
    }

    public String getBanklong() {
        return banklong;
    }

    public void setBanklong(String banklong) {
        this.banklong = banklong;
    }

    public String getBranchfoundationday() {
        return branchfoundationday;
    }

    public void setBranchfoundationday(String branchfoundationday) {
        this.branchfoundationday = branchfoundationday;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public GeoAddress getAddress() {
        return address;
    }

    public void setAddress(GeoAddress address) {
        this.address = address;
    }
    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }


}
