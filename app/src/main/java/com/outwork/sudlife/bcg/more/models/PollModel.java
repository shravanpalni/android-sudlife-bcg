package com.outwork.sudlife.bcg.more.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.outwork.sudlife.bcg.IvokoApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PollModel extends CommonDto implements Parcelable {

    private String pollId;
    private String pollQuestion;
    private String createdBy;
    private String postedDate;
    private int    totalVoteCount;
    private ArrayList<PollAnswer> pollAnswers;
    private boolean Isanswered;
    private int answered =0;
    private float highestOptionNr;
    private String userid;
    private String userName;
    private String userEmail;


    public String getAnswerdOption() {
        return answerdOption;
    }

    public void setAnswerdOption(String answerdOption) {
        this.answerdOption = answerdOption;
    }

    public boolean Isanswered() {
        return Isanswered;
    }

    public void setIsanswered(boolean isanswered) {
        this.Isanswered = isanswered;
    }

    private String answerdOption;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }



    public PollModel(){

    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public int getTotalVoteCount() {
        return totalVoteCount;
    }

    public void setTotalVoteCount(int totalVoteCount) {
        this.totalVoteCount = totalVoteCount;
    }

   public List<PollAnswer> getPollAnswers() {
        return pollAnswers;
    }

    public void setPollAnswers(ArrayList<PollAnswer> pollAnswers) {
        this.pollAnswers = pollAnswers;
    }

    public int getAnswered() {
        return answered;
    }

    public void setAnswered(int answered) {
        this.answered = answered;
    }

    public float getHighestOptionNr() {
        return highestOptionNr;
    }

    public void setHighestOptionNr(float highestOptionNr) {
        this.highestOptionNr = highestOptionNr;
    }



    public static final String JSON_POLLID = "pollid";
    public static final String JSON_QUESTION = "title";
    public static final String JSON_POSTEDDATE = "createddate";
    public static final String JSON_TOTALVOTECOUNT = "fileID";
    public static final String JSON_ANSWERS = "answeroptions";

    public static final String JSON_ANSWERID = "optionid";
    public static final String JSON_ANSWER = "optiontext";
    public static final String JSON_VOTECOUNT = "count";
    public static final String JSON_PERCENTAGE ="percentage";

    public static final String JSON_ANSWERED ="answer";
    public static final String JSON_ANSWER_OPTION ="optionnr";
    public static final String JSON_ANSWER_FLAG="answered";

    public static final String JSON_USERID = "userid";
    public static final String JSON_POSTED_USERNAME ="username";
    public static final String JSON_POSTED_USEREMAIL ="email";
    public static final String JSON_POSTEDBY = "postedby";

    public static final String JSON_POLL_ANSWERS="answers";

    public JSONObject getPollObject(PollModel dto)
            throws JSONException {

        JSONObject pollObject = new JSONObject();
        JSONArray answerArray = new JSONArray();
        pollObject.put(JSON_QUESTION, dto.getPollQuestion());
      //  pollObject.put(JSON_POLL_ANSWERS, parseAnswerList());

        return pollObject;
    }

    public static PollModel parse(JSONObject pollObject)throws JSONException {

        PollModel pollDto = new PollModel();
        pollDto.setIvokoType(IvokoApplication.POST_TYPE.POLL);
        if (!TextUtils.isEmpty(pollObject.getString(JSON_POLLID))) {
            pollDto.setPollId(pollObject.getString(JSON_POLLID));
            pollDto.setObjectId(pollObject.getString(JSON_POLLID));
        }

        pollDto.setPollQuestion(pollObject.getString(JSON_QUESTION));


        pollDto.setPostedDate(pollObject.getString(JSON_POSTEDDATE));




        if(pollObject.has(JSON_ANSWERED) && !pollObject.isNull(JSON_ANSWERED)){
            JSONObject answeredObject = new JSONObject(pollObject.getString("answer"));
            if(answeredObject.has(JSON_ANSWER_FLAG) && !answeredObject.isNull(JSON_ANSWER_FLAG)){
                if (answeredObject.getString(JSON_ANSWER_FLAG).equalsIgnoreCase("yes")){
                    pollDto.setIsanswered(true);
                    pollDto.setAnswered(1);

                } else {
                    pollDto.setIsanswered(false);
                }
            }

            if(answeredObject.has(JSON_ANSWER_OPTION) && !answeredObject.isNull(JSON_ANSWER_OPTION)){
               pollDto.setAnswerdOption(answeredObject.getString(JSON_ANSWER_OPTION));
            }
        }

        if(pollObject.has(JSON_ANSWERS) && !pollObject.isNull(JSON_ANSWERS)){
            pollDto.setPollAnswers(parseAnswers(pollObject.optJSONArray(JSON_ANSWERS)));
        }

        if (pollObject.has(JSON_POSTEDBY) && !pollObject.isNull(JSON_POSTEDBY)){
            JSONObject postedby = pollObject.getJSONObject(JSON_POSTEDBY);

            if (postedby.has(JSON_POSTED_USERNAME) && !TextUtils.isEmpty(JSON_POSTED_USERNAME)){
                pollDto.setUserName(postedby.getString(JSON_POSTED_USERNAME));
            }
            if (postedby.has(JSON_POSTED_USEREMAIL) && !TextUtils.isEmpty(JSON_POSTED_USEREMAIL)){
                pollDto.setUserEmail(postedby.getString(JSON_POSTED_USEREMAIL));
            }
            if (postedby.has(JSON_USERID) && !TextUtils.isEmpty(JSON_USERID)){
                pollDto.setUserid(postedby.getString(JSON_USERID));
            }
        }

        if (pollDto.getPollAnswers().size()>0){
            float prevpercentage =0;
            for (int i = 0; i < pollDto.getPollAnswers().size(); i++){

                PollAnswer answer = pollDto.getPollAnswers().get(i);
                if (answer.getOptionPercentage() >=prevpercentage){
                    pollDto.setHighestOptionNr(answer.getOptionPercentage());
                    prevpercentage = answer.getOptionPercentage();
                }
            }
        }


        return pollDto;
    }



    public  static ArrayList<PollAnswer> parseAnswers(JSONArray answersArray)throws JSONException {

        ArrayList<PollAnswer> answersList = new ArrayList<PollAnswer>();
        if(answersArray==null){
            return answersList;
        }
        float prevpercentage=0;
        for (int i = 0; i < answersArray.length(); i++) {
            JSONObject answer = answersArray.getJSONObject(i);
            PollAnswer answers = new PollAnswer();


            if (!TextUtils.isEmpty(answer.getString(JSON_ANSWERID))){
                answers.setOptionNr(answer.getString(JSON_ANSWERID));
            }

            if (!TextUtils.isEmpty(answer.getString(JSON_ANSWER))){
                answers.setOptionText(answer.getString(JSON_ANSWER));
            }

            if(answer.has(JSON_PERCENTAGE) && !answer.isNull(JSON_PERCENTAGE)) {
                if (!TextUtils.isEmpty(answer.getString(JSON_PERCENTAGE))) {
                    float currentPercent = Float.parseFloat(answer.getString(JSON_PERCENTAGE));
                    answers.setOptionPercentage(currentPercent);
                    answers.setHighest(0);
                   /* if (currentPercent > prevpercentage) {
                        answers.setHighest(1);
                        prevpercentage = currentPercent;

                    } else {
                        answers.setHighest(0);
                    }*/
                }
            }
            if (!TextUtils.isEmpty(answer.getString(JSON_VOTECOUNT))) {
                answers.setVoteCount(Integer.parseInt(answer.getString(JSON_VOTECOUNT)));
            }

            answersList.add(answers);

            }
        return answersList;
    }

    public static List<PollModel> getPollList(JSONArray pollsArray) throws JSONException {

        List<PollModel> pollList = new ArrayList<PollModel>();

        for(int i=0;i<pollsArray.length();i++){
            JSONObject obj = pollsArray.getJSONObject(i);
            pollList.add(PollModel.parse(obj));
        }

        return pollList;

    }




    protected PollModel(Parcel in) {
        this.pollId = in.readString();
        this.pollQuestion = in.readString();
        this.createdBy = in.readString();
        this.postedDate = in.readString();
        this.totalVoteCount = in.readInt();
        this.answerdOption = in.readString();
        this.answered=in.readInt();

     //   pollAnswers = in.readArray(PollAnswer)
        this.pollAnswers = in.readArrayList(PollAnswer.class.getClassLoader());
    }

    public static final Creator<PollModel> CREATOR = new Creator<PollModel>() {
        @Override
        public PollModel createFromParcel(Parcel in) {
            return new PollModel(in);
        }
        @Override
        public PollModel[] newArray(int size) {
            return new PollModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pollId);
        dest.writeString(pollQuestion);
        dest.writeString(postedDate);
        dest.writeString(createdBy);
        dest.writeInt(totalVoteCount);
        dest.writeInt(answered);
        dest.writeList(pollAnswers);
    }




    public Map<String, String> newPollAnswers = new HashMap<String, String>();

    public Map<String, String> get() {
        return newPollAnswers;
    }

    public void setNewPollAnswers(Map<String, String> optionObjects) {
        this.newPollAnswers = optionObjects;
    }

    public static Map<String, String> parseAnswerList(List<PollAnswer> pollAnswerList)
           {

        Map<String, String> pollAnswerHash = new HashMap<String, String>();

        if(pollAnswerList==null || pollAnswerList.size() ==0){
            return pollAnswerHash;
        }

        for (int i = 0; i < pollAnswerList.size(); i++) {
            PollAnswer answer = pollAnswerList.get(i);
            int x=1;
            if (TextUtils.isEmpty(answer.getOptionText()))
                continue;
            pollAnswerHash.put(String.valueOf(x), answer.getOptionText());
            x++;
        }
        return pollAnswerHash;
    }

}
