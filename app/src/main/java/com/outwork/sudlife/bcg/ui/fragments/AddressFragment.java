package com.outwork.sudlife.bcg.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Utils;

public class AddressFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";

    // TODO: Rename and change types of parameters
    private String madressline, mlocality, mcity, mlandmark, addressLine, locality, city, landmark, zipcode, fulladdress;
    private EditText eaddressline, elocality, ecity, elandmark, ezipcode;
    private Button done;
    private OnFragmentInteractionListener mListener;
    private Toolbar toolbar;
    private boolean isupdated = false;

    public AddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static AddressFragment newInstance(String iaddressline, String ilocality, String ilandmark, String icity) {
        AddressFragment fragment = new AddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, iaddressline);
        args.putString(ARG_PARAM2, ilocality);
        args.putString(ARG_PARAM3, icity);
        args.putString(ARG_PARAM4, ilandmark);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddressFragment newInstance(String iaddressline, String ilocality, String ilandmark, String icity, boolean isupdated) {
        AddressFragment fragment = new AddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, iaddressline);
        args.putString(ARG_PARAM2, ilocality);
        args.putString(ARG_PARAM3, icity);
        args.putString(ARG_PARAM4, ilandmark);
        args.putBoolean(ARG_PARAM5, isupdated);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            madressline = getArguments().getString(ARG_PARAM1);
            mlocality = getArguments().getString(ARG_PARAM2);
            mcity = getArguments().getString(ARG_PARAM3);
            mlandmark = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address, container, false);

        eaddressline = (EditText) view.findViewById(R.id.faddressline);
        elocality = (EditText) view.findViewById(R.id.fLocality);
        elandmark = (EditText) view.findViewById(R.id.flandmark);
        ecity = (EditText) view.findViewById(R.id.fcity);
        done = (Button) view.findViewById(R.id.fdone);

        eaddressline.setText(madressline);
        elocality.setText(mlocality);
        ecity.setText(mcity);
        if (!TextUtils.isEmpty(mlandmark)) {
            elandmark.setText(mlandmark);
        }

        toolbar = (Toolbar) view.findViewById(R.id.ftoolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAddressData();
            }
        });
        initToolBar();

        ((BaseActivity)getActivity()).hideKeyboard();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //   mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
           /* throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");*/
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onSetAddress(String addressline, String locality, String landmark, String city, String fulladdress, boolean isupdated);
    }

    private void setAddressData() {
        if (Utils.isNotNullAndNotEmpty(eaddressline.getText().toString())) {
            if (!eaddressline.getText().toString().equals(madressline)) {
                isupdated = true;
            }
            addressLine = eaddressline.getText().toString().trim();
        } else {
            addressLine = "";
        }
        if (Utils.isNotNullAndNotEmpty(elocality.getText().toString())) {
            if (!elocality.getText().toString().equals(mlocality)) {
                isupdated = true;
            }
            locality = elocality.getText().toString().trim();
        } else {
            locality = "";
        }
        if (Utils.isNotNullAndNotEmpty(elandmark.getText().toString())) {
            if (!elandmark.getText().toString().equals(mlandmark)) {
                isupdated = true;
            }
            landmark = elandmark.getText().toString().trim();
        } else {
            landmark = "";
        }
        if (Utils.isNotNullAndNotEmpty(ecity.getText().toString())) {
            if (!ecity.getText().toString().equals(mcity)) {
                isupdated = true;
            }
            city = ecity.getText().toString().trim();
        } else {
            city = "";
        }

        fulladdress = addressLine + " " + locality + " " + landmark + " " + city;
        mListener.onSetAddress(addressLine, locality, landmark, city, fulladdress, isupdated);
        getActivity().onBackPressed();
    }

    private void initToolBar() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Address");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
