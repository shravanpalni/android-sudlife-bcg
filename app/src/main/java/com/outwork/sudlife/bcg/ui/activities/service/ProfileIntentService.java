package com.outwork.sudlife.bcg.ui.activities.service;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.restinterfaces.ProfileService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.models.ProfileModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileIntentService extends JobIntentService {
    public static final String TAG = ProfileIntentService.class.getSimpleName();

    public static final String ACTION_GET_PROFILE = "com.outwork.sudlife.bcg.ui.activities.service.action.GET_PROFILE";
    private LocalBroadcastManager mgr;
    private static final Integer JOBID = 1005;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_PROFILE.equals(action)) {
                getProfile();
            }
        }
    }

    public static void insertProfiledata(Context context) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ProfileIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void getProfile() {
        ProfileService client = RestService.createService(ProfileService.class);
        Call<RestResponse> getProfile = client.getUserProfile(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        getProfile.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    if (!TextUtils.isEmpty(jsonResponse.getData())) {
                        ProfileModel profileModel = new Gson().fromJson(jsonResponse.getData(), ProfileModel.class);
                        SharedPreferenceManager.getInstance().storeProfileData(profileModel);
                        SharedPreferenceManager.getInstance().putString(Constants.PROFILE_LOADED, "loaded");
                        Intent intent = new Intent("profile_broadcast");
                        mgr = LocalBroadcastManager.getInstance(ProfileIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } else {
                    SharedPreferenceManager.getInstance().putString(Constants.PROFILE_LOADED, "notloaded");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Log.d("", "fail");
                SharedPreferenceManager.getInstance().putString(Constants.PROFILE_LOADED, "notloaded");
            }
        });
    }
}