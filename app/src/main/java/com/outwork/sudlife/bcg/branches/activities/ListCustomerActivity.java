package com.outwork.sudlife.bcg.branches.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.branches.adapter.CustomerOnlineAdapter;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.branches.services.ContactsIntentService;
import com.outwork.sudlife.bcg.branches.services.CustomerService;
import com.outwork.sudlife.bcg.lead.activities.AddOtherBranchLeadActivity;
import com.outwork.sudlife.bcg.lead.activities.CreateNewLeadActivity;
import com.outwork.sudlife.bcg.planner.activities.CreatePlanActivity;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ListCustomerActivity extends BaseActivity {
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;
    private ListView custListView;
    private AppCompatButton otherbranch;
    private ProgressBar progressBar;
    private FloatingActionButton addMember;
    private List<BranchesModel> customerList;
    private CustomerOnlineAdapter customerOnlineAdapter;
    private RelativeLayout searchLayout;
    private TextView noinfo, toolbar_title;
    private int plan_status;
    private String type, displaydate, fromclass;
    private EditText searchText;
   // private BroadcastReceiver mBroadcastReceiver;
    //private LocalBroadcastManager mgr;
    List<BranchesModel> branchesModelList = new ArrayList<>();
    public static ListCustomerActivity listCustomerActivity;
    private boolean isfromCalendar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        listCustomerActivity = this;

        //mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("type")) {
            type = getIntent().getStringExtra("type");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
            displaydate = getIntent().getStringExtra(Constants.PLAN_DATE);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
            plan_status = getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("fromclass")) {
            fromclass = getIntent().getStringExtra("fromclass");
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("fromCalendar")) {
            isfromCalendar = getIntent().getExtras().getBoolean("fromCalendar");


        }
        progressBar = (ProgressBar) findViewById(R.id.mProgressBar);
        noinfo = (TextView) findViewById(R.id.noMembers);
        otherbranch = (AppCompatButton) findViewById(R.id.otherbranch);
        custListView = (ListView) findViewById(R.id.memberList);
        addMember = (FloatingActionButton) findViewById(R.id.addcustomer);
        initToolbar();
        if (SharedPreferenceManager.getInstance().getString(Constants.CUSTOMERS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                ContactsIntentService.insertCustomersinDB(ListCustomerActivity.this);
            }
        }

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 5) {

            if (isNetworkAvailable()) {
                noinfo.setVisibility(View.GONE);

                new GetHierarchyCustomerList().execute();
            } else {
                showSimpleAlert("", "Oops...No Internet connection.");
                noinfo.setVisibility(View.VISIBLE);
                noinfo.setText("No Internet.");

            }


        } else {

            if (isNetworkAvailable()) {
                new GetCustomerList().execute();
            } else {
                if (Utils.isNotNullAndNotEmpty(type)) {
                    if (type.equalsIgnoreCase("select")) {
                        if (Utils.isNotNullAndNotEmpty(fromclass))
                            if (fromclass.equalsIgnoreCase("lead")) {
                                otherbranch.setVisibility(View.VISIBLE);
                            } else {
                                otherbranch.setVisibility(View.GONE);
                            }
                    } else {
                        otherbranch.setVisibility(View.GONE);
                    }
                } else {
                    otherbranch.setVisibility(View.GONE);
                }

                getCustomerList();
                setListener();
                Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, noinfo);
                Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText), otherbranch);

                dismissProgressDialog();
            }


        }











       /* getCustomerList();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, noinfo);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText), otherbranch);*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getCustomerList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
       // mgr.unregisterReceiver(mBroadcastReceiver);
    }

    private void setListener() {
        otherbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ListCustomerActivity.this, AddOtherBranchLeadActivity.class);
                startActivity(in);
                //finish();
            }
        });
        custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                if (Utils.isNotNullAndNotEmpty(type)) {
                    if (type.equalsIgnoreCase("select")) {
                        if (Utils.isNotNullAndNotEmpty(fromclass)) {
                            if (fromclass.equalsIgnoreCase("lead")) {
                                BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                Intent in = new Intent(ListCustomerActivity.this, CreateNewLeadActivity.class);
                                in.putExtra("customer", new Gson().toJson(dto));
                                in.putExtra("customerid", dto.getCustomerid());
                                in.putExtra("finishBranches",true);
                                in.putExtra("fromCalendar",isfromCalendar);
                                in.putExtra(Constants.PLAN_STATUS, plan_status);
                                in.putExtra(Constants.PLAN_DATE, displaydate);
                                startActivity(in);
                                //finish();
                            } else if (fromclass.equalsIgnoreCase("plan")) {
                                BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                Intent in = new Intent(ListCustomerActivity.this, CreatePlanActivity.class);
                                in.putExtra("customer", new Gson().toJson(dto));
                                in.putExtra("customerid", dto.getCustomerid());
                                in.putExtra("finishBranches",true);
                                in.putExtra(Constants.PLAN_STATUS, plan_status);
                                in.putExtra(Constants.PLAN_DATE, displaydate);
                                startActivity(in);
                                //finish();
                            }
                        }
                    } else {
                        BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                        Intent in = new Intent(ListCustomerActivity.this, ViewCustomerActivity.class);
                        in.putExtra("customer", new Gson().toJson(dto));
                        in.putExtra("customerid", dto.getCustomerid());
                        startActivity(in);
                    }
                } else {
                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                    Intent in = new Intent(ListCustomerActivity.this, ViewCustomerActivity.class);
                    in.putExtra("customer", new Gson().toJson(dto));
                    in.putExtra("customerid", dto.getCustomerid());
                    startActivity(in);
                }
            }
        });
        /*mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getCustomerList();
            }
        };*/
    }

    private void getCustomerList() {


        customerList = new ArrayList<BranchesModel>();
        customerList.clear();
        // customerList = new BranchesMgr(this).getonlineCustomerList(groupId, userid, "");
        customerList = BranchesMgr.getInstance(this).getonlineCustomerList(groupId, userid, "");


        if (customerList.size() > 0) {
            noinfo.setVisibility(View.GONE);
            customerOnlineAdapter = new CustomerOnlineAdapter(this, customerList, groupId, userid);
            custListView.setAdapter(customerOnlineAdapter);
            progressBar.setVisibility(View.GONE);

            setUpSearchLayout();
            /*if (customerList.size() > 10) {
                setUpSearchLayout();
            }*/
        } else {
            setNoCustomers();
        }
    }

    private void setNoCustomers() {
        if (progressBar.VISIBLE == 0) {
            progressBar.setVisibility(View.GONE);
        }
        noinfo.setVisibility(View.VISIBLE);
        //noinfo.setText("No Branches.");
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mListToolBar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(type)) {
            if (type.equalsIgnoreCase("select")) {
                toolbar_title.setText("Select Branch");
            } else {
                toolbar_title.setText("Branches");
            }
        } else {
            toolbar_title.setText("Branches");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setUpSearchLayout() {
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchLayout.setVisibility(View.VISIBLE);
        EditText searchText = (EditText) findViewById(R.id.searchText);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (customerOnlineAdapter != null && s != null) {
                    customerOnlineAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                data.putExtra("customerObj", data.getStringExtra("customerObj"));
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }


    private class GetCustomerList extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            String modifiedDate = "";
            if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.BRANCHES_LAST_FETCH_TIME, ""))) {
                modifiedDate = new BranchesMgr(ListCustomerActivity.this).getmaxModifiedDate(groupId, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
            } else {
                modifiedDate = "";
                SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "1531739767");
            }
            CustomerService client = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> getcustomers = client.getcustomers(userToken, groupId, "", modifiedDate);
            try {
                Response<RestResponse> restResponse = getcustomers.execute();
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<BranchesModel>>() {
                        }.getType();
                        List<BranchesModel> branchesModelList = gson.fromJson(restResponse.body().getData(), listType);
                        if (branchesModelList.size() > 0) {
                            new BranchesMgr(ListCustomerActivity.this).insertNewCustomerList(branchesModelList);
                        }

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
            }


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {


            if (Utils.isNotNullAndNotEmpty(type)) {
                if (type.equalsIgnoreCase("select")) {
                    if (Utils.isNotNullAndNotEmpty(fromclass))
                        if (fromclass.equalsIgnoreCase("lead")) {
                            otherbranch.setVisibility(View.VISIBLE);
                        } else {
                            otherbranch.setVisibility(View.GONE);
                        }
                } else {
                    otherbranch.setVisibility(View.GONE);
                }
            } else {
                otherbranch.setVisibility(View.GONE);
            }


            getCustomerList();
            setListener();
            Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, noinfo);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText), otherbranch);

            dismissProgressDialog();

        }

        @Override
        protected void onPreExecute() {

            showProgressDialog("Loading . . .");


        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    private class GetHierarchyCustomerList extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            String modifiedDate = "";
            if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.BRANCHES_LAST_FETCH_TIME, ""))) {
                modifiedDate = new BranchesMgr(ListCustomerActivity.this).getmaxModifiedDate(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
            } else {
                modifiedDate = "";
                SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "1531739767");
            }
            CustomerService client = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> getcustomers = client.getcustomerassignees(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "", modifiedDate);
            try {
                Response<RestResponse> restResponse = getcustomers.execute();
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<BranchesModel>>() {
                        }.getType();
                        branchesModelList = gson.fromJson(restResponse.body().getData(), listType);
                           /* if (branchesModelList.size() > 0) {
                                new BranchesMgr(getActivity()).insertNewCustomerList(branchesModelList);
                            }*/

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
            }


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {


            //getCustomerList();
            //setListener();
            //Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, noinfo);
            //Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText), otherbranch);
            // getCustomerList();



            custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    if (Utils.isNotNullAndNotEmpty(type)) {
                        if (type.equalsIgnoreCase("select")) {
                            if (Utils.isNotNullAndNotEmpty(fromclass)) {
                                if (fromclass.equalsIgnoreCase("lead")) {
                                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                    Intent in = new Intent(ListCustomerActivity.this, CreateNewLeadActivity.class);
                                    in.putExtra("customer", new Gson().toJson(dto));
                                    in.putExtra("customerid", dto.getCustomerid());
                                    in.putExtra(Constants.PLAN_STATUS, plan_status);
                                    in.putExtra(Constants.PLAN_DATE, displaydate);
                                    startActivity(in);
                                    finish();
                                } else if (fromclass.equalsIgnoreCase("plan")) {
                                    BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                                    Intent in = new Intent(ListCustomerActivity.this, CreatePlanActivity.class);
                                    in.putExtra("customer", new Gson().toJson(dto));
                                    in.putExtra("customerid", dto.getCustomerid());
                                    in.putExtra(Constants.PLAN_STATUS, plan_status);
                                    in.putExtra(Constants.PLAN_DATE, displaydate);
                                    startActivity(in);
                                    finish();
                                }
                            }
                        } else {
                            BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                            Intent in = new Intent(ListCustomerActivity.this, ViewCustomerActivity.class);
                            in.putExtra("customer", new Gson().toJson(dto));
                            in.putExtra("customerid", dto.getCustomerid());
                            startActivity(in);
                        }
                    } else {
                        BranchesModel dto = (BranchesModel) parent.getItemAtPosition(position);
                        Intent in = new Intent(ListCustomerActivity.this, ViewCustomerActivity.class);
                        in.putExtra("customer", new Gson().toJson(dto));
                        in.putExtra("customerid", dto.getCustomerid());
                        startActivity(in);
                    }
                }
            });



            if (branchesModelList.size() > 0) {
                noinfo.setVisibility(View.GONE);
                customerOnlineAdapter = new CustomerOnlineAdapter(ListCustomerActivity.this, branchesModelList, SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                custListView.setAdapter(customerOnlineAdapter);
                //progressBar.setVisibility(View.GONE);
                setUpSearchLayout();
                /*if (customerList.size() > 10) {
                    setUpSearchLayout();
                }*/
            } else {
                setNoCustomers();
            }

            dismissProgressDialog();

        }

        @Override
        protected void onPreExecute() {

            showProgressDialog("Loading . . .");


        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


}