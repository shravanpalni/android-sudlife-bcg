package com.outwork.sudlife.bcg.targets.services;

import com.outwork.sudlife.bcg.targets.models.MSBModel;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by bvlbh on 3/26/2018.
 */

public interface RestServicesForTragets {
    @GET("mobile/v1/target/{month}/{year}")
    Call<RestResponse> getTargets(@Header("utoken") String userToken,
                                  @Path("month") String groupid,
                                  @Path("year") String startdate);

    @GET("mobile/v1/target/user/{userid}/{month}/{year}")
    Call<RestResponse> getUserTargets(@Header("utoken") String userToken,
                                      @Path("userid") String userid,
                                      @Path("month") String month,
                                      @Path("year") String year);

    @GET("mobile/v1/target/{month}/{year}/{date}")
    Call<RestResponse> getUserTargetstillDate(@Header("utoken") String userToken,
                                              @Path("month") String month,
                                              @Path("year") String year,
                                              @Path("date") String date);

    @GET("mobile/v1/teams/members")
    Call<RestResponse> getTeamMembers(@Header("utoken") String userToken);

    @PUT("mobile/v1/target/{targetid}")
    Call<RestResponse> updateTarget(@Header("utoken") String userToken,
                                    @Path("targetid") String targetid,
                                    @Body MSBModel sJsonBody);

    @GET("mobile/v1/target/performance/team/{year}/{month}")
    Call<RestResponse> getTeamSummary(@Header("utoken") String userToken,
                                      @Path("year") String year,
                                      @Path("month") String month);
}
