package com.outwork.sudlife.bcg.more;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.listener.RecyclerTouchListener;
import com.outwork.sudlife.bcg.more.adapter.MessageRecyclerAdapter;
import com.outwork.sudlife.bcg.more.models.CommonDto;
import com.outwork.sudlife.bcg.more.models.MessageModel;
import com.outwork.sudlife.bcg.more.models.UIMessageViewDto;
import com.outwork.sudlife.bcg.more.service.PostService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPostMessageActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final int ADD_MSG = 191;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView adminRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar msgListProgressBar;
    private FloatingActionButton fab;
    private List<MessageModel> msgList = new ArrayList<MessageModel>();
    private List<MessageModel> tempmsgList = new ArrayList<MessageModel>();
    private MessageRecyclerAdapter messageRecyclerAdapter;
    private String postTypeCode, groupId, userToken, memberType;
    private String feedType = "admin";
    private IvokoApplication.POST_TYPE postType;
    private TextView nomessageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        postTypeCode = "";
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        memberType = SharedPreferenceManager.getInstance().getString(Constants.LOGINTYPE, "");

        initUi();
        initToolBar();
        initListeners();
        if (isNetworkAvailable()) {
            refreshList();
        } else {
            msgListProgressBar.setVisibility(View.GONE);
            nomessageview.setVisibility(View.VISIBLE);
            showToast("Oops....!No Internet");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isNetworkAvailable()) {
            refreshList();
        } else {
            msgListProgressBar.setVisibility(View.GONE);
            nomessageview.setVisibility(View.VISIBLE);
            showToast("Oops....!No Internet");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_message_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUi() {
        nomessageview = (TextView) findViewById(R.id.nomessages);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_msg_swipe_refresh_layout);
        msgListProgressBar = (ProgressBar) findViewById(R.id.messageListProgressBar);
        adminRecyclerView = (RecyclerView) findViewById(R.id.adminRecyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        adminRecyclerView.setHasFixedSize(true);
        messageRecyclerAdapter = new MessageRecyclerAdapter(this, msgList);
        adminRecyclerView.setAdapter(messageRecyclerAdapter);
        adminRecyclerView.setLayoutManager(linearLayoutManager);
        fab = (FloatingActionButton) findViewById(R.id.fabMsg);
    }

    private void initListeners() {
        if (SharedPreferenceManager.getInstance().getString(Constants.GRP_OBJECTTYPE, "").equalsIgnoreCase("Admin")) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ListPostMessageActivity.this, AddPostActivity.class);
                    //intent.putExtra("PostTypeCode", postTypeCode);
                    startActivityForResult(intent, ADD_MSG);

                }
            });
        } else {
            fab.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        adminRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                CommonDto commonDto = (MessageModel) msgList.get(position);
//                performAction(commonDto);
            }
        }));
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mPostToolBar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Messages");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        mSwipeRefreshLayout.setRefreshing(true);
        if (isNetworkAvailable()) {
            refreshList();
        } else {
            msgListProgressBar.setVisibility(View.GONE);
            nomessageview.setVisibility(View.VISIBLE);
            showToast("Oops....!No Internet");
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void refreshList() {
        nomessageview.setVisibility(View.INVISIBLE);
        PostService client = RestService.createService(PostService.class);
        Call<RestResponse> getFeed = client.getChannelPosts(userToken, groupId, "50", "", "asc", "");
        getFeed.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        if (!TextUtils.isEmpty(response.body().getData())) {
                            tempmsgList.clear();
                            Type listType = new TypeToken<ArrayList<MessageModel>>() {
                            }.getType();
                            tempmsgList = new Gson().fromJson(response.body().getData(), listType);

                            parseFeedResponse(tempmsgList, true);
                            Collections.reverse(tempmsgList);
                            msgList.clear();
                            for (MessageModel messageModel : tempmsgList) {
                                msgList.add(0, messageModel);
                            }

                            if (msgList.size() > 0) {
                                if (msgListProgressBar.VISIBLE == 0) {
                                    msgListProgressBar.setVisibility(View.INVISIBLE);
                                }
                                messageRecyclerAdapter.notifyDataSetChanged();
                            } else {
                                setNoMessages();
                            }
                        } else {
                            setNoMessages();
                        }

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        setNoMessages();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                setNoMessages();
            }
        });
    }

    private void setNoMessages() {
        if (msgList.size() == 0) {
            nomessageview.setVisibility(View.VISIBLE);
        }
        if (msgListProgressBar.VISIBLE == 0) {
            msgListProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void parseFeedResponse(List<MessageModel> messageModelList, boolean isStore) {
        try {
            for (int i = 0; i < messageModelList.size(); i++) {
                MessageModel messageModel = messageModelList.get(i);
                Object postObject = null;
                if (!TextUtils.isEmpty(messageModel.getRelatedObjectType()) && (messageModel.getRelatedObjectType() != "null")) {
//                    postObject = UIMessageViewDto.parse(messageModel);
                    if (postObject != null) {
//                        ((CommonDto) postObject).setPostId(messageModel.getString("ID"));
//                        ((CommonDto) postObject).setFeedDate(messageModel.getString("createddate"));
//                        tempmsgList.add(postObject);
//                        msgList.add(postObject);
                    }
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void performAction(CommonDto commonDto) {
        if (!isNetworkAvailable()) {
            Utils.displayToast(ListPostMessageActivity.this, "Please check your internet connection and try again");
            return;
        }

        postType = commonDto.getIvokoType();
        Intent in;
        switch (postType) {
            case MAIL:
            case EMAIL:
                UIMessageViewDto uiMessageViewDto = (UIMessageViewDto) commonDto;
                in = new Intent(ListPostMessageActivity.this, ViewPostMessageActivity.class);
                in.putExtra("OBJECTID", commonDto.getObjectId());
                in.putExtra("msg", true);
                in.putExtra("MessageType", uiMessageViewDto.getMessageType());
                in.putExtra("MessageSubType", uiMessageViewDto.getMessageSubType());
                in.putExtra("commentflag", uiMessageViewDto.isCommentEnabled());
                startActivity(in);
                break;
            case EVENT:
                UIMessageViewDto eventDto = (UIMessageViewDto) commonDto;
                in = new Intent(ListPostMessageActivity.this, ViewEventActivity.class);
                in.putExtra("eventid", commonDto.getObjectId());
                startActivity(in);
                break;
            case POLL:
                UIMessageViewDto pollDto = (UIMessageViewDto) commonDto;
                in = new Intent(ListPostMessageActivity.this, ViewPollActivity.class);
                in.putExtra("pollid", commonDto.getObjectId());
                // in.putExtra("haspollinfo",false);
                startActivity(in);
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_MSG) {
            if (resultCode == RESULT_OK && data != null) {
            }
        }
    }
}