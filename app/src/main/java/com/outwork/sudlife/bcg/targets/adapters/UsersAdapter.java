package com.outwork.sudlife.bcg.targets.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.ui.models.Userprofile;

import java.util.ArrayList;

/**
 * Created by bvlbh on 4/7/2018.
 */

public class UsersAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Userprofile> userprofilesArrayList;

    public UsersAdapter(Context context, ArrayList<Userprofile> userprofilesArrayList) {
        this.context = context;
        this.userprofilesArrayList = userprofilesArrayList;
    }

    @Override
    public int getCount() {
        return userprofilesArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.user_list_item, viewGroup, false);
        TextView tvUser = (TextView) itemView.findViewById(R.id.spinner_item);
        tvUser.setText(userprofilesArrayList.get(i).getFirstname() + " " + userprofilesArrayList.get(i).getLastname());
        return itemView;
    }
}
