package com.outwork.sudlife.bcg.notifications;

import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;

import java.util.List;

/**
 * Created by bvlbh on 4/9/2018.
 */

public class GlobalData {
    private static GlobalData globalData = null;


    public List<PlannerModel> getPlannerModelArrayList() {
        return plannerModelArrayList;
    }

    public void setPlannerModelArrayList(List<PlannerModel> plannerModelArrayList) {
        this.plannerModelArrayList = plannerModelArrayList;
    }

    private List<PlannerModel> plannerModelArrayList;


    public LeadModel getLeadModel() {
        return leadModel;
    }

    public void setLeadModel(LeadModel leadModel) {
        this.leadModel = leadModel;
    }

    private LeadModel leadModel;

    public LeadModel getLeadModelForFm() {
        return leadModelForFm;
    }

    public void setLeadModelForFm(LeadModel leadModelForFm) {
        this.leadModelForFm = leadModelForFm;
    }

    private LeadModel leadModelForFm;


    private GlobalData() {

    }
    public static GlobalData getInstance() {
        if (globalData == null) {
            globalData = new GlobalData();
        }
        return globalData;
    }
}