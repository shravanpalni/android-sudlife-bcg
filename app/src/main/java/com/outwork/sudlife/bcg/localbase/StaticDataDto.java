package com.outwork.sudlife.bcg.localbase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panch on 10/5/2015.
 */
public class StaticDataDto {
    @SerializedName("itemid")
    @Expose
    private String itemid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("dropdownlabel")
    @Expose
    private String dropdownlabel;
    @SerializedName("relatedto")
    @Expose
    private String relatedto;
    @SerializedName("isactive")
    @Expose
    private Boolean isactive;
    @SerializedName("lastmodifiedon")
    @Expose
    private String lastmodifiedon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("name")
    @Expose
    private String name;

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDropdownlabel() {
        return dropdownlabel;
    }

    public void setDropdownlabel(String dropdownlabel) {
        this.dropdownlabel = dropdownlabel;
    }

    public String getRelatedto() {
        return relatedto;
    }

    public void setRelatedto(String relatedto) {
        this.relatedto = relatedto;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getLastmodifiedon() {
        return lastmodifiedon;
    }

    public void setLastmodifiedon(String lastmodifiedon) {
        this.lastmodifiedon = lastmodifiedon;
    }
}