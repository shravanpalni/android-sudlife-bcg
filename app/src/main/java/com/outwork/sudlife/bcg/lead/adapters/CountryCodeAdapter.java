
package com.outwork.sudlife.bcg.lead.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.lead.model.CountryCodeModel;

import java.util.ArrayList;
import java.util.List;

public class CountryCodeAdapter  extends BaseAdapter  {

    private Context context;
    private final LayoutInflater mInflater;

    private List<CountryCodeModel> resultList = new ArrayList<>();

    private List<CountryCodeModel> origionalList = new ArrayList<>();

    static class ViewHolder {
        public TextView name;
        public ImageView picture;
    }


    public CountryCodeAdapter(Context context, List<CountryCodeModel> countryCodeModelList){
        this.context = context;
        this.resultList=countryCodeModelList;
        this.origionalList=countryCodeModelList;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return this.resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_product_dropdown, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)view.findViewById(R.id.productname);
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        CountryCodeModel dto = (CountryCodeModel) this.resultList.get(position);

        holder.name.setText(dto.getCountry());



        return view;
    }

   /* @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            public String convertResultToString(Object resultValue) {
                return ((CountryCodeModel) resultValue).getCountry();
            }
            @Override
            protected FilterResults performFiltering(CharSequence userinput) {
                FilterResults filterResults = new FilterResults();
                if (userinput != null) {

                    //List<ProductMasterDto> prodList = new ProductDataMgr(context).getProductList(groupid,userinput.toString());

                   *//* Predicate<String> myCityPredicate = Predicates.equalTo(userinput);
                    final List<CountryCodeModel> res = Lists.newArrayList(Iterables.filter(resultList , myCityPredicate));*//*




                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence userinput, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<CountryCodeModel>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }*/

}
