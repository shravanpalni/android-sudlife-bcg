package com.outwork.sudlife.bcg.lead.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.bcg.lead.LeadMgr;
import com.outwork.sudlife.bcg.lead.activities.fragments.FilterFragment;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.lead.adapters.LeadsAdapter;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.opportunity.services.FormsService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadsListActivity extends BaseActivity implements FilterFragment.OnFragmentInteractionListener {

    private RecyclerView rcvLeads;
    private RelativeLayout searchLayout;
    private BroadcastReceiver mBroadcastReceiver;
    private EditText searchText;
    private TabLayout tabLayout;
    private String searchby = "", stage = "";
    private int position = 5;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView filter;
    private EditText startdate, enddate;
    private Button submit;
    private String sdate, edate;
    private LocalBroadcastManager mgr;
    private LeadsAdapter leadsAdapter;
    private TextView fetcchtime, toolbar_title, tv_nodata;
    private Boolean isfiltered = false;
    private List<LeadModel> leadModels = new ArrayList<>();
    private RadioButton name, branchname, bankname, contactno, sudcode;
    private boolean isFirstTime = false;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                startdate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                sdate = TimeUtils.convertDatetoUnix(visitdt);
                startdate.setText(visitdt);
                startdate.setEnabled(true);
                startdate.setError(null);
            }
            if (customSelector == 1) {
                enddate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                edate = TimeUtils.convertDatetoUnix(visitdt);
                enddate.setText(visitdt);
                enddate.setEnabled(true);
                enddate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            startdate.setEnabled(true);
            enddate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_leads);
        mgr = LocalBroadcastManager.getInstance(this);
       /* if (isNetworkAvailable()) {
            getServerLeads();
        }*/


        if (SharedPreferenceManager.getInstance().getString(Constants.LEADS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertLeadstoServer(LeadsListActivity.this);
            }
        }
        if (isNetworkAvailable()) {
            if (LeadMgr.getInstance(LeadsListActivity.this).getOfflineLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0)
                ;
            LeadIntentService.syncLeadstoServer(LeadsListActivity.this);
        }
        initToolBar();
        rcvLeads = (RecyclerView) findViewById(R.id.rcv_leads);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.lead_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Open"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacted &\n meeting fixed"));
        tabLayout.addTab(tabLayout.newTab().setText("Proposition\n presented"));
        tabLayout.addTab(tabLayout.newTab().setText("Converted"));
        tabLayout.addTab(tabLayout.newTab().setText("Not\n Interested"));
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        findViewById(R.id.fb_lead_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeadsListActivity.this, ListCustomerActivity.class);
                intent.putExtra("type", "select");
                intent.putExtra("fromclass", "lead");
                startActivity(intent);
            }
        });
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchText = (EditText) findViewById(R.id.searchText);
        filter = (ImageView) findViewById(R.id.filter);
        fetcchtime = (TextView) findViewById(R.id.fetchtime);
        leadModels.clear();
        tabLayout.getTabAt(position).select();

        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        tv_nodata.setVisibility(View.VISIBLE);

        final List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
        if (leadModels.size() > 0) {
            isFirstTime = false;

        } else {
            isFirstTime = true;
        }
        showProgressDialog("Loading . . .");
        showList(leadModels, searchby);
        setListener();
        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                leadModels.clear();
                tabLayout.getTabAt(position).select();
                if (stage.length() > 0) {
                    // isFirstTime = true;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, stage);
                    if (leadModels.size() > 0) {
                        isFirstTime = true;
                    }
                    showList(leadModels, searchby);
                } else {
                    // isFirstTime = true;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
                    if (leadModels.size() > 0) {
                        isFirstTime = true;
                    }
                    showList(leadModels, searchby);
                }
            }
        };
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isNetworkAvailable()) {
            getServerLeads();
        }
        leadModels.clear();

        tabLayout.getTabAt(position).select();
        if (stage.length() > 0) {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {
                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                            sdate, edate, stage);
                } else {
                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                            sdate, edate);
                }
            } else {
                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, stage);
            }
        } else {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {
                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                            sdate, edate, stage);
                } else {
                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                            sdate, edate);
                }
            } else {
                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
            }
        }
        showList(leadModels, searchby);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("lead_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter, menu);
//        if (isfiltered) {
//            MenuItem menuItem = menu.findItem(R.id.filter);
//            MenuItemCompat.getActionView(menuItem);
//            menuItem.setIcon(R.drawable.ic_action_filter_filled);
//        } else {
//            MenuItem menuItem = menu.findItem(R.id.filter);
//            MenuItemCompat.getActionView(menuItem);
//            menuItem.setIcon(R.drawable.ic_action_filter);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.filter) {
//            FilterFragment filterFragment = new FilterFragment();
//            FragmentManager fm = getSupportFragmentManager();
//            FragmentTransaction ft = fm.beginTransaction();
//            ft.add(android.R.id.content, filterFragment);
//            ft.addToBackStack(null);
//            ft.commit();
            dateFilter();
//            if (isfiltered) {
//                item.setIcon(R.drawable.ic_action_filter_filled);
//            } else {
//                item.setIcon(R.drawable.ic_action_filter);
//            }
        }
        if (item.getItemId() == R.id.refresh) {
            if (isNetworkAvailable()) {
                showProgressDialog("Refreshing Data..Please Wait....");
                LeadIntentService.syncLeadstoServer(LeadsListActivity.this);
                final List<LeadModel> offlineLeadsList = LeadMgr.getInstance(LeadsListActivity.this).getOfflineLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
                if (offlineLeadsList.size() == 0) {
                    leadModels.clear();
                    getServerLeads();
                }
            } else {
                showToast("Please Check Internet Connection....");
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setListener() {
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                leadModels.clear();
                tabLayout.getTabAt(position).select();
                if (stage.length() > 0) {
                    if (isfiltered) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate, stage);
                        } else {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate);
                        }
                    } else {
                        leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, stage);
                    }
                } else {
                    if (isfiltered) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate, stage);
                        } else {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate);
                        }
                    } else {
                        leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
                    }
                }
                showList(leadModels, searchby);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equalsIgnoreCase("Open")) {

                    leadModels.clear();
                    stage = "Open";
                    position = 0;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, "Open");
                    showList(leadModels, searchby);
                } else if (tab.getText().toString().equalsIgnoreCase("Contacted &\n meeting fixed")) {

                    leadModels.clear();
                    stage = "Contacted & meeting fixed";
                    position = 1;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, "Contacted & meeting fixed");
                    showList(leadModels, searchby);
                } else if (tab.getText().toString().equalsIgnoreCase("Proposition\n presented")) {

                    leadModels.clear();
                    stage = "Proposition presented";
                    position = 2;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, "Proposition presented");
                    showList(leadModels, searchby);
                } else if (tab.getText().toString().equalsIgnoreCase("Converted")) {

                    leadModels.clear();
                    stage = "Converted";
                    position = 3;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, "Converted");
                    showList(leadModels, searchby);
                } else if (tab.getText().toString().equalsIgnoreCase("Not\n Interested")) {
                    leadModels.clear();
                    stage = "Not Interested";
                    position = 4;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, "Not Interested");
                    showList(leadModels, searchby);
                } else {

                    leadModels.clear();
                    stage = "";
                    position = 5;
                    isfiltered = false;
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
                    showList(leadModels, searchby);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Leads");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void setUpSearchLayout() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (leadsAdapter != null && s != null) {
                    leadsAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void showList(List<LeadModel> leadModels, String type) {

        // leadModels.clear();
//        tv_nodata.setVisibility(View.GONE);
        if (leadModels.size() > 0) {
            tv_nodata.setVisibility(View.GONE);
            rcvLeads.setVisibility(View.VISIBLE);
            leadsAdapter = new LeadsAdapter(leadModels, LeadsListActivity.this, type, stage);
            rcvLeads.setAdapter(leadsAdapter);
            rcvLeads.setLayoutManager(new LinearLayoutManager(LeadsListActivity.this));
            if (leadModels.size() > 10) {
                setUpSearchLayout();
            }
        } else {
            //tv_nodata.setVisibility(View.VISIBLE);
            if (isFirstTime) {
                tv_nodata.setText("Loading ...");

            } else {
                tv_nodata.setText("No Data");
            }


            rcvLeads.setVisibility(View.GONE);
        }

        dismissProgressDialog();




        fetcchtime.setText("Last Refreshed Time : " + TimeUtils.getFormattedDatefromUnix(SharedPreferenceManager.getInstance().getString(Constants.LEAD_LAST_FETCH_TIME, ""), "dd/MM/yyyy hh:mm a"));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public void showpopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LeadsListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.search_popup_layout, null);

        alertDialog.setView(convertView);
        RadioGroup type = (RadioGroup) convertView.findViewById(R.id.type);
        name = (RadioButton) convertView.findViewById(R.id.name);
        branchname = (RadioButton) convertView.findViewById(R.id.branchname);
        bankname = (RadioButton) convertView.findViewById(R.id.bankname);
        contactno = (RadioButton) convertView.findViewById(R.id.contactno);
        sudcode = (RadioButton) convertView.findViewById(R.id.sudcode);
        if (Utils.isNotNullAndNotEmpty(searchby)) {
            if (searchby.equalsIgnoreCase("name")) {
                name.setChecked(true);
            } else if (searchby.equalsIgnoreCase("branchname")) {
                branchname.setChecked(true);
            } else if (searchby.equalsIgnoreCase("bankname")) {
                bankname.setChecked(true);
            } else if (searchby.equalsIgnoreCase("contactno")) {
                contactno.setChecked(true);
            } else if (searchby.equalsIgnoreCase("sudcode")) {
                sudcode.setChecked(true);
            }
        }
        Utils.setTypefaces(IvokoApplication.robotoTypeface, name, branchname, bankname, contactno, sudcode);

        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Select Filter Type for search.....");
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                isfiltered = false;
                searchText.setText("");
                if (checkedId == R.id.name) {
                    searchby = "name";
                    searchText.setHint("Search with Name");
                    alert.cancel();
                } else if (checkedId == R.id.branchname) {
                    searchby = "branchname";
                    searchText.setHint("Search with Branch Name");
                    alert.cancel();
                } else if (checkedId == R.id.bankname) {
                    searchby = "bankname";
                    searchText.setHint("Search with Bank Name");
                    alert.cancel();
                } else if (checkedId == R.id.contactno) {
                    searchby = "contactno";
                    searchText.setHint("Search with Contactno.");
                    alert.cancel();
                } else if (checkedId == R.id.sudcode) {
                    searchby = "sudcode";
                    searchText.setHint("Search with SUD Code");
                    alert.cancel();
                }
                leadModels.clear();
                if (Utils.isNotNullAndNotEmpty(stage)) {
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, stage);
                    showList(leadModels, searchby);
                } else {
                    List<LeadModel> leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
                    showList(leadModels, searchby);
                }
            }
        });
        alert.show();
    }

    public void dateFilter() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LeadsListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_dates_filter, null);

        alertDialog.setView(convertView);
        startdate = (EditText) convertView.findViewById(R.id.strtdate);
        enddate = (EditText) convertView.findViewById(R.id.enddate);
        submit = (Button) convertView.findViewById(R.id.submit);
        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Date Filter");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        startdate.setText(TimeUtils.getFormattedDate(calendar.getTime()));
        sdate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(calendar.getTime()));
        enddate.setText(TimeUtils.getCurrentDate());
        edate = TimeUtils.convertDatetoUnix(TimeUtils.getCurrentDate());

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, -7);
                Date initdate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initdate)
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enddate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(startdate.getText().toString())) {
                    startdate.setError("Date required");
                    startdate.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(enddate.getText().toString())) {
                    enddate.setError("Date required");
                    enddate.requestFocus();
                } else {
                    if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
                        showToast("Startdate should not be greater than Endnddate");
                    } else {
                        leadModels.clear();
                        isfiltered = true;
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate, stage);
                        } else {
                            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                    sdate, edate);
                        }
                        showList(leadModels, searchby);
                        alert.cancel();
                    }
                }
            }
        });
        alert.show();
    }

    @Override
    public void onClickItem(String result) {
        leadModels.clear();
        if (result.matches("[a-zA-Z ]+")) {
            leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListforfilter(userid,
                    result);
        } else {
            String[] dates = result.split(" ");
            if (Utils.isNotNullAndNotEmpty(stage)) {
                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                        dates[0], dates[1], stage);
            } else {
                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                        dates[0], dates[1]);
            }
        }
        ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.GONE);
//        if (leadModels.size() > 0) {
//            rcvLeads.setVisibility(View.VISIBLE);
//            leadsAdapter = new LeadsAdapter(leadModels, LeadsListActivity.this);
//            rcvLeads.setAdapter(leadsAdapter);
//            rcvLeads.setLayoutManager(new LinearLayoutManager(LeadsListActivity.this));
//        } else {
//            ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
//            rcvLeads.setVisibility(View.GONE);
//        }
    }

    private void getServerLeads() {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeads = client.getServerLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), "");
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        getLeads.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<LeadModel>>() {
                        }.getType();
                        List<LeadModel> leadModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(LeadsListActivity.this).insertLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, newFetchTime);
                        SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "loaded");
                        tabLayout.getTabAt(position).select();
                        if (stage.length() > 0) {
                            if (isfiltered) {
                                if (Utils.isNotNullAndNotEmpty(stage)) {
                                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                            sdate, edate, stage);
                                } else {
                                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                            sdate, edate);
                                }
                            } else {
                                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsListonLeadStage(userid, stage);
                            }
                        } else {
                            if (isfiltered) {
                                if (Utils.isNotNullAndNotEmpty(stage)) {
                                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                            sdate, edate, stage);
                                } else {
                                    leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadListbetweendates(userid,
                                            sdate, edate);
                                }
                            } else {
                                leadModels = LeadMgr.getInstance(LeadsListActivity.this).getLeadsList(userid);
                            }
                        }
                        showList(leadModels, searchby);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
            }
        });
    }

}
