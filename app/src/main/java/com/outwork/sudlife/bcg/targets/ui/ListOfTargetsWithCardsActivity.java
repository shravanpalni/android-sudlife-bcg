package com.outwork.sudlife.bcg.targets.ui;

import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.targets.services.TargetsIntentService;
import com.outwork.sudlife.bcg.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.bcg.targets.adapters.BranchesAdapter;
import com.outwork.sudlife.bcg.targets.models.MSBModel;
import com.outwork.sudlife.bcg.targets.services.RestServicesForTragets;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListOfTargetsWithCardsActivity extends BaseActivity {

    RecyclerView rcvListOfBranchInfo;
    int ftm, activitiesPlanned, visitsPlanned, expectedConnections, leadsExpected, expectedBusiness, gapToTarget;
    TextView tvMonth, tvNodata;
    LinearLayout llRcv;
    String monthsArray[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_targets_with_cards);

        if (SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                TargetsIntentService.insertTargetRecords(ListOfTargetsWithCardsActivity.this);
            }
        }
        initToolbar();
        rcvListOfBranchInfo = (RecyclerView) findViewById(R.id.rcv_list_of_targets);
        tvMonth = (TextView) findViewById(R.id.toolbar_title);
        tvNodata = (TextView) findViewById(R.id.tv_nodata);
        llRcv = (LinearLayout) findViewById(R.id.ll_in_rcv);

        long currentmnth = Long.parseLong(TimeUtils.getCurrentDate("MM"));
        tvMonth.setText(getMonth(currentmnth));
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(ListOfTargetsWithCardsActivity.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (msbModelArrayList.size() > 0) {
            llRcv.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }

            MSBModel msbModel = new MSBModel();
            msbModel.setFtm(String.valueOf(ftm));
            msbModel.setActivitiesplanned(String.valueOf(activitiesPlanned));
            msbModel.setVisitsplanned(String.valueOf(visitsPlanned));
            msbModel.setExpectedconncetions(String.valueOf(expectedConnections));
            msbModel.setExpectedleads(String.valueOf(leadsExpected));
            msbModel.setExpectedbusiness(String.valueOf(expectedBusiness));
            msbModel.setGaptotarget(String.valueOf(gapToTarget));
            msbModel.setCustomername("Summary");
            msbModelArrayList.add(msbModel);

            Collections.reverse(msbModelArrayList);
            BranchesAdapter branchesAdapter = new BranchesAdapter(ListOfTargetsWithCardsActivity.this, msbModelArrayList);
            rcvListOfBranchInfo.setAdapter(branchesAdapter);
            rcvListOfBranchInfo.setLayoutManager(new LinearLayoutManager(ListOfTargetsWithCardsActivity.this));
        } else {
            llRcv.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), tvMonth);
//        Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calender_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.calender) {
            monthsCalendarDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mListToolBar);
        tvMonth = (TextView) findViewById(R.id.toolbar_title);
        //    textView.setText("Targets");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    public String getMonth(long month) {
        return new DateFormatSymbols().getMonths()[(int) month - 1];
    }

    public void monthsCalendarDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.list_item_for_month, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_month);
        ArrayAdapter arrayAdapter = new ArrayAdapter(ListOfTargetsWithCardsActivity.this, android.R.layout.simple_list_item_1, monthsArray);
        lvItems.setAdapter(arrayAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvMonth.setText(adapterView.getItemAtPosition(i).toString());
                getListOfTargets(i + 1);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    public void getListOfTargets(int month) {
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTargets(userToken, String.valueOf(month), year);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                Type listType = new TypeToken<ArrayList<MSBModel>>() {
                }.getType();
                if (response.body() != null) {
                    ArrayList<MSBModel> targetsArrayList = new Gson().fromJson(response.body().getData(), listType);
                    if (targetsArrayList != null) {
                        llRcv.setVisibility(View.VISIBLE);
                        tvNodata.setVisibility(View.GONE);
                        if (targetsArrayList.size() > 0) {
                            int ftm = 0, activitiesPlanned = 0, visitsPlanned = 0, expectedConnections = 0, leadsExpected = 0,
                                    expectedBusiness = 0, gapToTarget = 0;
                            for (int i = 0; i < targetsArrayList.size(); i++) {
                                ftm += Integer.parseInt(targetsArrayList.get(i).getFtm());
                                activitiesPlanned += Integer.parseInt(targetsArrayList.get(i).getActivitiesplanned());
                                visitsPlanned += Integer.parseInt(targetsArrayList.get(i).getVisitsplanned());
                                expectedConnections += Integer.parseInt(targetsArrayList.get(i).getExpectedconncetions());
                                leadsExpected += Integer.parseInt(targetsArrayList.get(i).getExpectedleads());
                                expectedBusiness += Integer.parseInt(targetsArrayList.get(i).getExpectedbusiness());
                                gapToTarget += Integer.parseInt(targetsArrayList.get(i).getFtm()) - Integer.parseInt(targetsArrayList.get(i).getExpectedbusiness());
                            }
                            MSBModel msbModel = new MSBModel();
                            msbModel.setFtm(String.valueOf(ftm));
                            msbModel.setActivitiesplanned(String.valueOf(activitiesPlanned));
                            msbModel.setVisitsplanned(String.valueOf(visitsPlanned));
                            msbModel.setExpectedconncetions(String.valueOf(expectedConnections));
                            msbModel.setExpectedleads(String.valueOf(leadsExpected));
                            msbModel.setExpectedbusiness(String.valueOf(expectedBusiness));
                            msbModel.setGaptotarget(String.valueOf(gapToTarget));
                            msbModel.setCustomername("Summary");
                            targetsArrayList.add(msbModel);
                            Collections.reverse(targetsArrayList);
                            BranchesAdapter branchesAdapter = new BranchesAdapter(ListOfTargetsWithCardsActivity.this, targetsArrayList);
                            rcvListOfBranchInfo.setAdapter(branchesAdapter);
                            rcvListOfBranchInfo.setLayoutManager(new LinearLayoutManager(ListOfTargetsWithCardsActivity.this));
                        }
                    } else {
                        llRcv.setVisibility(View.GONE);
                        tvNodata.setVisibility(View.VISIBLE);
                    }
                }
            }


            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Toast.makeText(ListOfTargetsWithCardsActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        int ftm = 0, activitiesPlanned = 0, visitsPlanned = 0, expectedConnections = 0, leadsExpected = 0,
                expectedBusiness = 0, gapToTarget = 0;
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(ListOfTargetsWithCardsActivity.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (msbModelArrayList.size() > 0) {
            llRcv.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            MSBModel msbModel = new MSBModel();
            msbModel.setFtm(String.valueOf(ftm));
            msbModel.setActivitiesplanned(String.valueOf(activitiesPlanned));
            msbModel.setVisitsplanned(String.valueOf(visitsPlanned));
            msbModel.setExpectedconncetions(String.valueOf(expectedConnections));
            msbModel.setExpectedleads(String.valueOf(leadsExpected));
            msbModel.setExpectedbusiness(String.valueOf(expectedBusiness));
            msbModel.setGaptotarget(String.valueOf(gapToTarget));
            msbModel.setCustomername("Summary");
            msbModelArrayList.add(msbModel);
            Collections.reverse(msbModelArrayList);
            BranchesAdapter branchesAdapter = new BranchesAdapter(ListOfTargetsWithCardsActivity.this, msbModelArrayList);
            rcvListOfBranchInfo.setAdapter(branchesAdapter);
            rcvListOfBranchInfo.setLayoutManager(new LinearLayoutManager(ListOfTargetsWithCardsActivity.this));
        } else {
            llRcv.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
    }
}
