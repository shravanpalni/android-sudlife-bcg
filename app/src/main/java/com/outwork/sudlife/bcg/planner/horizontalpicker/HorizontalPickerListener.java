package com.outwork.sudlife.bcg.planner.horizontalpicker;

public interface HorizontalPickerListener {
    void onStopDraggingPicker();
    void onDraggingPicker();
    void onDateSelected(Day item);
}