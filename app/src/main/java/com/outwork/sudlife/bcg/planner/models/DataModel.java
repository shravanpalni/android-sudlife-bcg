package com.outwork.sudlife.bcg.planner.models;

public class DataModel {

    String name;

    public boolean isEnabledValue() {
        return enabledValue;
    }

    public void setEnabledValue(boolean enabledValue) {
        this.enabledValue = enabledValue;
    }

    boolean enabledValue = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private String colorName;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    private String branchName;
}