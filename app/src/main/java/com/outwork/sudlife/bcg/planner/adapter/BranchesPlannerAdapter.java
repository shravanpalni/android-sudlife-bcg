package com.outwork.sudlife.bcg.planner.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.gson.Gson;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.lead.LeadMgr;
import com.outwork.sudlife.bcg.lead.activities.ViewActivityLeadActivity;
import com.outwork.sudlife.bcg.lead.activities.ViewLeadActivity;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.planner.activities.OpportunityViewActivity;
import com.outwork.sudlife.bcg.planner.activities.ViewActivity;
import com.outwork.sudlife.bcg.planner.activities.ViewplannedActivity;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */
public class BranchesPlannerAdapter extends RecyclerView.Adapter<BranchesPlannerAdapter.TaskViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PlannerModel> plannerModelList = new ArrayList<>();

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        private CardView card_view;
        public TextView line2, status,offline;
        //View offline;

        public TaskViewHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.profileImage);
            name = (TextView) v.findViewById(R.id.contactname);
            line2 = (TextView) v.findViewById(R.id.contactline2);
            status = (TextView) v.findViewById(R.id.status);
            card_view = (CardView) v.findViewById(R.id.card_view);
            offline = (TextView) v.findViewById(R.id.networkstatus);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, name);
        }
    }

    public BranchesPlannerAdapter(Context context, List<PlannerModel> plannerModelList) {
        this.context = context;
        this.plannerModelList = plannerModelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plan, parent, false);
        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, final int position) {
        final PlannerModel plannerModel = (PlannerModel) this.plannerModelList.get(position);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
            holder.offline.setText("Offline");
            holder.offline.setVisibility(View.VISIBLE);
        } else {
            holder.offline.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0)) {
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            String lead = "Lead";
            // generate color based on a key (same key returns the same color), useful for list/grid views
            int color2 = generator.getColor(lead.substring(0, 1));
            TextDrawable drawable = TextDrawable.builder().buildRound(lead.substring(0, 1), color2);
            if (drawable != null) {
                holder.picture.setImageDrawable(drawable);
            }
        } else if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
                ColorGenerator generator = ColorGenerator.MATERIAL;
                int color2 = generator.getColor(plannerModel.getActivitytype().substring(0, 1));
                TextDrawable drawable = TextDrawable.builder().buildRound(plannerModel.getActivitytype().substring(0, 1), color2);
                if (drawable != null) {
                    holder.picture.setImageDrawable(drawable);
                }
            }
        } else if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            String lead = "Activity";
            // generate color based on a key (same key returns the same color), useful for list/grid views
            int color2 = generator.getColor(lead.substring(0, 1));
            TextDrawable drawable = TextDrawable.builder().buildRound(lead.substring(0, 1), color2);
            if (drawable != null) {
                holder.picture.setImageDrawable(drawable);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
            StringBuilder stringBuilder = new StringBuilder();
            LeadModel leadModel = LeadMgr.getInstance(context).getLeadbyId(plannerModel.getLeadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
            if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
                stringBuilder.append(leadModel.getFirstname());
            }
            if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
                if (Utils.isNotNullAndNotEmpty(stringBuilder.toString())) {
                    stringBuilder.append(" " + leadModel.getLastname());
                } else {
                    stringBuilder.append(leadModel.getLastname());
                }
            }
            if (leadModel.getAge() != null) {
                stringBuilder.append("/" + leadModel.getAge());
            }
            holder.name.setText(stringBuilder.toString());
        } else if (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            LeadModel leadModel = LeadMgr.getInstance(context).getLeadbylocalId(plannerModel.getLocalleadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
            if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
                stringBuilder.append(leadModel.getFirstname());
            }
            if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
                if (Utils.isNotNullAndNotEmpty(stringBuilder.toString())) {
                    stringBuilder.append(" " + leadModel.getLastname());
                } else {
                    stringBuilder.append(leadModel.getLastname());
                }
            }
            if (leadModel.getAge() != null) {
                stringBuilder.append("/" + leadModel.getAge());
            }
            holder.name.setText(stringBuilder.toString());
        } else if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (plannerModel.getActivitytype().equalsIgnoreCase("visit")) {
                holder.name.setText(plannerModel.getActivitytype());
            } else if (plannerModel.getActivitytype().equalsIgnoreCase("activity")) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
                    holder.name.setText(plannerModel.getSubtype());
                } else {
                    holder.name.setText(plannerModel.getActivitytype());
                }
            } else {
                holder.name.setText(plannerModel.getActivitytype());
            }
        } else if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
            holder.name.setText(plannerModel.getSubtype());
        }
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() != 0) {
                holder.line2.setText((TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "EEE dd MMM yyyy")));
            } else {
                holder.line2.setVisibility(View.GONE);
            }
        } else {
            holder.line2.setVisibility(View.GONE);
        }
        if (plannerModel.getCompletestatus() != null) {
            if (plannerModel.getCompletestatus() == 0) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0)) {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_orange_400));
                    holder.status.setText("Missed");
                } else {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_light_green_700));
                    holder.status.setText("Not Started");
                }
            }
            if (plannerModel.getCompletestatus() == 1) {
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_indigo_800));
                holder.status.setText("Checked-In");
            }
            if (plannerModel.getCompletestatus() == 2) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0)) {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_brown_400));
                    holder.status.setText("Attended");
                } else {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_deep_orange_700));
                    holder.status.setText("Checked-Out");
                }
            }
            if (plannerModel.getCompletestatus() == 3) {
                holder.status.setVisibility(View.VISIBLE);
                holder.status.setTextColor(ContextCompat.getColor(context, R.color.material_light_yellow_800));
                holder.status.setText("Reschedule");
            }
        } else {
            holder.status.setVisibility(View.GONE);
        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getOpportunityid()) || (plannerModel.getLocalopportunityid() != null && plannerModel.getLocalopportunityid() > 0)) {
                    Intent intent = new Intent(context, OpportunityViewActivity.class);
                    intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                    context.startActivity(intent);
                } else if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() > 0)) {
                    if (plannerModel.getCompletestatus() == 0) {
                        Intent intent = new Intent(context, ViewActivityLeadActivity.class);
                        intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                        context.startActivity(intent);
                    }
                    if (plannerModel.getCompletestatus() == 2) {
                        LeadModel leadModel = new LeadModel();
                        if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                            leadModel = LeadMgr.getInstance(context).getLeadbyId(plannerModel.getLeadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        } else if (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0) {
                            leadModel = LeadMgr.getInstance(context).getLeadbylocalId(plannerModel.getLocalleadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        Intent intent = new Intent(context, ViewLeadActivity.class);
                        intent.putExtra("leadobj", new Gson().toJson(leadModel));
                        context.startActivity(intent);
                    }
                } else {
                    if (plannerModel.getCompletestatus() == 0 || plannerModel.getCompletestatus() == 1) {
                        if (plannerModel.getScheduletime() != null && plannerModel.getScheduletime() != 0)
                            if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                                    .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                                Intent intent = new Intent(context, ViewActivity.class);
                                intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                                context.startActivity(intent);
                            } else {
                                if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                                    Intent intent = new Intent(context, ViewplannedActivity.class);
                                    intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                                    context.startActivity(intent);
                                }
                            }
                    } else {
                        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                            Intent intent = new Intent(context, ViewplannedActivity.class);
                            intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                            context.startActivity(intent);
                        }
                    }
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return plannerModelList.size();
    }
}