package com.outwork.sudlife.bcg.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Postedby {
    @SerializedName("useremail")
    @Expose
    private String useremail;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("username")
    @Expose
    private String username;

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}