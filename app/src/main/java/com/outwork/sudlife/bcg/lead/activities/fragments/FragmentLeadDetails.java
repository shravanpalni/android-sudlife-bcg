package com.outwork.sudlife.bcg.lead.activities.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.lead.model.LeadModel;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

/**
 * Created by bvlbh on 4/17/2018.
 */
public class FragmentLeadDetails extends Fragment {
    TextView tvfName, tvlName, etLeadStatus, tvContactNumber, tvEmailid, tvAltContactno, tvAge, etExistingCustomer, etPremiumPaid, etProductName, etProductNameOther, tvaddress,
            etCity, etPincode, tvOccupation, etSourceOfLead, etOther, etBank, etBranchCode, familymembers, etBranchName, etZccSupportRequried, etPreferredLang,
            etPreferredDate, tvNotes, etStatus, tvEducation, etSubStatus, tvIncome, etPremiumExpected, etNextFollowupDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, tvMarriedStatus, tvProduct1, tvProduct2;
    LeadModel leadModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lead_details, null);
        if (getArguments() != null) {
            leadModel = new Gson().fromJson(getArguments().getString("leadobj"), LeadModel.class);
        }
        initilizeViews(view);
        if (leadModel != null)
            populateData(leadModel);
        return view;
    }

    public void initilizeViews(View view) {
        tvfName = (TextView) view.findViewById(R.id.tv_fname);
        tvlName = (TextView) view.findViewById(R.id.tv_lname);
        tvContactNumber = (TextView) view.findViewById(R.id.tv_contact_number);
        tvEmailid = (TextView) view.findViewById(R.id.tv_email_id);
        tvAltContactno = (TextView) view.findViewById(R.id.tv_alt_contactno);
        tvAge = (TextView) view.findViewById(R.id.tv_age);
        etExistingCustomer = (TextView) view.findViewById(R.id.tv_existing_sud_ife_customer);
        tvMarriedStatus = (TextView) view.findViewById(R.id.tv_married_status);
        etPremiumPaid = (TextView) view.findViewById(R.id.tv_premium_paid);
        etProductName = (TextView) view.findViewById(R.id.tv_product_name);
        tvProduct1 = (TextView) view.findViewById(R.id.tv_product_one);
        tvProduct2 = (TextView) view.findViewById(R.id.tv_product_two);
        etProductNameOther = (TextView) view.findViewById(R.id.tv_other_product_name);
        tvaddress = (TextView) view.findViewById(R.id.tv_address);
        etCity = (TextView) view.findViewById(R.id.tv_city);
        etPincode = (TextView) view.findViewById(R.id.tv_pincode);
        tvOccupation = (TextView) view.findViewById(R.id.tv_occupation);
        etSourceOfLead = (TextView) view.findViewById(R.id.tv_source_of_lead);
        etOther = (TextView) view.findViewById(R.id.tv_other);
        etBank = (TextView) view.findViewById(R.id.tv_bank);
        etBranchCode = (TextView) view.findViewById(R.id.tv_branch_code);
        familymembers = (TextView) view.findViewById(R.id.tv_no_of_family_members);
        etBranchName = (TextView) view.findViewById(R.id.tv_branch_name);
        etZccSupportRequried = (TextView) view.findViewById(R.id.tv_zcc_support_requried);
        etPreferredLang = (TextView) view.findViewById(R.id.tv_preferred_language);
        etPreferredDate = (TextView) view.findViewById(R.id.tv_preferred_Date);
        etLeadStatus = (TextView) view.findViewById(R.id.tv_lead_status);
        etStatus = (TextView) view.findViewById(R.id.tv_metting_status);
        etSubStatus = (TextView) view.findViewById(R.id.tv_sub_status);
        etPremiumExpected = (TextView) view.findViewById(R.id.tv_expected_actpremium);
        etNextFollowupDate = (TextView) view.findViewById(R.id.tv_next_followup_date);
        etConversionPropensity = (TextView) view.findViewById(R.id.tv_conversion_propensity);
        etProposalNumber = (TextView) view.findViewById(R.id.tv_proposal_no);
        etLeadCreatedDate = (TextView) view.findViewById(R.id.tv_lead_created_date);
        tvEducation = (TextView) view.findViewById(R.id.tv_education);
        tvIncome = (TextView) view.findViewById(R.id.tv_income);
        tvNotes = (TextView) view.findViewById(R.id.tv_notes);
    }

    public void populateData(LeadModel leadModel) {
        if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
            tvfName.setText(leadModel.getFirstname());
        } else {
            tvfName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
            tvlName.setText(leadModel.getLastname());
        } else {
            tvlName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getContactno())) {
            tvContactNumber.setText(leadModel.getContactno());
        } else {
            tvContactNumber.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getEmail())) {
            tvEmailid.setText(leadModel.getEmail());
        } else {
            tvEmailid.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAlternatecontactno())) {
            tvAltContactno.setText(leadModel.getAlternatecontactno());
        } else {
            tvAltContactno.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getAge() != null) {
            tvAge.setText(String.valueOf(leadModel.getAge()));
        } else {
            tvAge.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getIsexistingcustomer() != null) {
            if (leadModel.getIsexistingcustomer()) {
                etExistingCustomer.setText("Yes");
            } else {
                etExistingCustomer.setText("No");
            }
        } else {
            etExistingCustomer.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getPremiumpaid() != null) {
            etPremiumPaid.setText(String.valueOf(leadModel.getPremiumpaid()));
        } else {
            etPremiumPaid.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProductname())) {
            etProductName.setText(leadModel.getProductname());
        } else {
            etProductName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1())) {
            tvProduct1.setText(leadModel.getProduct1());
        } else {
            tvProduct1.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct2())) {
            tvProduct2.setText(leadModel.getProduct2());
        } else {
            tvProduct2.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAddress())) {
            tvaddress.setText(leadModel.getAddress());
        } else {
            tvaddress.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCity())) {
            etCity.setText(leadModel.getCity());
        } else {
            etCity.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPincode())) {
            etPincode.setText(leadModel.getPincode());
        } else {
            etPincode.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOccupation())) {
            tvOccupation.setText(leadModel.getOccupation());
        } else {
            tvOccupation.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadsource())) {
            etSourceOfLead.setText(leadModel.getLeadsource());
        } else {
            etSourceOfLead.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOtherleadsource())) {
            etOther.setText(leadModel.getOtherleadsource());
        } else {
            etOther.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBank())) {
            etBank.setText(leadModel.getBank());
        } else {
            etBank.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchcode())) {
            etBranchCode.setText(leadModel.getBankbranchcode());
        } else {
            etBranchCode.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getFamilymemberscount() != null) {
            familymembers.setText(String.valueOf(leadModel.getFamilymemberscount()));
        } else {
            familymembers.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchname())) {
            etBranchName.setText(leadModel.getBankbranchname());
        } else {
            etBranchName.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getZccsupportrequired() != null) {
            if (leadModel.getZccsupportrequired()) {
                etZccSupportRequried.setText("Yes");
            } else {
                etZccSupportRequried.setText("No");
            }
        } else {
            etZccSupportRequried.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getIsmarried() != null) {
            if (leadModel.getIsmarried()) {
                tvMarriedStatus.setText("Yes");
            } else {
                tvMarriedStatus.setText("No");
            }
        } else {
            etZccSupportRequried.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getPreferreddatetime() != null) {
            if (leadModel.getPreferreddatetime() == 0) {
                etPreferredDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getPreferreddatetime()))) {
                etPreferredDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getPreferreddatetime()), "dd/MM/yyyy hh:mm a"));
            } else {
                etPreferredDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etPreferredDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPreferredlanguage())) {
            etPreferredLang.setText(leadModel.getPreferredlanguage());
        } else {
            etPreferredLang.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getStatus())) {
            etStatus.setText(leadModel.getStatus());
        } else {
            etStatus.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getSubstatus())) {
            etSubStatus.setText(leadModel.getSubstatus());
        } else {
            etSubStatus.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getExpectedpremium())) {
            etPremiumExpected.setText(leadModel.getExpectedpremium());
        } else {
            etPremiumExpected.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getNextfollowupdate() != null) {
            if (leadModel.getNextfollowupdate() == 0) {
                etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getNextfollowupdate()))) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getNextfollowupdate()), "dd/MM/yyyy hh:mm a"));
            } else {
                etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getConversionpropensity())) {
            etConversionPropensity.setText(leadModel.getConversionpropensity());
        } else {
            etConversionPropensity.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProposalnumber())) {
            etProposalNumber.setText(String.valueOf(leadModel.getProposalnumber()));
        } else {
            etProposalNumber.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getLeadcreatedon() != null) {
            if (leadModel.getLeadcreatedon() == 0) {
                etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getLeadcreatedon()))) {
                etLeadCreatedDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getLeadcreatedon()), "dd/MM/yyyy hh:mm a"));
            } else {
                etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getEducation())) {
            tvEducation.setText(leadModel.getEducation());
        } else {
            tvEducation.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getIncomeband())) {
            tvIncome.setText(leadModel.getIncomeband());
        } else {
            tvIncome.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
            tvNotes.setText(leadModel.getRemarks());
        } else {
            tvNotes.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage())) {
            etLeadStatus.setText(leadModel.getLeadstage());
        } else {
            etLeadStatus.setText(getResources().getString(R.string.notAvailable));
        }
    }
}
