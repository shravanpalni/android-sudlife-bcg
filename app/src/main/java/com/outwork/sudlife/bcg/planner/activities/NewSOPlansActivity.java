package com.outwork.sudlife.bcg.planner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.bcg.R;

import com.outwork.sudlife.bcg.planner.adapter.SOPlansAdapter;
import com.outwork.sudlife.bcg.planner.adapter.TeamMembersAdapter;
import com.outwork.sudlife.bcg.planner.models.TeamMemberPlannerModel;
import com.outwork.sudlife.bcg.planner.models.TeamMembers;
import com.outwork.sudlife.bcg.planner.service.PlannerService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.targets.services.RestServicesForTragets;
import com.outwork.sudlife.bcg.ui.base.AppBaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 04-06-2018.
 */

public class NewSOPlansActivity extends AppBaseActivity {

    private TextView toolbar_title, team_task_nomessages, toolbar_month;
    private ImageButton toolbar_calendar_icon;
    private LinearLayout toolbar_calendar_layout;
    String[] listContent = {
            "January", "February", "March", "April",
            "May", "June", "July", "August", "September",
            "October", "November", "December"};
    static final int CUSTOM_DIALOG_ID = 0;
    ListView dialog_ListView;
    private String selectedMonthName = null;
    private CompactCalendarView compactCalendarView;
    private RecyclerView tpRecyclerView;
    private SwipeRefreshLayout tpSwipeRefreshLayout;
    private Spinner dynamicSpinner;
    ArrayList<TeamMembers> teamMembersArrayList = new ArrayList<>();
    private String userId = "";
    private ProgressDialog progressBar;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private SimpleDateFormat dateFormatDisplaying = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private List<TeamMemberPlannerModel> teamMemberplannerMList = new ArrayList<>();
    private SOPlansAdapter soPlansAdapter = null;
    private String clickedDate = null;
    private String clickedMonth = null;
    private String clickedYear = null;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private boolean doubleBackToExitPressedOnce = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsoplans);

        initToolBar();
        compactCalendarView = (CompactCalendarView) findViewById(R.id.soplans_compactcalendar_view);

        team_task_nomessages = (TextView) findViewById(R.id.team_task_nomessages);
        tpRecyclerView = (RecyclerView) findViewById(R.id.team_task_recyclerview);
        tpSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.team_task_swipe_refresh_layout);
        tpSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (clickedMonth == null) {

                    if (isNetworkAvailable()) {

                        getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")), Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));
                    } else {
                        Utils.noNetworkDialog(NewSOPlansActivity.this);
                    }

                } else {
                    if (isNetworkAvailable()) {
                        getTeamPlannerData(userId, clickedMonth, clickedYear);
                    } else {
                        Utils.noNetworkDialog(NewSOPlansActivity.this);
                    }
                }


                tpSwipeRefreshLayout.setRefreshing(false);
            }
        });

        dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);
        if (isNetworkAvailable()) {
            getTeamMembers();
        } else {
            Utils.noNetworkDialog(NewSOPlansActivity.this);
        }
        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                userId = teamMembersArrayList.get(position).getUserid();
                Log.i("NewSOPlansActivity", "userId = = " + userId);

                if (clickedMonth == null) {
                    if (isNetworkAvailable()) {

                        getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")), Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));

                    } else {

                        Utils.noNetworkDialog(NewSOPlansActivity.this);

                    }
                } else {

                    if (isNetworkAvailable()) {

                        getTeamPlannerData(userId, clickedMonth, clickedYear);

                    } else {

                        Utils.noNetworkDialog(NewSOPlansActivity.this);

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                Log.i("NewSOPlansActivity", "onDayClick = = " + dateClicked);
                loadEvents();

                String date = (dateFormat.format(dateClicked));
                String displaydate = dateFormatDisplaying.format(dateClicked);
                int day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", date));
                int monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", date));
                int year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", date));

                clickedDate = String.valueOf(day);
                clickedMonth = String.valueOf(monthno);
                clickedYear = String.valueOf(year);


                if (isNetworkAvailable()) {
                    getTeamPlannerData(userId, String.valueOf(monthno), String.valueOf(year));
                } else {

                    Utils.noNetworkDialog(NewSOPlansActivity.this);

                }

                Log.i("NewSOPlansActivity", "onDayClick date= = " + date);
                Log.i("NewSOPlansActivity", "onDayClick displaydate= = " + displaydate);
                Log.i("NewSOPlansActivity", "onDayClick day= = " + day);
                Log.i("NewSOPlansActivity", "onDayClick monthno= = " + monthno);
                Log.i("NewSOPlansActivity", "onDayClick year= = " + year);


            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                Log.i("NewSOPlansActivity", "onMonthScroll = = " + firstDayOfNewMonth);

                toolbar_month.setText(dateFormatForMonth.format(firstDayOfNewMonth));

                String mo = (dateFormatForMonth.format(firstDayOfNewMonth));
                Log.i("NewSOPlansActivity", "onMonthScroll mo= = " + mo);

                toolbar_month.setText(mo);

                String displaydate = dateFormatDisplaying.format(firstDayOfNewMonth);
                SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                String[] date = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).split("-");


                String displaymonth = displaydate.substring(3, 5);
                String displayyear = displaydate.substring(6, 10);

                if (isNetworkAvailable()) {
                    getTeamPlannerData(userId, displaymonth, displayyear);
                } else {
                    Utils.noNetworkDialog(NewSOPlansActivity.this);
                }

                Log.i("NewSOPlansActivity", "onMonthScroll displaydate= = " + displaydate);
                Log.i("NewSOPlansActivity", "onMonthScroll displaymonth= = " + displaymonth);
                Log.i("NewSOPlansActivity", "onMonthScroll displayyear= = " + displayyear);
                Log.i("NewSOPlansActivity", "onMonthScroll date= = " + date.toString());

            }
        });


        compactCalendarView.setUseThreeLetterAbbreviation(false);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //loadEvents();
        //loadEventsForYear(2017);
        compactCalendarView.invalidate();
        logEventsByMonth(compactCalendarView);


    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tmtoolbar);
        toolbar_calendar_layout = (LinearLayout) findViewById(R.id.calendar_layout);
        toolbar_title = (TextView) findViewById(R.id.tm_toolbar_title);
        toolbar_title.setText("Team Plans");
        toolbar_month = (TextView) findViewById(R.id.tm_month_display);

        toolbar_calendar_icon = (ImageButton) findViewById(R.id.calender_icon);

        Calendar mCalendar = Calendar.getInstance();
        String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        //String year = mCalendar.getDisplayName(Calendar.YEAR, Calendar.LONG, Locale.getDefault());
        int year = mCalendar.get(Calendar.YEAR);
        String currentYear = String.valueOf(year);
        toolbar_month.setText(month + " - " + currentYear);

        toolbar_calendar_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  showDialog(CUSTOM_DIALOG_ID);

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case CUSTOM_DIALOG_ID:
                dialog = new Dialog(NewSOPlansActivity.this);
                dialog.setContentView(R.layout.dialoglayout);
                dialog.setTitle("Custom Dialog");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });

                //Prepare ListView in dialog
                dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter
                        = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, listContent);
                dialog_ListView.setAdapter(adapter);
                dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                        toolbar_month.setText(parent.getItemAtPosition(position).toString());
                        selectedMonthName = parent.getItemAtPosition(position).toString();

                        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                        Calendar cal = Calendar.getInstance();
                        try {
                            cal.setTime(inputFormat.parse(selectedMonthName));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("NewSOPlansActivity", "monthNumber" + monthNumber);
                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        String currentYear = String.valueOf(year);
                        Log.i("NewSOPlansActivity", "year" + year);

                        //getCalendarMonthDates(monthNumber);
                        //getTeamPlannerData(userId, monthNumber, currentYear);

                        dismissDialog(CUSTOM_DIALOG_ID);

                    }
                });

                break;
        }

        return dialog;
    }


    public void getTeamMembers() {
        showProgresDialog("loading . . .");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful())
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamMembers>>() {
                        }.getType();
                        ArrayList<TeamMembers> tmArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (tmArrayList != null) {
                            if (tmArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                for (int i = 0; i < tmArrayList.size(); i++) {
                                    if (tmArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(tmArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    dynamicSpinner.setVisibility(View.VISIBLE);
                                    TeamMembersAdapter teamMembersAdapter = new TeamMembersAdapter(NewSOPlansActivity.this, teamMembersArrayList);
                                    dynamicSpinner.setAdapter(teamMembersAdapter);
                                }
                            } else {
                                dynamicSpinner.setVisibility(View.GONE);
                                team_task_nomessages.setVisibility(View.VISIBLE);
                            }
                        } else {
                            dynamicSpinner.setVisibility(View.GONE);
                            team_task_nomessages.setVisibility(View.VISIBLE);
                        }
                    }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dissmissProgressDialog();
            }
        });
    }

    private void showProgresDialog(String message) {
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(message);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();
    }

    private void dissmissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }


    private void getTeamPlannerData(String userId, String month, String year) {

        Log.i("NewSOPlansActivity", "" + userid + month + year);
        showProgresDialog("loading . . .");
        compactCalendarView.removeAllEvents();

        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getTeamMemberplans = client.getTeamMemberPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), userId, month, year);

        try {

            getTeamMemberplans.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                    if (response.isSuccessful())

                        if (teamMemberplannerMList.size() > 0) {
                            teamMemberplannerMList.clear();
                        }


                    if (Utils.isNotNullAndNotEmpty(response.body().getData().toString())) {

                        Log.i("NewSOPlansActivity", "++++" + response.body().getData().toString());

                        try {
                            Type listType = new TypeToken<ArrayList<TeamMemberPlannerModel>>() {
                            }.getType();

                            teamMemberplannerMList = new Gson().fromJson(response.body().getData(), listType);
                            Log.i("NewSOPlansActivity", "++++" + teamMemberplannerMList.size());
                            if (teamMemberplannerMList.size() > 0) {
                                team_task_nomessages.setVisibility(View.GONE);


                                loadEvents();

                                Log.i("NewSOPlansActivity", "+++++++++++++");

                                Collections.sort(teamMemberplannerMList, new Comparator<TeamMemberPlannerModel>() {
                                    @Override
                                    public int compare(TeamMemberPlannerModel lhs, TeamMemberPlannerModel rhs) {
                                        //return lhs.getScheduleday().compareTo(rhs.getScheduleday()); // ascending date
                                        return rhs.getScheduleday().compareTo(lhs.getScheduleday()); // descending date
                                    }
                                });


                                //tpRecyclerView.setVisibility(View.VISIBLE);
                                List<TeamMemberPlannerModel> newlist = new ArrayList<>();

                                for (int i = 0; teamMemberplannerMList.size() > i; i++) {
                                    if (clickedDate != null) {
                                        if (clickedDate.equalsIgnoreCase(teamMemberplannerMList.get(i).getScheduleday().toString())) {
                                            newlist.add(teamMemberplannerMList.get(i));
                                        }
                                    } else {

                                        Calendar cal = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                        String date = df.format(cal.getTime());
                                        String subdate = date.substring(8);
                                        if (subdate.substring(0, 1).equalsIgnoreCase("0")) {

                                            clickedDate = subdate.substring(1);
                                        } else {

                                            clickedDate = subdate;
                                        }

                                        if (clickedDate.equalsIgnoreCase(teamMemberplannerMList.get(i).getScheduleday().toString())) {
                                            newlist.add(teamMemberplannerMList.get(i));
                                        }


                                    }


                                }

                                if (newlist.size() > 0) {
                                    tpRecyclerView.setVisibility(View.VISIBLE);
                                    soPlansAdapter = new SOPlansAdapter(NewSOPlansActivity.this, newlist);
                                    tpRecyclerView.setAdapter(soPlansAdapter);
                                    tpRecyclerView.getAdapter().notifyDataSetChanged();
                                    tpRecyclerView.setLayoutManager(new LinearLayoutManager(NewSOPlansActivity.this));
                                    dissmissProgressDialog();
                                } else {

                                    team_task_nomessages.setVisibility(View.VISIBLE);
                                    //tpRecyclerView.getAdapter().notifyDataSetChanged();
                                    tpRecyclerView.setVisibility(View.GONE);
                                    tpRecyclerView.invalidate();
                                    dissmissProgressDialog();

                                }
                            } else {
                                team_task_nomessages.setVisibility(View.VISIBLE);
                                //tpRecyclerView.getAdapter().notifyDataSetChanged();
                                tpRecyclerView.setVisibility(View.GONE);
                                tpRecyclerView.invalidate();
                                dissmissProgressDialog();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        } finally {
                            dissmissProgressDialog();

                        }

                    } else {
                        team_task_nomessages.setVisibility(View.VISIBLE);
                      /*  if (soPlansAdapter!=null) {
                            tpRecyclerView.getAdapter().notifyDataSetChanged();
                        }*/

                        tpRecyclerView.setVisibility(View.GONE);
                        tpRecyclerView.invalidate();
                        dissmissProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dissmissProgressDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        //   Log.d(TAG, "Events for Aug with simple date formatter: " + dates);
        // Log.d(TAG, "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }


    private void loadEvents() {
        addEvents(-1, -1);
        //addEvents(Calendar.DECEMBER, -1);
        //addEvents(Calendar.AUGUST, -1);
    }

    private void addEvents(int month, int year) {

        compactCalendarView.removeAllEvents();

        Calendar calendar = Calendar.getInstance();
        if (teamMemberplannerMList.size() != 0) {
            for (int i = 0; i < teamMemberplannerMList.size(); i++) {
                calendar.set(teamMemberplannerMList.get(i).getScheduleyear(), teamMemberplannerMList.get(i).getSchedulemonth() - 1, teamMemberplannerMList.get(i).getScheduleday());
                //Event event = new Event(Color.parseColor(plannerModelArrayList.get(i).getColor()), calendar.getTimeInMillis());

                String act = teamMemberplannerMList.get(i).getSubtype();
                Log.i("NewSOPlans", "addEvents getSubtype()" + act);

                List<Event> events = getEvents(calendar.getTimeInMillis(), 1);
                compactCalendarView.addEvents(events);
            }
        }


       /* currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            if (year > -1) {
                currentCalender.set(Calendar.ERA, GregorianCalendar.AD);
                currentCalender.set(Calendar.YEAR, year);
            }
            currentCalender.add(Calendar.DATE, i);
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
        }*/
    }


    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }


}
