package com.outwork.sudlife.bcg.core;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.outwork.sudlife.bcg.restinterfaces.GeneralService;
import com.outwork.sudlife.bcg.restinterfaces.POJO.PermissionSettings;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by benjakuben on 12/17/14.
 */
public class LocationProvider extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    public static final String TAG = LocationProvider.class.getSimpleName();

    public abstract interface LocationCallback {
        public void handleNewLocation(Location location);

    }

    public LocationCallback mLocationCallback;
    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public LocationProvider(Context context, LocationCallback callback) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationCallback = callback;

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(60 * 1000)        // 600 seconds, in milliseconds
                .setFastestInterval(60 * 1000);
        mContext = context;
    }

    public void connect() {
        if (mGoogleApiClient != null)
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
    }

    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission((Activity) mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission((Activity) mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            mLocationCallback.handleNewLocation(location);
        } else {
            updatePermissions();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null){
            mLocationCallback.handleNewLocation(location);
        } else {
            Location location1 = new GPSTracker(mContext).getLocation();
        }*/
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocationCallback.handleNewLocation(location);

    }

    private void updatePermissions() {
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        String groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");

        PermissionSettings permissionSettings = new PermissionSettings();
        permissionSettings.setCoarselocation("0");
        permissionSettings.setFinelocation("0");
        GeneralService client = RestService.createService(GeneralService.class);
        Call<RestResponse> permission = client.updatepermissions(userToken, groupId, permissionSettings);
        permission.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Log.d("", "update permission failed");
            }
        });
    }
}
