package com.outwork.sudlife.bcg.branches;

import android.content.Context;
import android.text.TextUtils;

import java.util.List;

import com.outwork.sudlife.bcg.branches.dao.BranchesDao;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;

/**
 * Created by Panch on 2/23/2017.
 */
public class BranchesMgr {

    private static Context context;
    private static BranchesDao branchesDao;

    public BranchesMgr(Context context) {
        this.context = context;
        branchesDao = branchesDao.getInstance(context);
    }

    private static BranchesMgr instance;

    public static BranchesMgr getInstance(Context context) {
        if (instance == null)
            instance = new BranchesMgr(context);
        return instance;
    }

    public static boolean insertLocalCustomer(BranchesModel branchesModel, String network_status) {
        branchesDao.insertLocalCustomer(branchesModel, network_status);
        return true;
    }

    public boolean insertCustomerList(List<BranchesModel> branchesModelList) {
        branchesDao.insertCustomerList(branchesModelList);
        return true;

    }


    public boolean insertNewCustomerList(List<BranchesModel> branchesModelList) {
        branchesDao.insertNewCustomerList(branchesModelList);
        return true;

    }



    public String getmaxModifiedDate(String groupId, String userId) {
        String modifiedDate = branchesDao.getMaxModifiedDate(groupId, userId);
        if (!TextUtils.isEmpty(modifiedDate)) {
            return modifiedDate;
        } else {
            return "";
        }
    }

    public BranchesModel getCustomerData(String customerId) {
        return branchesDao.getCustomerData(customerId);
    }

    public List<BranchesModel> getCustomerList(String groupId, String userId) {
        return branchesDao.getCustomers(groupId, userId);
    }

    public List<BranchesModel> getonlineCustomerList(String groupId, String userId, String status) {
        return branchesDao.getonlineCustomersList(groupId, userId, status);
    }

    public List<BranchesModel> getCustomerList(String groupId, String userInput, String userId) {
        return branchesDao.getCustomers(groupId, userId, userInput);
    }

    public List<BranchesModel> getonlineCustomers(String groupId, String userInput, String userId, String net_status) {
        return branchesDao.getonlineCustomers(groupId, userInput, userId, net_status);
    }

    public List<String> getCustomerNamesList(String userId) {
        return branchesDao.getCustomerNamesList(userId);
    }

    public List<String> getCustomerTypesList(String userId) {
        return branchesDao.getCustomerTypesList(userId);
    }

    public List<BranchesModel> getCustomersbyStatus(String userId, String network_status) {
        return branchesDao.getCustomersbyStatus(userId, network_status);
    }

    public void updateCustomerOnlline(BranchesModel branchesModel, String userId, String customerId) {
        branchesDao.updateCustomerOnlline(branchesModel, userId, customerId);
    }

    public void updateCustomerInfo(BranchesModel branchesModel) {
        branchesDao.updateCustomerInfo(branchesModel);
    }
}