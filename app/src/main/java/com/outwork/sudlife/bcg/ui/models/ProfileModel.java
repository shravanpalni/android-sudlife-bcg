package com.outwork.sudlife.bcg.ui.models;

/**
 * Created by Habi on 22-08-2017.
 */

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel {

    @SerializedName("profileid")
    @Expose
    private String profileid;
    @SerializedName("conxnflag")
    @Expose
    private String conxnflag;
    @SerializedName("conxnstatus")
    @Expose
    private String conxnstatus;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("profiletype")
    @Expose
    private String profiletype;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("phonenumber")
    @Expose
    private String phonenumber;
    @SerializedName("countrycode")
    @Expose
    private String countrycode;
    @SerializedName("numbervisibility")
    @Expose
    private String numbervisibility;
    @SerializedName("fburl")
    @Expose
    private String fburl;
    @SerializedName("twitterurl")
    @Expose
    private String twitterurl;
    @SerializedName("url1")
    @Expose
    private String url1;
    @SerializedName("url2")
    @Expose
    private String url2;
    @SerializedName("url3")
    @Expose
    private String url3;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;
    @SerializedName("lastlogindate")
    @Expose
    private String lastlogindate;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("emailvisibility")
    @Expose
    private String emailvisibility;
    @SerializedName("linkedinvisibility")
    @Expose
    private String linkedinvisibility;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("fbvisibility")
    @Expose
    private String fbvisibility;
    @SerializedName("twittervisibility")
    @Expose
    private String twittervisibility;
    @SerializedName("orgvisibility")
    @Expose
    private String orgvisibility;
    @SerializedName("designationvisibility")
    @Expose
    private String designationvisibility;
    @SerializedName("locationvisibility")
    @Expose
    private String locationvisibility;
    @SerializedName("websitevisibility")
    @Expose
    private String websitevisibility;
    @SerializedName("bloxid")
    @Expose
    private Object bloxid;
    @SerializedName("objecttype")
    @Expose
    private Object objecttype;
    @SerializedName("connectionobject")
    @Expose
    private List<Object> connectionobject = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("currentlocation")
    @Expose
    private String currentlocation;
    @SerializedName("details")
    @Expose
    private Details details;
    @SerializedName("workexperience")
    @Expose
    private String workexperience;
    @SerializedName("channelid")
    @Expose
    private Object channelid;
    @SerializedName("lastcallreportdate")
    @Expose
    private Object lastcallreportdate;
    @SerializedName("zone")
    @Expose
    private Object zone;

    public String getProfileid() {
        return profileid;
    }

    public void setProfileid(String profileid) {
        this.profileid = profileid;
    }

    public String getConxnflag() {
        return conxnflag;
    }

    public void setConxnflag(String conxnflag) {
        this.conxnflag = conxnflag;
    }

    public String getConxnstatus() {
        return conxnstatus;
    }

    public void setConxnstatus(String conxnstatus) {
        this.conxnstatus = conxnstatus;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProfiletype() {
        return profiletype;
    }

    public void setProfiletype(String profiletype) {
        this.profiletype = profiletype;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getNumbervisibility() {
        return numbervisibility;
    }

    public void setNumbervisibility(String numbervisibility) {
        this.numbervisibility = numbervisibility;
    }

    public String getFburl() {
        return fburl;
    }

    public void setFburl(String fburl) {
        this.fburl = fburl;
    }

    public String getTwitterurl() {
        return twitterurl;
    }

    public void setTwitterurl(String twitterurl) {
        this.twitterurl = twitterurl;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(String lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmailvisibility() {
        return emailvisibility;
    }

    public void setEmailvisibility(String emailvisibility) {
        this.emailvisibility = emailvisibility;
    }

    public String getLinkedinvisibility() {
        return linkedinvisibility;
    }

    public void setLinkedinvisibility(String linkedinvisibility) {
        this.linkedinvisibility = linkedinvisibility;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getFbvisibility() {
        return fbvisibility;
    }

    public void setFbvisibility(String fbvisibility) {
        this.fbvisibility = fbvisibility;
    }

    public String getTwittervisibility() {
        return twittervisibility;
    }

    public void setTwittervisibility(String twittervisibility) {
        this.twittervisibility = twittervisibility;
    }

    public String getOrgvisibility() {
        return orgvisibility;
    }

    public void setOrgvisibility(String orgvisibility) {
        this.orgvisibility = orgvisibility;
    }

    public String getDesignationvisibility() {
        return designationvisibility;
    }

    public void setDesignationvisibility(String designationvisibility) {
        this.designationvisibility = designationvisibility;
    }

    public String getLocationvisibility() {
        return locationvisibility;
    }

    public void setLocationvisibility(String locationvisibility) {
        this.locationvisibility = locationvisibility;
    }

    public String getWebsitevisibility() {
        return websitevisibility;
    }

    public void setWebsitevisibility(String websitevisibility) {
        this.websitevisibility = websitevisibility;
    }

    public Object getBloxid() {
        return bloxid;
    }

    public void setBloxid(Object bloxid) {
        this.bloxid = bloxid;
    }

    public Object getObjecttype() {
        return objecttype;
    }

    public void setObjecttype(Object objecttype) {
        this.objecttype = objecttype;
    }

    public List<Object> getConnectionobject() {
        return connectionobject;
    }

    public void setConnectionobject(List<Object> connectionobject) {
        this.connectionobject = connectionobject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCurrentlocation() {
        return currentlocation;
    }

    public void setCurrentlocation(String currentlocation) {
        this.currentlocation = currentlocation;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public String getWorkexperience() {
        return workexperience;
    }

    public void setWorkexperience(String workexperience) {
        this.workexperience = workexperience;
    }

    public Object getChannelid() {
        return channelid;
    }

    public void setChannelid(Object channelid) {
        this.channelid = channelid;
    }

    public Object getLastcallreportdate() {
        return lastcallreportdate;
    }

    public void setLastcallreportdate(Object lastcallreportdate) {
        this.lastcallreportdate = lastcallreportdate;
    }

    public Object getZone() {
        return zone;
    }

    public void setZone(Object zone) {
        this.zone = zone;
    }

    public class Details {

        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("month")
        @Expose
        private String month;
        @SerializedName("batch")
        @Expose
        private String batch;
        @SerializedName("course")
        @Expose
        private String course;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getBatch() {
            return batch;
        }

        public void setBatch(String batch) {
            this.batch = batch;
        }

        public String getCourse() {
            return course;
        }

        public void setCourse(String course) {
            this.course = course;
        }

    }
}