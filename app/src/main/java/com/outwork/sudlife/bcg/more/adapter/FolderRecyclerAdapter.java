package com.outwork.sudlife.bcg.more.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.more.models.FileModel;
import com.outwork.sudlife.bcg.more.models.ResourceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 11/25/2016.
 */

public class FolderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ResourceModel> resourceList = new ArrayList<ResourceModel>();
    private String description;
    private static final int VIEW_FILE = 0;
    private static final int VIEW_FOLDER = 1;

    @Override
    public int getItemViewType(int position) {
        ResourceModel dto = (ResourceModel) this.resourceList.get(position);

        if (dto.getType().equalsIgnoreCase("file")) {
            return VIEW_FILE;
        } else {
            return VIEW_FOLDER;
        }
    }

    public FolderRecyclerAdapter(Context mContext, List<ResourceModel> listresourceitems) {
        this.context = mContext;
        this.resourceList = listresourceitems;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        switch (viewType) {
            case VIEW_FOLDER:
                View v1 = inflater.inflate(R.layout.list_resource_items, parent, false);
                viewHolder = new FolderRecyclerAdapter.FolderViewHolder(v1);
                break;
            case VIEW_FILE:
                View v2 = inflater.inflate(R.layout.list_resource_items, parent, false);
                viewHolder = new FolderRecyclerAdapter.FolderViewHolder(v2);
                break;
            default:
                View v3 = inflater.inflate(R.layout.list_resource_items, parent, false);
                viewHolder = new FolderRecyclerAdapter.FolderViewHolder(v3);
                break;
        }
        return viewHolder;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ResourceModel dto = (ResourceModel) this.resourceList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_FOLDER:
                FolderRecyclerAdapter.FolderViewHolder viewHolder = (FolderRecyclerAdapter.FolderViewHolder) holder;
                configureOrdinaryViewHolder(viewHolder, dto);
                break;
            case VIEW_FILE:
                FolderRecyclerAdapter.FolderViewHolder viewHolder2 = (FolderRecyclerAdapter.FolderViewHolder) holder;
                configureFileViewHolder(viewHolder2, dto);
                break;
            default:
                FolderRecyclerAdapter.FolderViewHolder viewHolder1 = (FolderRecyclerAdapter.FolderViewHolder) holder;
                configureOrdinaryViewHolder(viewHolder1, dto);
                break;
        }
    }


    @Override
    public int getItemCount() {
        if (resourceList != null) {
            return resourceList.size();
        }
        return 0;
    }

    public void setItems(List<ResourceModel> resourceItems) {
        if (this.resourceList.size() > 0) {
            this.resourceList.clear();
        }
        this.resourceList = resourceItems;
    }

    private void configureOrdinaryViewHolder(FolderRecyclerAdapter.FolderViewHolder holder, ResourceModel dto) {
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate color based on a key (same key returns the same color), useful for list/grid views
        final int color2;
        final String name;
        if (!TextUtils.isEmpty(dto.getTitle())) {
            color2 = generator.getColor(dto.getTitle().substring(0, 1));
            name = dto.getTitle().substring(0, 1).toUpperCase();
        } else {
            color2 = 12343;
            name = "N";
        }

        TextDrawable drawable = TextDrawable.builder().buildRound(name, color2);
        if (drawable != null) {
            holder.contenttype.setImageDrawable(drawable);
        } else {
            holder.contenttype.setImageDrawable(context.getResources().getDrawable(R.mipmap.messagee));
        }
        holder.title.setText(dto.getTitle());
    }


    private void setImageView(String imageUrl, MessageRecyclerAdapter.FeedExtraViewHolder holder) {
        holder.feedImage.setVisibility(View.VISIBLE);
        //holder.feedImage.setAspectRatio(1.78);
       /* Glide.with(context)
                .load(imageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(new ColorDrawable(ContextCompat.getColor(context, R.color.text_color_light_grey)))
                .into(holder.feedImage);*/
    }

    private void configureFileViewHolder(FolderRecyclerAdapter.FolderViewHolder holder, ResourceModel dto) {
        FileModel fileDto = dto.getFileobject();
        if (fileDto == null) {
            return;
        }
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate color based on a key (same key returns the same color), useful for list/grid views
        final int color2;
        final String name;
        if (!TextUtils.isEmpty(fileDto.getFileName())) {
            color2 = generator.getColor(fileDto.getFileName().substring(0, 1));
            name = fileDto.getFileName().substring(0, 1).toUpperCase();
        } else {
            color2 = 12343;
            name = "N";
        }

        TextDrawable drawable = TextDrawable.builder().buildRound(name, color2);
        if (drawable != null) {
            holder.contenttype.setImageDrawable(drawable);
        } else {
            holder.contenttype.setImageDrawable(context.getResources().getDrawable(R.mipmap.messagee));
        }
        holder.title.setText(dto.getTitle());
    }


    public static class FolderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView contenttype;
        View resouceView;

        public FolderViewHolder(View v) {
            super(v);
            contenttype = (ImageView) v.findViewById(R.id.resourceImage);
            title = (TextView) v.findViewById(R.id.resourceTitle);
            resouceView = v.findViewById(R.id.resource_view);
        }

        @Override
        public void onClick(View v) {
        }
    }
}