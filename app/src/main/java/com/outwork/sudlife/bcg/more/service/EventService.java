package com.outwork.sudlife.bcg.more.service;

import com.outwork.sudlife.bcg.more.models.RSVP;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 3/21/2016.
 */
public interface EventService {

    @GET("EventService/events")
    Call<RestResponse> getEvents(@Header("utoken") String userToken, @Query("groupid") String groupid, @Query("direction") String direction);


    @POST("EventService/event/{eventid}/rsvp")
    Call<RestResponse> submitrsvp(@Header("utoken") String userToken, @Path("eventid") String eventid, @Query("response") String response, @Body RSVP sJsonBody);

    @GET("EventService/event/{eventid}/rsvp/status")
    Call<RestResponse> getRSVPStatus(@Header("utoken") String userToken, @Path("eventid") String eventid);

    @GET("EventService/event/{eventid}")
    Call<RestResponse> getEvent(@Header("utoken") String userToken, @Path("eventid") String eventid);


}