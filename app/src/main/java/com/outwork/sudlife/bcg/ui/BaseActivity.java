package com.outwork.sudlife.bcg.ui;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.outwork.sudlife.bcg.core.LocationProvider;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;

public class BaseActivity extends AppCompatActivity implements LocationProvider.LocationCallback{

    private static final String TAG = BaseActivity.class.getSimpleName();
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    public static final int LOCATION_FETCH_TIME = 6000;
    public LocationManager locationManager;
    public static GoogleApiClient mGoogleApiClient;
   // public static double latitude, longitude;
    public static final int REQUEST_PERMISSION_PICK_PHOTO = 12;
    private String mCurrentPhotoPath;
    private InputMethodManager imm;
    private ProgressDialog dialog = null;
    public LocationProvider mLocationProvider;
    public double latitude, longitude;
    protected String userid, groupId, userToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the InputMethodManager for hiding the keyboard
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");

        // Don't auto-show the keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");

        PendingIntent broadcast = PendingIntent.getBroadcast(BaseActivity.this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 15);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationProvider = new LocationProvider(BaseActivity.this, this);
        mLocationProvider.connect();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mLocationProvider != null)
                    mLocationProvider.connect();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mLocationProvider != null)
                    mLocationProvider.disconnect();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(BaseActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public String getCurrentNetworkTime() {
        String timeSettings = android.provider.Settings.System.getString(this.getContentResolver(), android.provider.Settings.System.AUTO_TIME);
        if (timeSettings.contentEquals("0")) {
            android.provider.Settings.System.putString(this.getContentResolver(), android.provider.Settings.System.AUTO_TIME, "1");
        }
        Date now = new Date(System.currentTimeMillis());
        Log.d("Date", now.toString());
        return now.toString();
    }

    public void updateGPSStatus(String status) {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        int j = days;
        cal.add(Calendar.DAY_OF_YEAR, j);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void dismissProgressDialog() {
        try {
            if (dialog.isShowing()) dialog.dismiss();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public String getCurrentTopActivity() {
        ActivityManager mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> RunningTask = mActivityManager.getRunningTasks(1);
        ActivityManager.RunningTaskInfo ar = RunningTask.get(0);
        return ar.topActivity.getClassName().toString();
    }

    public void showProgressDialog(String message) {
        if (dialog == null) dialog = new ProgressDialog(BaseActivity.this);
        dialog.setMessage(message);

        dialog.setCancelable(false);
        try {
            if (!dialog.isShowing()) dialog.show();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    public void showAlert(String title,
                          String alertMsg,
                          String positiveActionName,
                          DialogInterface.OnClickListener positiveAction,
                          String negativeActionName,
                          DialogInterface.OnClickListener negativeAction,
                          boolean dual) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setTitle(title);
        builder.setMessage(alertMsg);
        builder.setCancelable(false);

        if (positiveActionName != null && positiveAction != null) {
            builder.setPositiveButton(positiveActionName, positiveAction);
        } else {

            if (positiveActionName != null) {
                builder.setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            } else {
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
        }

        if (dual) {
            if (negativeActionName != null && negativeAction != null) {
                builder.setNegativeButton(negativeActionName, negativeAction);
            } else {
                if (negativeActionName != null) {
                    builder.setNegativeButton(negativeActionName, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                } else {
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }
            }
        }
        AlertDialog alertDialog = builder.create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    public void showSimpleAlert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setTitle(title).setMessage(message).setPositiveButton("OK", null).show();
    }

    public void hideKeyboard() {
        // Hide views
        imm.hideSoftInputFromWindow(
                getWindow().getDecorView().findViewById(android.R.id.content).getWindowToken(), 0);
    }

    public File createImageFile() {
        String imageFileName = Constants.ME + System.currentTimeMillis();
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/" + Constants.ME);
        if (!storageDir.exists())
            storageDir.mkdir();
        File image = null;
        try {
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            Log.e(TAG, "Error in createImageFile: " + e.getMessage());
        }
        setCurrentPhotoPath(image.getAbsolutePath());
        return image;
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.e("lat long baseactivity ","lat long baseactivity "+latitude +" :::"+ longitude);
            LatLng latLng = new LatLng(latitude, longitude);
            //if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                //getLocation(latitude, longitude);
        }
    }


}
