package com.outwork.sudlife.bcg.localbase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.dao.DBHelper;
import com.outwork.sudlife.bcg.dao.IvokoProvider;
import com.outwork.sudlife.bcg.dao.IvokoProvider.LocalProducts;
import com.outwork.sudlife.bcg.lead.model.CategoryModel;

/**
 * Created by Panch on 2/16/2017.
 */

public class ProductMasterDao {
    public static final String TAG = ProductMasterDao.class.getSimpleName();
    private static ProductMasterDao instance;
    private Context applicationContext;

    public static ProductMasterDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new ProductMasterDao(applicationContext);
        }
        return instance;
    }

    private ProductMasterDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }


    public void insertMasterProduct(ProductMasterDto prodDto, String groupId, SQLiteDatabase db) {
        if (!isRecordExist(db, prodDto.getProductId())) {
            ContentValues values = new ContentValues();
            values.put(LocalProducts.productid, prodDto.getProductId());
            values.put(LocalProducts.productname, prodDto.getProductName());
            values.put(LocalProducts.groupid, groupId);
            values.put(LocalProducts.categoryid, prodDto.getCategory().getCategoryId());
            values.put(LocalProducts.categoryname, prodDto.getCategory().getCategoryName());
            values.put(LocalProducts.groupid, groupId);
            boolean createSuccessful = db.insert(LocalProducts.TABLE, null, values) > 0;
            if (createSuccessful) {
                Log.e(TAG, " created.");
            }
        }
    }

    public void insertMasterProuctList(List<ProductMasterDto> productList, String groupId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        deleteProductList(db, groupId);
        for (int i = 0; i < productList.size(); i++) {
            ProductMasterDto prodDto = (ProductMasterDto) productList.get(i);
            insertMasterProduct(prodDto, groupId, db);
        }
    }

    public List<ProductMasterDto> getProductList(String groupId) {
        List<ProductMasterDto> prodList = new ArrayList<ProductMasterDto>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, LocalProducts.TABLE)) {
            Cursor cursor = null;
            //security

           /* String sql = "";
            sql += "SELECT * FROM " + LocalProducts.TABLE;
            sql += " WHERE " + LocalProducts.groupid + " = '" + groupId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT * FROM " + LocalProducts.TABLE + " WHERE " + LocalProducts.groupid + " = ?"   , new String[]{groupId});


                if (cursor.moveToFirst()) {
                    do {
                        ProductMasterDto dto = new ProductMasterDto();
                        dto.setProductId(cursor.getString(cursor.getColumnIndex(LocalProducts.productid)));
                        dto.setProductName(cursor.getString(cursor.getColumnIndex(LocalProducts.productname)));
                        CategoryModel categoryModel = new CategoryModel();
                        categoryModel.setCategoryId(cursor.getString(cursor.getColumnIndex(LocalProducts.categoryid)));
                        categoryModel.setCategoryName(cursor.getString(cursor.getColumnIndex(LocalProducts.categoryname)));
                        dto.setCategory(categoryModel);
                        prodList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }

            }
        }
        return prodList;
    }

    public String getProductId(String groupId, String productName) {

        String prodId = "";
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, LocalProducts.TABLE)) {
            Cursor cursor = null;
           /* String sql = "";
            sql += "SELECT * FROM " + LocalProducts.TABLE;
            sql += " WHERE " + LocalProducts.groupid + " = '" + groupId + "'";
            sql += " AND " + LocalProducts.productname + " = '" + productName + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT * FROM " + LocalProducts.TABLE + " WHERE " + LocalProducts.groupid + " = ?" + " AND " + LocalProducts.productname + " = ?"   , new String[]{groupId,productName});


                if (cursor.moveToFirst()) {
                    do {

                        prodId = cursor.getString(cursor.getColumnIndex(LocalProducts.productid));
                        return prodId;
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return prodId;
    }

    public List<ProductMasterDto> getProductList(String groupId, String userInput) {
        List<ProductMasterDto> prodList = new ArrayList<ProductMasterDto>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        int status = 0;
        if (isTableExists(db, LocalProducts.TABLE)) {
            Cursor cursor = null;
            // security
           /* String sql = "";
            sql += "SELECT * FROM " + LocalProducts.TABLE;
            sql += " WHERE " + LocalProducts.groupid + " = '" + groupId + "'";
            sql += " AND " + LocalProducts.productname + " LIKE '%" + userInput + "%'";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+LocalProducts.TABLE+" where "+ "groupid=?  and productname like ? ",
                        new String [] {groupId, '%' + userInput + '%'});

                if (cursor.moveToFirst()) {
                    do {
                        ProductMasterDto dto = new ProductMasterDto();
                        dto.setProductId(cursor.getString(cursor.getColumnIndex(LocalProducts.productid)));
                        dto.setProductName(cursor.getString(cursor.getColumnIndex(LocalProducts.productname)));
                        CategoryModel categoryModel = new CategoryModel();
                        categoryModel.setCategoryId(cursor.getString(cursor.getColumnIndex(LocalProducts.categoryid)));
                        categoryModel.setCategoryName(cursor.getString(cursor.getColumnIndex(LocalProducts.categoryname)));
                        dto.setCategory(categoryModel);
                        prodList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return prodList;
    }


    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private boolean isRecordExist(SQLiteDatabase db, String formId) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + LocalProducts.TABLE + " WHERE " + LocalProducts.productid + " = ?", new String[]{formId});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    private void deleteProductList(SQLiteDatabase db, String groupId) {
        db.delete(IvokoProvider.LocalProducts.TABLE, IvokoProvider.LocalProducts.groupid + "=?", new String[]{groupId});
    }
}