package com.outwork.sudlife.bcg.localbase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.R;
/**
 * Created by Panch on 2/23/2017.
 */

public class StaticDataAutocompleteAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;

    private List<StaticDataDto> resultList = new ArrayList<>();
    private static final int MAX_RESULTS = 10;
    private String groupid;
    private String listType;


    static class ViewHolder {
        public TextView name;
        public ImageView picture;
    }

    public StaticDataAutocompleteAdapter(Context context, String groupId, String listtype){
        this.context = context;
        this.groupid=groupId;
        this.listType=listtype;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.resultList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        View view = convertView;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_product_dropdown, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)view.findViewById(R.id.productname);
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        StaticDataDto dto = (StaticDataDto) this.resultList.get(position);

        holder.name.setText(dto.getValue());

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            public String convertResultToString(Object resultValue) {
                return ((StaticDataDto) resultValue).getValue();
            }
            @Override
            protected FilterResults performFiltering(CharSequence userinput) {
                FilterResults filterResults = new FilterResults();
                if (userinput != null) {

                    List<StaticDataDto> staticList = new StaticDataMgr(context).getStaticList(groupid,listType);
                    filterResults.values = staticList;
                    filterResults.count = staticList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence userinput, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<StaticDataDto>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }


}