package com.outwork.sudlife.bcg.opportunity.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.opportunity.models.OpportunityModel;
import com.outwork.sudlife.bcg.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.activities.CreateOpportuntiyPlanActivity;
import com.outwork.sudlife.bcg.planner.adapter.PlannerAdapter;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ViewOpportunityActivity extends BaseActivity {
    private OpportunityModel opportunityModel = new OpportunityModel();
    private TabLayout tabLayout;
    private RecyclerView planRecyclerView;
    private TextView noMembers;
    private PlannerAdapter plannerAdapter;
    private PlannerModel plannerModel = new PlannerModel();
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private View infoView, contactView;
    private Button schedileview;
    private TextView firstname, lastname, age, gender, occupation, income, familymem, remarks, opprtValue, vfclosingdate, vfstage;
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                schedileview.setEnabled(true);
                String scheduledate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(date));
                Intent intent = new Intent(ViewOpportunityActivity.this, CreateOpportuntiyPlanActivity.class);
                intent.putExtra("opportunity", new Gson().toJson(opportunityModel));
                intent.putExtra("date", scheduledate);
                startActivity(intent);
            }
        }

        @Override
        public void onDateTimeCancel() {
            schedileview.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_opprtform);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FORMDTO")) {
            opportunityModel = (OpportunityModel) getIntent().getSerializableExtra("FORMDTO");
        }
        initToolBar();
        initializeViews();
        initValues();
        setListener();
    }

    private void setListener() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equalsIgnoreCase("Details")) {
                    contactView.setVisibility(View.GONE);
                    infoView.setVisibility(View.VISIBLE);
                } else {
                    infoView.setVisibility(View.GONE);
                    contactView.setVisibility(View.VISIBLE);
                    if (Utils.isNotNullAndNotEmpty(opportunityModel.getOpportunityid())) {
                        plannerModelList = PlannerMgr.getInstance(ViewOpportunityActivity.this).getPlannerListByOpportunityID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), opportunityModel.getOpportunityid());
                        if (plannerModelList.size() > 0) {
                            plannerAdapter = new PlannerAdapter(ViewOpportunityActivity.this, plannerModelList, "opp");
                            planRecyclerView.setAdapter(plannerAdapter);
                            planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewOpportunityActivity.this));
                        } else {
                            noMembers.setVisibility(View.VISIBLE);
                        }
                    } else if (opportunityModel.getId() > 0) {
                        plannerModelList = PlannerMgr.getInstance(ViewOpportunityActivity.this).getPlannerListBylocalOpportunityID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), opportunityModel.getId());
                        if (plannerModelList.size() > 0) {
                            plannerAdapter = new PlannerAdapter(ViewOpportunityActivity.this, plannerModelList, "opp");
                            planRecyclerView.setAdapter(plannerAdapter);
                            planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewOpportunityActivity.this));
                        } else {
                            noMembers.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        schedileview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schedileview.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews() {
        firstname = (TextView) findViewById(R.id.firstname);
        lastname = (TextView) findViewById(R.id.lastname);
        gender = (TextView) findViewById(R.id.gender);
        age = (TextView) findViewById(R.id.age);
        occupation = (TextView) findViewById(R.id.occupation);
        income = (TextView) findViewById(R.id.income);
        familymem = (TextView) findViewById(R.id.familymem);
        infoView = findViewById(R.id.details);
        contactView = findViewById(R.id.contactview);
        schedileview = (Button) findViewById(R.id.submit);
        vfclosingdate = (EditText) findViewById(R.id.vfclosingdate);
        remarks = (EditText) findViewById(R.id.fRemarks);
        vfstage = (TextView) findViewById(R.id.vfstage);
        planRecyclerView = (RecyclerView) findViewById(R.id.memberList);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        noMembers = (TextView) findViewById(R.id.noMembers);
        tabLayout.addTab(tabLayout.newTab().setText("Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Activities"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void initValues() {
        String navlblText = "NA";

        if (!TextUtils.isEmpty(opportunityModel.getFirstname())) {
            firstname.setText(opportunityModel.getFirstname());
        } else {
            firstname.setText(navlblText);
        }
        if (!TextUtils.isEmpty(opportunityModel.getLastname())) {
            lastname.setText(opportunityModel.getLastname());
        } else {
            lastname.setText(navlblText);
        }
        if (opportunityModel.getAge() != null) {
            age.setText(String.valueOf(opportunityModel.getAge()));
        } else {
            age.setText(navlblText);
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getGender())) {
            gender.setText(opportunityModel.getGender());
        }
        if (!TextUtils.isEmpty(opportunityModel.getOccupation())) {
            occupation.setText(opportunityModel.getOccupation());
        } else {
            occupation.setText(navlblText);
        }
        if (opportunityModel.getIncome() != null) {
            income.setText(String.valueOf(opportunityModel.getIncome()));
        } else {
            income.setText(navlblText);
        }
        if (opportunityModel.getFamilymembers() != null) {
            familymem.setText(String.valueOf(opportunityModel.getFamilymembers()));
        } else {
            familymem.setText(navlblText);
        }
        if (!TextUtils.isEmpty(opportunityModel.getRemarks())) {
            remarks.setText(opportunityModel.getRemarks());
        } else {
            remarks.setText("NA");
        }
//        if (Utils.valueOf(opportunityModel.getValue())) {
//            opprtValue.setText(String.valueOf(opportunityModel.getValue()));
//        } else {
//            opprtValue.setText("0");
//        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getClosingdate())) {
            vfclosingdate.setText(TimeUtils.getFormattedDatefromUnix(opportunityModel.getClosingdate(), "dd/MM/yyyy"));
        } else {
            vfclosingdate.setText(navlblText);
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getStage())) {
            vfstage.setText(opportunityModel.getStage());
        } else {
            vfstage.setText(navlblText);
        }
    }

    private void postcreateplan(String scheduledat) {
        plannerModel.setGroupid(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
        plannerModel.setUserid(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        plannerModel.setTitle(opportunityModel.getFirstname() + opportunityModel.getLastname());
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getOpportunityid())) {
            plannerModel.setOpportunityid(opportunityModel.getOpportunityid());
            plannerModel.setLocalopportunityid(opportunityModel.getId());
        } else {
            plannerModel.setLocalopportunityid(opportunityModel.getId());
        }
        plannerModel.setActivitytype("Activity");
        plannerModel.setType("Activity");
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(scheduledat, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(scheduledat, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(scheduledat, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setScheduletime(Integer.parseInt(scheduledat));
        plannerModel.setPlanstatus(0);
        plannerModel.setCompletestatus(0);
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(ViewOpportunityActivity.this).insertPlanner(plannerModel, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
        OpportunityIntentService.syncOfflineRecords(ViewOpportunityActivity.this);
        showAlert("", "Your Plan is saved successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.vftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("View Lead");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }
}

