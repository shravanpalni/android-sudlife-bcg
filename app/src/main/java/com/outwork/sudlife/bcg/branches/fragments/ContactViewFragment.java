package com.outwork.sudlife.bcg.branches.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.branches.ContactMgr;
import com.outwork.sudlife.bcg.branches.adapter.ContactsAdapter;
import com.outwork.sudlife.bcg.branches.models.ContactModel;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Habi on 10-05-2018.
 */

public class ContactViewFragment extends Fragment {
    private List<ContactModel> contactsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ContactsAdapter mAdapter;
    private String groupId, userId;
    private TextView noinfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
        noinfo = (TextView) view.findViewById(R.id.noMembers);
        recyclerView = (RecyclerView) view.findViewById(R.id.contactslist);

       /* if (SharedPreferenceManager.getInstance().getString(Constants.CONTACTS_LOADED, "").equals("notloaded")) {
            if (((BaseActivity) getActivity()).isNetworkAvailable()) {
                ContactsIntentService.insertContactsinDB(getActivity());
            }
        }*/
        getContactList();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, noinfo);

        return view;
    }

    public void getContactList() {
        noinfo.setVisibility(View.GONE);
        contactsList.clear();
        contactsList = new ContactMgr(getActivity()).getContactList(groupId, userId);
        if (contactsList.size() > 0) {
            recyclerView.setAdapter(null);
            mAdapter = new ContactsAdapter(getActivity(), contactsList);
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(manager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new ContactsAdapter(getActivity(), contactsList);
            recyclerView.setAdapter(mAdapter);
        } else {
            noinfo.setVisibility(View.VISIBLE);
        }
    }
}
