package com.outwork.sudlife.bcg.notifications.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.outwork.sudlife.bcg.IvokoApplication;

/**
 * Created by Panch on 12/3/2016.
 */

public class MyFireBaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // sendRegistrationToServer(refreshedToken);
        saveFCMToken(refreshedToken);
    }
    // [END refresh_token]

    /***
     If you want to send messages to this application instance or
     manage this apps subscriptions on the server side, send the
     Instance ID token to your app server.

     Persist token to third-party servers.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }


    private void saveFCMToken(String token) {
        // TODO: Implement this method to send token to your app server.
        IvokoApplication application = (IvokoApplication) IvokoApplication.getInstance();
        String oldtoken = application.getFCMToken(this);

        if (oldtoken.equalsIgnoreCase("default")) {
            application.saveFCMToken(this, token);
        } else {
            application.saveOldFCMToken(this, oldtoken);
            application.saveFCMToken(this, token);
        }
//        new PlayServicesHelper(this);
    }
}
