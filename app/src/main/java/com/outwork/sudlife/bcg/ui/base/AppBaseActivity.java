package com.outwork.sudlife.bcg.ui.base;
/**
 * Created by Habi on 11-05-2018.
 */

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.activities.CalenderActivity;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.ui.activities.DashBoardActivity;
import com.outwork.sudlife.bcg.ui.activities.ViewProfileActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

public abstract class AppBaseActivity extends BaseActivity implements MenuItem.OnMenuItemClickListener {
    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    private TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.app_base_layout);// The base layout that contains your navigation drawer.
        view_stub = (FrameLayout) findViewById(R.id.view_stub);
        navigation_view = (NavigationView) findViewById(R.id.navigation_view);
        navigation_view.setItemIconTintList(null);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerToggle = new ActionBarDrawerToggle(AppBaseActivity.this, mDrawerLayout, 0, 0) {
//            public void onDrawerClosed(View view) {
//                supportInvalidateOptionsMenu();
//            }
//
//            public void onDrawerOpened(View drawerView) {
//                supportInvalidateOptionsMenu();
//            }
//
//            @Override
//            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                view_stub.setTranslationX(slideOffset * drawerView.getWidth());
//                mDrawerLayout.bringChildToFront(drawerView);
//                mDrawerLayout.requestLayout();
//            }
//        };
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu);

        mDrawerToggle = new ActionBarDrawerToggle(AppBaseActivity.this, mDrawerLayout, 0, 0);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        drawerMenu = navigation_view.getMenu();
        username = (TextView) navigation_view.getHeaderView(0).findViewById(R.id.name);
//        if (SharedPreferenceManager.getInstance().getString(Constants.INTERNALROLE, "").equalsIgnoreCase("Supervisor")) {
//            navigation_view.getMenu().findItem(R.id.team_leads).setVisible(true);
//            navigation_view.getMenu().findItem(R.id.newSO_plans).setVisible(true);
//            navigation_view.getMenu().findItem(R.id.team_plans).setVisible(true);
//        } else {
//            navigation_view.getMenu().findItem(R.id.team_leads).setVisible(false);
//            navigation_view.getMenu().findItem(R.id.newSO_plans).setVisible(false);
//            navigation_view.getMenu().findItem(R.id.team_plans).setVisible(false);
//        }
        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, ""))) {
            username.setText(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, "") + " " + SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, ""));
        }
        for (int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, username);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                if (!getCurrentTopActivity().equalsIgnoreCase("com.outwork.sudlife.bcg.planner.activities.CalenderActivity")) {
                    Intent in = new Intent(AppBaseActivity.this, CalenderActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                break;

            case R.id.dashboard:
                if (!getCurrentTopActivity().equalsIgnoreCase("com.outwork.sudlife.bcg.ui.activities.DashBoardActivity")) {
                    Intent in = new Intent(AppBaseActivity.this, DashBoardActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                break;
            case R.id.profile:
                if (!getCurrentTopActivity().equalsIgnoreCase("com.outwork.sudlife.bcg.ui.activities.ViewProfileActivity")) {
                    Intent in = new Intent(AppBaseActivity.this, ViewProfileActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                break;

//            case R.id.team_plans:
//                if (!getCurrentTopActivity().equalsIgnoreCase(" com.outwork.sudlife.bcg.planner.activities.TeamPlansActivity")) {
//                    Intent inte = new Intent(AppBaseActivity.this, TeamPlansActivity.class);
//
//                    startActivity(inte);
//                    finish();
//                } else {
//                    mDrawerLayout.closeDrawers();
//                    return true;
//                }
//                break;
//
//            case R.id.newSO_plans:
//                if (!getCurrentTopActivity().equalsIgnoreCase(" com.outwork.sudlife.bcg.planner.activities.NewSOPlansActivity")) {
//                    Intent inte = new Intent(AppBaseActivity.this, NewSOPlansActivity.class);
//                    startActivity(inte);
//                    finish();
//                } else {
//                    mDrawerLayout.closeDrawers();
//                    return true;
//                }
//                break;
//
//            case R.id.team_leads:
//                if (!getCurrentTopActivity().equalsIgnoreCase(" com.outwork.sudlife.bcg.lead.activities.MyTeamLeadsListActivity")) {
//                    Intent inte = new Intent(AppBaseActivity.this, MyTeamLeadsListActivity.class);
//                    startActivity(inte);
//                    finish();
//                } else {
//                    mDrawerLayout.closeDrawers();
//                    return true;
//                }
//                break;
        }
        return false;
    }
}