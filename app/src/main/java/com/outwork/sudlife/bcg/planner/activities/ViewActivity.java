package com.outwork.sudlife.bcg.planner.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.core.LocationProvider;
import com.outwork.sudlife.bcg.dto.Geocode;
import com.outwork.sudlife.bcg.dto.geocode.STATUS;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.localbase.StaticDataMgr;
import com.outwork.sudlife.bcg.planner.PlannerMgr;
import com.outwork.sudlife.bcg.planner.SupervisorMgr;
import com.outwork.sudlife.bcg.planner.adapter.MultipleItemsSelectionAdapter;
import com.outwork.sudlife.bcg.planner.adapter.MultipleItemsSelectionAdapterForPurpose;
import com.outwork.sudlife.bcg.planner.adapter.SupervisorListAdapter;
import com.outwork.sudlife.bcg.planner.models.DataModel;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.planner.models.SupervisorModel;
import com.outwork.sudlife.bcg.planner.service.SupervisorIntentService;
import com.outwork.sudlife.bcg.restinterfaces.ReverseGeoInterface;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ViewActivity extends BaseActivity implements LocationProvider.LocationCallback {

    private String groupId, userid, onclick;
    private Boolean ismandatory = false;
    private AppCompatSpinner reason, activitytype, supervisor;
    private TextView branchname, plandate, supervisorname, toolbar_title;
    private EditText follouupdate, followupaction, reschduledate, checkintime, purpose, otherpurpose, otherfollowupaction, notes, others;
    private TextInputLayout otherslabel;
    private CheckBox checkBox;
    private List<SupervisorModel> supervisorModelList = new ArrayList<>();
    private List<String> activitytypeList = new ArrayList<>();
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private Button reschdule, checkin, confirm, checkout;
    private PlannerModel plannerModel;
    private RadioGroup type;
    private ArrayAdapter<String> activityTypeAdapter, reasonAdapter;
    private SupervisorListAdapter supervisorListAdapter;
    private RadioButton visit, activity;
    private MultipleItemsSelectionAdapter adapter;
    private List<DataModel> selectedDataList = new LinkedList<>();
    private LocationProvider mLocationProvider;
    private double latitude, longitude;
    private String addressline1, addressline2, locality, city, state, zip;
    private String addressline = "";
    private List<DataModel> selectedDataListPurpose = new LinkedList<>();
    private MultipleItemsSelectionAdapterForPurpose adapterPurpose;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private int counter = 0;
    int flag;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                reschduledate.setVisibility(View.VISIBLE);
                reason.setVisibility(View.VISIBLE);
                reschduledate.setEnabled(true);
                reschduledate.setText(TimeUtils.getFormattedDate(date));
                getReason();
                findViewById(R.id.bottomView).setVisibility(View.GONE);
                checkout.setVisibility(View.GONE);
                findViewById(R.id.rescview).setVisibility(View.VISIBLE);
                confirm.setVisibility(View.VISIBLE);
            }
            if (customSelector == 1) {
                follouupdate.setText(TimeUtils.getFormattedDate(date));
                String followupdt = TimeUtils.getFormattedDate(date);
                follouupdate.setText(followupdt);
                follouupdate.setEnabled(true);
                plannerModel.setFollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(followupdt)));
            }
        }

        @Override
        public void onDateTimeCancel() {
            reschduledate.setEnabled(true);
            reschduledate.setVisibility(View.GONE);
            reason.setVisibility(View.GONE);
            follouupdate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complted_plan);

        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("plannerModel")) {
            plannerModel = new Gson().fromJson(getIntent().getStringExtra("plannerModel"), PlannerModel.class);
        }
        mgr = LocalBroadcastManager.getInstance(ViewActivity.this);
        if (SharedPreferenceManager.getInstance().getString(Constants.SUPERVISORS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                SupervisorIntentService.insertSupervisorsList(ViewActivity.this);
            }
        }
        mLocationProvider = new LocationProvider(ViewActivity.this, this);
        mLocationProvider.connect();
        initToolBar();
        initializeViews();
        getActivitytypes();
        setListener();
        populateData();
        followupaction.setFocusable(false);
        followupaction.setFocusableInTouchMode(false);
        purpose.setFocusable(false);
        purpose.setFocusableInTouchMode(false);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) findViewById(R.id.vbranchnamelbl), (TextView) findViewById(R.id.suprvisorlbl),
                (TextView) findViewById(R.id.plandatelbl), (TextView) findViewById(R.id.textView1));
        Utils.setTypefaces(IvokoApplication.robotoTypeface, visit, activity, plandate, branchname,
                visit, activity, plandate, branchname, supervisorname, otherpurpose, otherfollowupaction, others,
                reschduledate, followupaction, follouupdate, purpose, checkintime);
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, checkin, reschdule);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, confirm, checkout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mLocationProvider != null)
                    mLocationProvider.connect();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new Thread(new Runnable() {
            @Override
            public void run() {
               if (mLocationProvider != null)
                    mLocationProvider.disconnect();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("supervisor_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("masterlist_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                getLocation(latitude, longitude);
        }
    }

    private void getReason() {
        final List<String> reschedulereasonlist = StaticDataMgr.getInstance(ViewActivity.this).getListNames(groupId, "reschedulereason");
        if (reschedulereasonlist.size() > 0) {
            reasonAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, reschedulereasonlist);
            reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            reason.setAdapter(reasonAdapter);
            reason.setSelection(0);
            reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (reschedulereasonlist.get(position).toString().equalsIgnoreCase("Other")) {
                        findViewById(R.id.notesview).setVisibility(View.VISIBLE);
                        ismandatory = true;
                    } else {
                        ismandatory = false;
                        findViewById(R.id.notesview).setVisibility(View.GONE);
                        reason.setSelection(position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            findViewById(R.id.reasn).setVisibility(View.GONE);
            reason.setVisibility(View.GONE);
        }
    }

    private void getSupervisors() {
        supervisorModelList.clear();
        supervisorModelList = SupervisorMgr.getInstance(ViewActivity.this).getsupervisorList(userid);
        if (supervisorModelList.size() > 0) {
            supervisorListAdapter = new SupervisorListAdapter(this, supervisorModelList);
            supervisor.setAdapter(supervisorListAdapter);
            supervisor.setSelection(0);
            for (int i = 0; i < supervisorModelList.size(); i++)
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname()))
                    if (plannerModel.getSupervisorname().trim().replaceAll(" ", "").equalsIgnoreCase(supervisorModelList.get(i).getUsername().trim().replaceAll(" ", ""))) {
                        supervisor.setSelection(i);
                        otherslabel.setVisibility(View.GONE);
                        return;
                    } else {
                        supervisor.setSelection(i);
                        otherslabel.setVisibility(View.VISIBLE);
                        others.setText(plannerModel.getSupervisorname());
                    }
            supervisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (supervisorModelList.get(position).getUsername().equalsIgnoreCase("Others")) {
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                        otherslabel.setVisibility(View.VISIBLE);
                    } else {
                        otherslabel.setVisibility(View.GONE);
                        supervisor.setSelection(position);
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                        plannerModel.setSupervisorname(supervisorModelList.get(position).getUsername());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void setListener() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getSupervisors();
            }
        };

        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.visit) {
                    plannerModel.setType("Visit");
                    plannerModel.setActivitytype("Visit");
                    findViewById(R.id.atype).setVisibility(View.GONE);
                    activitytype.setVisibility(View.GONE);
//                    plannerModel.setSubtype("");
                } else {
                    plannerModel.setActivitytype("Activity");
                    plannerModel.setType("Activity");
                    findViewById(R.id.atype).setVisibility(View.VISIBLE);
                    activitytype.setVisibility(View.VISIBLE);
                }
            }
        });

        follouupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                follouupdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });

        reschduledate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reschduledate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });

        reschdule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reschduledate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(initialDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick = "confirm";
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    dispatchlocationIntent(confirm);
                } else {
                    boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!gpsStatus) {
                        showSettingDialog();
                    } else {
                        if (ismandatory && !Utils.isNotNullAndNotEmpty(notes.getText().toString())) {
                            notes.setError("Specify Other Reschedule Reason");
                            notes.requestFocus();
                        } else {
                            postRescheduleData();
                        }
                    }
                }
            }
        });

        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick = "checkin";
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    dispatchlocationIntent(checkin);
                } else {
                    boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!gpsStatus) {
                        showSettingDialog();
                    } else {

                        List<PlannerModel> plannerModels = PlannerMgr.getInstance(ViewActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd",  plandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", plandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", plandate.getText().toString())));
                        List<PlannerModel> plannersModelList = new ArrayList<>();
                        for (PlannerModel plannerModel : plannerModels) {
                            if (plannerModel != null)
                                if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                    plannersModelList.add(plannerModel);
                                }
                        }
                        if (plannersModelList.size() > 0) {
                            //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {

                            showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }, "", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }, false);
                        } else {



                            if (latitude > 0.0 || longitude > 0.0) {
                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                    getLocation(latitude, longitude);
                                postCheckinData();
                            } else {
                                if (mLocationProvider != null) {
                                    mLocationProvider.connect();
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckinData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                } else {
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckinData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                }
                            }


                        }






                    }
                }
            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick = "checkout";
                if (counter==0) {
                    hasConnection();
                    //Log.e("counter is ", String.valueOf(counter));
                }
                else
                {
                    hasConnection();
                }
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    dispatchlocationIntent(checkout);
                } else {

                    boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!gpsStatus) {
                        showSettingDialog();
                    } else {
                        if (!Utils.isNotNullAndNotEmpty(purpose.getText().toString())) {
                            purpose.setFocusableInTouchMode(true);
                            purpose.setError("Select an Action");
                            purpose.requestFocus();
                        } else if (purpose.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherpurpose.getText().toString())) {
                            otherpurpose.setError("Please Specify other action Completed");
                            otherpurpose.requestFocus();
                        } else if (followupaction.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherfollowupaction.getText().toString())) {
                            otherfollowupaction.setError("Please Specify other followup action");
                            otherfollowupaction.requestFocus();
                        } else {
                            hasConnection();
                            if (latitude > 0.0 || longitude > 0.0) {
                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                    getLocation(latitude, longitude);
                                postCheckOutData();
                            } else {
                                if (mLocationProvider != null) {
                                    mLocationProvider.connect();
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckOutData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                } else {
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckOutData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                }
                            }
                        }
                    }
                }
            }
        });
        followupaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                followupaction.setEnabled(false);
                List<String> strinsArrayList = StaticDataMgr.getInstance(ViewActivity.this).getListNames(groupId, "followupaction");
                List<DataModel> actionDataModelArraylist = new LinkedList<>();
                for (int i = 0; i < strinsArrayList.size(); i++) {
                    DataModel dataModel = new DataModel();
                    dataModel.setName(strinsArrayList.get(i));
                    dataModel.setEnabledValue(false);
                    actionDataModelArraylist.add(dataModel);
                }

                if (selectedDataList.size() != 0) {
                    List<DataModel> selectedItemList = new ArrayList<>();
                    selectedItemList = adapter.getSelectedItems();
                    multiSelectSpinnerDialogBox(selectedItemList, followupaction);
                } else {
                    multiSelectSpinnerDialogBox(actionDataModelArraylist, followupaction);
                }
            }
        });
        purpose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                purpose.setFocusableInTouchMode(false);
                purpose.setEnabled(false);
                List<String> strinsArrayList = StaticDataMgr.getInstance(ViewActivity.this).getListNames(groupId, "purpose");
                List<DataModel> purposeDataModelArraylist = new LinkedList<>();
                for (int i = 0; i < strinsArrayList.size(); i++) {
                    DataModel dataModel = new DataModel();
                    dataModel.setName(strinsArrayList.get(i));
                    dataModel.setEnabledValue(false);
                    purposeDataModelArraylist.add(dataModel);
                }
                if (selectedDataListPurpose.size() != 0) {
                    List<DataModel> selectedItemList = new ArrayList<>();
                    selectedItemList = adapterPurpose.getSelectedItems();
                    multiSelectSpinnerDialogBoxforPusrPose(selectedItemList, purpose);
                } else {
                    multiSelectSpinnerDialogBoxforPusrPose(purposeDataModelArraylist, purpose);
                }

            }
        });






        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.VISIBLE);
                    getSupervisors();
                } else {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.GONE);
                    supervisor.setVisibility(View.GONE);
                    otherslabel.setVisibility(View.GONE);
                    others.setVisibility(View.GONE);
                }
            }
        });
        notes.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        } else {


           /* showAlert("", "Please check your Internet Connection", "ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, "", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }, false);*/
            if (counter == 0) {
                flag = 0;                     //GPS OFF and Internet OFF
                Toast.makeText(ViewActivity.this, "No internet connection....", Toast.LENGTH_LONG).show();
                counter++;
                return false;
            }
            else{
                flag = 1;                    // GPS ON and Internet OFF
                Log.e("checkout no internet"+"", String.valueOf(flag));
                return false;
            }
        }

    }

    private void getActivitytypes() {
        activitytypeList = StaticDataMgr.getInstance(ViewActivity.this).getListNames(groupId, "subtype");
        if (activitytypeList.size() > 0) {
            activityTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, activitytypeList);
            activityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activitytype.setAdapter(activityTypeAdapter);
            for (int i = 0; i < activitytypeList.size(); i++)
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype()))
                    if (plannerModel.getSubtype().equalsIgnoreCase(activitytypeList.get(i).toString())) {
                        activitytype.setSelection(i);
                    }
            activitytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activitytype.setSelection(position);
                    plannerModel.setSubtype(activitytype.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            findViewById(R.id.atype).setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void showSettingDialog() {
        initGoogleAPIClient();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(3 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (onclick.equalsIgnoreCase("checkout")) {
                            if (!Utils.isNotNullAndNotEmpty(purpose.getText().toString())) {
                               // purpose.setFocusableInTouchMode(true);
                                purpose.setError("Select an Action");
                                purpose.requestFocus();
                            } else if (purpose.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherpurpose.getText().toString())) {
                                otherpurpose.setError("Please Specify other Action Completed");
                                otherpurpose.requestFocus();
                            } else if (followupaction.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherfollowupaction.getText().toString())) {
                                otherfollowupaction.setError("Please Specify other followup action");
                                otherfollowupaction.requestFocus();
                            } else {
                              //  hasConnection();
                                if (latitude > 0.0 || longitude > 0.0) {
                                    if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                        getLocation(latitude, longitude);
                                    postCheckOutData();
                                } else {
                                    if (mLocationProvider != null) {
                                        mLocationProvider.connect();
                                        showProgressDialog("Searching for location....");
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                    getLocation(latitude, longitude);
                                                postCheckOutData();
                                                dismissProgressDialog();
                                            }
                                        }, LOCATION_FETCH_TIME);
                                    } else {
                                        showProgressDialog("Searching for location....");
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                    getLocation(latitude, longitude);
                                                postCheckOutData();
                                                dismissProgressDialog();
                                            }
                                        }, LOCATION_FETCH_TIME);
                                    }
                                }
                            }
                        } else if (onclick.equalsIgnoreCase("checkin")) {
                            if (latitude > 0.0 || longitude > 0.0) {
                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                    getLocation(latitude, longitude);
                                postCheckinData();
                            } else {
                                if (mLocationProvider != null) {
                                    mLocationProvider.connect();
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckinData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                } else {
                                    showProgressDialog("Searching for location....");
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postCheckinData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                }
                            }
                        } else if (onclick.equalsIgnoreCase("confirm")) {
                            if (ismandatory && !Utils.isNotNullAndNotEmpty(notes.getText().toString())) {
                                notes.setError("Specify Other Reschedule Reason");
                                notes.requestFocus();
                            } else {
                                postRescheduleData();
                            }
                        }
                        updateGPSStatus("GPS is Enabled in your device");
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ViewActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    public void dispatchlocationIntent(Button button) {
        int fineLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED || coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!gpsStatus) {
                showSettingDialog();
            } else {
                if (button.getId() == R.id.checkout) {
                    if (!Utils.isNotNullAndNotEmpty(purpose.getText().toString())) {
                        purpose.setFocusableInTouchMode(true);
                        purpose.setError("Select an Action");
                        purpose.requestFocus();
                    } else if (purpose.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherpurpose.getText().toString())) {
                        otherpurpose.setError("Please Specify other action Complted");
                        otherpurpose.requestFocus();
                    } else if (followupaction.getText().toString().contains("Others") && !Utils.isNotNullAndNotEmpty(otherfollowupaction.getText().toString())) {
                        otherfollowupaction.setError("Please Specify other followup action");
                        otherfollowupaction.requestFocus();
                    } else {
                       hasConnection();
                        if (latitude > 0.0 || longitude > 0.0) {
                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                getLocation(latitude, longitude);
                            postCheckOutData();
                        } else {
                            if (mLocationProvider != null) {
                                mLocationProvider.connect();
                                showProgressDialog("Searching for location....");
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                            getLocation(latitude, longitude);
                                        postCheckOutData();
                                        dismissProgressDialog();
                                    }
                                }, LOCATION_FETCH_TIME);
                            } else {
                                showProgressDialog("Searching for location....");
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                            getLocation(latitude, longitude);
                                        postCheckOutData();
                                        dismissProgressDialog();
                                    }
                                }, LOCATION_FETCH_TIME);
                            }
                        }
                    }
                }
                if (button.getId() == R.id.checkin) {

                    List<PlannerModel> plannerModels = PlannerMgr.getInstance(ViewActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                            Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd",  plandate.getText().toString())),
                            Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", plandate.getText().toString())),
                            Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", plandate.getText().toString())));
                    List<PlannerModel> plannersModelList = new ArrayList<>();
                    for (PlannerModel plannerModel : plannerModels) {
                        if (plannerModel != null)
                            if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                plannersModelList.add(plannerModel);
                            }
                    }
                    if (plannersModelList.size() > 0) {
                        //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {

                        showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }, "", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }, false);
                    } else {

                        if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                            getLocation(latitude, longitude);
                        postCheckinData();

                    }








                }
                if (button.getId() == R.id.confirm) {
                    if (ismandatory && !Utils.isNotNullAndNotEmpty(notes.getText().toString())) {
                        notes.setError("Specify Other Reschedule Reason");
                        notes.requestFocus();
                    } else {
                        postRescheduleData();
                    }
                }
            }
        }
    }

    private void postCheckinData() {
        if (checkBox.isChecked()) {
            plannerModel.setJointvisit(true);
        } else {
            plannerModel.setJointvisit(false);
        }
        if (Utils.isNotNullAndNotEmpty(others.getText().toString())) {
            plannerModel.setSupervisorname(others.getText().toString());
        }
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(1);
        plannerModel.setCheckintime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (Utils.isNotNullAndNotEmpty(addressline))
            plannerModel.setCheckinlocation(addressline);
        plannerModel.setCheckinlat(String.valueOf(latitude));
        plannerModel.setCheckinlong(String.valueOf(longitude));
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (plannerModel.getScheduletime() != null)
            if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                    .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                PlannerMgr.getInstance(ViewActivity.this).updateCheckInPlanner(plannerModel);
                LeadIntentService.syncLeadstoServer(ViewActivity.this);
                showAlert("", "You have Checked-In successfully",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);
            } else {
                showToast(getResources().getString(R.string.cantcheckin));
            }
    }

    private void postCheckOutData() {
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(2);
        plannerModel.setCheckouttime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (Utils.isNotNullAndNotEmpty(addressline))
            plannerModel.setCheckoutlocation(addressline);
        plannerModel.setCheckoutlat(String.valueOf(latitude));
        plannerModel.setCheckoutlong(String.valueOf(longitude));
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        plannerModel.setPurpose(purpose.getText().toString());
        if (Utils.isNotNullAndNotEmpty(otherpurpose.getText().toString()))
            plannerModel.setOtheractions(otherpurpose.getText().toString());
        if (Utils.isNotNullAndNotEmpty(otherfollowupaction.getText().toString()))
            plannerModel.setOtherfollowupactions(otherfollowupaction.getText().toString());
        if (Utils.isNotNullAndNotEmpty(follouupdate.getText().toString()))
            plannerModel.setFollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(follouupdate.getText().toString())));
        plannerModel.setFollowupaction(followupaction.getText().toString());
        plannerModel.setNotes(notes.getText().toString());
        if (plannerModel.getScheduletime() != null)
            if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                    .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                PlannerMgr.getInstance(ViewActivity.this).updateCheckoutPlanner(plannerModel);
                LeadIntentService.syncLeadstoServer(ViewActivity.this);
                showAlert("", "You have Checked Out successfully",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);
            } else {
                showToast(getResources().getString(R.string.cantcheckout));
            }

    }

    private void postRescheduleData() {
        if (checkBox.isChecked()) {
            plannerModel.setJointvisit(checkBox.isChecked());
        }
        if (Utils.isNotNullAndNotEmpty(reschduledate.getText().toString())) {
            plannerModel.setReschduledate(Integer.parseInt(TimeUtils.convertDatetoUnix(reschduledate.getText().toString())));
        }
        if (Utils.isNotNullAndNotEmpty(reason.getSelectedItem().toString())) {
            plannerModel.setReshdreason(reason.getSelectedItem().toString());
        }
        if (Utils.isNotNullAndNotEmpty(notes.getText().toString())) {
            plannerModel.setNotes(notes.getText().toString());
        }
        if (Utils.isNotNullAndNotEmpty(others.getText().toString())) {
            plannerModel.setSupervisorname(others.getText().toString());
        }
        plannerModel.setNetwork_status("offline");
        plannerModel.setCompletestatus(3);
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        PlannerMgr.getInstance(ViewActivity.this).updateReschedulePlanner(plannerModel);
        //creating new row
        plannerModel.setActivityid("");
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", reschduledate.getText().toString())));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", reschduledate.getText().toString())));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", reschduledate.getText().toString())));
        plannerModel.setScheduletime(Integer.parseInt(TimeUtils.convertDatetoUnix(reschduledate.getText().toString())));
        plannerModel.setCompletestatus(0);
        PlannerMgr.getInstance(ViewActivity.this).insertPlanner(plannerModel, userid, groupId);
        LeadIntentService.syncLeadstoServer(ViewActivity.this);
        showAlert("", "Your Plan has been Rescheduled",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void multiSelectSpinnerDialogBox(final List<DataModel> dataModelList, final EditText editText) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.multi_spinner_for_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_items);
        adapter = new MultipleItemsSelectionAdapter(this, dataModelList);
        lvItems.setAdapter(adapter);
        deleteDialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followupaction.setEnabled(true);
                //your business logic
                selectedDataList = adapter.getSelectedItems();
                StringBuilder str = new StringBuilder();
                if (selectedDataList.size() != 0) {
                    for (int i = 0; i < selectedDataList.size(); i++) {
                        if (selectedDataList.get(i).isEnabledValue()) {
                            if (Utils.isNotNullAndNotEmpty(str.toString())) {
                                str.append(", " + selectedDataList.get(i).getName());
                            } else {
                                str.append(selectedDataList.get(i).getName());
                            }
                        }
                    }
                    editText.setError(null);
                    deleteDialog.dismiss();
                    editText.setText(str);
                    if (editText.getText().toString().contains("Others")) {
                        findViewById(R.id.otherfollowupdatelbl).setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.otherfollowupdatelbl).setVisibility(View.GONE);
                    }
                }
            }
        });
        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followupaction.setEnabled(true);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    public void multiSelectSpinnerDialogBoxforPusrPose(final List<DataModel> dataModelList, final EditText editText) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.multi_spinner_for_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_items);
        adapterPurpose = new MultipleItemsSelectionAdapterForPurpose(this, dataModelList);
        lvItems.setAdapter(adapterPurpose);
        deleteDialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                purpose.setEnabled(true);
                selectedDataListPurpose = adapterPurpose.getSelectedItems();
                StringBuilder str = new StringBuilder();
                if (selectedDataListPurpose.size() != 0) {
                    for (int i = 0; i < selectedDataListPurpose.size(); i++) {
                        if (selectedDataListPurpose.get(i).isEnabledValue()) {
                            if (Utils.isNotNullAndNotEmpty(str.toString())) {
                                str.append(", " + selectedDataListPurpose.get(i).getName());
                            } else {
                                str.append(selectedDataListPurpose.get(i).getName());
                            }
                        }
                    }
                    editText.setError(null);
                    deleteDialog.dismiss();
                    editText.setText(str);
                    if (editText.getText().toString().contains("Others")) {
                        findViewById(R.id.otherpurposelbl).setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.otherpurposelbl).setVisibility(View.GONE);
                    }
                }
            }
        });

        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purpose.setEnabled(true);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }


    private void initializeViews() {
        branchname = (TextView) findViewById(R.id.vbranchname);
        purpose = (EditText) findViewById(R.id.purpose);
        otherpurpose = (EditText) findViewById(R.id.otherpurpose);
        otherfollowupaction = (EditText) findViewById(R.id.otherfollowupdate);
        notes = (EditText) findViewById(R.id.notes);
        follouupdate = (EditText) findViewById(R.id.ffollowupdate);
        followupaction = (EditText) findViewById(R.id.ffollowupaction);
        reschduledate = (EditText) findViewById(R.id.vreschddate);
        reason = (AppCompatSpinner) findViewById(R.id.vreason);
        supervisor = (AppCompatSpinner) findViewById(R.id.supervisor);
        supervisorname = (TextView) findViewById(R.id.supervisorname);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkintime = (EditText) findViewById(R.id.checkedindate);
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
        reschdule = (Button) findViewById(R.id.reschdule);
        checkin = (Button) findViewById(R.id.checkin);
        checkout = (Button) findViewById(R.id.checkout);
        confirm = (Button) findViewById(R.id.confirm);
        plandate = (TextView) findViewById(R.id.plandate);
        otherslabel = (TextInputLayout) findViewById(R.id.otherslabel);
        others = (EditText) findViewById(R.id.others);
        activitytype = (AppCompatSpinner) findViewById(R.id.activitytype);
    }

    private void populateData() {
        if (plannerModel != null)
            if (plannerModel.getCompletestatus() != null)
                if (plannerModel.getCompletestatus() == 0) {
                    findViewById(R.id.rescview).setVisibility(View.GONE);
                    findViewById(R.id.compltview).setVisibility(View.GONE);
                    findViewById(R.id.bottomView).setVisibility(View.VISIBLE);
                    checkout.setVisibility(View.GONE);
                    findViewById(R.id.notesview).setVisibility(View.GONE);
                } else if (plannerModel.getCompletestatus() == 1) {  //after checkin
                    findViewById(R.id.rescview).setVisibility(View.GONE);
                    findViewById(R.id.compltview).setVisibility(View.VISIBLE);
                    findViewById(R.id.bottomView).setVisibility(View.GONE);
                    checkout.setVisibility(View.VISIBLE);
                    checkBox.setEnabled(false);
                    supervisor.setVisibility(View.GONE);
                    supervisorname.setVisibility(View.VISIBLE);
                    findViewById(R.id.notesview).setVisibility(View.VISIBLE);
                    if (plannerModel.getFollowupdate() != null)
                        if (plannerModel.getFollowupdate() == 0) {
                        } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getFollowupdate()))) {
                            follouupdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getFollowupdate()), "dd/MM/yyyy hh:mm a"));
                        }
                    if (plannerModel.getCheckintime() != null)
                        if (plannerModel.getCheckintime() == 0) {
                        } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getCheckintime()))) {
                            checkintime.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getCheckintime()), "dd/MM/yyyy hh:mm a"));
                            checkintime.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
                        }
                }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername())) {
            branchname.setText(plannerModel.getCustomername());
        }
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                checkBox.setChecked(plannerModel.getJointvisit());
                if (plannerModel.getCompletestatus() == 1) {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.GONE);
                    supervisorname.setVisibility(View.VISIBLE);
                    if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname())) {
                        supervisorname.setText(plannerModel.getSupervisorname());
                    } else {
                        supervisorname.setText("None");
                    }
                } else {
                    findViewById(R.id.suprvisorlbl).setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.VISIBLE);
                    supervisorname.setVisibility(View.GONE);
                    getSupervisors();
                }
            } else {
                findViewById(R.id.suprvisorlbl).setVisibility(View.GONE);
                supervisor.setVisibility(View.GONE);
                supervisorname.setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.suprvisorlbl).setVisibility(View.GONE);
            supervisor.setVisibility(View.GONE);
            supervisorname.setVisibility(View.GONE);
        }

        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (plannerModel.getActivitytype().equalsIgnoreCase("Visit")) {
                visit.setChecked(true);
//                plannerModel.setSubtype("");
            } else {
                activity.setChecked(true);
                findViewById(R.id.atype).setVisibility(View.VISIBLE);
                activitytype.setVisibility(View.VISIBLE);
            }
        }

        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() == 0) {
                plandate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getScheduletime()))) {
                plandate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy hh:mm a"));
            }
        } else {
            plandate.setText(getResources().getString(R.string.notAvailable));
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            toolbar_title.setText(plannerModel.getCustomername());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        }
    }

    private void getLocation(double lat, double lng) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ReverseGeoInterface.MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String latlong = lat + "," + lng;
        ReverseGeoInterface service = retrofit.create(ReverseGeoInterface.class);
        Call<Geocode> locationdetails = service.getLocation("GEOMETRIC_CENTER", latlong, "AIzaSyB6QdommPXh8xlvGJL9IkauaajCpKfNWpc");

        locationdetails.enqueue(new Callback<Geocode>() {
            @Override
            public void onResponse(Call<Geocode> call, Response<Geocode> response) {
                Geocode geocode = response.body();
                if (geocode != null)
                    if (geocode.getResults() != null)
                        if (geocode.getStatus() == STATUS.OK) {
                            if (geocode.getResults().size() > 0) {
                                if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents())) {
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(0).getLongName()))
                                        addressline1 = geocode.getResults().get(0).getAddressComponents().get(0).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(1).getLongName()))
                                        addressline2 = geocode.getResults().get(0).getAddressComponents().get(1).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(2).getLongName()))
                                        locality = geocode.getResults().get(0).getAddressComponents().get(2).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(3).getLongName()))
                                        city = geocode.getResults().get(0).getAddressComponents().get(3).getLongName();
                                    addressline = addressline1 + " " + addressline2 + " " + locality + " " + city;
                                }
                            }
                        }
            }

            @Override
            public void onFailure(Call<Geocode> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "Result OK");
                        if (onclick.equalsIgnoreCase("checkout")) {
                            dispatchlocationIntent(checkout);
                        } else if (onclick.equalsIgnoreCase("checkin")) {
                            dispatchlocationIntent(checkin);
                        } else if (onclick.equalsIgnoreCase("confirm")) {
                            dispatchlocationIntent(confirm);
                        }
                        updateGPSStatus("GPS is Enabled in your device");
                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("Settings", "Result Cancel");
                        showToast(getResources().getString(R.string.locationtoast));
                        showSettingDialog();
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dismissProgressDialog();
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            initGoogleAPIClient();
                        } else {
                            if (onclick.equalsIgnoreCase("checkout")) {
                                dispatchlocationIntent(checkout);
                            } else if (onclick.equalsIgnoreCase("checkin")) {
                                dispatchlocationIntent(checkin);
                            } else if (onclick.equalsIgnoreCase("confirm")) {
                                dispatchlocationIntent(confirm);
                            }
                        }
                        updateGPSStatus("GPS is Enabled in your device");
                    } else {
                        showToast(getResources().getString(R.string.locationtoast));
                    }
                } else {
                    showToast(getResources().getString(R.string.locationtoast));
                }
                return;
            }
        }
    }
}
