package com.outwork.sudlife.bcg.planner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.bcg.IvokoApplication;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.planner.models.PlannerModel;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.TimeUtils;
import com.outwork.sudlife.bcg.utilities.Utils;

public class ViewplannedActivity extends BaseActivity {
    private LinearLayout checkindata, rescheduledata;
    private TextView jointvisit, suprvisorlbl, chechinlbl, checkoutlbl, purposelbl, otherpurposelbl, otherfollowupdatelbl, follow_desc, notes_desc, reschlbl, reason_desc;
    private TextView branchname, vfplandate, activitytype, supervisor,
            checkbox, checkedin, checkout, purpose, otherpurpose, otherfollowupdate, followupaction, notes, rescdate, reason, toolbat_title;
    private PlannerModel plannerModel;
    private RadioGroup type;
    private RadioButton visit, activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planned_view);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("plannerModel")) {
            plannerModel = new Gson().fromJson(getIntent().getStringExtra("plannerModel"), PlannerModel.class);
        }
        initToolBar();
        initializeViews();
        populateData();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, visit, activity, branchname, supervisor, vfplandate, activitytype,
                checkedin, checkout, purpose, otherpurpose, otherfollowupdate, followupaction,
                notes, rescdate, reason);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, suprvisorlbl, (TextView) findViewById(R.id.branchnamelbl),
                (TextView) findViewById(R.id.plandate),(TextView) findViewById(R.id.activitytypelbl),jointvisit,
                (TextView) findViewById(R.id.reason_desc),(TextView) findViewById(R.id.reschedulelbl),
                chechinlbl, checkoutlbl, purposelbl, otherpurposelbl, otherfollowupdatelbl, follow_desc, notes_desc);
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbat_title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.edit) {
            Intent intent = new Intent(ViewplannedActivity.this, EditPlanActivity.class);
            intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (plannerModel.getCompletestatus() == 0) {
            if (plannerModel.getScheduletime() != null)
                if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                        .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                    MenuItem item = menu.findItem(R.id.edit);
                    item.setVisible(true);
                } else if (plannerModel.getScheduletime() > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                    MenuItem item = menu.findItem(R.id.edit);
                    item.setVisible(true);
                } else {
                    MenuItem item = menu.findItem(R.id.edit);
                    item.setVisible(false);
                }
        } else {
            MenuItem item = menu.findItem(R.id.edit);
            item.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void initializeViews() {
        branchname = (TextView) findViewById(R.id.branchname);
        supervisor = (TextView) findViewById(R.id.supervisor);
        vfplandate = (TextView) findViewById(R.id.vfplandate);
        checkbox = (TextView) findViewById(R.id.checkbox);
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
        activitytype = (TextView) findViewById(R.id.activitytype);
        jointvisit = (TextView) findViewById(R.id.jointvisit);
        suprvisorlbl = (TextView) findViewById(R.id.suprvisorlbl);
        chechinlbl = (TextView) findViewById(R.id.chechinlbl);
        checkoutlbl = (TextView) findViewById(R.id.checkoutlbl);
        purposelbl = (TextView) findViewById(R.id.purposelbl);
        otherpurposelbl = (TextView) findViewById(R.id.otherpurposelbl);
        otherfollowupdatelbl = (TextView) findViewById(R.id.otherfollowupdatelbl);
        follow_desc = (TextView) findViewById(R.id.follow_desc);
        notes_desc = (TextView) findViewById(R.id.notes_desc);
        checkedin = (TextView) findViewById(R.id.checkedin);
        checkout = (TextView) findViewById(R.id.checkout);
        purpose = (TextView) findViewById(R.id.purpose);
        otherpurpose = (TextView) findViewById(R.id.otherpurpose);
        otherfollowupdate = (TextView) findViewById(R.id.otherfollowupdate);
        followupaction = (TextView) findViewById(R.id.followupaction);
        notes = (TextView) findViewById(R.id.notes);
        checkindata = (LinearLayout) findViewById(R.id.checkindata);
        rescdate = (TextView) findViewById(R.id.rescheduledate);
        reason = (TextView) findViewById(R.id.reason);
        rescheduledata = (LinearLayout) findViewById(R.id.reschduledata);
    }

    private void populateData() {
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername())) {
            branchname.setText(plannerModel.getCustomername());
        } else {
            branchname.setText(getResources().getString(R.string.notAvailable));
        }
        if (plannerModel.getJointvisit() != null) {
            if (plannerModel.getJointvisit()) {
                checkbox.setText("Yes");
                if (Utils.isNotNullAndNotEmpty(plannerModel.getSupervisorname())) {
                    supervisor.setText(plannerModel.getSupervisorname());
                } else {
                    supervisor.setText(getResources().getString(R.string.notAvailable));
                }
            } else {
                checkbox.setText("No");
                suprvisorlbl.setVisibility(View.GONE);
                supervisor.setVisibility(View.GONE);
            }
        } else {
            checkbox.setText("No");
            suprvisorlbl.setVisibility(View.GONE);
            supervisor.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivitytype())) {
            if (plannerModel.getActivitytype().equalsIgnoreCase("Visit")) {
                visit.setChecked(true);
            } else {
                activity.setChecked(true);
            }
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
            if (Utils.isNotNullAndNotEmpty(plannerModel.getSubtype())) {
                activitytype.setText(plannerModel.getSubtype());
            } else {
                activitytype.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            activitytype.setText(getResources().getString(R.string.notAvailable));
        }
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() == 0) {
                vfplandate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getScheduletime()))) {
                vfplandate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy hh:mm a"));
            } else {
                findViewById(R.id.followdateview).setVisibility(View.GONE);
                vfplandate.setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.plandate).setVisibility(View.GONE);
            vfplandate.setVisibility(View.GONE);
        }
        if (plannerModel.getCheckintime() != null) {
            if (plannerModel.getCheckintime() == 0) {
                checkedin.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getCheckintime()))) {
                checkedin.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getCheckintime()), "dd/MM/yyyy hh:mm a"));
            } else {
                chechinlbl.setVisibility(View.GONE);
                checkedin.setVisibility(View.GONE);
            }
        } else {
            chechinlbl.setVisibility(View.GONE);
            checkedin.setVisibility(View.GONE);
        }
        if (plannerModel.getCheckouttime() != null) {
            if (plannerModel.getCheckouttime() == 0) {
                checkout.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getCheckouttime()))) {
                checkout.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getCheckouttime()), "dd/MM/yyyy hh:mm a"));
            } else {
                checkoutlbl.setVisibility(View.GONE);
                checkout.setVisibility(View.GONE);
            }
        } else {
            checkoutlbl.setVisibility(View.GONE);
            checkout.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getPurpose())) {
            purpose.setText(plannerModel.getPurpose());
            if (plannerModel.getPurpose().contains("Others")) {
                otherpurposelbl.setVisibility(View.VISIBLE);
                otherpurpose.setVisibility(View.VISIBLE);
                if (Utils.isNotNullAndNotEmpty(plannerModel.getOtheractions())) {
                    otherpurpose.setText(plannerModel.getOtheractions());
                } else {
                    otherpurpose.setText(getResources().getString(R.string.notAvailable));
                }
            } else {
                otherpurposelbl.setVisibility(View.GONE);
                otherpurpose.setVisibility(View.GONE);
            }
        } else {
            otherpurposelbl.setVisibility(View.GONE);
            otherpurpose.setVisibility(View.GONE);
            purpose.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getFollowupaction())) {
            followupaction.setText(plannerModel.getFollowupaction());
            if (plannerModel.getFollowupaction().contains("Others")) {
                otherfollowupdatelbl.setVisibility(View.VISIBLE);
                otherfollowupdate.setVisibility(View.VISIBLE);
                if (Utils.isNotNullAndNotEmpty(plannerModel.getOtherfollowupactions())) {
                    otherfollowupdate.setText(plannerModel.getOtherfollowupactions());
                } else {
                    otherfollowupdate.setText(getResources().getString(R.string.notAvailable));
                }
            } else {
                otherfollowupdatelbl.setVisibility(View.GONE);
                otherfollowupdate.setVisibility(View.GONE);
            }
        } else {
            otherfollowupdatelbl.setVisibility(View.GONE);
            otherfollowupdate.setVisibility(View.GONE);
            followupaction.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes())) {
            notes.setText(plannerModel.getNotes());
        } else {
            notes.setText(getResources().getString(R.string.notAvailable));
        }
        if (plannerModel.getCompletestatus() == 0) {
            checkindata.setVisibility(View.GONE);
            rescheduledata.setVisibility(View.GONE);
            chechinlbl.setVisibility(View.GONE);
            checkedin.setVisibility(View.GONE);
            notes_desc.setVisibility(View.GONE);
            notes.setVisibility(View.GONE);
        } else if (plannerModel.getCompletestatus() == 1) {
            checkindata.setVisibility(View.GONE);
            rescheduledata.setVisibility(View.GONE);
            checkedin.setVisibility(View.VISIBLE);
            chechinlbl.setVisibility(View.VISIBLE);
            notes_desc.setVisibility(View.GONE);
            notes.setVisibility(View.GONE);
        } else if (plannerModel.getCompletestatus() == 2) {
            checkindata.setVisibility(View.VISIBLE);
            rescheduledata.setVisibility(View.GONE);
            checkoutlbl.setVisibility(View.VISIBLE);
            checkout.setVisibility(View.VISIBLE);
            notes_desc.setVisibility(View.VISIBLE);
            notes.setVisibility(View.VISIBLE);
        } else if (plannerModel.getCompletestatus() == 3) {
            chechinlbl.setVisibility(View.GONE);
            checkedin.setVisibility(View.GONE);
            checkindata.setVisibility(View.GONE);
            rescheduledata.setVisibility(View.VISIBLE);
            notes_desc.setVisibility(View.VISIBLE);
            notes.setVisibility(View.VISIBLE);

            if (Utils.isNotNullAndNotEmpty(plannerModel.getReshdreason())) {
                reason.setText(plannerModel.getReshdreason());
            } else {
                reason.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(plannerModel.getNotes())) {
                notes.setText(plannerModel.getNotes());
            } else {
                notes.setText(getResources().getString(R.string.notAvailable));
            }
            if (plannerModel.getReschduledate() != null) {
                if (plannerModel.getReschduledate() == 0) {
                    rescdate.setText(getResources().getString(R.string.notAvailable));
                } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getReschduledate()))) {
                    rescdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getReschduledate()), "dd/MM/yyyy hh:mm a"));
                } else {
                    rescdate.setVisibility(View.GONE);
                }
            } else {
                rescdate.setVisibility(View.GONE);
            }
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbat_title = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(plannerModel.getCustomername()))
            toolbat_title.setText("View Plan");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        }
    }
}
