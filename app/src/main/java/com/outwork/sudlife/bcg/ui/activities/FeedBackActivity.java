package com.outwork.sudlife.bcg.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.outwork.sudlife.bcg.R;
import com.outwork.sudlife.bcg.restinterfaces.GeneralService;
import com.outwork.sudlife.bcg.restinterfaces.POJO.Feedback;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.ui.BaseActivity;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedBackActivity extends BaseActivity {

    private TextView fromemail;
    private EditText feedback;
    private String userEmail, feedbackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        userEmail = SharedPreferenceManager.getInstance().getString(Constants.USER_EMAIL, "");
        fromemail = (TextView) findViewById(R.id.feedbackuseremail);
        feedback = (EditText) findViewById(R.id.feedback);
        fromemail.setText(userEmail);
        initialiseToolBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.send_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.send) {
            feedbackText = feedback.getText().toString();
            Feedback feedbackObj = new Feedback();
            feedbackObj.setFromemail(userEmail);
            feedbackObj.setFeedback(feedbackText);
            if (!TextUtils.isEmpty(feedbackText)) {
                sendFeedBack(feedbackObj);
            } else {
                showToast("You have not provided any feedback. Please type in your feedback");
            }
            return true;
        }
        return false;
    }

    private void initialiseToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.vFeedBackToolBar);

        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Feedback");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void sendFeedBack(Feedback feedbackobj) {
        GeneralService feedbackclient = RestService.createService(GeneralService.class);
        Call<RestResponse> submitfeedback = feedbackclient.feedback(userToken, groupId, feedbackobj);
        submitfeedback.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                   hideKeyboard();
                    showAlert("", "Thank you for submitting your feedback. We will get back to you shortly", "Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }, "", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }, false);
                } else {
                    showToast("Could not submit your feedback. Please try again");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                showToast("Could not submit your feedback. Please try again");
            }
        });
    }
}
