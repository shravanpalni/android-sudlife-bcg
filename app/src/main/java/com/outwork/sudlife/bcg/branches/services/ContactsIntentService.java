package com.outwork.sudlife.bcg.branches.services;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.bcg.branches.BranchesMgr;
import com.outwork.sudlife.bcg.branches.ContactMgr;
import com.outwork.sudlife.bcg.branches.models.BranchesModel;
import com.outwork.sudlife.bcg.branches.models.ContactModel;
import com.outwork.sudlife.bcg.lead.services.LeadIntentService;
import com.outwork.sudlife.bcg.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.bcg.planner.service.SupervisorIntentService;
import com.outwork.sudlife.bcg.restinterfaces.RestResponse;
import com.outwork.sudlife.bcg.restinterfaces.RestService;
import com.outwork.sudlife.bcg.targets.services.TargetsIntentService;
import com.outwork.sudlife.bcg.utilities.Constants;
import com.outwork.sudlife.bcg.utilities.SharedPreferenceManager;
import com.outwork.sudlife.bcg.utilities.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsIntentService extends JobIntentService {
    public static final String ACTION_GET_CONTACTS = "com.outwork.sudlife.bcg.branches.services.action.GET_CONTACTS";
    public static final String ACTION_GET_CUSTOMERS = "com.outwork.sudlife.bcg.branches.services.action.GET_CUSTOMERS";
    //public static final String ACTION_GET_HIERARCHY_CUSTOMERS = "com.outwork.sudlife.bcg.branches.services.action.GET_HIERARCHY_CUSTOMERS";
    public static final String ACTION_CONTACTS_OFFLINE_SYNC = "com.outwork.sudlife.bcg.branches.services.action.CONTACTS_OFFLINE_SYNC";
    private static final String ACTION_CUSTOMERS_OFFLINE_SYNC = "com.outwork.sudlife.bcg.branches.services.action.CUSTOMERS_OFFLINE_SYNC";
    public static final String ACTION_POST_CONTACT = "com.outwork.sudlife.bcg.branches.services.action.POST_CONTACT";
    public static final String ACTION_POST_CUSTOMER = "com.outwork.sudlife.bcg.branches.services.action.POST_CUSTOMER";
    public static final String ACTION_UPDATE_CONTACT = "com.outwork.sudlife.bcg.branches.services.action.UPDATE_CONTACT";
    public static final String CUSTOMER_MODEL = "com.outwork.sudlife.bcg.branches.services.extra.CUSTOMER_MODEL";
    public static final String CONTACT_MODEL = "com.outwork.sudlife.bcg.branches.services.extra.CONTACT_MODEL";
    private static final Integer JOBID = 1000;

    private LocalBroadcastManager mgr;
    private String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
    private String groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
    private String userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_CUSTOMERS.equals(action)) {
                getCustomerList();
            }

           /* if (ACTION_GET_HIERARCHY_CUSTOMERS.equals(action)) {
                getHierarchyCustomerList();
            }*/


            if (ACTION_GET_CONTACTS.equals(action)) {
                getContactList();
            }
            if (ACTION_CONTACTS_OFFLINE_SYNC.equals(action)) {
                syncOfflineContacts();
            }
            if (ACTION_CUSTOMERS_OFFLINE_SYNC.equals(action)) {
                syncOfflineCustomers();
            }
            if (ACTION_UPDATE_CONTACT.equals(action)) {
                final String contactmodel = intent.getStringExtra(CONTACT_MODEL);
                updateContact(new Gson().fromJson(contactmodel, ContactModel.class));
            }
            if (ACTION_POST_CUSTOMER.equals(action)) {
                final String opprtmodel = intent.getStringExtra(CUSTOMER_MODEL);
                postCustomer(new Gson().fromJson(opprtmodel, BranchesModel.class));
            }
        }
    }

    public static void insertCustomersinDB(Context context) {
        Intent intent = new Intent(context, ContactsIntentService.class);
        intent.setAction(ACTION_GET_CUSTOMERS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ContactsIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }


  /*  public static void insertHierarchyCustomersinDB(Context context) {
        Intent intent = new Intent(context, ContactsIntentService.class);
        intent.setAction(ACTION_GET_HIERARCHY_CUSTOMERS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ContactsIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }*/

    public static void syncCustomerstoServer(Context context) {
        Intent intent = new Intent(context, ContactsIntentService.class);
        intent.setAction(ACTION_CUSTOMERS_OFFLINE_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, ContactsIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void updateContact(final ContactModel contactModel) {
        CustomerService client = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> postContact = client.updateContact(userToken, contactModel.getContactid(), contactModel);
        postContact.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        ContactMgr.getInstance(ContactsIntentService.this).updateContact(contactModel, userId);
                        Intent intent = new Intent("customer_broadcast");
                        mgr = LocalBroadcastManager.getInstance(ContactsIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
    }

    private void getContactList() {
        String modifiedDate = ContactMgr.getInstance(ContactsIntentService.this).getMaxModifiedDate(groupId, userId);
        CustomerService client = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> getcontacts = client.getContacts(userToken, groupId, "", modifiedDate);
        try {
            Response<RestResponse> restResponse = getcontacts.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<ContactModel>>() {
                    }.getType();
                    List<ContactModel> contactModelList = gson.fromJson(restResponse.body().getData(), listType);
                    if (contactModelList.size() > 0) {
                        ContactMgr.getInstance(ContactsIntentService.this).insertContactList(contactModelList);
                    }
                    SharedPreferenceManager.getInstance().putString(Constants.CONTACTS_LOADED, "loaded");
                    Intent intent = new Intent("customer_broadcast");
                    mgr = LocalBroadcastManager.getInstance(ContactsIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            SharedPreferenceManager.getInstance().putString(Constants.CONTACTS_LOADED, "notloaded");
        }
    }

    private void syncOfflineContacts() {
        final List<ContactModel> contactsList = ContactMgr.getInstance(ContactsIntentService.this).getContactByStatus(userId, "offline");
        if (contactsList.size() > 0) {
            for (final ContactModel contactModel : contactsList) {
                if (Utils.isNotNullAndNotEmpty(contactModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                    }
                }
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    /*********************Customer**************************************/
    private void getCustomerList() {
        String modifiedDate = "";
        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.BRANCHES_LAST_FETCH_TIME, ""))) {
            modifiedDate = new BranchesMgr(this).getmaxModifiedDate(groupId, userId);
        } else {
            modifiedDate = "";
            SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "1531739767");
        }
        CustomerService client = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> getcustomers = client.getcustomers(userToken, groupId, "", modifiedDate);
        try {
            Response<RestResponse> restResponse = getcustomers.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<BranchesModel>>() {
                    }.getType();
                    List<BranchesModel> branchesModelList = gson.fromJson(restResponse.body().getData(), listType);
                    if (branchesModelList.size() > 0) {
                        new BranchesMgr(ContactsIntentService.this).insertCustomerList(branchesModelList);
                    }
                    SupervisorIntentService.insertSupervisorsList(ContactsIntentService.this);
                    OpportunityIntentService.insertOpportunityStages(ContactsIntentService.this);
                    getContactList();
//                    OpportunityIntentService.insertOpportunity(ContactsIntentService.this, getCalculatedDate("yyyy-MM-dd", -180), getCalculatedDate("yyyy-MM-dd", 7));
                    SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "loaded");
                    Intent intent = new Intent("customer_broadcast");
                    mgr = LocalBroadcastManager.getInstance(ContactsIntentService.this);
                    mgr.sendBroadcast(intent);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
        }
    }



   /* private void getHierarchyCustomerList(){


        String modifiedDate = "";
        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.BRANCHES_LAST_FETCH_TIME, ""))) {
            modifiedDate = new BranchesMgr(ContactsIntentService.this).getmaxModifiedDate(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        } else {
            modifiedDate = "";
            SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "1531739767");
        }
        CustomerService client = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> getcustomers = client.getcustomerassignees(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "", modifiedDate);
        try {
            Response<RestResponse> restResponse = getcustomers.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<ArrayList<BranchesModel>>() {
                    }.getType();
                    List<BranchesModel> branchesModelList = gson.fromJson(restResponse.body().getData(), listType);
                            if (branchesModelList.size() > 0) {
                                new BranchesMgr(ContactsIntentService.this).insertNewCustomerList(branchesModelList);
                            }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            //SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
        }




    }*/

    private void syncOfflineCustomers() {
        final List<BranchesModel> branchesModelList = BranchesMgr.getInstance(ContactsIntentService.this).getCustomersbyStatus(userId, "offline");
        if (branchesModelList.size() > 0) {
            for (final BranchesModel branchesModel : branchesModelList) {
                if (Utils.isNotNullAndNotEmpty(branchesModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (!Utils.isNotNullAndNotEmpty(branchesModel.getCustomerid())) {
                            postCustomer(branchesModel);
                        }
                    }
                }
            }
            LeadIntentService.syncLeadstoServer(ContactsIntentService.this);
            TargetsIntentService.updateRecord(ContactsIntentService.this);
        } else {
            TargetsIntentService.updateRecord(ContactsIntentService.this);
            LeadIntentService.syncLeadstoServer(ContactsIntentService.this);
        }
    }

    private void postCustomer(final BranchesModel branchesModel) {
        CustomerService client = RestService.createService(CustomerService.class);
        Call<RestResponse> postCustomer = client.addCustomer(userToken, groupId, branchesModel);
        try {
            Response<RestResponse> restResponse = postCustomer.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    BranchesMgr.getInstance(ContactsIntentService.this).updateCustomerOnlline(branchesModel, userId, restResponse.body().getData());
                    Intent intent = new Intent("customer_broadcast");
                    mgr = LocalBroadcastManager.getInstance(ContactsIntentService.this);
                    mgr.sendBroadcast(intent);
                } else {
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            SharedPreferenceManager.getInstance().putString(Constants.CUSTOMERS_LOADED, "notloaded");
        }
    }
}