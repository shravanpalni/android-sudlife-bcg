package com.outwork.sudlife.bcg.planner.horizontalpicker;

import org.joda.time.DateTime;

public interface DatePickerListener {
    void onDateSelected(DateTime dateSelected);
}